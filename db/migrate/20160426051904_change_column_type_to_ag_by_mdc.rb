class ChangeColumnTypeToAgByMdc < ActiveRecord::Migration
  def up
    change_column :ag_by_mdcs, :hospital_days_average, :numeric
  end

  def down
    change_column :ag_by_mdcs, :hospital_days_average, :integer
  end
end
