class CreateWhytPlotApis < ActiveRecord::Migration
  def change
    create_table :whyt_plot_apis do |t|
      t.string :url
      t.string :chart_jp
      t.string :chart_en
      t.string :api
      t.string :api_attributes

      t.timestamps null: false
    end
  end
end
