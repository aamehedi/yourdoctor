class CreateSocieties < ActiveRecord::Migration
  def change
    create_table :societies do |t|
      t.text :name
      t.text :english_name
      t.text :address
      t.text :email

      t.timestamps null: false
    end
  end
end
