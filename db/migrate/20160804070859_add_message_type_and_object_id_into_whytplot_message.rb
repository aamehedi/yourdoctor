class AddMessageTypeAndObjectIdIntoWhytplotMessage < ActiveRecord::Migration[5.0]
  def change
    add_column :whytplot_chatrooms, :message_type, :integer, default: 0
    add_column :whytplot_chatrooms, :object_id, :integer
  end
end
