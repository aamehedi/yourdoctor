class CreateDrMrChatrooms < ActiveRecord::Migration[5.0]
  def change
    create_table :dr_mr_chatrooms do |t|
      t.references :doctor, foreign_key: true
      t.references :mr, foreign_key: true
      t.boolean :accepted, default: false
      t.boolean :favorite, default: false

      t.timestamps
    end
  end
end
