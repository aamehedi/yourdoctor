class AddInviterToTemporaryDoctors < ActiveRecord::Migration
  def change
    add_reference :temporary_doctors, :doctor, index: true
    add_foreign_key :temporary_doctors, :doctors
  end
end
