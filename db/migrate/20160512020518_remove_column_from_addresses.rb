class RemoveColumnFromAddresses < ActiveRecord::Migration
  def change
    remove_column :addresses, :local_government_code
  end
end
