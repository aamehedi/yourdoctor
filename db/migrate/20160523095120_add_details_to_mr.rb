class AddDetailsToMr < ActiveRecord::Migration[5.0]
  def change
    add_column :mrs, :company_id, :integer
    add_column :mrs, :branch_id, :integer
    add_column :mrs, :first_name, :string
    add_column :mrs, :last_name, :string
    add_column :mrs, :middle_name, :string
    add_column :mrs, :first_name_initial, :string
    add_column :mrs, :last_name_initial, :string
    add_column :mrs, :middle_name_initial, :string
    add_column :mrs, :sex, :integer
    add_column :mrs, :birthdate, :date
  end
end
