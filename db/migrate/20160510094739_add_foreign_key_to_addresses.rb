class AddForeignKeyToAddresses < ActiveRecord::Migration
  def change
    add_column :addresses, :municipality_id, :integer
    add_foreign_key :addresses, :municipalities
  end
end
