class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.references :doctor, index: true
      t.text :content
      t.references :post, index: true

      t.timestamps null: false
    end
    add_foreign_key :comments, :doctors
    add_foreign_key :comments, :posts
  end
end
