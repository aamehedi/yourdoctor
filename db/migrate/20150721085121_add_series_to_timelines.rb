class AddSeriesToTimelines < ActiveRecord::Migration
  def change
    add_column :timelines, :series, :integer, :default => -1
  end
end
