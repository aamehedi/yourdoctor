class AddNoNameToDepartment < ActiveRecord::Migration
  def change
    add_column :departments, :no_name, :boolean, default: false
    add_column :departments, :comment, :text
  end
end
