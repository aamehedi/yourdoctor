class CreateDepartmentPositions < ActiveRecord::Migration
  def change
    create_table :department_positions do |t|
      t.string :name
      t.references :department, index: true

      t.timestamps null: false
    end
    add_foreign_key :department_positions, :departments
  end
end
