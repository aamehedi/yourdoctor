class CreateSkills < ActiveRecord::Migration
  def change
    create_table :skills do |t|
      t.references :doctor, index: true
      t.references :icd10, index: true

      t.timestamps null: false
    end
    add_foreign_key :skills, :doctors
    add_foreign_key :skills, :icd10s
  end
end
