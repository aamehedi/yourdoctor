class CreateMrProjects < ActiveRecord::Migration[5.0]
  def change
    create_table :mr_projects do |t|
      t.references :mr, foreign_key: true
      t.references :project, foreign_key: true

      t.timestamps
    end
  end
end
