class RenameColumnSuggestionToTextFromSuggestions < ActiveRecord::Migration
  def self.up
    rename_column :suggestions, :suggestion, :text
  end
  def self.down
    rename_column :suggestion, :text, :suggestion
  end
end
