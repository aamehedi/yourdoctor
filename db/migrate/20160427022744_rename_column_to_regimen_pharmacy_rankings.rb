class RenameColumnToRegimenPharmacyRankings < ActiveRecord::Migration
  def change
    rename_column :regimen_pharmacy_rankings, :aggregate_by_mdc_layer1_id, :ag_by_mdc_l1_id
  end
end
