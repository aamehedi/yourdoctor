class AddIndexToAddresses < ActiveRecord::Migration
  def change
    add_index :addresses, :postal_code, :unique => true
  end
end
