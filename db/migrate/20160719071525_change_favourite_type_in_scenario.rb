class ChangeFavouriteTypeInScenario < ActiveRecord::Migration[5.0]
  def change
    change_column :scenarios, :favourite, 'boolean USING CAST(favourite AS boolean)'
    rename_column :scenarios, :favourite, :favorite
    rename_column :charts, :favourite, :favorite
  end
end
