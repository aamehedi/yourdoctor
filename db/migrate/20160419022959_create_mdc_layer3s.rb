class CreateMdcLayer3s < ActiveRecord::Migration
  def change
    create_table :mdc_layer3s do |t|
      t.string :code_1
      t.string :code_2
      t.string :code_3
      t.string :code_4
      t.belongs_to :mdc_layer2, index: true

      t.timestamps null: false
    end
    add_foreign_key :mdc_layer3s, :mdc_layer2s
  end
end
