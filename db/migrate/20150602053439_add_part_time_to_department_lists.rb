class AddPartTimeToDepartmentLists < ActiveRecord::Migration
  def change
    add_column :department_lists, :part_time, :boolean
  end
end
