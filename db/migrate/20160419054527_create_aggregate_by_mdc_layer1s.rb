class CreateAggregateByMdcLayer1s < ActiveRecord::Migration
  def change
    create_table :aggregate_by_mdc_layer1s do |t|
      t.integer :drugs_num
      t.integer :regimens_num
      t.integer :cases_num
      t.integer :hospitals_num
      t.references :mdc_layer1, index: true

      t.timestamps null: false
    end
    add_foreign_key :aggregate_by_mdc_layer1s, :mdc_layer1s
  end
end
