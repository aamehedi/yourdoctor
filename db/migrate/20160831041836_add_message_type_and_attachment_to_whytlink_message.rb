class AddMessageTypeAndAttachmentToWhytlinkMessage < ActiveRecord::Migration[5.0]
  def change
    add_column :whytlink_messages, :attachment, :string, default: ""
    add_column :whytlink_messages, :message_type, :integer, default: 0
  end
end
