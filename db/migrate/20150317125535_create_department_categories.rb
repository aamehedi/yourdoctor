class CreateDepartmentCategories < ActiveRecord::Migration
  def change
    create_table :department_categories do |t|
      t.string :name
      t.string :code
      t.text :comment

      t.timestamps null: false
    end
  end
end
