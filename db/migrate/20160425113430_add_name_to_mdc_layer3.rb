class AddNameToMdcLayer3 < ActiveRecord::Migration
  def change
    add_column :mdc_layer3s, :name, :string
  end
end
