class CreateWhytlinkChatrooms < ActiveRecord::Migration[5.0]
  def change
    create_table :whytlink_chatrooms do |t|
      t.integer :type, default: 0

      t.timestamps
    end
  end
end
