class AddReadingToDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :first_name_reading, :string, default: ''
    add_column :doctors, :last_name_reading, :string, default: ''
    add_column :doctors, :middle_name_reading, :string, default: ''
  end
end
