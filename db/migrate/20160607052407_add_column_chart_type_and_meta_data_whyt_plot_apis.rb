class AddColumnChartTypeAndMetaDataWhytPlotApis < ActiveRecord::Migration
  def change
    add_column :whyt_plot_apis, :chart_type, :Integer, null: false, default: 0
    add_column :whyt_plot_apis, :meta_data, :Text
  end
end
