class AddUrlToPreMonographsModeratedScrapingHistory < ActiveRecord::Migration
  def change
    add_column :pre_monographs_moderated_scraping_histories, :url, :text
  end
end
