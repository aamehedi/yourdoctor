class CreateDrMrMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :dr_mr_messages do |t|
      t.references :doctor, foreign_key: true
      t.references :mr, foreign_key: true
      t.references :dr_mr_chatroom, foreign_key: true
      t.string :content

      t.timestamps
    end
  end
end
