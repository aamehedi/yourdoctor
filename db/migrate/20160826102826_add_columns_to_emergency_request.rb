class AddColumnsToEmergencyRequest < ActiveRecord::Migration[5.0]
  def change
    add_column :emergency_requests, :status, :integer
    add_column :emergency_requests, :closing_comment, :string
    add_reference :emergency_requests, :emergency_request_hospital, foreign_key: true
  end
end
