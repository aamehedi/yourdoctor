class AddWhytplotChatroomToBranchAndProject < ActiveRecord::Migration[5.0]
  def change
    add_reference :branches, :whytplot_chatroom, default: nil
    add_reference :projects, :whytplot_chatroom, default: nil
  end
end
