class CreateWhytplotMrChatrooms < ActiveRecord::Migration[5.0]
  def change
    create_table :whytplot_mr_chatrooms do |t|
      t.references :whytplot_chatroom, foreign_key: true
      t.references :mr, foreign_key: true
      t.integer :last_read_message_id
      t.boolean :favorite, default: true

      t.timestamps
    end
  end
end
