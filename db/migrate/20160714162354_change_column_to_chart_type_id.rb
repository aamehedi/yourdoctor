class ChangeColumnToChartTypeId < ActiveRecord::Migration
  def change
    rename_column :whyt_plot_apis, :chart_type, :chart_type_id 
  end
end
