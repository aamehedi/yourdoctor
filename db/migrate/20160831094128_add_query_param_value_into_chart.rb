class AddQueryParamValueIntoChart < ActiveRecord::Migration[5.0]
  def change
  	add_column :charts, :query_param_values, :text
  end
end
