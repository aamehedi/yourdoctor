class AddColumnToPopulation < ActiveRecord::Migration
  def change
    add_column :populations, :dpc_male_0to2, :integer
    add_column :populations, :dpc_female_0to2, :integer
    add_column :populations, :dpc_male_3to5, :integer
    add_column :populations, :dpc_female_3to5, :integer
    add_column :populations, :dpc_male_6to15, :integer
    add_column :populations, :dpc_female_6to15, :integer
    add_column :populations, :dpc_male_16to20, :integer
    add_column :populations, :dpc_female_16to20, :integer
    add_column :populations, :dpc_male_21to40, :integer
    add_column :populations, :dpc_female_21to40, :integer
    add_column :populations, :dpc_male_41to60, :integer
    add_column :populations, :dpc_female_41to60, :integer
    add_column :populations, :dpc_male_61to79, :integer
    add_column :populations, :dpc_female_61to79, :integer
    add_column :populations, :dpc_male_over80, :integer
    add_column :populations, :dpc_female_over80, :integer
  end
end
