class CreateSocietyPositionLists < ActiveRecord::Migration
  def change
    create_table :society_position_lists do |t|
      t.references :doctor, index: true
      t.references :society_position, index: true

      t.timestamps null: false
    end
    add_foreign_key :society_position_lists, :doctors
    add_foreign_key :society_position_lists, :society_positions
  end
end
