class ChangeColumnToDpcHospitals < ActiveRecord::Migration
  def change
    remove_column :dpc_hospitals, :status_after_leaving_outpatient_from_internal,:decimal
    remove_column :dpc_hospitals, :status_after_leaving_outpatient_from_external,:decimal
    remove_column :dpc_hospitals, :status_after_leaving_moving_to_another_hospital,:decimal
    remove_column :dpc_hospitals, :status_after_leaving_discharge,:decimal
    remove_column :dpc_hospitals, :status_after_leaving_move_to_nursing_facility,:decimal
    remove_column :dpc_hospitals, :status_after_leaving_other,:decimal
    remove_column :dpc_hospitals, :status_after_leaving_unknown,:decimal
  end
end
