class RenameUrlToPreMonographsModeratedScrapingHistory < ActiveRecord::Migration
  def up
    add_column :pre_monographs_moderated_scraping_histories, :page_info, :text
    remove_column :pre_monographs_moderated_scraping_histories, :url
  end
  def down
    add_column :pre_monographs_moderated_scraping_histories, :url, :string
    remove_column :pre_monographs_moderated_scraping_histories, :page_info
  end
end
