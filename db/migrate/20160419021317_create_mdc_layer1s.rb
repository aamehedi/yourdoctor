class CreateMdcLayer1s < ActiveRecord::Migration
  def change
    create_table :mdc_layer1s do |t|
      t.string :code
      t.string :name
      t.belongs_to :mdc_layer1_code, index: true

      t.timestamps null: false
    end
    add_foreign_key :mdc_layer1s, :mdc_layer1_codes
  end
end
