class AddColumnsToDoctor < ActiveRecord::Migration[5.0]
  def change
    add_column :doctors, :emergency, :boolean, default: false
    add_column :doctors, :rest_mode, :boolean, default: false
    add_column :doctors, :rest_time_start, :time
    add_column :doctors, :rest_time_end, :time
  end
end
