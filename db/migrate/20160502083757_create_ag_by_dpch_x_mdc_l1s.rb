class CreateAgByDpchXMdcL1s < ActiveRecord::Migration
  def change
    create_table :ag_by_dpch_x_mdc_l1s do |t|
      t.integer :number
      t.decimal :days
      t.string :label
      t.boolean :is_operated
      t.string :type
      t.references :dpc_hospital, index: true
      t.references :mdc_layer1, index: true

      t.timestamps null: false
    end
    add_foreign_key :ag_by_dpch_x_mdc_l1s, :dpc_hospitals
    add_foreign_key :ag_by_dpch_x_mdc_l1s, :mdc_layer1s
  end
end
