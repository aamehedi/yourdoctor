class CreateAgByDpchXReasonMajors < ActiveRecord::Migration
  def change
    create_table :ag_by_dpch_x_reason_majors do |t|
      t.decimal :same_reason
      t.decimal :different_reason
      t.decimal :same_reason_nonchemo
      t.decimal :different_reason_nonchemo
      t.references :reason_for_rehospitalization, index: { name: 'index_abd_x_rmajors_on_rfr_id' }
      t.references :dpc_hospital, index: true

      t.timestamps null: false
    end
    add_foreign_key :ag_by_dpch_x_reason_majors, :dpc_hospitals
  end
end
