class RemoveAffliationIdFromWhytlinkGroupChatInfos < ActiveRecord::Migration[5.0]
  def change
    remove_column :whytlink_group_chat_infos, :affliation_id, :integer
  end
end
