class AddEstimateDoctorIdToTemporaryDoctors < ActiveRecord::Migration
  def change
    add_column :temporary_doctors, :estimate_doctor_id, :integer
  end
end
