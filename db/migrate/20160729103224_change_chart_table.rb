class ChangeChartTable < ActiveRecord::Migration[5.0]
  def change
    remove_column :charts, :hospital_info
    remove_column :charts, :disease_info
    add_column :charts, :raw_query_param_values, :string
  end
end
