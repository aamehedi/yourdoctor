class CreateEmergencyRequestHospitals < ActiveRecord::Migration[5.0]
  def change
    create_table :emergency_request_hospitals do |t|
      t.references :emergency_request, foreign_key: true
      t.references :hospital, foreign_key: true

      t.timestamps
    end
  end
end
