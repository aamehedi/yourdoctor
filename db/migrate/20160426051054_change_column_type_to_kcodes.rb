class ChangeColumnTypeToKcodes < ActiveRecord::Migration
  def up
    change_column :kcodes, :code, :string
  end

  def down
    change_column :kcodes, :code, :integer
  end
end
