class ChangeColumnNameToPopulations < ActiveRecord::Migration
  def change
    rename_column :populations, :locale_id, :municipality_id
    add_foreign_key :populations, :municipalities
  end
end
