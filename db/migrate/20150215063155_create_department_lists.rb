class CreateDepartmentLists < ActiveRecord::Migration
  def change
    create_table :department_lists do |t|
      t.references :department, index: true
      t.references :doctor, index: true

      t.timestamps null: false
    end
    add_foreign_key :department_lists, :departments
    add_foreign_key :department_lists, :doctors
  end
end
