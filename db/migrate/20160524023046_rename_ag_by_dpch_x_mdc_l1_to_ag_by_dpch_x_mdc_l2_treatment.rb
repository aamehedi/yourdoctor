class RenameAgByDpchXMdcL1ToAgByDpchXMdcL2Treatment < ActiveRecord::Migration
  def up
    rename_table    :ag_by_dpch_x_mdc_l1s, :ag_by_dpch_x_mdc_l2_treatments
    remove_column   :ag_by_dpch_x_mdc_l2_treatments, :mdc_layer1_id
    add_column      :ag_by_dpch_x_mdc_l2_treatments, :mdc_layer2_id, :integer
    add_foreign_key :ag_by_dpch_x_mdc_l2_treatments, :mdc_layer2s
    rename_column   :ag_by_dpch_x_mdc_l2_treatments, :is_operated, :treatmented
  end
  def down
    rename_table    :ag_by_dpch_x_mdc_l2_treatments, :ag_by_dpch_x_mdc_l1s
    remove_column   :ag_by_dpch_x_mdc_l1s, :mdc_layer2_id
    add_column      :ag_by_dpch_x_mdc_l1s, :mdc_layer1_id, :integer
    add_foreign_key :ag_by_dpch_x_mdc_l1s, :mdc_layer1s
    rename_column   :ag_by_dpch_x_mdc_l2_treatments, :treatmented, :is_operated
  end
end
