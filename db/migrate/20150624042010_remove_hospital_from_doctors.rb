class RemoveHospitalFromDoctors < ActiveRecord::Migration
  def change
    remove_foreign_key :doctors, column: :hospital_id
  end
end
