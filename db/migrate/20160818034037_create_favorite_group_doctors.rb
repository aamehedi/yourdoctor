class CreateFavoriteGroupDoctors < ActiveRecord::Migration[5.0]
  def change
    create_table :favorite_group_doctors do |t|
      t.references :doctor, foreign_key: true
      t.references :favorite_group, foreign_key: true

      t.timestamps
    end
  end
end
