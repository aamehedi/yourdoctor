class ChangeTypeOfChartType < ActiveRecord::Migration[5.0]
  def change
  	change_column :charts, :chart_type, :string
  end
end
