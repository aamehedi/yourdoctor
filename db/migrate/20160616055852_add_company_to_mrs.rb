class AddCompanyToMrs < ActiveRecord::Migration[5.0]
  def change
    remove_column :mrs, :company_id
    add_reference :mrs, :company, foreign_key: true
  end
end
