class AddOrderNumToDepartmentLists < ActiveRecord::Migration
  def change
    add_column :department_lists, :order_num, :integer
  end
end
