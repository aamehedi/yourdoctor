class CreateEmergencyRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :emergency_requests do |t|
      t.string :title
      t.string :message
      t.references :doctor, foreign_key: true
      t.references :whytlink_chatroom, foreign_key: true
      t.datetime :time
      t.boolean :isOpen
      t.integer :closing_reason
      t.references :hospital, foreign_key: true

      t.timestamps
    end
  end
end
