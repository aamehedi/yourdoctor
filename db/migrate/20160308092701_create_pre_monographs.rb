class CreatePreMonographs < ActiveRecord::Migration
  def change
    create_table "pre_monographs", force: :cascade do |t|
      t.integer  "doctor_id"
      t.string   "title"
      t.integer  "year"
      t.text     "description"
      t.text     "url"
      t.datetime "created_at",     null: false
      t.datetime "updated_at",     null: false
      t.text     "writers"
      t.text     "media"
      t.text     "page"
      t.datetime "published"
      t.string   "published_type"
    end

    add_index "pre_monographs", ["doctor_id"], name: "index_pre_monographs_on_doctor_id", using: :btree

    add_foreign_key "pre_monographs", "doctors"
  end
end
