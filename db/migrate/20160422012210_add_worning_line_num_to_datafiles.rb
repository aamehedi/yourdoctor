class AddWorningLineNumToDatafiles < ActiveRecord::Migration
  def change
    add_column :datafiles, :warning_line_num, :integer
  end
end
