class RenameFullAndFullKanaColumnToAddresses < ActiveRecord::Migration
  def change
    rename_column :addresses, :full, :full_address
    rename_column :addresses, :full_kana, :full_address_kana
  end
end
