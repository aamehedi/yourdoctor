class CreateMrSessions < ActiveRecord::Migration[5.0]
  def change
    create_table :mr_sessions do |t|
      t.references :mr, foreign_key: true
      t.string :device_token
      t.string :access_token

      t.timestamps
    end
    add_index :mr_sessions, :access_token
  end
end
