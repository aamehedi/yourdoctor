class AddForeignKeyToMedicalMunicipality < ActiveRecord::Migration
  def change
    add_column :medical_municipalities, :municipality_id, :integer
    add_foreign_key :medical_municipalities, :municipalities
  end
end
