class CreateDoctors < ActiveRecord::Migration
  def change
    create_table :doctors do |t|
      t.references :hospital, index: true
      t.string :first_name
      t.string :last_name
      t.text :position
      t.references :university, index: true
      t.string :graduation_year
      t.text :society
      t.string :profession
      t.text :skill
      t.text :result
      t.text :url
      t.text :history
      t.integer :top_or_next
      t.string :user
      t.string :update_user
      t.text :comment

      t.timestamps null: false
    end
    add_foreign_key :doctors, :hospitals
    add_foreign_key :doctors, :universities
  end
end
