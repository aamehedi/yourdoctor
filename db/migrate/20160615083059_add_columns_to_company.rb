class AddColumnsToCompany < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :max_number_of_approach, :integer
    add_column :companies, :remaining_approach, :integer
  end
end
