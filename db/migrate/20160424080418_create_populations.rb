class CreatePopulations < ActiveRecord::Migration
  def change
    create_table :populations do |t|
      t.integer :year
      t.integer :male_0to4
      t.integer :female_0to4
      t.integer :male_5to9
      t.integer :female_5to9
      t.integer :male_10to14
      t.integer :female_10to14
      t.integer :male_15to19
      t.integer :female_15to19
      t.integer :male_20to24
      t.integer :female_20to24
      t.integer :male_25to29
      t.integer :female_25to29
      t.integer :male_30to34
      t.integer :female_30to34
      t.integer :male_35to39
      t.integer :female_35to39
      t.integer :male_40to44
      t.integer :female_40to44
      t.integer :male_45to49
      t.integer :female_45to49
      t.integer :male_50to54
      t.integer :female_50to54
      t.integer :male_55to59
      t.integer :female_55to59
      t.integer :male_60to64
      t.integer :female_60to64
      t.integer :male_65to69
      t.integer :female_65to69
      t.integer :male_70to74
      t.integer :female_70to74
      t.integer :male_75to79
      t.integer :female_75to79
      t.integer :male_80to84
      t.integer :female_80to84
      t.integer :male_85to89
      t.integer :female_85to89
      t.integer :male_over90
      t.integer :female_over90
      t.integer :locale_id

      t.timestamps null: false
    end
  end
end
