class CreateShines < ActiveRecord::Migration
  def change
    create_table :shines do |t|
      t.integer :parent_id
      t.string :parent_type
      t.references :doctor, index: true
      t.timestamps null: false
    end
  end
end
