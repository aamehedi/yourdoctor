class AddMessageTypeAndAttachmentToDrMrMessage < ActiveRecord::Migration[5.0]
  def change
    add_column :dr_mr_messages, :message_type, :integer, default: 0
    add_column :dr_mr_messages, :attachment, :string
  end
end
