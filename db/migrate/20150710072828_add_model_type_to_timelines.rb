class AddModelTypeToTimelines < ActiveRecord::Migration
  def change
    add_column :timelines, :model_type, :string
  end
end
