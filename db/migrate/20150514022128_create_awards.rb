class CreateAwards < ActiveRecord::Migration
  def change
    create_table :awards do |t|
      t.references :doctor, index: true
      t.string :title
      t.integer :year
      t.text :description

      t.timestamps null: false
    end
    add_foreign_key :awards, :doctors
  end
end
