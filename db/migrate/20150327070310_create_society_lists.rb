class CreateSocietyLists < ActiveRecord::Migration
  def change
    create_table :society_lists do |t|
      t.references :doctor, index: true
      t.references :society, index: true

      t.timestamps null: false
    end
    add_foreign_key :society_lists, :doctors
    add_foreign_key :society_lists, :societies
  end
end
