class ChangeAdminUserToUserOfHospital < ActiveRecord::Migration
  def change
   remove_column :hospitals, :admin_user_id
   add_column :hospitals, :user_id, :integer
   add_index :hospitals, :user_id
  end
end
