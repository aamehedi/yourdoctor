class RemoveColumnsFromEmergencyRequest < ActiveRecord::Migration[5.0]
  def change
    remove_column :emergency_requests, :title, :string
    remove_column :emergency_requests, :message, :string
    remove_column :emergency_requests, :isOpen, :boolean
    remove_reference :emergency_requests, :hospital, foreign_key: true
  end
end
