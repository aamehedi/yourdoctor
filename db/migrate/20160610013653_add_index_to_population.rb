class AddIndexToPopulation < ActiveRecord::Migration
  def up
    add_index :populations, :year
    add_index :populations, :municipality_id
  end
  def down
    remove_index :populations, :year
    remove_index :populations, :municipality_id
  end
end
