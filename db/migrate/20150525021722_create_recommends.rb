class CreateRecommends < ActiveRecord::Migration
  def change
    create_table :recommends do |t|
      t.references :doctor, index: true
      t.references :skill, index: true
      t.integer    :point
      t.timestamps null: false
    end
    add_foreign_key :recommends, :doctors
    add_foreign_key :recommends, :skills
  end
end
