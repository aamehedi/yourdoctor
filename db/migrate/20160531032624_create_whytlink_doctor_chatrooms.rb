class CreateWhytlinkDoctorChatrooms < ActiveRecord::Migration[5.0]
  def change
    create_table :whytlink_doctor_chatrooms do |t|
      t.references :whytlink_chatroom, foreign_key: true
      t.references :doctor, foreign_key: true
      t.integer :last_read_message_id

      t.timestamps
    end
  end
end
