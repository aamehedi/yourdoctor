class CreateEmergencyDoctorHospitals < ActiveRecord::Migration[5.0]
  def change
    create_table :emergency_doctor_hospitals do |t|
      t.references :doctor, foreign_key: true
      t.references :hospital, foreign_key: true

      t.timestamps
    end
  end
end
