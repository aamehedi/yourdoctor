class CreateAggregateByMdcs < ActiveRecord::Migration
  def change
    create_table :aggregate_by_mdcs do |t|
      t.string :code
      t.boolean :inclusion
      t.integer :dpc_num_for_the_mdc
      t.integer :cases_num_for_the_mdc
      t.integer :number
      t.decimal :dpc_ratio_for_all_cases
      t.integer :male_num
      t.integer :female_num
      t.integer :from0to2_num
      t.integer :from3to5_num
      t.integer :from6to15_num
      t.integer :from16to20_num
      t.integer :from21to40_num
      t.integer :from41to60_num
      t.integer :from61to79_num
      t.integer :over80_num
      t.integer :introduction_num
      t.integer :outpatient_num
      t.integer :transported_num
      t.integer :unexpected_hospitalization_num
      t.integer :emergency_hospitalization_num
      t.integer :status_healed
      t.integer :status_improvement
      t.integer :status_palliation
      t.integer :status_unchanging
      t.integer :status_worse
      t.integer :status_death_of_illness_costed_most
      t.integer :status_death_of_other_illness_costed_most
      t.integer :status_other
      t.integer :hospital_days_average
      t.integer :hospital_days_minimum
      t.integer :hospital_days_maximum
      t.integer :hospital_days_coefficient_of_variation
      t.integer :hospital_days_value_of_25percentile
      t.integer :hospital_days_value_of_50percentile
      t.integer :hospital_days_value_of_75percentile
      t.integer :hospital_days_value_of_90percentile
      t.integer :artificial_respiration
      t.integer :artificial_kidney
      t.integer :central_venous_injection
      t.integer :blood_transfusion
      t.references :mdc_layer3, index: true

      t.timestamps null: false
    end
    add_foreign_key :aggregate_by_mdcs, :mdc_layer3s
  end
end
