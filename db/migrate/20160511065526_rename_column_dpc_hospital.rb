class RenameColumnDpcHospital < ActiveRecord::Migration
  def up
    rename_column :dpc_hospitals,:dcp_type                                   ,:dpc_type
    rename_column :dpc_hospitals,:dcp_calc_bed_num                           ,:dpc_calc_bed_num
    rename_column :dpc_hospitals,:dcp_calc_hospitalization_basic_fee_type    ,:dpc_calc_hospitalization_basic_fee_type
    rename_column :dpc_hospitals,:dcp_calc_hospitalization_basic_fee_lratio  ,:dpc_calc_hospitalization_basic_fee_lratio
    rename_column :dpc_hospitals,:dcp_calc_hospitalization_basic_fee_rratio  ,:dpc_calc_hospitalization_basic_fee_rratio
    rename_column :dpc_hospitals,:dcp_mental_hospital_bed_num                ,:dpc_mental_hospital_bed_num
    rename_column :dpc_hospitals,:dcp_recuperation_hospital_bed_num          ,:dpc_recuperation_hospital_bed_num
    rename_column :dpc_hospitals,:dcp_tuberculosis_hospital_bed_num          ,:dpc_tuberculosis_hospital_bed_num
    rename_column :dpc_hospitals,:dcp_total_bed_num                          ,:dpc_total_bed_num
    rename_column :dpc_hospitals,:submitted_months_num                       ,:dpc_submitted_months_num
    rename_column :dpc_hospitals,:rehospitalization_within_6wks_same_cond    ,:rehospitalize_within_6wks_same_cond
    rename_column :dpc_hospitals,:rehospitalization_after_6wks_same_cond     ,:rehospitalize_after_6wks_same_cond
    rename_column :dpc_hospitals,:rehospitalization_within_6weeks_diff_cond  ,:rehospitalize_within_6weeks_diff_cond
    rename_column :dpc_hospitals,:rehospitalization_after_6wks_diff_cond     ,:rehospitalize_after_6wks_diff_cond
  end
  def down
    rename_column :dpc_hospitals,:dpc_type                                    ,:dcp_type
    rename_column :dpc_hospitals,:dpc_calc_bed_num                            ,:dcp_calc_bed_num
    rename_column :dpc_hospitals,:dpc_calc_hospitalization_basic_fee_type     ,:dcp_calc_hospitalization_basic_fee_type
    rename_column :dpc_hospitals,:dpc_calc_hospitalization_basic_fee_lratio   ,:dcp_calc_hospitalization_basic_fee_lratio
    rename_column :dpc_hospitals,:dpc_calc_hospitalization_basic_fee_rratio   ,:dcp_calc_hospitalization_basic_fee_rratio
    rename_column :dpc_hospitals,:dpc_mental_hospital_bed_num                 ,:dcp_mental_hospital_bed_num
    rename_column :dpc_hospitals,:dpc_recuperation_hospital_bed_num           ,:dcp_recuperation_hospital_bed_num
    rename_column :dpc_hospitals,:dpc_tuberculosis_hospital_bed_num           ,:dcp_tuberculosis_hospital_bed_num
    rename_column :dpc_hospitals,:dpc_total_bed_num                           ,:dcp_total_bed_num
    rename_column :dpc_hospitals,:dpc_submitted_months_num                    ,:submitted_months_num                       
    rename_column :dpc_hospitals,:rehospitalize_within_6wks_same_cond         ,:rehospitalization_within_6wks_same_cond
    rename_column :dpc_hospitals,:rehospitalize_after_6wks_same_cond          ,:rehospitalization_after_6wks_same_cond
    rename_column :dpc_hospitals,:rehospitalize_within_6weeks_diff_cond       ,:rehospitalization_within_6weeks_diff_cond
    rename_column :dpc_hospitals,:rehospitalize_after_6wks_diff_cond          ,:rehospitalization_after_6wks_diff_cond
  end
end
