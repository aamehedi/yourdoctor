class CreateNumberByCodes < ActiveRecord::Migration
  def change
    create_table :number_by_codes do |t|
      t.integer :number
      t.string :type
      t.string :costly_comorbidity
      t.integer :index
      t.references :codeable, polymorphic: true
      t.references :aggregate_by_mdc, index: true

      t.timestamps null: false
    end
    add_foreign_key :number_by_codes, :aggregate_by_mdcs
  end
end
