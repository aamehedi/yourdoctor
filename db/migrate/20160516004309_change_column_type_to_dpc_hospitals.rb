class ChangeColumnTypeToDpcHospitals < ActiveRecord::Migration
  def up
    change_column :dpc_hospitals, :advanced_medi_care_surgery,            :decimal
    change_column :dpc_hospitals, :advanced_medi_care_chemotherapy,       :decimal
    change_column :dpc_hospitals, :advanced_medi_care_radiation_therapy,  :decimal
    change_column :dpc_hospitals, :advanced_medi_care_ambulance_transport,:decimal
    change_column :dpc_hospitals, :advanced_medi_care_any_of_these,       :decimal
    change_column :dpc_hospitals, :advanced_medi_care_anesthesia,         :decimal
  end
  def down
    change_column :dpc_hospitals, :advanced_medi_care_surgery,            :integer
    change_column :dpc_hospitals, :advanced_medi_care_chemotherapy,       :integer
    change_column :dpc_hospitals, :advanced_medi_care_radiation_therapy,  :integer
    change_column :dpc_hospitals, :advanced_medi_care_ambulance_transport,:integer
    change_column :dpc_hospitals, :advanced_medi_care_any_of_these,       :integer
    change_column :dpc_hospitals, :advanced_medi_care_anesthesia,         :integer
  end
end
