class AddDefaultValueOfFavoriteInScenario < ActiveRecord::Migration[5.0]
  def change
    change_column_default :scenarios, :favorite, false
  end
end
