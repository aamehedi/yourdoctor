class AddIcd10ToMdcLayer1 < ActiveRecord::Migration
  def change
    add_reference :mdc_layer1s, :icd10, index: true
    add_foreign_key :mdc_layer1s, :icd10s
  end
end
