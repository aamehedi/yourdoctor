class AddEditorTypeToTimelines < ActiveRecord::Migration
  def change
    add_column :timelines, :editor_type, :string
  end
end
