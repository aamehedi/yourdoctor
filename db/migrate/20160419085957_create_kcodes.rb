class CreateKcodes < ActiveRecord::Migration
  def change
    create_table :kcodes do |t|
      t.integer :code
      t.string :name

      t.timestamps null: false
    end
  end
end
