class RenameAggregateByMdcIdToNumberByCodes < ActiveRecord::Migration
  def change
    rename_column :number_by_codes, :aggregate_by_mdc_id, :ag_by_mdc_id
  end
end
