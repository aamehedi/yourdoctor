class CreateSocietyPositions < ActiveRecord::Migration
  def change
    create_table :society_positions do |t|
      t.text :name
      t.text :comment
      t.references :society, index: true

      t.timestamps null: false
    end
    add_foreign_key :society_positions, :societies
  end
end
