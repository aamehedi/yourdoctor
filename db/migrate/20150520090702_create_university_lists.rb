class CreateUniversityLists < ActiveRecord::Migration
  def change
    create_table :university_lists do |t|
      t.references :doctor, index: true
      t.references :university, index: true

      t.timestamps null: false
    end
    add_foreign_key :university_lists, :doctors
    add_foreign_key :university_lists, :universities
  end
end
