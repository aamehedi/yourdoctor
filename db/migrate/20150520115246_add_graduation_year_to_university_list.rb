class AddGraduationYearToUniversityList < ActiveRecord::Migration
  def change
    add_column :university_lists, :graduation_year, :integer
  end
end
