class CreateDuplicateDoctorLists < ActiveRecord::Migration
  def change
    create_table :duplicate_doctor_lists do |t|
      t.integer :doctor_ids, array: true
      t.boolean :check,      default: false
      t.integer :merged_ids, array: true

      t.timestamps null: false
    end
  end
end
