class AddVersionYearToHospitals < ActiveRecord::Migration
  def change
    add_column :hospitals, :version_year, :integer
  end
end
