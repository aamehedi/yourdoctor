class CreateWhytplotChatrooms < ActiveRecord::Migration[5.0]
  def change
    create_table :whytplot_chatrooms do |t|
      t.string :name
      t.integer :chatroom_type

      t.timestamps
    end
  end
end
