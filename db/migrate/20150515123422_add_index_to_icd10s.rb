class AddIndexToIcd10s < ActiveRecord::Migration
  def change
    add_column :icd10s, :index, :integer
  end
end
