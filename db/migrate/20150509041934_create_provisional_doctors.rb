class CreateProvisionalDoctors < ActiveRecord::Migration
  def change
    create_table :provisional_doctors do |t|
      t.string :email
      t.string :password
      t.string :registration_number
      t.string :first_name
      t.string :last_name
      t.string :uuid

      t.timestamps null: false
    end
  end
end
