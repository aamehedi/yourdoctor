class AddStiToUsers < ActiveRecord::Migration
  def change
    add_column :users, :type, :string
    add_reference :users, :user, index: true
  end
end
