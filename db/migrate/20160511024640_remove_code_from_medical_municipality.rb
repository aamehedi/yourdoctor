class RemoveCodeFromMedicalMunicipality < ActiveRecord::Migration
  def change
    remove_column :medical_municipalities, :code, :integer
  end
end
