class AddDefaultStatusInEmergencyRequest < ActiveRecord::Migration[5.0]
  def change
    change_column :emergency_requests, :status, :integer, default: 0
  end
end
