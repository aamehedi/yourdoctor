class AddForeignKeyToPopulation < ActiveRecord::Migration
  def up
    add_reference :populations, :prefecture, index: true
    add_foreign_key :populations, :prefectures
  end
  def down
    remove_reference :populations, :prefecture, index: true
    remove_foreign_key :populations, :prefectures
  end
end
