class AddHospitalRefToDepartment < ActiveRecord::Migration
  def change
    add_column :departments, :hospital_id, :integer
    add_index :departments, :hospital_id
  end
end
