class CreateDatatableDatafiles < ActiveRecord::Migration
  def change
    create_table :datatable_datafiles do |t|
      t.references :datable, polymorphic: true
      t.references :datafile, index: true

      t.timestamps null: false
    end
    add_foreign_key :datatable_datafiles, :datafiles
  end
end
