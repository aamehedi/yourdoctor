class AddDetailToTimelines < ActiveRecord::Migration
  def change
    add_column :timelines, :editor_first_name, :string
    add_column :timelines, :editor_last_name, :string
    add_column :timelines, :editor_hospital_name, :string
    add_column :timelines, :editor_hospital_id, :integer
    add_column :timelines, :editor_university_name, :string
    add_column :timelines, :editor_university_id, :integer
    add_column :timelines, :editor_graduation_year, :integer
    add_column :timelines, :doctor_first_name, :string
    add_column :timelines, :doctor_last_name, :string
    add_column :timelines, :doctor_hospital_name, :string
    add_column :timelines, :doctor_hospital_id, :integer
    add_column :timelines, :doctor_university_name, :string
    add_column :timelines, :doctor_university_id, :integer
    add_column :timelines, :doctor_graduation_year, :integer
  end
end
