class AddSexToTemporaryDoctors < ActiveRecord::Migration
  def change
    add_column :temporary_doctors, :sex, :integer
  end
end
