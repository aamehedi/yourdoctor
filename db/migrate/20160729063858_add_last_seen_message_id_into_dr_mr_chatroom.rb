class AddLastSeenMessageIdIntoDrMrChatroom < ActiveRecord::Migration[5.0]
  def change
    add_column :dr_mr_chatrooms, :last_seen_message_id_dr, :integer
    add_column :dr_mr_chatrooms, :last_seen_message_id_mr, :integer
  end
end
