class ChangeMrForeignKeys < ActiveRecord::Migration[5.0]
  def change
    remove_column :mrs, :company_id
    remove_column :mrs, :branch_id
    add_reference :mrs, :branch, foreign_key: true
    add_reference :mrs, :company, foreign_key: true
  end
end
