class CreateMedicalGroups < ActiveRecord::Migration
  def change
    create_table :medical_groups do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
