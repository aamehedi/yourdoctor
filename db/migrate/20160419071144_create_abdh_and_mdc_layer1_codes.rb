class CreateAbdhAndMdcLayer1Codes < ActiveRecord::Migration
  def change
    create_table :abdh_and_mdc_layer1_codes do |t|
      t.integer :patient_number_surgery
      t.integer :patient_number_no_surgery
      t.integer :hospitalization_planned_num
      t.integer :hospitalization_chemo_num
      t.integer :hospitalization_unplanned_num
      t.integer :hospitalization_emergency_num
      t.integer :ambulance_transport
      t.integer :ambulance_transport_total
      t.integer :death_within_24hours_num
      t.integer :death_within_24hours_num_total
      t.integer :avg_hospital_days_number
      t.decimal :avg_hospital_days_by_hopital
      t.decimal :avg_hospital_days_whole_country
      t.decimal :avg_hospital_days_index_patient_format
      t.decimal :avg_hospital_days_whole_country_patient_format
      t.decimal :avg_hospital_days_index
      t.decimal :rehospitalization_surgery_rate
      t.decimal :rehospitalization_chemical_treatment_rate
      t.references :dpc_hospital, index: true
      t.references :mdc_layer1_code, index: true

      t.timestamps null: false
    end
    add_foreign_key :abdh_and_mdc_layer1_codes, :dpc_hospitals
    add_foreign_key :abdh_and_mdc_layer1_codes, :mdc_layer1_codes
  end
end
