class RemoveCostlyComorbidityFromNumberByCode < ActiveRecord::Migration
  def change
    remove_column :number_by_codes, :costly_comorbidity, :string
  end
end
