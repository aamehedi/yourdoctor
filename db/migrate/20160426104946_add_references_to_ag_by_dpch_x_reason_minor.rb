class AddReferencesToAgByDpchXReasonMinor < ActiveRecord::Migration
  def change
    add_reference :ag_by_dpch_x_reason_minors, :parent, references: :ag_by_dpch_x_reason_major, index: true
    add_foreign_key :ag_by_dpch_x_reason_minors, :ag_by_dpch_x_reason_majors, column: :parent_id
  end
end
