class RemoveMessageTypeAndObjectIdFromWhytplotChatroom < ActiveRecord::Migration[5.0]
  def change
    remove_column :whytplot_chatrooms, :message_type, :integer
    remove_column :whytplot_chatrooms, :object_id, :integer
  end
end
