class AddMrTypeAndDefaultsToMrs < ActiveRecord::Migration[5.0]
  def change
    add_column :mrs, :mr_type, :integer
    change_column_default :mrs, :first_name, ""
    change_column_default :mrs, :last_name, ""
    change_column_default :mrs, :middle_name, ""
    change_column_default :mrs, :first_name_initial, ""
    change_column_default :mrs, :last_name_initial, ""
    change_column_default :mrs, :middle_name_initial, ""
    change_column_default :mrs, :sex, 0
  end
end
