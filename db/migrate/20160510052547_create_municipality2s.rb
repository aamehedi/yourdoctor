class CreateMunicipality2s < ActiveRecord::Migration
  def change
    create_table :municipality2s do |t|
      t.integer :code
      t.string :prefecture_name
      t.string :city_name
      t.string :prefecture_name_kana
      t.string :city_name_kana
      t.boolean :is_ordinance_designated_city, :default => false

      t.timestamps null: false
    end
  end
end
