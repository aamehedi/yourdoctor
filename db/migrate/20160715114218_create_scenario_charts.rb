class CreateScenarioCharts < ActiveRecord::Migration[5.0]
  def change
    create_table :scenario_charts do |t|
      t.integer :chart_order
      t.references :scenario, foreign_key: true
      t.references :chart, foreign_key: true

      t.timestamps
    end
  end
end
