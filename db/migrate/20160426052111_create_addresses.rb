class CreateAddresses < ActiveRecord::Migration
  def change
    create_table :addresses do |t|
      t.string :city
      t.string :city_kana
      t.string :town
      t.string :town_kana
      t.string :postal_code
      t.integer :local_government_code
      t.string :city1
      t.string :city2
      t.string :full
      t.string :full_kana
      t.references :prefecture, index: true

      t.timestamps null: false
    end
    add_foreign_key :addresses, :prefectures
  end
end
