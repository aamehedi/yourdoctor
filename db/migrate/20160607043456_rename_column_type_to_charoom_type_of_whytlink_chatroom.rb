class RenameColumnTypeToCharoomTypeOfWhytlinkChatroom < ActiveRecord::Migration[5.0]
  def change
    rename_column :whytlink_chatrooms, :type, :chatroom_type
  end
end
