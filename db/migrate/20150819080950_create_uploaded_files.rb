class CreateUploadedFiles < ActiveRecord::Migration
  def change
    create_table :uploaded_files do |t|
      t.text :name
      t.text :url
      t.integer :parent_id
      t.string :parent_type
      t.string :file_info

      t.timestamps null: false
    end
  end
end
