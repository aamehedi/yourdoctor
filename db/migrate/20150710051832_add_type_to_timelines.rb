class AddTypeToTimelines < ActiveRecord::Migration
  def change
    add_column :timelines, :type, :string
    add_column :timelines, :meta, :text
  end
end
