class AddDetailToHospitals < ActiveRecord::Migration
  def change
    add_column :hospitals, :code_region, :string
    add_column :hospitals, :code_number, :string
    add_column :hospitals, :code_checkdigit, :string
    add_column :hospitals, :phone, :string
    add_column :hospitals, :number_of_fulltime_medicine, :integer
    add_column :hospitals, :number_of_fulltime_dentistry, :integer
    add_column :hospitals, :number_of_fulltime_pharmacy, :integer
    add_column :hospitals, :number_of_parttime_medicine, :integer
    add_column :hospitals, :number_of_parttime_dentistry, :integer
    add_column :hospitals, :number_of_parttime_pharmacy, :integer
    add_column :hospitals, :establisher_org, :string
    add_column :hospitals, :establisher_first_name, :string
    add_column :hospitals, :establisher_last_name, :string
    add_column :hospitals, :director_first_name, :string
    add_column :hospitals, :director_last_name, :string
    add_column :hospitals, :designation_date, :date
    add_column :hospitals, :designation_reason, :string
    add_column :hospitals, :designation_start, :date
    add_column :hospitals, :beds_hash, :string, array: true
    add_column :hospitals, :departments_array, :string, array: true
    add_column :hospitals, :state, :string
  end
end
