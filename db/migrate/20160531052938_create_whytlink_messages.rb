class CreateWhytlinkMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :whytlink_messages do |t|
      t.references :doctor, foreign_key: true
      t.references :whytlink_chatroom, foreign_key: true
      t.text :content

      t.timestamps
    end
  end
end
