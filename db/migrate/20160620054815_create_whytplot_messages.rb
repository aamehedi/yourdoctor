class CreateWhytplotMessages < ActiveRecord::Migration[5.0]
  def change
    create_table :whytplot_messages do |t|
      t.references :mr, foreign_key: true
      t.references :whytplot_chatroom, foreign_key: true
      t.text :content

      t.timestamps
    end
  end
end
