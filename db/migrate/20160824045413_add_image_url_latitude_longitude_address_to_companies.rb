class AddImageUrlLatitudeLongitudeAddressToCompanies < ActiveRecord::Migration[5.0]
  def change
    add_column :companies, :company_image, :string
    add_column :companies, :latitude, :float
    add_column :companies, :longitude, :float
    add_column :companies, :address, :string
  end
end
