class RemoveEmailUniqueFromDoctors < ActiveRecord::Migration
  def change
    remove_index :doctors, :email
  end
end
