class CreateDoctorFeedbacks < ActiveRecord::Migration[5.0]
  def change
    create_table :doctor_feedbacks do |t|
      t.references :doctor, foreign_key: true
      t.references :emergency_request, foreign_key: true
      t.integer :status

      t.timestamps
    end
  end
end
