class AddMessageTypeAndObjectIdToWhytplotMessage < ActiveRecord::Migration[5.0]
  def change
    add_column :whytplot_messages, :message_type, :integer, default: 0
    add_column :whytplot_messages, :object_id, :integer
  end
end
