class RenameTableMunicipality2ToMunicipalities < ActiveRecord::Migration
  def change
    rename_table :municipality2s, :municipalities
  end
end
