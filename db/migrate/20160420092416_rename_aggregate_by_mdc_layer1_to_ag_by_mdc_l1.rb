class RenameAggregateByMdcLayer1ToAgByMdcL1 < ActiveRecord::Migration
  def change
    rename_table :aggregate_by_mdc_layer1s, :ag_by_mdc_l1s
  end
end
