class MailSendTimeToDoctor < ActiveRecord::Migration
  def change
    add_column :doctors, :mail_sent_time, :datetime
  end
end
