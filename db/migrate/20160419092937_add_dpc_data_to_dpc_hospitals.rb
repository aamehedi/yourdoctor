class AddDpcDataToDpcHospitals < ActiveRecord::Migration
  def change
    add_column :dpc_hospitals, :dcp_type, :string
    add_column :dpc_hospitals, :dcp_calc_bed_num, :string
    add_column :dpc_hospitals, :dcp_calc_hospitalization_basic_fee_type, :string
    add_column :dpc_hospitals, :dcp_calc_hospitalization_basic_fee_lratio, :integer
    add_column :dpc_hospitals, :dcp_calc_hospitalization_basic_fee_rratio, :integer
    add_column :dpc_hospitals, :dcp_mental_hospital_bed_num, :integer
    add_column :dpc_hospitals, :dcp_recuperation_hospital_bed_num, :integer
    add_column :dpc_hospitals, :dcp_tuberculosis_hospital_bed_num, :integer
    add_column :dpc_hospitals, :dcp_total_bed_num, :integer
    add_column :dpc_hospitals, :submitted_months_num, :integer
  end
end
