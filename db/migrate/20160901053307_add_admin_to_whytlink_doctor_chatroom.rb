class AddAdminToWhytlinkDoctorChatroom < ActiveRecord::Migration[5.0]
  def change
    add_column :whytlink_doctor_chatrooms, :admin, :boolean, default: false
  end
end
