class RemoveCodeFromMunicipalities < ActiveRecord::Migration
  def change
    remove_column :municipalities, :code, :integer
  end
end
