class AddMrToCompanies < ActiveRecord::Migration[5.0]
  def change
    add_reference :companies, :mr, foreign_key: true
  end
end
