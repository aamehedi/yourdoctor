class AddForeignKeyToMedicalRegion < ActiveRecord::Migration
  def up
    add_reference :medical_regions, :prefecture, index: true
    add_reference :medical_regions, :area, index: true
    add_foreign_key :medical_regions, :prefectures
    add_foreign_key :medical_regions, :areas
  end
  def down
    remove_reference :medical_regions, :prefecture, index: true
    remove_reference :medical_regions, :area, index: true
    remove_foreign_key :medical_regions, :prefectures
    remove_foreign_key :medical_regions, :areas
  end
end
