class CreatePreMonographsModeratedScrapingHistories < ActiveRecord::Migration
  def change
    create_table :pre_monographs_moderated_scraping_histories, force: :cascade do |t|
      t.integer :doctor_id, null: false
      t.integer :source, null: false
      t.string :url, null: false

      t.timestamps null: false
    end

    add_index "pre_monographs_moderated_scraping_histories", ["doctor_id"], name: "index_pre_monographs_moderated_scraping_histories_on_doctor_id", using: :btree

    add_foreign_key "pre_monographs_moderated_scraping_histories", "doctors"
  end
end
