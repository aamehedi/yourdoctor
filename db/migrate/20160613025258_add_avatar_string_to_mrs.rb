class AddAvatarStringToMrs < ActiveRecord::Migration[5.0]
  def change
    add_column :mrs, :avatar_string, :string
  end
end
