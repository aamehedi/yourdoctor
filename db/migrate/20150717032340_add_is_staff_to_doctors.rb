class AddIsStaffToDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :is_staff, :boolean, :default => false
  end
end
