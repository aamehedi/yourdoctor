class CreateSheets < ActiveRecord::Migration
  def change
    create_table :sheets do |t|
      t.string :area
      t.string :medical_group
      t.string :work_status
      t.string :hospital_id
      t.string :hospital_name
      t.string :doctor_name
      t.string :position
      t.string :department
      t.string :university
      t.string :graduation_year
      t.text :society
      t.string :profession
      t.text :skill
      t.text :result
      t.text :link
      t.text :history
      t.string :top_or_next
      t.string :user
      t.string :created_at
      t.string :updated_at
      t.string :update_user
      t.text :comment

      t.timestamps null: false
    end
  end
end
