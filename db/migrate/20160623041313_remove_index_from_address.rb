class RemoveIndexFromAddress < ActiveRecord::Migration
  def change
    remove_index :addresses, :postal_code
  end
end
