class CreateWhytlinkGroupChatInfos < ActiveRecord::Migration[5.0]
  def change
    create_table :whytlink_group_chat_infos do |t|
      t.string :name
      t.string :avatar
      t.integer :affliation_type, default: 0
      t.integer :affliation_id
      t.boolean :opened
      t.references :whytlink_chatroom, foreign_key: true

      t.timestamps
    end
  end
end
