class AddModelIdToTimelines < ActiveRecord::Migration
  def change
    add_column :timelines, :model_id, :integer
  end
end
