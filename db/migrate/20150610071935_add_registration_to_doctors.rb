class AddRegistrationToDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :registration_date, :date
  end
end
