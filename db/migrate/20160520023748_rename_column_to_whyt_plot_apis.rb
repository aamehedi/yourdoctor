class RenameColumnToWhytPlotApis < ActiveRecord::Migration
  def change
    rename_column :whyt_plot_apis, :url, :name
    rename_column :whyt_plot_apis, :chart_jp, :purpose_jp
    rename_column :whyt_plot_apis, :chart_en, :purpose_en
    rename_column :whyt_plot_apis, :api, :api_sql
    rename_column :whyt_plot_apis, :api_attributes, :version
    change_column :whyt_plot_apis, :version, 'integer USING CAST(version AS integer)'
    change_column_null :whyt_plot_apis, :name, false
    change_column_null :whyt_plot_apis, :api_sql, false
    change_column_null :whyt_plot_apis, :version, false
  end
end
