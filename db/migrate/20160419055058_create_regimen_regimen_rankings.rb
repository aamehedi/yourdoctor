class CreateRegimenRegimenRankings < ActiveRecord::Migration
  def change
    create_table :regimen_regimen_rankings do |t|
      t.integer :no
      t.integer :cases_num
      t.decimal :cases_ratio
      t.decimal :cases_cumulative_ratio
      t.integer :hospitals_num
      t.decimal :hospitals_ratio
      t.decimal :average_hospital_days
      t.string :regimen
      t.references :aggregate_by_mdc_layer1, index: true

      t.timestamps null: false
    end
    add_foreign_key :regimen_regimen_rankings, :aggregate_by_mdc_layer1s
  end
end
