class CreateWhytlinkGroupChatRequests < ActiveRecord::Migration[5.0]
  def change
    create_table :whytlink_group_chat_requests do |t|
      t.references :doctor, foreign_key: true
      t.references :whytlink_group_chat_info, foreign_key: true, index: {:name => "index_group_chat_request_on_whytlink_group_chat_info_id"}
      t.integer :status

      t.timestamps
    end
  end
end
