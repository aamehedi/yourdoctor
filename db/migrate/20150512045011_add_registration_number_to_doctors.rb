class AddRegistrationNumberToDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :registration_number, :string
  end
end
