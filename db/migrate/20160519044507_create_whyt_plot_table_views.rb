class CreateWhytPlotTableViews < ActiveRecord::Migration
  def change
    create_table :whyt_plot_table_views do |t|
      t.string :name
      t.string :create_view_sql
      t.boolean :is_created, default: false

      t.timestamps null: false
    end
  end
end
