class RemoveDuplicatesFromBranches < ActiveRecord::Migration[5.0]
  def change
    remove_column :branches, :max_number_of_approach
    remove_column :branches, :remaining_approach
  end
end
