class CreateLinks < ActiveRecord::Migration
  def change
    create_table :links do |t|
      t.integer :followee_id
      t.integer :follower_id
      t.integer :state
      t.text :message

      t.timestamps null: false      
    end
    add_index :links, [:followee_id, :follower_id], :unique => true
  end
end
