class RenameRequestColumns < ActiveRecord::Migration[5.0]
  def change
    change_column_default :companies, :max_number_of_approach, 0
    change_column_default :companies, :remaining_approach, 0

    change_column_default :branches, :number_approach_of_month, 0
    change_column_default :branches, :number_approach_remaining, 0

    change_column_default :projects, :max_number_of_approach, 0
    change_column_default :projects, :remaining_approach, 0

    rename_column :companies, :max_number_of_approach, :max_request
    rename_column :companies, :remaining_approach, :bonus_request

    rename_column :branches, :number_approach_of_month, :this_month_request
    rename_column :branches, :number_approach_remaining, :next_month_request

    rename_column :projects, :max_number_of_approach, :this_month_request
    rename_column :projects, :remaining_approach, :next_month_request
  end
end
