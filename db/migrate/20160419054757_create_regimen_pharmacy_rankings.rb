class CreateRegimenPharmacyRankings < ActiveRecord::Migration
  def change
    create_table :regimen_pharmacy_rankings do |t|
      t.integer :no
      t.integer :regimens_num
      t.integer :cases_num
      t.integer :hospitals_num
      t.decimal :regimens_ratio
      t.decimal :cases_ratio
      t.decimal :hospitals_ratio
      t.references :aggregate_by_mdc_layer1, index: true

      t.timestamps null: false
    end
    add_foreign_key :regimen_pharmacy_rankings, :aggregate_by_mdc_layer1s
  end
end
