class CreateDepartmentPositionLists < ActiveRecord::Migration
  def change
    create_table :department_position_lists do |t|
      t.references :department_list, index: true
      t.references :department_position, index: true

      t.timestamps null: false
    end
    add_foreign_key :department_position_lists, :department_lists
    add_foreign_key :department_position_lists, :department_positions
  end
end
