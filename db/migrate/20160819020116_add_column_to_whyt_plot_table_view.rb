class AddColumnToWhytPlotTableView < ActiveRecord::Migration
  def change
    add_column :whyt_plot_table_views, :description, :text
    rename_column :whyt_plot_table_views, :is_created, :is_view
  end
end
