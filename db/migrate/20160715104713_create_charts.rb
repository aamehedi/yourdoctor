class CreateCharts < ActiveRecord::Migration[5.0]
  def change
    create_table :charts do |t|
      t.string :name
      t.string :description
      t.boolean :favourite
      t.references :mr, foreign_key: true
      t.string :hospital_info
      t.string :disease_info
      t.integer :chart_type

      t.timestamps
    end
  end
end
