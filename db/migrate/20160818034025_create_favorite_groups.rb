class CreateFavoriteGroups < ActiveRecord::Migration[5.0]
  def change
    create_table :favorite_groups do |t|
      t.string :name
      t.references :doctor, foreign_key: true

      t.timestamps
    end
  end
end
