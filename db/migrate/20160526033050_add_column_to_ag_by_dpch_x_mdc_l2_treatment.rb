class AddColumnToAgByDpchXMdcL2Treatment < ActiveRecord::Migration
  def up
    add_reference   :ag_by_dpch_x_mdc_l2_treatments, :mdc_layer1, index: true
    add_foreign_key :ag_by_dpch_x_mdc_l2_treatments, :mdc_layer1s
    add_index       :ag_by_dpch_x_mdc_l2_treatments, :mdc_layer2_id #追加忘れ
  end
  def down
    remove_foreign_key  :ag_by_dpch_x_mdc_l2_treatments, :mdc_layer1s
    remove_index        :ag_by_dpch_x_mdc_l2_treatments, :mdc_layer1_id
    remove_reference    :ag_by_dpch_x_mdc_l2_treatments, :mdc_layer1s
  end
end
