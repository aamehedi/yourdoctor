class CreateMunicipalities < ActiveRecord::Migration
  def change
    create_table :municipalities do |t|
      t.string :name
      t.integer :code
      t.string :medical_region_code

      t.timestamps null: false
    end
  end
end
