class AddNewAttrToDr < ActiveRecord::Migration
  def change
    add_column :doctors, :middle_name, :string
    add_column :doctors, :first_name_initial, :string
    add_column :doctors, :last_name_initial, :string
    add_column :doctors, :middle_name_initial, :string
    add_column :department_lists, :active, :boolean, default: true
    add_column :department_lists, :department_seq, :integer
    add_column :department_lists, :dr_seq, :integer
    add_column :department_lists, :position, :integer
    add_column :department_lists, :term_start, :datetime
    add_column :department_lists, :term_end, :datetime
  end
end
