class AddMessagesToDatafiles < ActiveRecord::Migration
  def change
    add_column :datafiles, :messages, :string
  end
end
