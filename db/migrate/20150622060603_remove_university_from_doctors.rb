class RemoveUniversityFromDoctors < ActiveRecord::Migration
  def change
    remove_foreign_key :doctors, column: :university_id
  end
end
