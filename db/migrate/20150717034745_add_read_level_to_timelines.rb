class AddReadLevelToTimelines < ActiveRecord::Migration
  def change
    add_column :timelines, :read_level, :integer, :default => 0
  end
end
