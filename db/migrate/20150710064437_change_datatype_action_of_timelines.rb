class ChangeDatatypeActionOfTimelines < ActiveRecord::Migration
  def change
    change_column :timelines, :action, :string
  end
end
