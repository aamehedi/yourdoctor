class ChangeAcceptedToStatusInDrMrChatroom < ActiveRecord::Migration[5.0]
  def change
    change_column_default :dr_mr_chatrooms, :accepted, nil
    change_column :dr_mr_chatrooms, :accepted, 'integer USING (accepted::integer)'
    rename_column :dr_mr_chatrooms, :accepted, :status
    change_column_default :dr_mr_chatrooms, :status, 0
  end
end
