class AddSignInToAppColumnsToDoctor < ActiveRecord::Migration
  def up
    add_column :doctors, :current_sign_in_to_app_at, :datetime
  end
  def down
    remove_column :doctors, :current_sign_in_to_app_at, :datetime
  end
end
