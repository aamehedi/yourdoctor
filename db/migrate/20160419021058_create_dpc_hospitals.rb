class CreateDpcHospitals < ActiveRecord::Migration
  def change
    create_table :dpc_hospitals do |t|
      t.integer :created_year
      t.integer :notice_number
      t.integer :serial_number
      t.string :name
      t.integer :out_of_analysis_eceptions_num
      t.integer :out_of_analysis_psychiatric_wards_num
      t.integer :out_of_analysis_general_wards_num
      t.integer :out_of_analysis_duplicate_medical_record_num
      t.integer :out_of_analysis_stay_1day_or_less_num
      t.integer :out_of_analysis_stay_sleepover_or_less_num
      t.integer :out_of_analysis_age_under0_over120
      t.integer :out_of_analysis_error_of_hospitalized_birth_num
      t.integer :out_of_analysis_movement_other_general_ward_num
      t.integer :out_of_analysis_death_within_24hours
      t.integer :out_of_analysis_transplant_surgery_num
      t.integer :out_of_analysis_no_insurance_num
      t.integer :out_of_analysis_not_applicable_dpc
      t.integer :out_of_analysis_ad_apr_earlier_and_expect_dis_jun_to_dec_num
      t.integer :out_of_analysis_trial_num
      t.integer :out_of_analysis_death_within_7days_after_birth_num
      t.integer :out_of_analysis_stipulated_by_ministry_of_health
      t.integer :out_of_analysis_analyzed_num
      t.decimal :out_of_analysis_analyzed_ratio
      t.integer :hospital_days_num
      t.decimal :hospital_days_average_value
      t.decimal :hospital_days_coefficient_of_variation
      t.integer :hospital_day_sminimum_vanue
      t.decimal :hospital_days_percentile25
      t.decimal :hospital_days_percentile50
      t.decimal :hospital_days_percentile75
      t.integer :hospital_days_maximum_value
      t.decimal :status_after_leaving_outpatient_from_internal
      t.decimal :status_after_leaving_outpatient_from_external
      t.decimal :status_after_leaving_moving_to_another_hospital
      t.decimal :status_after_leaving_discharge
      t.decimal :status_after_leaving_move_to_nursing_facility
      t.decimal :status_after_leaving_other
      t.decimal :status_after_leaving_unknown
      t.decimal :status_after_leaving_home_care_visiting_same_hospital
      t.decimal :status_after_leaving_home_care_visiting_other_hospital
      t.decimal :status_after_leaving_home_care_other
      t.decimal :status_after_leaving_changing_hospital
      t.decimal :status_after_leaving_entering_nursing_home
      t.decimal :status_after_leaving_entering_asissted_nursing_home
      t.decimal :status_after_leaving_entering_residential_facility
      t.decimal :status_after_leaving_death
      t.decimal :status_after_leaving_other_method
      t.decimal :status_outcome_healed
      t.decimal :status_outcome_improvement
      t.decimal :status_outcome_palliation
      t.decimal :status_outcome_unchanging
      t.decimal :status_outcome_worse
      t.decimal :status_outcome_death_of_illness_costed_most
      t.decimal :status_outcome_death_of_other_illness_costed_most
      t.decimal :status_outcome_other
      t.decimal :rehospitalization_within_6wks_same_cond
      t.decimal :rehospitalization_after_6wks_same_cond
      t.decimal :rehospitalization_within_6weeks_diff_cond
      t.decimal :rehospitalization_after_6wks_diff_cond
      t.integer :advanced_medi_care_surgery
      t.integer :advanced_medi_care_chemotherapy
      t.integer :advanced_medi_care_radiation_therapy
      t.integer :advanced_medi_care_ambulance_transport
      t.integer :advanced_medi_care_any_of_these
      t.integer :advanced_medi_care_anesthesia
      t.decimal :rehospitalization_ratio_rehospitalization
      t.decimal :rehospitalization_ratio_change_ward_again
      t.decimal :rehospitalization_length_of_hospital_days_within_3days
      t.decimal :rehospitalization_length_of_hospital_days_within_4to7days
      t.decimal :rehospitalization_length_of_hospital_days_within_8to14days
      t.decimal :rehospitalization_length_of_hospital_days_within_15to28days
      t.decimal :rehospitalization_length_of_hospital_days_within_29to42days
      t.decimal :rehospitalization_length_of_stay_first_time
      t.decimal :rehospitalization_length_of_stay_second_time
      t.decimal :rehospitalization_length_of_stay_third_time
      t.decimal :rehospitalization_times_of_hospitalization
      t.references :hospital, index: true

      t.timestamps null: false
    end
    add_foreign_key :dpc_hospitals, :hospitals
  end
end
