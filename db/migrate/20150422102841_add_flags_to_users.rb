class AddFlagsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :visible_wages, :boolean, :default => true
    add_column :users, :suspended, :boolean,     :default => false
  end
end
