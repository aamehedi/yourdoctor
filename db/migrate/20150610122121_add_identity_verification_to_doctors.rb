class AddIdentityVerificationToDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :identity_verification, :integer
  end
end
