class AddNewAttrToMonograph < ActiveRecord::Migration
  def change
    add_column :monographs, :writers, :text
    add_column :monographs, :media, :text
    add_column :monographs, :page, :text
    add_column :monographs, :published, :datetime
  end
end
