class AddDateTypeToMonograph < ActiveRecord::Migration
  def change
    add_column :monographs, :published_type, :string    
  end
end
