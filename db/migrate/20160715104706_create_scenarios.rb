class CreateScenarios < ActiveRecord::Migration[5.0]
  def change
    create_table :scenarios do |t|
      t.string :name
      t.string :description
      t.string :favourite
      t.references :mr, foreign_key: true

      t.timestamps
    end
  end
end
