class CreateAgByDpchXReasonMinors < ActiveRecord::Migration
  def change
    create_table :ag_by_dpch_x_reason_minors do |t|
      t.decimal :ratio
      t.references :reason_for_rehospitalization, index: { name: 'index_abd_x_rminors_on_rfr_id' }

      t.timestamps null: false
    end
  end
end
