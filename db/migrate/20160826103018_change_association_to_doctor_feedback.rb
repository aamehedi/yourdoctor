class ChangeAssociationToDoctorFeedback < ActiveRecord::Migration[5.0]
  def change
    remove_reference :doctor_feedbacks, :emergency_request, foreign_key: true
    add_reference :doctor_feedbacks, :emergency_request_hospital, foreign_key: true
  end
end
