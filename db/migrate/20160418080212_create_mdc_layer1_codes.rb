class CreateMdcLayer1Codes < ActiveRecord::Migration
  def change
    create_table :mdc_layer1_codes do |t|
      t.integer :year
      t.string :code
      t.string :name

      t.timestamps null: false
    end
  end
end
