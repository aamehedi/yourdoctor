class AddColumnsToBranch < ActiveRecord::Migration[5.0]
  def change
    add_column :branches, :max_number_of_approach, :integer
    add_column :branches, :remaining_approach, :integer
  end
end
