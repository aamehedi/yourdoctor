class CreateMonographs < ActiveRecord::Migration
  def change
    create_table :monographs do |t|
      t.references :doctor, index: true
      t.string :title
      t.integer :year
      t.text :description
      t.text :url

      t.timestamps null: false
    end
    add_foreign_key :monographs, :doctors
  end
end
