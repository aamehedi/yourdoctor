class RenameMunicipalityToMedicalMunicipality < ActiveRecord::Migration
  def change
    rename_table :municipalities, :medical_municipalities
  end
end
