class CreateStaffPosts < ActiveRecord::Migration
  def change
    create_table :staff_posts do |t|
      t.text :body
      t.datetime :start_time
      t.datetime :end_time

      t.timestamps null: false
    end
  end
end
