class AddIsConfirmedToPreMonographs < ActiveRecord::Migration
  def change
    add_column :pre_monographs, :is_confirmed, :boolean, null: false, default: false
  end
end
