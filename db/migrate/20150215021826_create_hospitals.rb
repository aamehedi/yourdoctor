class CreateHospitals < ActiveRecord::Migration
  def change
    create_table :hospitals do |t|
      t.references :medical_group, index: true
      t.references :area, index: true
      t.references :prefecture, index: true
      t.integer :std_region_code
      t.text :name
      t.integer :postal_code
      t.text :address
      t.string :tmp_head
      t.text :url
      t.integer :bed_num
      t.integer :acute_phase_bed_num
      t.integer :admin_user_id

      t.timestamps null: false
    end
    add_index :hospitals, :admin_user_id
    add_foreign_key :hospitals, :medical_groups
    add_foreign_key :hospitals, :areas
    add_foreign_key :hospitals, :prefectures
  end
end
