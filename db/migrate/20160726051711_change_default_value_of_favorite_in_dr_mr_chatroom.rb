class ChangeDefaultValueOfFavoriteInDrMrChatroom < ActiveRecord::Migration[5.0]
  def change
    change_column_default :dr_mr_chatrooms, :favorite, true
  end
end
