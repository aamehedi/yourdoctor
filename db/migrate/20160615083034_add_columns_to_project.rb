class AddColumnsToProject < ActiveRecord::Migration[5.0]
  def change
    add_column :projects, :max_number_of_approach, :integer
    add_column :projects, :remaining_approach, :integer
  end
end
