class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.references :doctor, index: true
      t.text :memo
      t.integer :read_level
      t.references :suggestion, index: true

      t.timestamps null: false
    end
    add_foreign_key :posts, :doctors
    add_foreign_key :posts, :suggestions
  end
end
