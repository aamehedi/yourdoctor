class CreateAbdhAndMdcLayer2s < ActiveRecord::Migration
  def change
    create_table :abdh_and_mdc_layer2s do |t|
      t.integer :number
      t.decimal :days
      t.references :dpc_hospital, index: true
      t.references :mdc_layer2, index: true

      t.timestamps null: false
    end
    add_foreign_key :abdh_and_mdc_layer2s, :dpc_hospitals
    add_foreign_key :abdh_and_mdc_layer2s, :mdc_layer2s
  end
end
