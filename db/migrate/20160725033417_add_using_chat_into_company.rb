class AddUsingChatIntoCompany < ActiveRecord::Migration[5.0]
  def change
  	add_column :companies, :using_chat, :boolean, default: false
  end
end
