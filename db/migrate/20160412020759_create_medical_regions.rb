class CreateMedicalRegions < ActiveRecord::Migration
  def change
    create_table :medical_regions do |t|
      t.string :name
      t.integer :level
      t.string :code
      t.integer :parent_id
      t.integer :children_id

      t.timestamps null: false
    end
  end
end
