class AddSocietyListToSocietyPositionLists < ActiveRecord::Migration
  def change
    add_reference :society_position_lists, :society_list, index: true
    add_foreign_key :society_position_lists, :society_lists
  end
end
