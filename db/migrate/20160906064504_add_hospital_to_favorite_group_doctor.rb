class AddHospitalToFavoriteGroupDoctor < ActiveRecord::Migration[5.0]
  def change
    add_reference :favorite_group_doctors, :hospital, foreign_key: true
  end
end
