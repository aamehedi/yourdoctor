class AddTagTypeToTaggings < ActiveRecord::Migration
  def change
    add_column :taggings, :tag_type, :string
  end
end
