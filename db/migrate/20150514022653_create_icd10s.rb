class CreateIcd10s < ActiveRecord::Migration
  def change
    create_table :icd10s do |t|
      t.string :name
      t.string :code
      t.string :replace_code
      t.integer :depth
      t.references :icd10, index: true

      t.timestamps null: false
    end
    add_foreign_key :icd10s, :icd10s
  end
end
