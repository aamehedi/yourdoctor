class AddCommentToSkills < ActiveRecord::Migration
  def change
    add_column :skills, :comment, :text
  end
end
