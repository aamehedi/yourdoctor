class CreateMdcLayer2s < ActiveRecord::Migration
  def change
    create_table :mdc_layer2s do |t|
      t.string :code
      t.string :name
      t.boolean :no_procedure
      t.belongs_to :mdc_layer1, index: true
      t.timestamps null: false
    end
    add_foreign_key :mdc_layer2s, :mdc_layer1s
  end
end
