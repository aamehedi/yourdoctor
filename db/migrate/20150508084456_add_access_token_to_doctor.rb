class AddAccessTokenToDoctor < ActiveRecord::Migration
  def change
    add_column :doctors, :access_token, :string
  end
end
