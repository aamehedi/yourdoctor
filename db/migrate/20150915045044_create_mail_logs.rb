class CreateMailLogs < ActiveRecord::Migration
  def change
    create_table :mail_logs do |t|
      t.references :doctor, index: true
      t.references :all_mail, index: true
      t.boolean :done, default: false

      t.timestamps null: false
    end
    add_foreign_key :mail_logs, :doctors
    add_foreign_key :mail_logs, :all_mails
  end
end
