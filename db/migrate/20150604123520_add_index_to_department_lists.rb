class AddIndexToDepartmentLists < ActiveRecord::Migration
  def change
    add_column :department_lists, :index, :integer
  end
end
