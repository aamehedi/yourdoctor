class ChangeDataTypeForNumberApproachRemaining < ActiveRecord::Migration[5.0]
  def change
    change_column :branches, :number_approach_remaining,
      "integer USING CAST(number_approach_remaining AS integer)"
  end
end
