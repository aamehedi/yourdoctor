class AddForeignKeyCodeToMedicalMunicipality < ActiveRecord::Migration
  def up
    add_index :medical_regions, :code, unique: true
    add_foreign_key :medical_municipalities, :medical_regions, column: "medical_region_code",primary_key: "code"
  end
  def down
    remove_index :medical_regions, :code
    remove_foreign_key :medical_municipalities, column: "medical_region_code"
  end
end
