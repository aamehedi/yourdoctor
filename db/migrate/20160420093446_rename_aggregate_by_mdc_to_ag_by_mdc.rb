class RenameAggregateByMdcToAgByMdc < ActiveRecord::Migration
  def change
    rename_table :aggregate_by_mdcs, :ag_by_mdcs
  end
end
