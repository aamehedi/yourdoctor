class ChangeColumnTypeDpcCalcBedNumToDpcHospitals < ActiveRecord::Migration
  def up
    execute 'ALTER TABLE dpc_hospitals ALTER COLUMN dpc_calc_bed_num TYPE integer USING (dpc_calc_bed_num::integer)'
  end
  def down
    execute 'ALTER TABLE dpc_hospitals ALTER COLUMN dpc_calc_bed_num TYPE string USING (dpc_calc_bed_num::string)'
  end
end
