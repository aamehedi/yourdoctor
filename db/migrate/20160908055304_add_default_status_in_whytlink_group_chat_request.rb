class AddDefaultStatusInWhytlinkGroupChatRequest < ActiveRecord::Migration[5.0]
  def change
    change_column :whytlink_group_chat_requests, :status, :integer, default: 0
  end
end
