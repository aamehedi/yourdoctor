class CreatePrefectures < ActiveRecord::Migration
  def change
    create_table :prefectures do |t|
      t.string :name
      t.references :area, index: true

      t.timestamps null: false
    end
    add_foreign_key :prefectures, :areas
  end
end
