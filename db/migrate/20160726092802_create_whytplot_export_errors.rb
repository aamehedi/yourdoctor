class CreateWhytplotExportErrors < ActiveRecord::Migration
  def change
    create_table :whytplot_export_errors do |t|
      t.string :message
      t.string :detail

      t.timestamps null: false
    end
  end
end
