class CreateDatafiles < ActiveRecord::Migration
  def change
    create_table :datafiles do |t|
      t.string :filepath
      t.integer :status
      t.integer :total_line_num
      t.integer :error_line_num

      t.timestamps null: false
    end
  end
end
