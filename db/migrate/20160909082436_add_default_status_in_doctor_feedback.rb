class AddDefaultStatusInDoctorFeedback < ActiveRecord::Migration[5.0]
  def change
    change_column :doctor_feedbacks, :status, :integer, default: 0
  end
end
