class AddDetailToTemporaryDoctors < ActiveRecord::Migration
  def change
    add_column :temporary_doctors, :birthdate, :date
    add_column :temporary_doctors, :registration_date, :date
  end
end
