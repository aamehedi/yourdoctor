class CreateAllMails < ActiveRecord::Migration
  def change
    create_table :all_mails do |t|
      t.text :title
      t.text :content
      t.boolean :done, default: false

      t.timestamps null: false
    end
  end
end
