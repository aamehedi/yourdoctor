class CreateBranches < ActiveRecord::Migration[5.0]
  def change
    create_table :branches do |t|
      t.string :name
      t.references :company, foreign_key: true
      t.references :mr, foreign_key: true
      t.string :number_approach_of_month
      t.string :number_approach_remaining

      t.timestamps
    end
  end
end
