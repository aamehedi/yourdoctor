class ChangeDataTypeForNumberApproachOfMonth < ActiveRecord::Migration[5.0]
  def change
    change_column :branches, :number_approach_of_month,
      "integer USING CAST(number_approach_of_month AS integer)"
  end
end
