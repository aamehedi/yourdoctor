class CreateStaffLogs < ActiveRecord::Migration
  def change
    create_table :staff_logs do |t|
      t.references :user, index: true
      t.integer :staff_log_id
      t.string :staff_log_type

      t.timestamps null: false
    end
    add_foreign_key :staff_logs, :users
  end
end
