class AddImagesToDoctors < ActiveRecord::Migration
  def change
    add_column :doctors, :avatar_image, :string, default: ''
    add_column :doctors, :cover_image, :string, default: ''
  end
end
