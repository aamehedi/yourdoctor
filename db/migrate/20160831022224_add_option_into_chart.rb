class AddOptionIntoChart < ActiveRecord::Migration[5.0]
  def change
    add_column :charts, :option, :string
  end
end
