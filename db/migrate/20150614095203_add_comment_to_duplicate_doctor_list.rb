class AddCommentToDuplicateDoctorList < ActiveRecord::Migration
  def change
    add_column :duplicate_doctor_lists, :comment, :text
    add_column :staff_logs, :sub, :string
  end
end
