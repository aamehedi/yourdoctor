class CreateTimelines < ActiveRecord::Migration
  def change
    create_table :timelines do |t|
      t.integer :log_id
      t.string :log_type
      t.integer :action
      t.integer :paper_trail_id 
      t.integer :doctor_id
      t.integer :editor_id
      t.timestamps null: false
    end
  end
end
