class RemoveDoctorIdFromSocietyPositionLists < ActiveRecord::Migration
  def change
    remove_column :society_position_lists, :doctor_id, :integer
  end
end
