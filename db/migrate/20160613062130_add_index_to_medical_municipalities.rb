class AddIndexToMedicalMunicipalities < ActiveRecord::Migration
  def change
    add_index :medical_municipalities, :municipality_id, :unique => true
  end
end
