class AddIndexToDpcHospitalYear < ActiveRecord::Migration
  def up
    add_index :dpc_hospitals, :created_year
    add_index :mdc_layer1_codes, :year
    add_index :mdc_layer1_codes, :code
    add_index :mdc_layer1s, :code
    add_index :mdc_layer2s, :code
  end
  def down
    remove_index :dpc_hospitals, :created_year
    remove_index :mdc_layer1_codes, :year
    remove_index :mdc_layer1_codes, :code
    remove_index :mdc_layer1s, :code
    remove_index :mdc_layer2s, :code
  end
end
