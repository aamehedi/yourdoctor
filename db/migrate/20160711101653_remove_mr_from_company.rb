class RemoveMrFromCompany < ActiveRecord::Migration[5.0]
  def change
    remove_column :companies, :mr_id
  end
end
