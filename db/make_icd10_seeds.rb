require 'csv'
#require 'date'

##########################################
# ログメソッド
##########################################
def log(str)
  $stdout.print str + "\n"
end

def logr(str)
  $stdout.print str + "\r"
end

##########################################
# ICD10データオブジェクト
##########################################
class ICD10
  attr_accessor :source_filename
  attr_accessor :name
  attr_accessor :code
  attr_accessor :replace_code
  attr_accessor :depth
  attr_accessor :icd10_id
  attr_accessor :index
  attr_accessor :icd10_current_key
  attr_accessor :icd10_parent_key

  def initialize(filename)
    self.source_filename = filename
  end

  #UTFに変換または 空文字
  def format(str)
  	str ? str.encode!("UTF-8").strip : ""
  end

  #UTFに変換 & 配列切り出し
  def formatAndSplit(str)
    str ? str.encode!("UTF-8").split(/　|\s/).compact.delete_if(&:empty?) : []
  end


  #第１階層
  def parseDepth1(cell)
    return if cell.nil?
    md = cell.encode!("UTF-8").match(/^[\s　]+第(.+)章[\s　]+(.+)/)
    return if md.nil?

    self.name = md[2].strip
    self.depth = 1
    
    case md[1].strip
    when 'Ⅰ'
      self.index = 1
    when 'Ⅱ'
      self.index = 2
    when 'Ⅲ'
      self.index = 3
    when 'Ⅳ'
      self.index = 4
    when 'Ⅴ'
      self.index = 5
    when 'Ⅵ'
      self.index = 6
    when 'Ⅶ'
      self.index = 7
    when 'Ⅷ'
      self.index = 8
    when 'Ⅸ'
      self.index = 9
    when 'Ⅹ'
      self.index = 10
    when 'ⅩⅠ'
      self.index = 11
    when 'ⅩⅡ'
      self.index = 12
    when 'ⅩⅢ'
      self.index = 13
    when 'ⅩⅣ'
      self.index = 14
    when 'ⅩⅤ'
      self.index = 15
    when 'ⅩⅥ'
      self.index = 16
    when 'ⅩⅦ'
      self.index = 17
    when 'ⅩⅧ'
      self.index = 18
    when 'ⅩⅨ'
      self.index = 19
    when 'ⅩⅩ'
      self.index = 20
    when 'ⅩⅩⅠ'
      self.index = 21
    when 'ⅩⅩⅡ'
      self.index = 22
    end
    
  end


  #第２階層
  def parseDepth2(cell)
    return if cell.nil?
    md = format(cell).match(/^([^　]+)\([Ａ-ＺU][0-9].+\)/)
    return if md.nil?
    self.name = md[1]
    self.depth = 2
    self.index = nil
  end
  
  
  #第３階層
  def parseDepth3(cell)
    return if cell.nil?    
    md = format(cell).match(/^([Ａ-ＺU])([0-9][0-9]).+[　 ]+([^　 ].*)$/)
    return if md.nil?    
    hashZen2Han = {"Ａ"=>"A","Ｂ"=>"B","Ｃ"=>"C","Ｄ"=>"D","Ｅ"=>"E","Ｆ"=>"F","Ｇ"=>"G","Ｈ"=>"H","Ｉ"=>"I","Ｊ"=>"J","Ｋ"=>"K","Ｌ"=>"L","Ｍ"=>"M","Ｎ"=>"N","Ｏ"=>"O","Ｐ"=>"P","Ｑ"=>"Q","Ｒ"=>"R","Ｓ"=>"S","Ｔ"=>"T","Ｕ"=>"U","Ｖ"=>"V","Ｗ"=>"W","Ｘ"=>"X","Ｙ"=>"Y","Ｚ"=>"Z","U"=>"U"}
    codekey = md[1]
    if hashZen2Han[codekey]
      codekey = hashZen2Han[codekey]
    end
    
    self.code = codekey + md[2]
    self.name = md[3]
    self.depth = 3
    self.index = nil
  end
  
  #第３階層イレギュラー１
  def parseDepth31(cell)
    return if cell.nil?    
    md = format(cell).match(/^　([Ａ-ＺU])([0-9][0-9]).－[^a-z]+[　 ]([^　 ]+)/)
    return if md.nil?    
    hashZen2Han = {"Ａ"=>"A","Ｂ"=>"B","Ｃ"=>"C","Ｄ"=>"D","Ｅ"=>"E","Ｆ"=>"F","Ｇ"=>"G","Ｈ"=>"H","Ｉ"=>"I","Ｊ"=>"J","Ｋ"=>"K","Ｌ"=>"L","Ｍ"=>"M","Ｎ"=>"N","Ｏ"=>"O","Ｐ"=>"P","Ｑ"=>"Q","Ｒ"=>"R","Ｓ"=>"S","Ｔ"=>"T","Ｕ"=>"U","Ｖ"=>"V","Ｗ"=>"W","Ｘ"=>"X","Ｙ"=>"Y","Ｚ"=>"Z","U"=>"U"}
    codekey = md[1]
    if hashZen2Han[codekey]
      codekey = hashZen2Han[codekey]
    end
    
    self.code = codekey + md[2]
    self.name = md[3]
    self.depth = 3
    self.index = nil
  end
  
  #第３階層イレギュラー２
  def parseDepth32(cell)
    return if cell.nil?    
    md = format(cell).match(/^([Ａ-ＺU])([0-9][0-9]).+[　 ]+([^　 ].*)$/)
    return if md.nil?    
    hashZen2Han = {"Ａ"=>"A","Ｂ"=>"B","Ｃ"=>"C","Ｄ"=>"D","Ｅ"=>"E","Ｆ"=>"F","Ｇ"=>"G","Ｈ"=>"H","Ｉ"=>"I","Ｊ"=>"J","Ｋ"=>"K","Ｌ"=>"L","Ｍ"=>"M","Ｎ"=>"N","Ｏ"=>"O","Ｐ"=>"P","Ｑ"=>"Q","Ｒ"=>"R","Ｓ"=>"S","Ｔ"=>"T","Ｕ"=>"U","Ｖ"=>"V","Ｗ"=>"W","Ｘ"=>"X","Ｙ"=>"Y","Ｚ"=>"Z","U"=>"U"}
    codekey = md[1]
    if hashZen2Han[codekey]
      codekey = hashZen2Han[codekey]
    end
    
    self.code = codekey + md[2]
    self.name = md[3]
    self.depth = 3
    self.index = nil
  end
  
  #第４階層
  def parseDepth4(cell)
    return if cell.nil?    
    md = format(cell).match(/^　([Ａ-ＺU])([0-9][0-9]).([0-9])[^0-9]*[ 　]+([^　 ].*)$/)
    return if md.nil?

    hashZen2Han = {"Ａ"=>"A","Ｂ"=>"B","Ｃ"=>"C","Ｄ"=>"D","Ｅ"=>"E","Ｆ"=>"F","Ｇ"=>"G","Ｈ"=>"H","Ｉ"=>"I","Ｊ"=>"J","Ｋ"=>"K","Ｌ"=>"L","Ｍ"=>"M","Ｎ"=>"N","Ｏ"=>"O","Ｐ"=>"P","Ｑ"=>"Q","Ｒ"=>"R","Ｓ"=>"S","Ｔ"=>"T","Ｕ"=>"U","Ｖ"=>"V","Ｗ"=>"W","Ｘ"=>"X","Ｙ"=>"Y","Ｚ"=>"Z","U"=>"U"}
    codekey = md[1]
    if hashZen2Han[codekey]
      codekey = hashZen2Han[codekey]
    end
    
    self.code = codekey + md[2] + md[3]
    self.name = md[4]
    self.depth = 4
    self.index = nil
  end
  
  #細分類項目の指示
  def parseDetailDerection(cell)
    return if cell.nil?    
    md = format(cell).match(/^下記の４桁細分類項目[^外]+項目([Ａ-ＺU])([0-9][0-9])－([Ａ-ＺU])([0-9][0-9])/)
    return if md.nil?
    
    hashZen2Han = {"Ａ"=>"A","Ｂ"=>"B","Ｃ"=>"C","Ｄ"=>"D","Ｅ"=>"E","Ｆ"=>"F","Ｇ"=>"G","Ｈ"=>"H","Ｉ"=>"I","Ｊ"=>"J","Ｋ"=>"K","Ｌ"=>"L","Ｍ"=>"M","Ｎ"=>"N","Ｏ"=>"O","Ｐ"=>"P","Ｑ"=>"Q","Ｒ"=>"R","Ｓ"=>"S","Ｔ"=>"T","Ｕ"=>"U","Ｖ"=>"V","Ｗ"=>"W","Ｘ"=>"X","Ｙ"=>"Y","Ｚ"=>"Z","U"=>"U"}
    codekey_start = md[1]
    if hashZen2Han[codekey_start]
      codekey_start = hashZen2Han[codekey_start]
    end
    
    codekey_end = md[3]
    if hashZen2Han[codekey_end]
      codekey_end = hashZen2Han[codekey_end]
    end
    
    $detail_start_code = codekey_start + md[2]
    $detail_end_code = codekey_end + md[4]
    self.index = nil

  end

  #細分類項目
  def parseDetail(cell)
    return if cell.nil?    
    md = format(cell).match(/^　　　.([0-9])[^　 ]*[　 ]+([^　 ]+)$/)
    return if md.nil?
    
    $detail_items[md[1]] = md[2]

    self.index = nil
  end


end #Icd10


#■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■
#□■□■□■□■□■□■  Entry Point  □■□■□■□■□■□■□■
#■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■
CSV_PATH = '/csv/ICD10'
BYOMEI_TXT = '/csv/byomei/nmain316.txt'


##########################################
# コマンドパラメータ
##########################################
seeds_filename = ARGV.shift

if seeds_filename.nil?
  log "Please Input Seeds File Name And Version Year"
  log "Usage  ) >ruby " + File.basename(__FILE__) + " [seed file]"
  log "Example) >ruby " + File.basename(__FILE__) + " seeds.rb"
  exit
end

##########################################
# CSV読み込み & パース & データオブジェクト作成
##########################################
csv_path      = File.expand_path(File.dirname(__FILE__)) + CSV_PATH
csv_filenames = Dir::entries(csv_path).select {|item| /\.csv$/ =~ item }

icd10s    = Array.new
icd10     = nil
index     = nil
depth     = 0

icd10_childs = Array.new
icd10_child = nil

$detail_flg  = false
$detail_start_code  = nil
$detail_end_code  = nil
$detail_items = Hash.new()

$key_idx = 0

last_depth_key = Array.new(5)
hash_depth4_keys = Hash.new()

log "#######################"
log "Reading CSV Files...."
log "-----------------------"
csv_filenames.each do | csv_filename |
  log csv_filename
  CSV.foreach(csv_path + "/" + csv_filename, row_sep: "\r\n", encoding: "SJIS") do |row|

    row[0].encode!("UTF-8")
    $key_idx += 1

    #行頭の一は読み飛ばす
  	next if /^ー/ =~ row[0]

    if /^　　　第.+章/ =~ row[0]
      #第１階層
      depth = 1
    elsif /^[^　]+\([Ａ-ＺU][0-9].+\)/ =~ row[0]
      #第２階層
      depth = 2
    elsif /^[Ａ-ＺU]/ =~ row[0]
      #第３階層
      depth = 3
    elsif /^　([Ａ-ＺU])([0-9][0-9]).－([^a-z]+)/ =~ row[0]
      #第３階層イレギュラー１
      depth = 31

#    elsif /^　([Ａ-ＺU])([0-9][0-9]).－([a-z])/ =~ row[0]
#      #第３階層イレギュラー２
#      depth = 32
#      md = format(row[0]).match(/^　[Ａ-ＺU][0-9][0-9].－([a-z])/)
#      unless md.nil?
#        sub_key = md[1]
#      end

    elsif /^　([Ａ-ＺU])([0-9][0-9]).([0-9])[^0-9]*[ 　]+([^　 ].*)$/ =~ row[0]
      #第４階層
      depth = 4
    elsif /^下記の４桁細分類項目[^外]+項目([Ａ-ＺU])([0-9][0-9])－([Ａ-ＺU])([0-9][0-9])/ =~ row[0]
      #細分類項目の指示
      depth = 91 #便宜的なdepth
    elsif /^　　　.([0-9])[^　 ]*[　 ]+([^　 ]+)$/ =~ row[0]
      #細分類項目
      depth = 92 #便宜的なdepth
#    elsif /^下記の４桁細分類項目は，外因の発生した場所を表示するために，/ =~ row[0]
#      #細分類項目の指示（例外なので飛ばす）
#      depth = 99 #便宜的なdepth
    else
      depth = 0
    end

    #無関係な行は読み飛ばす
    next if depth == 0

	  icd10 = ICD10.new(csv_filename)
	  icd10.icd10_current_key = 'icd' + $key_idx.to_s
	  case depth
	  when 1
	    #第1階層
	    icd10.parseDepth1(row[0])
	    last_depth_key[0] = icd10.icd10_current_key
	    
	  when 2
	    #第2階層
	    icd10.parseDepth2(row[0])
	    icd10.icd10_parent_key = last_depth_key[0]
	    last_depth_key[1] = icd10.icd10_current_key
	  when 3
	    #第3階層
	    icd10.parseDepth3(row[0])
	    icd10.icd10_parent_key = last_depth_key[1]
	    last_depth_key[2] = icd10.icd10_current_key
	    
	    #ぶらさがりの詳細があるか？
	    if $detail_flg
	      icd10_childs = Array.new
	      $detail_items.each{ |key, val|
	        icd10_child = ICD10.new(csv_filename)
	        icd10_child.code = icd10.code + key
	        icd10_child.name = icd10.name + '，' + val
	        icd10_child.depth = 4
	        $key_idx += 1
	        icd10_child.icd10_current_key = 'icd' + $key_idx.to_s
	        icd10_child.icd10_parent_key = icd10.icd10_current_key
	        
          icd10_childs  << icd10_child
	      }
        if icd10.code == $detail_end_code
          $detail_flg = false
        end
	    end
	  when 31
	    #第３階層イレギュラー１
	    icd10.parseDepth31(row[0])
	    icd10.icd10_parent_key = last_depth_key[1]
	    last_depth_key[2] = icd10.icd10_current_key
	    
	    #ぶらさがりの詳細があるか？
	    if $detail_flg
	      icd10_childs = Array.new
	      $detail_items.each{ |key, val|
	        icd10_child = ICD10.new(csv_filename)
	        icd10_child.code = icd10.code + key
	        icd10_child.name = icd10.name + '，' + val
	        icd10_child.depth = 4
	        $key_idx += 1
	        icd10_child.icd10_current_key = 'icd' + $key_idx.to_s
	        icd10_child.icd10_parent_key = icd10.icd10_current_key
	        
          icd10_childs  << icd10_child
	      }
        if icd10.code == $detail_end_code
          $detail_flg = false
        end
	    end
#	  when 32
#	    #第３階層イレギュラー２
#	    icd10.parseDepth32(row[0])
#	    icd10.icd10_parent_key = last_depth_key[1]
#	    last_depth_key[2] = icd10.icd10_current_key
#	    
#	    #ぶらさがりの詳細があるか？
#	    if $detail_flg
#	      icd10_childs = Array.new
#	      $detail_items.each{ |key, val|
#	        icd10_child = ICD10.new(csv_filename)
#	        icd10_child.code = icd10.code + key
#	        icd10_child.name = icd10.name + '，' + val
#	        icd10_child.depth = 4
#	        $key_idx += 1
#	        icd10_child.icd10_current_key = 'icd' + $key_idx.to_s
#	        icd10_child.icd10_parent_key = icd10.icd10_current_key
#	        
#          icd10_childs  << icd10_child
#	      }
#       if icd10.code == $detail_end_code
#          $detail_flg = false
#        end
#	    end
	  
	  when 4
	    #第4階層
	    icd10.parseDepth4(row[0])
	    icd10.icd10_parent_key = last_depth_key[2]
	    last_depth_key[3] = icd10.icd10_current_key
    when 91
      #細分類項目の指示
      icd10.parseDetailDerection(row[0])
      $detail_flg = true
      $detail_items = Hash.new()
      icd10_childs = Array.new
    when 92
      #細分類項目
      if $detail_flg
        icd10.parseDetail(row[0])
      end
    end

	  unless icd10.nil?
	  	icd10s        << icd10
	  	if depth == 4 || depth == 3
	  	  hash_depth4_keys[icd10.code] = icd10s.length - 1
	  	end
	  end
	  
	  unless icd10_childs.empty?
	    icd10_childs.each{ |item|
	      icd10s      << item
	      hash_depth4_keys[item.code] = icd10s.length - 1
	    }
	    icd10_childs = Array.new
	  end

  end #CSV
end #File


### 病名TEXT読み込み＆パース
text_filename      = File.expand_path(File.dirname(__FILE__)) + BYOMEI_TXT

log "#######################"
log "Reading TEXT Files...."
log "-----------------------"

log text_filename

CSV.foreach(text_filename, row_sep: "\r\n", encoding: "SJIS") do |row|
  
  if hash_depth4_keys[row[6]]
    $key_idx += 1
    icd10 = ICD10.new(text_filename)
    icd10.icd10_current_key = 'icd' + $key_idx.to_s
    
    icd10.code = row[6]
    icd10.replace_code = row[5]
    if icd10.code.length == 4
      icd10.depth = 5
    else
      icd10.depth = 4
    end
    icd10.name = row[2].encode("UTF-8")
    icd10.icd10_parent_key = icd10s[hash_depth4_keys[row[6]]].icd10_current_key
    
    unless icd10.nil?
	  	icd10s << icd10
	  end
  end

end #CSV


##########################################
#            seedファイル出力
##########################################

log "#######################"
log "Writing Seeds File...."
log "-----------------------"

seeds_file = open(seeds_filename, "w")

#前処理
seeds_file.write("ActiveRecord::Base.connection.reset_pk_sequence!('icd10s')\n")
seeds_file.write("\n")
#ICD10追加/上書きシーケンス
icd10s.each_with_index do |icd, i|
  index = nil
  index = icd.index unless icd.index.nil?
#  if icd.depth == 4
#    objname = "icd" + icd.depth.to_s + icd.code.to_s
#  else
#    objname = "icd" + icd.depth.to_s
#  end

#  case icd.depth
#  when 2,3,4 then
#    parent_id = "icd" + (icd.depth - 1).to_s + ".id"
#  when 5 then
#    parent_id = "icd" + (icd.depth - 1).to_s + icd.code.to_s + ".id"
#  else
#    parent_id = nil
#  end
  
  if icd.icd10_current_key
    objname = icd.icd10_current_key
  else
    objname = "icd_" + icd.depth.to_s
  end
  
  if icd.icd10_parent_key
    parent_id = icd.icd10_parent_key + ".id"
  else
    parent_id = nil
  end

  line  = "print \"#### %s/%s\\n\"\n" % [i, icd10s.length]
  line += objname + " = Icd10.new\n"
  line += objname + ".name = \"%s\"\n" % icd.name
  line += objname + ".code = '%s'\n" % icd.code if icd.code
  line += objname + ".replace_code = '%s'\n" % icd.replace_code if icd.replace_code
  line += objname + ".depth = '%d'\n" % icd.depth if icd.depth
  unless icd.index.nil?
    line += objname + ".index = '%d'\n" % index 
  end
  line += objname + ".icd10_id = %s\n" % parent_id unless parent_id.nil?
  line += objname + ".save(:validate => false)\n"
  line += "\n"
  seeds_file.write(line)
  logr "Icd10 Num:%d/%d" % [i+1, icd10s.length]
end
log "Icd10 Num:%d/%d" % [icd10s.length, icd10s.length]

#後処理
seeds_file.write("puts '#### %s/%s'\n" % [icd10s.length, icd10s.length])

seeds_file.close
