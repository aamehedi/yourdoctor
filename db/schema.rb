# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160927032151) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "pg_stat_statements"

  create_table "addresses", force: :cascade do |t|
    t.string   "city"
    t.string   "city_kana"
    t.string   "town"
    t.string   "town_kana"
    t.string   "postal_code"
    t.string   "city1"
    t.string   "city2"
    t.string   "full_address"
    t.string   "full_address_kana"
    t.integer  "prefecture_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "municipality_id"
    t.index ["prefecture_id"], name: "index_addresses_on_prefecture_id", using: :btree
  end

  create_table "ag_by_dpch_x_mdc_l1cs", force: :cascade do |t|
    t.integer  "patient_number_surgery"
    t.integer  "patient_number_no_surgery"
    t.integer  "hospitalization_planned_num"
    t.integer  "hospitalization_chemo_num"
    t.integer  "hospitalization_unplanned_num"
    t.integer  "hospitalization_emergency_num"
    t.integer  "ambulance_transport"
    t.integer  "ambulance_transport_total"
    t.integer  "death_within_24hours_num"
    t.integer  "death_within_24hours_num_total"
    t.integer  "avg_hospital_days_number"
    t.decimal  "avg_hospital_days_by_hopital"
    t.decimal  "avg_hospital_days_whole_country"
    t.decimal  "avg_hospital_days_index_patient_format"
    t.decimal  "avg_hospital_days_whole_country_patient_format"
    t.decimal  "avg_hospital_days_index"
    t.decimal  "rehospitalization_surgery_rate"
    t.decimal  "rehospitalization_chemical_treatment_rate"
    t.integer  "dpc_hospital_id"
    t.integer  "mdc_layer1_code_id"
    t.datetime "created_at",                                     null: false
    t.datetime "updated_at",                                     null: false
    t.index ["dpc_hospital_id"], name: "index_ag_by_dpch_x_mdc_l1cs_on_dpc_hospital_id", using: :btree
    t.index ["mdc_layer1_code_id"], name: "index_ag_by_dpch_x_mdc_l1cs_on_mdc_layer1_code_id", using: :btree
  end

  create_table "ag_by_dpch_x_mdc_l2_treatments", force: :cascade do |t|
    t.integer  "number"
    t.decimal  "days"
    t.string   "label"
    t.boolean  "treatmented"
    t.string   "type"
    t.integer  "dpc_hospital_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "mdc_layer2_id"
    t.integer  "mdc_layer1_id"
    t.index ["dpc_hospital_id"], name: "index_ag_by_dpch_x_mdc_l2_treatments_on_dpc_hospital_id", using: :btree
    t.index ["mdc_layer1_id"], name: "index_ag_by_dpch_x_mdc_l2_treatments_on_mdc_layer1_id", using: :btree
    t.index ["mdc_layer2_id"], name: "index_ag_by_dpch_x_mdc_l2_treatments_on_mdc_layer2_id", using: :btree
  end

  create_table "ag_by_dpch_x_mdc_l2s", force: :cascade do |t|
    t.integer  "number"
    t.decimal  "days"
    t.integer  "dpc_hospital_id"
    t.integer  "mdc_layer2_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "label"
    t.index ["dpc_hospital_id"], name: "index_ag_by_dpch_x_mdc_l2s_on_dpc_hospital_id", using: :btree
    t.index ["mdc_layer2_id"], name: "index_ag_by_dpch_x_mdc_l2s_on_mdc_layer2_id", using: :btree
  end

  create_table "ag_by_dpch_x_reason_majors", force: :cascade do |t|
    t.decimal  "same_reason"
    t.decimal  "different_reason"
    t.decimal  "same_reason_nonchemo"
    t.decimal  "different_reason_nonchemo"
    t.integer  "reason_for_rehospitalization_id"
    t.integer  "dpc_hospital_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "reason"
    t.index ["dpc_hospital_id"], name: "index_ag_by_dpch_x_reason_majors_on_dpc_hospital_id", using: :btree
    t.index ["reason_for_rehospitalization_id"], name: "index_abd_x_rmajors_on_rfr_id", using: :btree
  end

  create_table "ag_by_dpch_x_reason_minors", force: :cascade do |t|
    t.decimal  "ratio"
    t.integer  "reason_for_rehospitalization_id"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.integer  "reason"
    t.integer  "parent_id"
    t.index ["parent_id"], name: "index_ag_by_dpch_x_reason_minors_on_parent_id", using: :btree
    t.index ["reason_for_rehospitalization_id"], name: "index_abd_x_rminors_on_rfr_id", using: :btree
  end

  create_table "ag_by_mdc_l1s", force: :cascade do |t|
    t.integer  "drugs_num"
    t.integer  "regimens_num"
    t.integer  "cases_num"
    t.integer  "hospitals_num"
    t.integer  "mdc_layer1_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["mdc_layer1_id"], name: "index_ag_by_mdc_l1s_on_mdc_layer1_id", using: :btree
  end

  create_table "ag_by_mdcs", force: :cascade do |t|
    t.string   "code"
    t.boolean  "inclusion"
    t.integer  "dpc_num_for_the_mdc"
    t.integer  "cases_num_for_the_mdc"
    t.integer  "number"
    t.decimal  "dpc_ratio_for_all_cases"
    t.integer  "male_num"
    t.integer  "female_num"
    t.integer  "from0to2_num"
    t.integer  "from3to5_num"
    t.integer  "from6to15_num"
    t.integer  "from16to20_num"
    t.integer  "from21to40_num"
    t.integer  "from41to60_num"
    t.integer  "from61to79_num"
    t.integer  "over80_num"
    t.integer  "introduction_num"
    t.integer  "outpatient_num"
    t.integer  "transported_num"
    t.integer  "unexpected_hospitalization_num"
    t.integer  "emergency_hospitalization_num"
    t.integer  "status_healed"
    t.integer  "status_improvement"
    t.integer  "status_palliation"
    t.integer  "status_unchanging"
    t.integer  "status_worse"
    t.integer  "status_death_of_illness_costed_most"
    t.integer  "status_death_of_other_illness_costed_most"
    t.integer  "status_other"
    t.decimal  "hospital_days_average"
    t.integer  "hospital_days_minimum"
    t.integer  "hospital_days_maximum"
    t.integer  "hospital_days_coefficient_of_variation"
    t.integer  "hospital_days_value_of_25percentile"
    t.integer  "hospital_days_value_of_50percentile"
    t.integer  "hospital_days_value_of_75percentile"
    t.integer  "hospital_days_value_of_90percentile"
    t.integer  "artificial_respiration"
    t.integer  "artificial_kidney"
    t.integer  "central_venous_injection"
    t.integer  "blood_transfusion"
    t.integer  "mdc_layer3_id"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.index ["mdc_layer3_id"], name: "index_ag_by_mdcs_on_mdc_layer3_id", using: :btree
  end

  create_table "all_mails", force: :cascade do |t|
    t.text     "title"
    t.text     "content"
    t.boolean  "done",       default: false
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
  end

  create_table "areas", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "awards", force: :cascade do |t|
    t.integer  "doctor_id"
    t.string   "title"
    t.integer  "year"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["doctor_id"], name: "index_awards_on_doctor_id", using: :btree
  end

  create_table "branches", force: :cascade do |t|
    t.string   "name"
    t.integer  "company_id"
    t.integer  "mr_id"
    t.integer  "this_month_request",   default: 0
    t.integer  "next_month_request",   default: 0
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "whytplot_chatroom_id"
    t.index ["company_id"], name: "index_branches_on_company_id", using: :btree
    t.index ["mr_id"], name: "index_branches_on_mr_id", using: :btree
    t.index ["whytplot_chatroom_id"], name: "index_branches_on_whytplot_chatroom_id", using: :btree
  end

  create_table "chart_types", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "charts", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.boolean  "favorite"
    t.integer  "mr_id"
    t.string   "chart_type"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.string   "raw_query_param_values"
    t.string   "option"
    t.text     "query_param_values"
    t.index ["mr_id"], name: "index_charts_on_mr_id", using: :btree
  end

  create_table "comments", force: :cascade do |t|
    t.integer  "doctor_id"
    t.text     "content"
    t.integer  "post_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["doctor_id"], name: "index_comments_on_doctor_id", using: :btree
    t.index ["post_id"], name: "index_comments_on_post_id", using: :btree
  end

  create_table "companies", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "max_request",   default: 0
    t.integer  "bonus_request", default: 0
    t.boolean  "using_chat",    default: false
    t.string   "company_image"
    t.float    "latitude"
    t.float    "longitude"
    t.string   "address"
  end

  create_table "datafiles", force: :cascade do |t|
    t.string   "filepath"
    t.integer  "status"
    t.integer  "total_line_num"
    t.integer  "error_line_num"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.string   "messages"
    t.integer  "warning_line_num"
  end

  create_table "datatable_datafiles", force: :cascade do |t|
    t.string   "datable_type"
    t.integer  "datable_id"
    t.integer  "datafile_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["datafile_id"], name: "index_datatable_datafiles_on_datafile_id", using: :btree
  end

  create_table "delayed_jobs", force: :cascade do |t|
    t.integer  "priority",   default: 0, null: false
    t.integer  "attempts",   default: 0, null: false
    t.text     "handler",                null: false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.index ["priority", "run_at"], name: "delayed_jobs_priority", using: :btree
  end

  create_table "department_categories", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.text     "comment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "department_lists", force: :cascade do |t|
    t.integer  "department_id"
    t.integer  "doctor_id"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.boolean  "active",         default: true
    t.integer  "department_seq"
    t.integer  "dr_seq"
    t.integer  "position"
    t.datetime "term_start"
    t.datetime "term_end"
    t.integer  "order_num"
    t.boolean  "part_time"
    t.integer  "index"
    t.index ["department_id"], name: "index_department_lists_on_department_id", using: :btree
    t.index ["doctor_id"], name: "index_department_lists_on_doctor_id", using: :btree
  end

  create_table "department_position_lists", force: :cascade do |t|
    t.integer  "department_list_id"
    t.integer  "department_position_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["department_list_id"], name: "index_department_position_lists_on_department_list_id", using: :btree
    t.index ["department_position_id"], name: "index_department_position_lists_on_department_position_id", using: :btree
  end

  create_table "department_positions", force: :cascade do |t|
    t.string   "name"
    t.integer  "department_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["department_id"], name: "index_department_positions_on_department_id", using: :btree
  end

  create_table "departments", force: :cascade do |t|
    t.string   "name"
    t.string   "kana_name"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "hospital_id"
    t.boolean  "no_name",     default: false
    t.text     "comment"
    t.index ["hospital_id"], name: "index_departments_on_hospital_id", using: :btree
  end

  create_table "doctor_feedbacks", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "status",                        default: 0
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.integer  "emergency_request_hospital_id"
    t.index ["doctor_id"], name: "index_doctor_feedbacks_on_doctor_id", using: :btree
    t.index ["emergency_request_hospital_id"], name: "index_doctor_feedbacks_on_emergency_request_hospital_id", using: :btree
  end

  create_table "doctor_specialties", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "specialty_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["doctor_id"], name: "index_doctor_specialties_on_doctor_id", using: :btree
    t.index ["specialty_id"], name: "index_doctor_specialties_on_specialty_id", using: :btree
  end

  create_table "doctors", force: :cascade do |t|
    t.integer  "hospital_id"
    t.string   "first_name"
    t.string   "last_name"
    t.text     "position"
    t.integer  "university_id"
    t.string   "graduation_year"
    t.text     "society"
    t.string   "profession"
    t.text     "skill"
    t.text     "result"
    t.text     "url"
    t.text     "history"
    t.integer  "top_or_next"
    t.string   "user"
    t.string   "update_user"
    t.text     "comment"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.string   "middle_name"
    t.string   "first_name_initial"
    t.string   "last_name_initial"
    t.string   "middle_name_initial"
    t.string   "email"
    t.string   "encrypted_password",        default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",             default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "access_token"
    t.string   "registration_number"
    t.string   "first_name_reading",        default: ""
    t.string   "last_name_reading",         default: ""
    t.string   "middle_name_reading",       default: ""
    t.string   "avatar_image",              default: ""
    t.string   "cover_image",               default: ""
    t.integer  "sex"
    t.date     "birthdate"
    t.date     "registration_date"
    t.integer  "identity_verification"
    t.boolean  "is_staff",                  default: false
    t.datetime "mail_sent_time"
    t.datetime "current_sign_in_to_app_at"
    t.boolean  "emergency",                 default: false
    t.boolean  "rest_mode",                 default: false
    t.time     "rest_time_start"
    t.time     "rest_time_end"
    t.index ["hospital_id"], name: "index_doctors_on_hospital_id", using: :btree
    t.index ["reset_password_token"], name: "index_doctors_on_reset_password_token", unique: true, using: :btree
    t.index ["university_id"], name: "index_doctors_on_university_id", using: :btree
  end

  create_table "dpc_hospitals", force: :cascade do |t|
    t.integer  "created_year"
    t.integer  "notice_number"
    t.integer  "serial_number"
    t.string   "name"
    t.integer  "out_of_analysis_eceptions_num"
    t.integer  "out_of_analysis_psychiatric_wards_num"
    t.integer  "out_of_analysis_general_wards_num"
    t.integer  "out_of_analysis_duplicate_medical_record_num"
    t.integer  "out_of_analysis_stay_1day_or_less_num"
    t.integer  "out_of_analysis_stay_sleepover_or_less_num"
    t.integer  "out_of_analysis_age_under0_over120"
    t.integer  "out_of_analysis_error_of_hospitalized_birth_num"
    t.integer  "out_of_analysis_movement_other_general_ward_num"
    t.integer  "out_of_analysis_death_within_24hours"
    t.integer  "out_of_analysis_transplant_surgery_num"
    t.integer  "out_of_analysis_no_insurance_num"
    t.integer  "out_of_analysis_not_applicable_dpc"
    t.integer  "out_of_analysis_ad_apr_earlier_and_expect_dis_jun_to_dec_num"
    t.integer  "out_of_analysis_trial_num"
    t.integer  "out_of_analysis_death_within_7days_after_birth_num"
    t.integer  "out_of_analysis_stipulated_by_ministry_of_health"
    t.integer  "out_of_analysis_analyzed_num"
    t.decimal  "out_of_analysis_analyzed_ratio"
    t.integer  "hospital_days_num"
    t.decimal  "hospital_days_average_value"
    t.decimal  "hospital_days_coefficient_of_variation"
    t.integer  "hospital_day_sminimum_vanue"
    t.decimal  "hospital_days_percentile25"
    t.decimal  "hospital_days_percentile50"
    t.decimal  "hospital_days_percentile75"
    t.integer  "hospital_days_maximum_value"
    t.decimal  "status_after_leaving_home_care_visiting_same_hospital"
    t.decimal  "status_after_leaving_home_care_visiting_other_hospital"
    t.decimal  "status_after_leaving_home_care_other"
    t.decimal  "status_after_leaving_changing_hospital"
    t.decimal  "status_after_leaving_entering_nursing_home"
    t.decimal  "status_after_leaving_entering_asissted_nursing_home"
    t.decimal  "status_after_leaving_entering_residential_facility"
    t.decimal  "status_after_leaving_death"
    t.decimal  "status_after_leaving_other_method"
    t.decimal  "status_outcome_healed"
    t.decimal  "status_outcome_improvement"
    t.decimal  "status_outcome_palliation"
    t.decimal  "status_outcome_unchanging"
    t.decimal  "status_outcome_worse"
    t.decimal  "status_outcome_death_of_illness_costed_most"
    t.decimal  "status_outcome_death_of_other_illness_costed_most"
    t.decimal  "status_outcome_other"
    t.decimal  "rehospitalize_within_6wks_same_cond"
    t.decimal  "rehospitalize_after_6wks_same_cond"
    t.decimal  "rehospitalize_within_6weeks_diff_cond"
    t.decimal  "rehospitalize_after_6wks_diff_cond"
    t.decimal  "advanced_medi_care_surgery"
    t.decimal  "advanced_medi_care_chemotherapy"
    t.decimal  "advanced_medi_care_radiation_therapy"
    t.decimal  "advanced_medi_care_ambulance_transport"
    t.decimal  "advanced_medi_care_any_of_these"
    t.decimal  "advanced_medi_care_anesthesia"
    t.decimal  "rehospitalization_ratio_rehospitalization"
    t.decimal  "rehospitalization_ratio_change_ward_again"
    t.decimal  "rehospitalization_length_of_hospital_days_within_3days"
    t.decimal  "rehospitalization_length_of_hospital_days_within_4to7days"
    t.decimal  "rehospitalization_length_of_hospital_days_within_8to14days"
    t.decimal  "rehospitalization_length_of_hospital_days_within_15to28days"
    t.decimal  "rehospitalization_length_of_hospital_days_within_29to42days"
    t.decimal  "rehospitalization_length_of_stay_first_time"
    t.decimal  "rehospitalization_length_of_stay_second_time"
    t.decimal  "rehospitalization_length_of_stay_third_time"
    t.decimal  "rehospitalization_times_of_hospitalization"
    t.integer  "hospital_id"
    t.datetime "created_at",                                                   null: false
    t.datetime "updated_at",                                                   null: false
    t.string   "dpc_type"
    t.integer  "dpc_calc_bed_num"
    t.string   "dpc_calc_hospitalization_basic_fee_type"
    t.integer  "dpc_calc_hospitalization_basic_fee_lratio"
    t.integer  "dpc_calc_hospitalization_basic_fee_rratio"
    t.integer  "dpc_mental_hospital_bed_num"
    t.integer  "dpc_recuperation_hospital_bed_num"
    t.integer  "dpc_tuberculosis_hospital_bed_num"
    t.integer  "dpc_total_bed_num"
    t.integer  "dpc_submitted_months_num"
    t.index ["created_year"], name: "index_dpc_hospitals_on_created_year", using: :btree
    t.index ["hospital_id"], name: "index_dpc_hospitals_on_hospital_id", using: :btree
  end

  create_table "dr_mr_chatrooms", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "mr_id"
    t.integer  "status",                  default: 0
    t.boolean  "favorite",                default: true
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
    t.integer  "last_seen_message_id_dr"
    t.integer  "last_seen_message_id_mr"
    t.index ["doctor_id"], name: "index_dr_mr_chatrooms_on_doctor_id", using: :btree
    t.index ["mr_id"], name: "index_dr_mr_chatrooms_on_mr_id", using: :btree
  end

  create_table "dr_mr_messages", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "mr_id"
    t.integer  "dr_mr_chatroom_id"
    t.string   "content"
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "message_type",      default: 0
    t.string   "attachment"
    t.index ["doctor_id"], name: "index_dr_mr_messages_on_doctor_id", using: :btree
    t.index ["dr_mr_chatroom_id"], name: "index_dr_mr_messages_on_dr_mr_chatroom_id", using: :btree
    t.index ["mr_id"], name: "index_dr_mr_messages_on_mr_id", using: :btree
  end

  create_table "duplicate_doctor_lists", force: :cascade do |t|
    t.integer  "doctor_ids",                              array: true
    t.boolean  "check",      default: false
    t.integer  "merged_ids",                              array: true
    t.datetime "created_at",                 null: false
    t.datetime "updated_at",                 null: false
    t.text     "comment"
  end

  create_table "emergency_doctor_hospitals", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "hospital_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["doctor_id"], name: "index_emergency_doctor_hospitals_on_doctor_id", using: :btree
    t.index ["hospital_id"], name: "index_emergency_doctor_hospitals_on_hospital_id", using: :btree
  end

  create_table "emergency_request_hospitals", force: :cascade do |t|
    t.integer  "emergency_request_id"
    t.integer  "hospital_id"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.index ["emergency_request_id"], name: "index_emergency_request_hospitals_on_emergency_request_id", using: :btree
    t.index ["hospital_id"], name: "index_emergency_request_hospitals_on_hospital_id", using: :btree
  end

  create_table "emergency_requests", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "whytlink_chatroom_id"
    t.datetime "time"
    t.integer  "closing_reason"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
    t.integer  "status",                        default: 0
    t.string   "closing_comment"
    t.integer  "emergency_request_hospital_id"
    t.index ["doctor_id"], name: "index_emergency_requests_on_doctor_id", using: :btree
    t.index ["emergency_request_hospital_id"], name: "index_emergency_requests_on_emergency_request_hospital_id", using: :btree
    t.index ["whytlink_chatroom_id"], name: "index_emergency_requests_on_whytlink_chatroom_id", using: :btree
  end

  create_table "favorite_group_doctors", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "favorite_group_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "hospital_id"
    t.index ["doctor_id"], name: "index_favorite_group_doctors_on_doctor_id", using: :btree
    t.index ["favorite_group_id"], name: "index_favorite_group_doctors_on_favorite_group_id", using: :btree
    t.index ["hospital_id"], name: "index_favorite_group_doctors_on_hospital_id", using: :btree
  end

  create_table "favorite_groups", force: :cascade do |t|
    t.string   "name"
    t.integer  "doctor_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["doctor_id"], name: "index_favorite_groups_on_doctor_id", using: :btree
  end

  create_table "hospitals", force: :cascade do |t|
    t.integer  "medical_group_id"
    t.integer  "area_id"
    t.integer  "prefecture_id"
    t.integer  "std_region_code"
    t.text     "name"
    t.integer  "postal_code"
    t.text     "address"
    t.string   "tmp_head"
    t.text     "url"
    t.integer  "bed_num"
    t.integer  "acute_phase_bed_num"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.string   "tmp_user"
    t.integer  "user_id"
    t.string   "code_region"
    t.string   "code_number"
    t.string   "code_checkdigit"
    t.string   "phone"
    t.integer  "number_of_fulltime_medicine"
    t.integer  "number_of_fulltime_dentistry"
    t.integer  "number_of_fulltime_pharmacy"
    t.integer  "number_of_parttime_medicine"
    t.integer  "number_of_parttime_dentistry"
    t.integer  "number_of_parttime_pharmacy"
    t.string   "establisher_org"
    t.string   "establisher_first_name"
    t.string   "establisher_last_name"
    t.string   "director_first_name"
    t.string   "director_last_name"
    t.date     "designation_date"
    t.string   "designation_reason"
    t.date     "designation_start"
    t.string   "beds_hash",                                 array: true
    t.string   "departments_array",                         array: true
    t.string   "state"
    t.integer  "version_year"
    t.float    "latitude"
    t.float    "longitude"
    t.index ["area_id"], name: "index_hospitals_on_area_id", using: :btree
    t.index ["medical_group_id"], name: "index_hospitals_on_medical_group_id", using: :btree
    t.index ["prefecture_id"], name: "index_hospitals_on_prefecture_id", using: :btree
    t.index ["user_id"], name: "index_hospitals_on_user_id", using: :btree
  end

  create_table "icd10s", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.string   "replace_code"
    t.integer  "depth"
    t.integer  "icd10_id"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "index"
    t.index ["icd10_id"], name: "index_icd10s_on_icd10_id", using: :btree
  end

  create_table "kcodes", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "links", force: :cascade do |t|
    t.integer  "followee_id"
    t.integer  "follower_id"
    t.integer  "state"
    t.text     "message"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["followee_id", "follower_id"], name: "index_links_on_followee_id_and_follower_id", unique: true, using: :btree
  end

  create_table "mail_logs", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "all_mail_id"
    t.boolean  "done",        default: false
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["all_mail_id"], name: "index_mail_logs_on_all_mail_id", using: :btree
    t.index ["doctor_id"], name: "index_mail_logs_on_doctor_id", using: :btree
  end

  create_table "mdc_layer1_codes", force: :cascade do |t|
    t.integer  "year"
    t.string   "code"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["code"], name: "index_mdc_layer1_codes_on_code", using: :btree
    t.index ["year"], name: "index_mdc_layer1_codes_on_year", using: :btree
  end

  create_table "mdc_layer1s", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.integer  "mdc_layer1_code_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "icd10_id"
    t.index ["code"], name: "index_mdc_layer1s_on_code", using: :btree
    t.index ["icd10_id"], name: "index_mdc_layer1s_on_icd10_id", using: :btree
    t.index ["mdc_layer1_code_id"], name: "index_mdc_layer1s_on_mdc_layer1_code_id", using: :btree
  end

  create_table "mdc_layer2s", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.boolean  "no_procedure"
    t.integer  "mdc_layer1_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["code"], name: "index_mdc_layer2s_on_code", using: :btree
    t.index ["mdc_layer1_id"], name: "index_mdc_layer2s_on_mdc_layer1_id", using: :btree
  end

  create_table "mdc_layer3s", force: :cascade do |t|
    t.string   "code_1"
    t.string   "code_2"
    t.string   "code_3"
    t.string   "code_4"
    t.integer  "mdc_layer2_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "name"
    t.index ["mdc_layer2_id"], name: "index_mdc_layer3s_on_mdc_layer2_id", using: :btree
  end

  create_table "medical_groups", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "medical_municipalities", force: :cascade do |t|
    t.string   "name"
    t.string   "medical_region_code"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "municipality_id"
    t.index ["municipality_id"], name: "index_medical_municipalities_on_municipality_id", unique: true, using: :btree
  end

  create_table "medical_regions", force: :cascade do |t|
    t.string   "name"
    t.integer  "level"
    t.string   "code"
    t.integer  "parent_id"
    t.integer  "children_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "prefecture_id"
    t.integer  "area_id"
    t.index ["area_id"], name: "index_medical_regions_on_area_id", using: :btree
    t.index ["code"], name: "index_medical_regions_on_code", unique: true, using: :btree
    t.index ["prefecture_id"], name: "index_medical_regions_on_prefecture_id", using: :btree
  end

  create_table "monographs", force: :cascade do |t|
    t.integer  "doctor_id"
    t.string   "title"
    t.integer  "year"
    t.text     "description"
    t.text     "url"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.text     "writers"
    t.text     "media"
    t.text     "page"
    t.datetime "published"
    t.string   "published_type"
    t.index ["doctor_id"], name: "index_monographs_on_doctor_id", using: :btree
  end

  create_table "mr_projects", force: :cascade do |t|
    t.integer  "mr_id"
    t.integer  "project_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["mr_id"], name: "index_mr_projects_on_mr_id", using: :btree
    t.index ["project_id"], name: "index_mr_projects_on_project_id", using: :btree
  end

  create_table "mr_sessions", force: :cascade do |t|
    t.integer  "mr_id"
    t.string   "device_token"
    t.string   "access_token"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.index ["access_token"], name: "index_mr_sessions_on_access_token", using: :btree
    t.index ["mr_id"], name: "index_mr_sessions_on_mr_id", using: :btree
  end

  create_table "mrs", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "first_name",             default: ""
    t.string   "last_name",              default: ""
    t.string   "middle_name",            default: ""
    t.string   "first_name_initial",     default: ""
    t.string   "last_name_initial",      default: ""
    t.string   "middle_name_initial",    default: ""
    t.integer  "sex",                    default: 0
    t.date     "birthdate"
    t.integer  "mr_type"
    t.string   "avatar_string"
    t.integer  "branch_id"
    t.integer  "company_id"
    t.index ["branch_id"], name: "index_mrs_on_branch_id", using: :btree
    t.index ["company_id"], name: "index_mrs_on_company_id", using: :btree
    t.index ["email"], name: "index_mrs_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_mrs_on_reset_password_token", unique: true, using: :btree
  end

  create_table "municipalities", force: :cascade do |t|
    t.string   "prefecture_name"
    t.string   "city_name"
    t.string   "prefecture_name_kana"
    t.string   "city_name_kana"
    t.boolean  "is_ordinance_designated_city", default: false
    t.datetime "created_at",                                   null: false
    t.datetime "updated_at",                                   null: false
  end

  create_table "number_by_codes", force: :cascade do |t|
    t.integer  "number"
    t.string   "type"
    t.integer  "index"
    t.string   "codeable_type"
    t.integer  "codeable_id"
    t.integer  "ag_by_mdc_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.string   "code"
    t.index ["ag_by_mdc_id"], name: "index_number_by_codes_on_ag_by_mdc_id", using: :btree
  end

  create_table "populations", force: :cascade do |t|
    t.integer  "year"
    t.integer  "male_0to4"
    t.integer  "female_0to4"
    t.integer  "male_5to9"
    t.integer  "female_5to9"
    t.integer  "male_10to14"
    t.integer  "female_10to14"
    t.integer  "male_15to19"
    t.integer  "female_15to19"
    t.integer  "male_20to24"
    t.integer  "female_20to24"
    t.integer  "male_25to29"
    t.integer  "female_25to29"
    t.integer  "male_30to34"
    t.integer  "female_30to34"
    t.integer  "male_35to39"
    t.integer  "female_35to39"
    t.integer  "male_40to44"
    t.integer  "female_40to44"
    t.integer  "male_45to49"
    t.integer  "female_45to49"
    t.integer  "male_50to54"
    t.integer  "female_50to54"
    t.integer  "male_55to59"
    t.integer  "female_55to59"
    t.integer  "male_60to64"
    t.integer  "female_60to64"
    t.integer  "male_65to69"
    t.integer  "female_65to69"
    t.integer  "male_70to74"
    t.integer  "female_70to74"
    t.integer  "male_75to79"
    t.integer  "female_75to79"
    t.integer  "male_80to84"
    t.integer  "female_80to84"
    t.integer  "male_85to89"
    t.integer  "female_85to89"
    t.integer  "male_over90"
    t.integer  "female_over90"
    t.integer  "municipality_id"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.integer  "dpc_male_0to2"
    t.integer  "dpc_female_0to2"
    t.integer  "dpc_male_3to5"
    t.integer  "dpc_female_3to5"
    t.integer  "dpc_male_6to15"
    t.integer  "dpc_female_6to15"
    t.integer  "dpc_male_16to20"
    t.integer  "dpc_female_16to20"
    t.integer  "dpc_male_21to40"
    t.integer  "dpc_female_21to40"
    t.integer  "dpc_male_41to60"
    t.integer  "dpc_female_41to60"
    t.integer  "dpc_male_61to79"
    t.integer  "dpc_female_61to79"
    t.integer  "dpc_male_over80"
    t.integer  "dpc_female_over80"
    t.integer  "prefecture_id"
    t.index ["municipality_id"], name: "index_populations_on_municipality_id", using: :btree
    t.index ["prefecture_id"], name: "index_populations_on_prefecture_id", using: :btree
    t.index ["year"], name: "index_populations_on_year", using: :btree
  end

  create_table "posts", force: :cascade do |t|
    t.integer  "doctor_id"
    t.text     "memo"
    t.integer  "read_level"
    t.integer  "suggestion_id"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["doctor_id"], name: "index_posts_on_doctor_id", using: :btree
    t.index ["suggestion_id"], name: "index_posts_on_suggestion_id", using: :btree
  end

  create_table "pre_monographs", force: :cascade do |t|
    t.integer  "doctor_id"
    t.string   "title"
    t.integer  "year"
    t.text     "description"
    t.text     "url"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.text     "writers"
    t.text     "media"
    t.text     "page"
    t.datetime "published"
    t.string   "published_type"
    t.boolean  "is_confirmed",   default: false, null: false
    t.index ["doctor_id"], name: "index_pre_monographs_on_doctor_id", using: :btree
  end

  create_table "pre_monographs_moderated_scraping_histories", force: :cascade do |t|
    t.integer  "doctor_id",  null: false
    t.integer  "source",     null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text     "page_info"
    t.text     "url"
    t.index ["doctor_id"], name: "index_pre_monographs_moderated_scraping_histories_on_doctor_id", using: :btree
  end

  create_table "pre_monographs_scraping_histories", force: :cascade do |t|
    t.integer  "doctor_id",     null: false
    t.integer  "created_by_id", null: false
    t.datetime "completed_at"
    t.integer  "source",        null: false
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.index ["created_by_id"], name: "index_pre_monographs_scraping_histories_on_created_by_id", using: :btree
    t.index ["doctor_id"], name: "index_pre_monographs_scraping_histories_on_doctor_id", using: :btree
  end

  create_table "prefectures", force: :cascade do |t|
    t.string   "name"
    t.integer  "area_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["area_id"], name: "index_prefectures_on_area_id", using: :btree
  end

  create_table "projects", force: :cascade do |t|
    t.string   "name"
    t.integer  "mr_id"
    t.integer  "company_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.string   "description"
    t.integer  "this_month_request",   default: 0
    t.integer  "next_month_request",   default: 0
    t.integer  "whytplot_chatroom_id"
    t.index ["company_id"], name: "index_projects_on_company_id", using: :btree
    t.index ["mr_id"], name: "index_projects_on_mr_id", using: :btree
    t.index ["whytplot_chatroom_id"], name: "index_projects_on_whytplot_chatroom_id", using: :btree
  end

  create_table "provisional_doctors", force: :cascade do |t|
    t.string   "email"
    t.string   "password"
    t.string   "registration_number"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "uuid"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "recommends", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "skill_id"
    t.integer  "point"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["doctor_id"], name: "index_recommends_on_doctor_id", using: :btree
    t.index ["skill_id"], name: "index_recommends_on_skill_id", using: :btree
  end

  create_table "regimen_pharmacy_rankings", force: :cascade do |t|
    t.integer  "no"
    t.integer  "regimens_num"
    t.integer  "cases_num"
    t.integer  "hospitals_num"
    t.decimal  "regimens_ratio"
    t.decimal  "cases_ratio"
    t.decimal  "hospitals_ratio"
    t.integer  "ag_by_mdc_l1_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.index ["ag_by_mdc_l1_id"], name: "index_regimen_pharmacy_rankings_on_ag_by_mdc_l1_id", using: :btree
  end

  create_table "regimen_regimen_rankings", force: :cascade do |t|
    t.integer  "no"
    t.integer  "cases_num"
    t.decimal  "cases_ratio"
    t.decimal  "cases_cumulative_ratio"
    t.integer  "hospitals_num"
    t.decimal  "hospitals_ratio"
    t.decimal  "average_hospital_days"
    t.string   "regimen"
    t.integer  "ag_by_mdc_l1_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.index ["ag_by_mdc_l1_id"], name: "index_regimen_regimen_rankings_on_ag_by_mdc_l1_id", using: :btree
  end

  create_table "relationships", force: :cascade do |t|
    t.integer  "follower_id"
    t.integer  "followed_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["followed_id"], name: "index_relationships_on_followed_id", using: :btree
    t.index ["follower_id"], name: "index_relationships_on_follower_id", using: :btree
  end

  create_table "scenario_charts", force: :cascade do |t|
    t.integer  "chart_order"
    t.integer  "scenario_id"
    t.integer  "chart_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["chart_id"], name: "index_scenario_charts_on_chart_id", using: :btree
    t.index ["scenario_id"], name: "index_scenario_charts_on_scenario_id", using: :btree
  end

  create_table "scenarios", force: :cascade do |t|
    t.string   "name"
    t.string   "description"
    t.boolean  "favorite",    default: false
    t.integer  "mr_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.index ["mr_id"], name: "index_scenarios_on_mr_id", using: :btree
  end

  create_table "sheets", force: :cascade do |t|
    t.string   "area"
    t.string   "medical_group"
    t.string   "work_status"
    t.string   "hospital_id"
    t.string   "hospital_name"
    t.string   "doctor_name"
    t.string   "position"
    t.string   "department"
    t.string   "university"
    t.string   "graduation_year"
    t.text     "society"
    t.string   "profession"
    t.text     "skill"
    t.text     "result"
    t.text     "link"
    t.text     "history"
    t.string   "top_or_next"
    t.string   "user"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "update_user"
    t.text     "comment"
  end

  create_table "shines", force: :cascade do |t|
    t.integer  "parent_id"
    t.string   "parent_type"
    t.integer  "doctor_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.index ["doctor_id"], name: "index_shines_on_doctor_id", using: :btree
  end

  create_table "skills", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "icd10_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text     "comment"
    t.index ["doctor_id"], name: "index_skills_on_doctor_id", using: :btree
    t.index ["icd10_id"], name: "index_skills_on_icd10_id", using: :btree
  end

  create_table "societies", force: :cascade do |t|
    t.text     "name"
    t.text     "english_name"
    t.text     "address"
    t.text     "email"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "society_lists", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "society_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["doctor_id"], name: "index_society_lists_on_doctor_id", using: :btree
    t.index ["society_id"], name: "index_society_lists_on_society_id", using: :btree
  end

  create_table "society_position_lists", force: :cascade do |t|
    t.integer  "society_position_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "society_list_id"
    t.index ["society_list_id"], name: "index_society_position_lists_on_society_list_id", using: :btree
    t.index ["society_position_id"], name: "index_society_position_lists_on_society_position_id", using: :btree
  end

  create_table "society_positions", force: :cascade do |t|
    t.text     "name"
    t.text     "comment"
    t.integer  "society_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["society_id"], name: "index_society_positions_on_society_id", using: :btree
  end

  create_table "specialties", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "staff_logs", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "staff_log_id"
    t.string   "staff_log_type"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.string   "sub"
    t.index ["user_id"], name: "index_staff_logs_on_user_id", using: :btree
  end

  create_table "staff_posts", force: :cascade do |t|
    t.text     "body"
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "suggestions", force: :cascade do |t|
    t.string   "text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id"
    t.string   "taggable_type"
    t.integer  "taggable_id"
    t.string   "tagger_type"
    t.integer  "tagger_id"
    t.string   "context",       limit: 128
    t.datetime "created_at"
    t.string   "tag_type"
    t.index ["context"], name: "index_taggings_on_context", using: :btree
    t.index ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
    t.index ["tag_id"], name: "index_taggings_on_tag_id", using: :btree
    t.index ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree
    t.index ["taggable_id", "taggable_type", "tagger_id", "context"], name: "taggings_idy", using: :btree
    t.index ["taggable_id"], name: "index_taggings_on_taggable_id", using: :btree
    t.index ["taggable_type"], name: "index_taggings_on_taggable_type", using: :btree
    t.index ["tagger_id", "tagger_type"], name: "index_taggings_on_tagger_id_and_tagger_type", using: :btree
    t.index ["tagger_id"], name: "index_taggings_on_tagger_id", using: :btree
  end

  create_table "tags", force: :cascade do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
    t.index ["name"], name: "index_tags_on_name", unique: true, using: :btree
  end

  create_table "temporary_doctors", force: :cascade do |t|
    t.string   "email"
    t.string   "password"
    t.string   "registration_number"
    t.string   "first_name"
    t.string   "last_name"
    t.string   "uuid"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
    t.integer  "doctor_id"
    t.integer  "sex"
    t.integer  "estimate_doctor_id"
    t.date     "birthdate"
    t.date     "registration_date"
    t.index ["doctor_id"], name: "index_temporary_doctors_on_doctor_id", using: :btree
  end

  create_table "timelines", force: :cascade do |t|
    t.integer  "log_id"
    t.string   "log_type"
    t.string   "action"
    t.integer  "paper_trail_id"
    t.integer  "doctor_id"
    t.integer  "editor_id"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.string   "editor_type"
    t.string   "type"
    t.text     "meta"
    t.string   "model_type"
    t.integer  "model_id"
    t.string   "editor_first_name"
    t.string   "editor_last_name"
    t.string   "editor_hospital_name"
    t.integer  "editor_hospital_id"
    t.string   "editor_university_name"
    t.integer  "editor_university_id"
    t.integer  "editor_graduation_year"
    t.string   "doctor_first_name"
    t.string   "doctor_last_name"
    t.string   "doctor_hospital_name"
    t.integer  "doctor_hospital_id"
    t.string   "doctor_university_name"
    t.integer  "doctor_university_id"
    t.integer  "doctor_graduation_year"
    t.integer  "read_level",             default: 0
    t.integer  "series",                 default: -1
  end

  create_table "universities", force: :cascade do |t|
    t.string   "name"
    t.string   "kana_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "university_lists", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "university_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "graduation_year"
    t.index ["doctor_id"], name: "index_university_lists_on_doctor_id", using: :btree
    t.index ["university_id"], name: "index_university_lists_on_university_id", using: :btree
  end

  create_table "uploaded_files", force: :cascade do |t|
    t.text     "name"
    t.text     "url"
    t.integer  "parent_id"
    t.string   "parent_type"
    t.string   "file_info"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "users", force: :cascade do |t|
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "admin",                  default: false
    t.integer  "role"
    t.string   "type"
    t.integer  "user_id"
    t.boolean  "visible_wages",          default: true
    t.boolean  "suspended",              default: false
    t.string   "name"
    t.index ["email"], name: "index_users_on_email", unique: true, using: :btree
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
    t.index ["user_id"], name: "index_users_on_user_id", using: :btree
  end

  create_table "version_associations", force: :cascade do |t|
    t.integer "version_id"
    t.string  "foreign_key_name", null: false
    t.integer "foreign_key_id"
    t.index ["foreign_key_name", "foreign_key_id"], name: "index_version_associations_on_foreign_key", using: :btree
    t.index ["version_id"], name: "index_version_associations_on_version_id", using: :btree
  end

  create_table "versions", force: :cascade do |t|
    t.string   "item_type",      null: false
    t.integer  "item_id",        null: false
    t.string   "event",          null: false
    t.string   "whodunnit"
    t.text     "object"
    t.datetime "created_at"
    t.text     "object_changes"
    t.integer  "transaction_id"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id", using: :btree
    t.index ["transaction_id"], name: "index_versions_on_transaction_id", using: :btree
  end

  create_table "whyt_plot_apis", force: :cascade do |t|
    t.string   "name",                      null: false
    t.string   "purpose_jp"
    t.string   "purpose_en"
    t.string   "api_sql",                   null: false
    t.integer  "version",                   null: false
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.integer  "chart_type_id", default: 0, null: false
    t.text     "meta_data"
  end

  create_table "whyt_plot_table_views", force: :cascade do |t|
    t.string   "name"
    t.string   "create_view_sql"
    t.boolean  "is_view",         default: false
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
    t.text     "description"
  end

  create_table "whytlink_chatrooms", force: :cascade do |t|
    t.integer  "chatroom_type", default: 0
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
  end

  create_table "whytlink_doctor_chatrooms", force: :cascade do |t|
    t.integer  "whytlink_chatroom_id"
    t.integer  "doctor_id"
    t.integer  "last_read_message_id"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.boolean  "admin",                default: false
    t.index ["doctor_id"], name: "index_whytlink_doctor_chatrooms_on_doctor_id", using: :btree
    t.index ["whytlink_chatroom_id"], name: "index_whytlink_doctor_chatrooms_on_whytlink_chatroom_id", using: :btree
  end

  create_table "whytlink_group_chat_infos", force: :cascade do |t|
    t.string   "name"
    t.string   "avatar"
    t.integer  "affliation_type",      default: 0
    t.boolean  "opened"
    t.integer  "whytlink_chatroom_id"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "affliation_id"
    t.index ["whytlink_chatroom_id"], name: "index_whytlink_group_chat_infos_on_whytlink_chatroom_id", using: :btree
  end

  create_table "whytlink_group_chat_requests", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "whytlink_group_chat_info_id"
    t.integer  "status",                      default: 0
    t.datetime "created_at",                              null: false
    t.datetime "updated_at",                              null: false
    t.index ["doctor_id"], name: "index_whytlink_group_chat_requests_on_doctor_id", using: :btree
    t.index ["whytlink_group_chat_info_id"], name: "index_group_chat_request_on_whytlink_group_chat_info_id", using: :btree
  end

  create_table "whytlink_messages", force: :cascade do |t|
    t.integer  "doctor_id"
    t.integer  "whytlink_chatroom_id"
    t.text     "content"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "attachment",           default: ""
    t.integer  "message_type",         default: 0
    t.index ["doctor_id"], name: "index_whytlink_messages_on_doctor_id", using: :btree
    t.index ["whytlink_chatroom_id"], name: "index_whytlink_messages_on_whytlink_chatroom_id", using: :btree
  end

  create_table "whytplot_chatrooms", force: :cascade do |t|
    t.string   "name"
    t.integer  "chatroom_type"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
  end

  create_table "whytplot_export_errors", force: :cascade do |t|
    t.string   "message"
    t.string   "detail"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "whytplot_messages", force: :cascade do |t|
    t.integer  "mr_id"
    t.integer  "whytplot_chatroom_id"
    t.text     "content"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "message_type",         default: 0
    t.integer  "object_id"
    t.index ["mr_id"], name: "index_whytplot_messages_on_mr_id", using: :btree
    t.index ["whytplot_chatroom_id"], name: "index_whytplot_messages_on_whytplot_chatroom_id", using: :btree
  end

  create_table "whytplot_mr_chatrooms", force: :cascade do |t|
    t.integer  "whytplot_chatroom_id"
    t.integer  "mr_id"
    t.integer  "last_read_message_id"
    t.boolean  "favorite",             default: true
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.index ["mr_id"], name: "index_whytplot_mr_chatrooms_on_mr_id", using: :btree
    t.index ["whytplot_chatroom_id"], name: "index_whytplot_mr_chatrooms_on_whytplot_chatroom_id", using: :btree
  end

  add_foreign_key "addresses", "municipalities"
  add_foreign_key "addresses", "prefectures"
  add_foreign_key "ag_by_dpch_x_mdc_l1cs", "dpc_hospitals"
  add_foreign_key "ag_by_dpch_x_mdc_l1cs", "mdc_layer1_codes"
  add_foreign_key "ag_by_dpch_x_mdc_l2_treatments", "dpc_hospitals"
  add_foreign_key "ag_by_dpch_x_mdc_l2_treatments", "mdc_layer1s"
  add_foreign_key "ag_by_dpch_x_mdc_l2_treatments", "mdc_layer2s"
  add_foreign_key "ag_by_dpch_x_mdc_l2s", "dpc_hospitals"
  add_foreign_key "ag_by_dpch_x_mdc_l2s", "mdc_layer2s"
  add_foreign_key "ag_by_dpch_x_reason_majors", "dpc_hospitals"
  add_foreign_key "ag_by_dpch_x_reason_minors", "ag_by_dpch_x_reason_majors", column: "parent_id"
  add_foreign_key "ag_by_mdc_l1s", "mdc_layer1s"
  add_foreign_key "ag_by_mdcs", "mdc_layer3s"
  add_foreign_key "awards", "doctors"
  add_foreign_key "branches", "companies"
  add_foreign_key "branches", "mrs"
  add_foreign_key "charts", "mrs"
  add_foreign_key "comments", "doctors"
  add_foreign_key "comments", "posts"
  add_foreign_key "datatable_datafiles", "datafiles"
  add_foreign_key "department_lists", "departments"
  add_foreign_key "department_lists", "doctors"
  add_foreign_key "department_position_lists", "department_lists"
  add_foreign_key "department_position_lists", "department_positions"
  add_foreign_key "department_positions", "departments"
  add_foreign_key "doctor_feedbacks", "doctors"
  add_foreign_key "doctor_feedbacks", "emergency_request_hospitals"
  add_foreign_key "doctor_specialties", "doctors"
  add_foreign_key "doctor_specialties", "specialties"
  add_foreign_key "dpc_hospitals", "hospitals"
  add_foreign_key "dr_mr_chatrooms", "doctors"
  add_foreign_key "dr_mr_chatrooms", "mrs"
  add_foreign_key "dr_mr_messages", "doctors"
  add_foreign_key "dr_mr_messages", "dr_mr_chatrooms"
  add_foreign_key "dr_mr_messages", "mrs"
  add_foreign_key "emergency_doctor_hospitals", "doctors"
  add_foreign_key "emergency_doctor_hospitals", "hospitals"
  add_foreign_key "emergency_request_hospitals", "emergency_requests"
  add_foreign_key "emergency_request_hospitals", "hospitals"
  add_foreign_key "emergency_requests", "doctors"
  add_foreign_key "emergency_requests", "emergency_request_hospitals"
  add_foreign_key "emergency_requests", "whytlink_chatrooms"
  add_foreign_key "favorite_group_doctors", "doctors"
  add_foreign_key "favorite_group_doctors", "favorite_groups"
  add_foreign_key "favorite_group_doctors", "hospitals"
  add_foreign_key "favorite_groups", "doctors"
  add_foreign_key "hospitals", "areas"
  add_foreign_key "hospitals", "medical_groups"
  add_foreign_key "hospitals", "prefectures"
  add_foreign_key "icd10s", "icd10s"
  add_foreign_key "mail_logs", "all_mails"
  add_foreign_key "mail_logs", "doctors"
  add_foreign_key "mdc_layer1s", "icd10s"
  add_foreign_key "mdc_layer1s", "mdc_layer1_codes"
  add_foreign_key "mdc_layer2s", "mdc_layer1s"
  add_foreign_key "mdc_layer3s", "mdc_layer2s"
  add_foreign_key "medical_municipalities", "medical_regions", column: "medical_region_code", primary_key: "code"
  add_foreign_key "medical_municipalities", "municipalities"
  add_foreign_key "medical_regions", "areas"
  add_foreign_key "medical_regions", "prefectures"
  add_foreign_key "monographs", "doctors"
  add_foreign_key "mr_projects", "mrs"
  add_foreign_key "mr_projects", "projects"
  add_foreign_key "mr_sessions", "mrs"
  add_foreign_key "mrs", "branches"
  add_foreign_key "mrs", "companies"
  add_foreign_key "number_by_codes", "ag_by_mdcs"
  add_foreign_key "populations", "municipalities"
  add_foreign_key "populations", "prefectures"
  add_foreign_key "posts", "doctors"
  add_foreign_key "posts", "suggestions"
  add_foreign_key "pre_monographs", "doctors"
  add_foreign_key "pre_monographs_moderated_scraping_histories", "doctors"
  add_foreign_key "pre_monographs_scraping_histories", "doctors"
  add_foreign_key "prefectures", "areas"
  add_foreign_key "projects", "companies"
  add_foreign_key "projects", "mrs"
  add_foreign_key "recommends", "doctors"
  add_foreign_key "recommends", "skills"
  add_foreign_key "regimen_pharmacy_rankings", "ag_by_mdc_l1s"
  add_foreign_key "regimen_regimen_rankings", "ag_by_mdc_l1s"
  add_foreign_key "scenario_charts", "charts"
  add_foreign_key "scenario_charts", "scenarios"
  add_foreign_key "scenarios", "mrs"
  add_foreign_key "skills", "doctors"
  add_foreign_key "skills", "icd10s"
  add_foreign_key "society_lists", "doctors"
  add_foreign_key "society_lists", "societies"
  add_foreign_key "society_position_lists", "society_lists"
  add_foreign_key "society_position_lists", "society_positions"
  add_foreign_key "society_positions", "societies"
  add_foreign_key "staff_logs", "users"
  add_foreign_key "temporary_doctors", "doctors"
  add_foreign_key "university_lists", "doctors"
  add_foreign_key "university_lists", "universities"
  add_foreign_key "whytlink_doctor_chatrooms", "doctors"
  add_foreign_key "whytlink_doctor_chatrooms", "whytlink_chatrooms"
  add_foreign_key "whytlink_group_chat_infos", "whytlink_chatrooms"
  add_foreign_key "whytlink_group_chat_requests", "doctors"
  add_foreign_key "whytlink_group_chat_requests", "whytlink_group_chat_infos"
  add_foreign_key "whytlink_messages", "doctors"
  add_foreign_key "whytlink_messages", "whytlink_chatrooms"
  add_foreign_key "whytplot_messages", "mrs"
  add_foreign_key "whytplot_messages", "whytplot_chatrooms"
  add_foreign_key "whytplot_mr_chatrooms", "mrs"
  add_foreign_key "whytplot_mr_chatrooms", "whytplot_chatrooms"
end
