require 'csv'
require 'date'

##########################################
# ログメソッド
##########################################
def log(str)
  $stdout.print str + "\n"
end

def logr(str)
  $stdout.print str + "\r"
end

##########################################
# 和暦パースメソッド
##########################################
def parse_jcstr(str)
  str_date = str.encode!("UTF-8").gsub(/\s/, "").gsub(/明治|明/, "M").gsub(/大正|大/, "T").gsub(/昭和|昭/, "S").gsub(/平成|平/, "H").gsub(/元年|元/, "1").gsub("年", ".").gsub("月", ".").gsub("日", "").gsub("/", ".")
end

def parse_jc(str)
  str_date = parse_jcstr(str)
  Date.parse(str_date)
end

##########################################
# 病院データオブジェクト
##########################################
class Hospital
  attr_accessor :source_filename
  attr_accessor :code_region
  attr_accessor :code_number
  attr_accessor :code_checkdigit
  attr_accessor :name
  attr_accessor :postal_code
  attr_accessor :address
  attr_accessor :phone
  attr_accessor :number_of_fulltime_medicine
  attr_accessor :number_of_fulltime_dentistry
  attr_accessor :number_of_fulltime_pharmacy
  attr_accessor :number_of_parttime_medicine
  attr_accessor :number_of_parttime_dentistry
  attr_accessor :number_of_parttime_pharmacy
  attr_accessor :establisher_org
  attr_accessor :establisher_first_name
  attr_accessor :establisher_last_name
  attr_accessor :director_first_name
  attr_accessor :director_last_name
  attr_accessor :designation_date
  attr_accessor :designation_reason
  attr_accessor :designation_start
  attr_accessor :beds_hash
  attr_accessor :departments_array
  attr_accessor :state
  attr_accessor :prefecture_id

  def initialize(filename)
    self.source_filename = filename
    self.departments_array = Array.new
    self.beds_hash        = Array.new
  end

  def prefecture
    pref = ""
    case postal_code
    when /^0(0|[4-9])/
      pref = '北海道'
    when /^01/
      pref = '秋田県'
    when /^02/
      pref = '岩手県'
    when /^03/
      pref = '青森県'
    when /^1/
      pref = '東京都'
    when /^20/
      pref = '東京都'
    when /^2[1-5]/
      pref = '神奈川県'
    when /^2[6-9]/
      pref = '千葉県'
    when /^3[0-1]/
      pref = '茨城県'
    when /^32/
      pref = '栃木県'
    when /^3[3-6]/
      pref = '埼玉県'
    when /^37/
      pref = '群馬県'
    when /^3[8-9]/
      pref = '長野県'
    when /^40/
      pref = '山梨県'
    when /^4[1-3]/
      pref = '静岡県'
    when /^4[4-9]/
      pref = '愛知県'
    when /^50/
      pref = '岐阜県'
    when /^51/
      pref = '三重県'
    when /^52/
      pref = '滋賀県'
    when /^5[3-9]/
      pref = '大阪府'
    when /^6[0-2]/
      pref = '京都府'
    when /^63/
      pref = '奈良県'
    when /^64/
      pref = '和歌山県'
    when /^6[5-7]/
      pref = '兵庫県'
    when /^68/
      pref = '鳥取県'
    when /^69/
      pref = '島根県'
    when /^7[0-1]/
      pref = '岡山県'
    when /^7[2-3]/
      pref = '広島県'
    when /^7[4-5]/
      pref = '山口県'
    when /^76/
      pref = '香川県'
    when /^77/
      pref = '徳島県'
    when /^78/
      pref = '高知県'
    when /^79/
      pref = '愛媛県'
    when /^8[0-3]/
      pref = '福岡県'
    when /^84/
      pref = '佐賀県'
    when /^85/
      pref = '長崎県'
    when /^86/
      pref = '熊本県'
    when /^87/
      pref = '大分県'
    when /^88/
      pref = '宮崎県'
    when /^89/
      pref = '鹿児島県'
    when /^90/
      pref = '沖縄県'
    when /^91/
      pref = '福井県'
    when /^92/
      pref = '石川県'
    when /^93/
      pref = '富山県'
    when /^9[4-5]/
      pref = '新潟県'
    when /^9[6-7]/
      pref = '福島県'
    when /^98/
      pref = '宮城県'
    when /^99/
      pref = '山形県'
    else
      pref = ''
    end
    return pref
  end

  #UTFに変換または 空文字
  def format(str)
  	str ? str.encode!("UTF-8").strip : ""
  end

  #UTFに変換 & 配列切り出し
  def formatAndSplit(str)
    str ? str.encode!("UTF-8").split(/　|\s/).compact.delete_if(&:empty?) : []
  end

  #日付を和暦から西暦フォーマットに変換
  def formatDate(str)
    begin
      parse_jc(str)
    rescue
      log "Warning:DateParseError:" + self.source_filename + ":" + self.name + ":" + format(str)
    end
  end

  #病院コードをパース
  def parseCode(cell)
  	return if cell.nil?
  	ids = cell.split(/\D/)
  	self.code_region     = ids[0].strip if ids.count > 0
  	self.code_number     = ids[1].strip if ids.count > 1
  	self.code_checkdigit = ids[2].strip if ids.count > 2
  end

  #病院名をパース
  def parseName(cell)
  	self.name = format(cell)
  end

  #住所をパース
  def parseAddress(cell)
  	if /([\d]{3})-([\d]{4})(.+)/ =~ cell
  	  self.postal_code = $1 + $2
  	  self.address     = format($3)
  	end  	
  end

  #電話番号をパース
  def parsePhone(cell)
  	self.phone = format(cell).gsub(/\(|\)|\-/, "")
  end

  #常勤/非常勤数のカラムをパース
  def parseWorkersNumber(cell)
    item = format(cell)
    case item
    when /非常勤\D*\s*(\d)/
      @full = false
    when /常\s*勤\D*\s*(\d)/
      @full = true
    when /医\D*\s*(\d)/
      if @full
        self.number_of_fulltime_medicine = $1
      else
        self.number_of_parttime_medicine = $1
      end
    when /歯\D*\s*(\d)/
      if @full
        self.number_of_fulltime_dentistry = $1
      else
        self.number_of_parttime_dentistry = $1
      end
    when /薬\D*\s*(\d)/      
      if @full
        self.number_of_fulltime_pharmacy = $1
      else
        self.number_of_parttime_pharmacy = $1
      end
    end      
  end

  #開設者カラムをパース
  def parseEstablisher(cell)
    establisher = formatAndSplit(cell)
    self.establisher_first_name = establisher.pop  if establisher.count > 0
    self.establisher_last_name  = establisher.pop  if establisher.count > 0
    self.establisher_org        = establisher.join if establisher.count > 0
  end

  #院長カラムをパース
  def parseDirector(cell)
    director = formatAndSplit(cell)
    self.director_first_name = director.pop  if director.count > 0
    self.director_last_name  = director.pop  if director.count > 0
  end

  #指定年月日をパース
  def parseDesignationDate(cell)
    self.designation_date = formatDate(cell)
  end

  #登録理由をパース
  def parseDesignationReason(cell)
     self.designation_reason = format(cell)
  end

  #指定年月日開始をパース
  def parseDesignationStart(cell)
    self.designation_start = formatDate(cell)
  end

  #診療科名・病床数をパース
  def parseDepartments(cell)
    items = formatAndSplit(cell)
    if items.length > 1 && /\d+/ =~ items[1]
      #self.departments.push(items[0])
      self.beds_hash.push(items)
    else
      self.departments_array.concat(items)
    end
  end

  def parseDepartments2Col(cell1, cell2)
  end

  #備考をパース
  def parseState(cell)
  	self.state = format(cell)
  end  
end #Hospital


#■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■
#□■□■□■□■□■□■  Entry Point  □■□■□■□■□■□■□■
#■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■□■
CSV_PATH = '/csv/hospitals'


##########################################
# コマンドパラメータ
##########################################
seeds_filename = ARGV.shift
version_year   = ARGV.shift

if seeds_filename.nil? || version_year.nil?
  log "Please Input Seeds File Name And Version Year"
  log "Usage  ) >ruby " + File.basename(__FILE__) + " [seed file] [version year]"
  log "Example) >ruby " + File.basename(__FILE__) + " seeds.rb 2015"
  exit
end

##########################################
# CSV読み込み & パース & データオブジェクト作成
##########################################
csv_path      = File.expand_path(File.dirname(__FILE__)) + CSV_PATH
csv_filenames = Dir::entries(csv_path).select {|item| /\.csv$/ =~ item }
hospitals        = Array.new
names            = Array.new
name_and_postals = Array.new
hospital  = nil
index     = nil
type      = 1

log "#######################"
log "Reading CSV Files...."
log "-----------------------"
csv_filenames.each do | csv_filename |
  log csv_filename
  CSV.foreach(csv_path + "/" + csv_filename, row_sep: "\r\n", encoding: "SJIS") do |row|
    #A列に項番があればデータの開始行とみなす
  	if /^[0-9]+$/ =~ row[0]
  	  unless hospital.nil?
  	  	hospitals        << hospital
        names            << hospital.name.to_s
        name_and_postals << hospital.name.to_s + hospital.postal_code.to_s
  	  end
  	  hospital = Hospital.new(csv_filename)
	    index    = 0
  	  fulltime = true
  	end

    #項番が見つかるまでは読み飛ばす
    next if index.nil?

    #B列 
  	if index == 0
  	  hospital.parseCode(row[1])
  	end
  
  	#C列
  	if index == 0  	
  	  hospital.parseName(row[2])
  	end
  
  	#D列
  	if index == 0  
	  hospital.parseAddress(row[3])
  	end
  
    #E列 可変
    if index == 0
      hospital.parsePhone(row[4])
    else
      hospital.parseWorkersNumber(row[4])
    end
  
  	#F列
  	if index == 0  
  	  hospital.parseEstablisher(row[5])
    end
  
    #G列
  	if index == 0    
      hospital.parseDirector(row[6])
    end
  
    #H列以降のフォーマットが2通りある
    #H列が日付であればTYPE1、でなければTYPE2と呼称する
    if index == 0        
      str_date = parse_jcstr(row[7])
      if /\w\d+\.\d+\.\d+/ =~ str_date
        type = 1
      else
      	type = 2
      end
    end

    if type == 1
      #H列 3行固定
      case index
      when 0 then hospital.parseDesignationDate(row[7])
      when 1 then hospital.parseDesignationReason(row[7])         
      when 2 then hospital.parseDesignationStart(row[7])
      end
      
      #I列
      if index >= 0
        hospital.parseDepartments(row[8])
      end
  
      #J列
      if index == 0
        hospital.parseState(row[9])
      end
    else
      #2015.4.20現在 TYPE2の場合はH,I,J列は読み込まない
=begin
      #H,I列
      if index >= 0
        hospital.parseDepartments2Col(row[7], row[8])
      end
  
      #J列 3行固定
      case index
      when 0 then hospital.parseDesignationDate(row[9])
      when 1 then hospital.parseDesignationReason(row[9])         
      when 2 then hospital.parseDesignationStart(row[9])
      end      
=end
    end
  	index += 1
  end #CSV
end #File

##########################################
#            seedファイル出力
##########################################

log "#######################"
log "Writing Seeds File...."
log "-----------------------"

seeds_file = open(seeds_filename, "w")

#前処理
seeds_file.write("ActiveRecord::Base.connection.reset_pk_sequence!('hospitals')\n")
seeds_file.write("prefectures = Prefecture.all\n")
seeds_file.write("prefecturesHash = Hash.new\n")
seeds_file.write("prefectures.each do |prefecture|\n")
seeds_file.write("  prefecturesHash[prefecture.name] = prefecture.id\n")
seeds_file.write("end\n")
seeds_file.write("Hospital.all.each do |h|\n")
seeds_file.write("  h.name = h.name.gsub(/　|\\\\s|/, '')\n")
seeds_file.write("  h.save(:validate => false)\n")
seeds_file.write("end\n")
seeds_file.write("names            = Hospital.all.map {|h| h.name.to_s }\n")
seeds_file.write("name_and_postals = Hospital.all.map {|h| (h.name.to_s + h.postal_code.to_s)}\n")
seeds_file.write("\n")

#病院追加/上書きシーケンス
hospitals.each_with_index do |h, i|
  name = h.name.to_s.gsub(/　|\\s|/, '')
  line  = "print \"#### %s/%s\\r\"\n" % [i, hospitals.length]
  line += "h = Hospital.where(postal_code: '%s', name: '%s').first\n" % [h.postal_code.to_s, name]
  line += "h = Hospital.new if h.nil?\n"
  line += "h.version_year = %s\n" % version_year
  line += "h.code_region = '%s'\n" % h.code_region if h.code_region
  line += "h.code_number = '%s'\n" % h.code_number if h.code_number
  line += "h.code_checkdigit = '%s'\n" % h.code_checkdigit if h.code_checkdigit
  line += "h.name = '%s'\n" % name
  line += "h.postal_code = '%s'\n" % h.postal_code if h.postal_code
  line += "h.address = '%s'\n" % h.address if h.address
  line += "h.phone = '%s'\n" % h.phone if h.phone
  line += "h.number_of_fulltime_medicine = %s\n" % h.number_of_fulltime_medicine if h.number_of_fulltime_medicine
  line += "h.number_of_fulltime_dentistry = %s\n" % h.number_of_fulltime_dentistry if h.number_of_fulltime_dentistry
  line += "h.number_of_fulltime_pharmacy = %s\n" % h.number_of_fulltime_pharmacy if h.number_of_fulltime_pharmacy
  line += "h.number_of_parttime_medicine = %s\n" % h.number_of_parttime_medicine if h.number_of_parttime_medicine
  line += "h.number_of_parttime_dentistry = %s\n" % h.number_of_parttime_dentistry if h.number_of_parttime_dentistry
  line += "h.number_of_parttime_pharmacy = %s\n" % h.number_of_parttime_pharmacy if h.number_of_parttime_pharmacy
  line += "h.establisher_org = '%s'\n" % h.establisher_org if h.establisher_org
  line += "h.establisher_first_name = '%s'\n" % h.establisher_first_name if h.establisher_first_name
  line += "h.establisher_last_name = '%s'\n" % h.establisher_last_name if h.establisher_last_name
  line += "h.director_first_name = '%s'\n" % h.director_first_name if h.director_first_name
  line += "h.director_last_name = '%s'\n" % h.director_last_name if h.director_last_name
  line += "h.designation_date = '%s'\n" % h.designation_date if h.designation_date
  line += "h.designation_reason = '%s'\n" % h.designation_reason if h.designation_reason && !h.designation_reason.empty?
  line += "h.designation_start = '%s'\n" % h.designation_start if h.designation_start
  line += "h.beds_hash = %s\n" % h.beds_hash.to_s if h.beds_hash.length > 0
  line += "h.departments_array = %s\n" % h.departments_array.to_s if h.departments_array.length > 0
  line += "h.state = '%s'\n" % h.state if h.state
  line += "h.prefecture_id = prefecturesHash['%s']\n" % h.prefecture
  line += "h.save(:validate => false)\n"
  line += "name_and_postals.delete('%s')\n" % (name + h.postal_code.to_s)
  line += "\n"
  seeds_file.write(line)
  logr "Hospital Num:%d/%d" % [i+1, hospitals.length]
end
log "Hospital Num:%d/%d" % [hospitals.length, hospitals.length]

#後処理
seeds_file.write("puts '#### %s/%s'\n" % [hospitals.length, hospitals.length])
seeds_file.write("if name_and_postals.length > 0\n")
seeds_file.write("  puts 'Warning: Incongruous Hospitals:'\n")
seeds_file.write("  name_and_postals.each do |name|\n")
seeds_file.write("    puts name\n")
seeds_file.write("  end\n")
seeds_file.write("end\n")

seeds_file.close