require 'csv'

# インポートファイルを読み込む
def import_read( file_name )
  lines = CSV.read("db/csv/#{file_name}", encoding: 'SJIS:UTF-8')
  lines.each_with_index do |row, index|
    next unless row.length > 0

    yield(row, index) if block_given?
  end
end

# CSVファイルの取込
import_read('KEN_ALL_20160426.CSV') do |row, index|

  prefecture = Prefecture.find_by(name: row[6])
  prefecture_id = prefecture.id

  case row[7]
  when /郡/ then
    city1 = $` + $&
    city2 = $'
  else
    city1 = row[7]
  end

  Address.seed do |s|
    s.id                    = index
    s.city                  = row[7]
    s.city_kana             = row[4]
    s.town                  = row[8]
    s.town_kana             = row[5]
    s.postal_code           = row[2]
    s.local_government_code = row[0]
    s.city1                 = city1
    s.city2                 = city2
    s.full_address          = row[7] + row[8]
    s.full_address_kana     = row[4] + row[5]
    s.prefecture_id         = prefecture_id
  end
end
