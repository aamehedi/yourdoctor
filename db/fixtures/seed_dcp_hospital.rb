require 'roo'
require 'roo-xls'

######################################
#エクセルファイルを開く
######################################
def open_book(filename)
  case File.extname(filename)
  when '.xlsx', '.ods', '.csv' then
    Roo::Excelx.new(filename)
  when '.xls' then
    Roo::Excel.new(filename)
  else
    nil
  end
end

book          = open_book('db/csv/施設概要表.xls')
rw_info_sheet = book.sheet('rw_info') 

if rw_info_sheet.last_row < 1
  p "設定がありません"
  return 
end

info_row      = rw_info_sheet.row(2)

file_type     = info_row[0]
start_row     = info_row[1].to_i
start_col     = info_row[2].to_cn0
sheet_name    = info_row[3]
col01         = info_row[4].to_cn0   #告示番号
col02         = info_row[5].to_cn0   #通番
col03         = info_row[6].to_cn0   #施設名
col04         = info_row[7].to_cn0   #病院類型
col05         = info_row[8].to_cn0   #DPC算定病床数
col06         = info_row[9].to_cn0   #DPC算定病床の入院基本料
col07         = info_row[10].to_cn0  #DPC算定病床割合
col08         = info_row[11].to_cn0  #精神病床数
col09         = info_row[12].to_cn0  #療養病床数
col10         = info_row[13].to_cn0  #結核病床数 
col11         = info_row[14].to_cn0  #病床総数
col12         = info_row[15].to_cn0  #提出月数


data_sheet = book.sheet(sheet_name) 

p "START"
start_row.upto(data_sheet.last_row) do |index|
  row = data_sheet.row(index)
  #告示番号のある行だけを有効行とみなす
  if row[col01] =~ /\d{5,7}/
    p index
    notice_number                      = row[col01].to_i
    serial_number                      = row[col02].to_i
    name                               = row[col03]
    dcp_type                           = row[col04]
    dcp_calc_bed_num                   = row[col05].to_i
    dcp_calc_hospitalization_basic_fee = row[col06].tr("０-９", "0-9")
    dcp_calc_bed_rate                  = row[col07] #データチェック用
    dcp_mental_hospital_bed_num        = row[col08].to_i
    dcp_recuperation_hospital_bed_num  = row[col09].to_i
    dcp_tuberculosis_hospital_bed_num  = row[col10].to_i
    dcp_total_bed_num                  = row[col11].to_i
    submitted_months_num               = row[col12].to_i

    dpch = DpcHospital.new
    dpch.notice_number                      = notice_number
    dpch.name                               = name
    dpch.dcp_type                           = dcp_type
    dpch.dcp_calc_bed_num                   = dcp_calc_bed_num
    if dcp_calc_hospitalization_basic_fee =~ /(.+)(\d)(.+)(\d)/
      dpch.dcp_calc_hospitalization_basic_fee_type = $1
      dpch.dcp_calc_hospitalization_basic_fee_lratio = $2
      dpch.dcp_calc_hospitalization_basic_fee_rratio = $4
    end

    #dpch.dcp_calc_bed_rate                  = dcp_calc_bed_rate
    dpch.dcp_mental_hospital_bed_num        = dcp_mental_hospital_bed_num
    dpch.dcp_recuperation_hospital_bed_num  = dcp_recuperation_hospital_bed_num
    dpch.dcp_tuberculosis_hospital_bed_num  = dcp_tuberculosis_hospital_bed_num
    dpch.dcp_total_bed_num                  = dcp_total_bed_num
    dpch.submitted_months_num               = submitted_months_num
    p dpch.name
    dpch.save
  end
end

