ChartType.seed_once(:id,
  { :id => 1, :name => 'bubble', :description => 'バブルチャート' },
  { :id => 2, :name => 'line', :description => '折れ線' },
  { :id => 3, :name => 'scatter', :description => '散布' },
  { :id => 4, :name => 'radar', :description => 'レーダー' },
  { :id => 5, :name => 'mix_stack_bar_line', :description => '複数のグラフ' },
  { :id => 6, :name => 'mix_scatter_line', :description => '複数' },
  { :id => 7, :name => 'bar', :description => 'バーチャート' },
  { :id => 8, :name => 'group', :description => 'グループバーチャート' },
  { :id => 9, :name => 'stack', :description => '棒グラフ' }
)
