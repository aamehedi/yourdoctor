require 'roo'
require 'roo-xls'

########################################################
#MDCマスタ作成用seedflie
########################################################

START_COLUMN = 1
START_LINE = 4 #Index columnをはじく

SHEET_NUM = 2
FILE_NAME = "./db/csv/（６）診断群分類毎の集計.xls"
CURRENT_YEAR = 2015

######################################
#エクセルファイルを開く
######################################
def open_spreadsheet(file)

   case File.extname(file)
     when '.xlsx' then
       Roo::Spreadsheet.open(file.path, extension: :xlsx)
     when '.ods' then
       Roo::Spreadsheet.open(file.path, extension: :ods)
     when '.csv' then
       Roo::Spreadsheet.open(file.path, extension: :csv)
     when '.xls' then
       Roo::Excel.new(file)
     else
       return false
   end
end

####################################
# number_by_code(icd10)テーブル更新
####################################
def insert_icd10_costly(ag_by_mdc, book, line, col, index)
  nbc        = ag_by_mdc.number_by_icd10_costlies.build()
  #icd10との紐付け
  icd10_code = book.cell(line, col)
puts icd10_code
  icd10s = Icd10.where(:code => icd10_code).where(:depth => 4)
  if icd10s.count > 0
    nbc.codeable = icd10s[0]
  end
  if book.cell(line, col+1).is_a then
    nbc.number = book.cell(line, col+1)
  else
    puts "値が数値以外です。"
  end
  nbc.index  = index
  if ag_by_mdc.save! then
    puts "saved"
  end
end

####################################
# エクセルを読み込みテーブル更新
####################################
def excel_read
  #puts "Total:" + base_data.length.to_s
  book  = open_spreadsheet(FILE_NAME)
  errorCount = 0
  if book
    #今は最初のシートで
    book.default_sheet = book.sheets[SHEET_NUM]
    h = Hash.new([])
    START_LINE.upto(book.last_row) do |line|
      #医療資源を最も投入した傷病ICD10
      #number = book.cell(line, "BF".to_cn) #ICD10
      #condition_type = ""
      #codeable = book.cell(line, "BE".to_cn)#belongs_to	icd10 or kcode
      #aggregate_by_mdc = "???"	#belongs_to
#NumberByCode.create(number: number, type: condition_type, costly_comorbidity: costly_comorbidity, index: index, codeable: codeable, aggregate_by_mdc: aggregate_by_mdc)

      #puts book.cell(line, 1)
      #NumberByCode.create()
      #当該MDCに含まれるDPCの数
      code	=	book.cell(line, "A".to_cn)  #	string	14桁のコード e.g.)010010xx99000x

      #	boolean	包括
      if book.cell(line, "C".to_cn).to_s != "" then
        inclusion	=	true
      else
        inclusion = false
      end
      dpc_num_for_the_mdc	=	book.cell(line, "F".to_cn).to_i  #	integer	当該MDCに含まれるDPCの数
      cases_num_for_the_mdc	=	book.cell(line, "G".to_cn)  #	integer	当該MDCの症例数
      number	=	book.cell(line, "H".to_cn).to_i  #	integer	件数
      dpc_ratio_for_all_cases	=	book.cell(line, "J".to_cn)  #	double	全症例に対する当該DPCの比率
      male_num	=	book.cell(line, "K".to_cn)  #	integer	男性の件数
      female_num	=	book.cell(line, "M".to_cn)  #	integer	女性の件数
      from0to2_num	=	book.cell(line, "O".to_cn)  #	integer	＜年齢＞0~2歳の件数
      from3to5_num	=	book.cell(line, "Q".to_cn)  #	integer	3~5歳の件数
      from6to15_num	=	book.cell(line, "S".to_cn)  #	integer	6~15歳の件数
      from16to20_num	=	book.cell(line, "U".to_cn)  #	integer	16~20歳の件数
      from21to40_num	=	book.cell(line, "W".to_cn)  #	integer	21~40歳の件数
      from41to60_num	=	book.cell(line, "Y".to_cn)  #	integer	41~60歳の件数
      from61to79_num	=	book.cell(line, "AA".to_cn)  #	integer	61~79歳の件数
      over80_num	=	book.cell(line, "AC".to_cn)  #	integer	80歳以上の件数
      introduction_num	=	book.cell(line, "AE".to_cn)  #	integer	＜経路＞他院よりの紹介
      outpatient_num	=	book.cell(line, "AG".to_cn)  #	integer	自院の外来からの入院
      transported_num	=	book.cell(line, "AI".to_cn)  #	integer	救急車による搬送
      unexpected_hospitalization_num	=	book.cell(line, "AK".to_cn)  #	integer	予定外入院
      emergency_hospitalization_num	=	book.cell(line, "AM".to_cn)  #	integer	救急医療入院
      status_healed	=	book.cell(line, "AO".to_cn)  #	integer	＜退院時転帰＞治癒
      status_improvement	=	book.cell(line, "AP".to_cn)  #	integer	軽快
      status_palliation	=	book.cell(line, "AQ".to_cn)  #	integer	寛解
      status_unchanging	=	book.cell(line, "AR".to_cn)  #	integer	不変
      status_worse	=	book.cell(line, "AS".to_cn)  #	integer	増悪
      status_death_of_illness_costed_most	=	book.cell(line, "AT".to_cn)  #	integer	死亡(医療資源病名）
      status_death_of_other_illness_costed_most	=	book.cell(line, "AU".to_cn)  #	integer	死亡(医療資源病名以外）
      status_other	=	book.cell(line, "AV".to_cn)  #	integer	その他
      hospital_days_average	=	book.cell(line, "AW".to_cn)  #	integer	＜在院日数＞平均値
      hospital_days_minimum	=	book.cell(line, "AX".to_cn)  #	integer	最小値
      hospital_days_maximum	=	book.cell(line, "AY".to_cn)  #	integer	最大値
      hospital_days_coefficient_of_variation	=	book.cell(line, "AZ".to_cn)  #	integer	変動係数
      hospital_days_value_of_25percentile	=	book.cell(line, "BA".to_cn)  #	integer	25パーセンタイル値
      hospital_days_value_of_50percentile	=	book.cell(line, "BB".to_cn)  #	integer	50パーセンタイル値
      hospital_days_value_of_75percentile	=	book.cell(line, "BC".to_cn)  #	integer	75パーセンタイル値
      hospital_days_value_of_90percentile	=	book.cell(line, "BD".to_cn)  #	integer	90パーセンタイル値
      artificial_respiration	=	book.cell(line, "EQ".to_cn)  #	integer	人工呼吸
      artificial_kidney	=	book.cell(line, "ES".to_cn)  #	integer	人工腎臓
      central_venous_injection	=	book.cell(line, "EU".to_cn)  #	integer	中心静脈注射
      blood_transfusion	=	book.cell(line, "EW".to_cn)  #	integer	輸血


      c	=	book.cell(line, "A".to_cn)
      mdc_layer1_code	=	book.cell(line, "D".to_cn)  #	belongs_to MDC
      #mdc3に従属 010010xx9901xx
      c1 = code[10]
      c2 = code[11]
      c3 = code[12]
      c4 = code[13]
      mdc_c1 = code[0,2]
      mdc_c2 = code[8,2]
      #mdc1cd = MdcLayer1Code.where(code:mdc_c1)
      #MdcLayer2.includes{:mdc_layer3s}.joins{:mdc_layer3s}

      #mdc2 = MdcLayer2.where(mdc_layer1:mdc_c1).where(code:mdc_c2)
      #mdc3 = mdc2[0].mdc_layer3s.build

      #mdc1 = mdc1cd[0].mdc_layer1s[0]#where(code_1:c1)
      #mdc3 = MdcLayer3.joins(:mdc_layer2 => {:mdc_layer1 => {:mdc_layer1_code}}).where(code_1:c1).where(code_2:c2).where(code_3:c3).where(code_4:c4).where(mdc_layer2:mdc_c2).where("mdc_layer1s.code = " + code1 )
      #mdc3 = MdcLayer3.where(code_1:c1).where(code_2:c2).where(code_3:c3).where(code_4:c4).where(mdc_layer2:mdc_c2).where("mdc_layer1_codes.code = " + mdc_layer1_code )
      #mdc_layer1_code.yearが絞れない？？

      #ag_by_mdc = mdc3.aggregate_by_mdc.build
      #mdc3に従属
      #toriaezu;...
      mdc3 = MdcLayer3.where(code_1:c1).where(code_2:c2).where(code_3:c3).where(code_4:c4).where(mdc_layer2:mdc_c2)
      ag_by_mdc = AgByMdc.new
      ag_by_mdc.mdc_layer3 = mdc3[0]
      ag_by_mdc.attributes = {code: code}
      ag_by_mdc.attributes = {inclusion:	inclusion}
      ag_by_mdc.attributes = {dpc_num_for_the_mdc:	dpc_num_for_the_mdc	}
      ag_by_mdc.attributes = {cases_num_for_the_mdc:	cases_num_for_the_mdc	}
      ag_by_mdc.attributes = {number:	number	}
      ag_by_mdc.attributes = {dpc_ratio_for_all_cases:	dpc_ratio_for_all_cases	}
      ag_by_mdc.attributes = {male_num:	male_num	}
      ag_by_mdc.attributes = {female_num:	female_num	}
      ag_by_mdc.attributes = {from0to2_num:	from0to2_num	}
      ag_by_mdc.attributes = {from3to5_num:	from3to5_num	}
      ag_by_mdc.attributes = {from6to15_num:	from6to15_num	}
      ag_by_mdc.attributes = {from16to20_num:	from16to20_num	}
      ag_by_mdc.attributes = {from21to40_num:	from21to40_num	}
      ag_by_mdc.attributes = {from41to60_num:	from41to60_num	}
      ag_by_mdc.attributes = {from61to79_num:	from61to79_num	}
      ag_by_mdc.attributes = {over80_num:	over80_num	}
      ag_by_mdc.attributes = {introduction_num:	introduction_num	}
      ag_by_mdc.attributes = {outpatient_num:	outpatient_num	}
      ag_by_mdc.attributes = {transported_num:	transported_num	}
      ag_by_mdc.attributes = {unexpected_hospitalization_num:	unexpected_hospitalization_num	}
      ag_by_mdc.attributes = {emergency_hospitalization_num:	emergency_hospitalization_num	}
      ag_by_mdc.attributes = {status_healed:	status_healed	}
      ag_by_mdc.attributes = {status_improvement:	status_improvement	}
      ag_by_mdc.attributes = {status_palliation:	status_palliation	}
      ag_by_mdc.attributes = {status_unchanging:	status_unchanging	}
      ag_by_mdc.attributes = {status_worse:	status_worse	}
      ag_by_mdc.attributes = {status_death_of_illness_costed_most:	status_death_of_illness_costed_most	}
      ag_by_mdc.attributes = {status_death_of_other_illness_costed_most:	status_death_of_other_illness_costed_most	}
      ag_by_mdc.attributes = {status_other:	status_other	}
      ag_by_mdc.attributes = {hospital_days_average:	hospital_days_average	}
      ag_by_mdc.attributes = {hospital_days_minimum:	hospital_days_minimum	}
      ag_by_mdc.attributes = {hospital_days_maximum:	hospital_days_maximum	}
      ag_by_mdc.attributes = {hospital_days_coefficient_of_variation:	hospital_days_coefficient_of_variation	}
      ag_by_mdc.attributes = {hospital_days_value_of_25percentile:	hospital_days_value_of_25percentile	}
      ag_by_mdc.attributes = {hospital_days_value_of_50percentile:	hospital_days_value_of_50percentile	}
      ag_by_mdc.attributes = {hospital_days_value_of_75percentile:	hospital_days_value_of_75percentile	}
      ag_by_mdc.attributes = {hospital_days_value_of_90percentile:	hospital_days_value_of_90percentile	}
      ag_by_mdc.attributes = {artificial_respiration:	artificial_respiration	}
      ag_by_mdc.attributes = {artificial_kidney:	artificial_kidney	}
      ag_by_mdc.attributes = {central_venous_injection:	central_venous_injection	}
      ag_by_mdc.attributes = {blood_transfusion:	blood_transfusion }

      nbc        = ag_by_mdc.number_by_icd10_costlies.build
      #icd10との紐付け
      icd10_code = book.cell(line, "BE".to_cn)
      #puts book.cell(line, "BE".to_cn)#C793
      icd10s = Icd10.where(:code => icd10_code).where(:replace_code => nil)
      if icd10s.count > 0
        nbc.codeable = icd10s[0]
      end
      nbc.number = book.cell(line, "BF".to_cn)
      nbc.index  = 1
      if ag_by_mdc.save!  then
        puts "saved!!!"
        puts ag_by_mdc.id
        puts ag_by_mdc.code
        puts ag_by_mdc.inclusion
        puts ag_by_mdc.dpc_num_for_the_mdc
      end
exit

=begin
=end
    #医療資源を最も投入した傷病ICD10
      col = "BE".to_cn
      insert_icd10_costly(ag_by_mdc, book, line, col, 1)
      insert_icd10_costly(ag_by_mdc, book, line, col+=3, 2)
      insert_icd10_costly(ag_by_mdc, book, line, col+=3, 3)
      insert_icd10_costly(ag_by_mdc, book, line, col+=3, 4)
      insert_icd10_costly(ag_by_mdc, book, line, col+=3, 5)
      insert_icd10_costly(ag_by_mdc, book, line, col+=3, 6)
      insert_icd10_costly(ag_by_mdc, book, line, col+=3, 7)
      insert_icd10_costly(ag_by_mdc, book, line, col+=3, 8)
      insert_icd10_costly(ag_by_mdc, book, line, col+=3, 9)
      insert_icd10_costly(ag_by_mdc, book, line, col+=3, 10)

      #入院時併存症及び入院後発症疾患ICD10
      col = "CI".to_cn
      #insert_icd10_comorbidity(ag_by_mdc, book, line, col, 1)




      #kcode
      col = "DM".to_cn
  exit


    end
  end
end
puts "Start"
excel_read
