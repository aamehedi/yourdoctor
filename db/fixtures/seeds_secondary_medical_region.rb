require 'csv'
require 'roo'
require 'roo-xls'

COLUMN_NUM = 10
LINE_NUM = 2
SHEET_NUM = 0

medical_region_list = []
# TODO: S3のbucket指定
Dir.glob('db/csv/*').each do |file|
  # Excelファイルチェック
  case file
  when /xls\Z/ then
    # .xls を読み込むときは Roo::Excel.new
    excel_file = Roo::Excel.new(file)
    puts file
  when /xlsx\Z/ then
    # .xlsx を読み込むときは Roo::Excelx.new
    excel_file = Roo::Excelx.new(file)
    puts file
    if excel_file
      excel_file.default_sheet = excel_file.sheets[SHEET_NUM]
      LINE_NUM.upto(excel_file.last_row) do |line|
        medical_region_code = excel_file.cell(line, 6)
        medical_region_name = excel_file.cell(line, 7)
        municipality_id = excel_file.cell(line, 8)
        municipality_name = excel_file.cell(line, 9)
        unless medical_region_list.include?(medical_region_name)
          MedicalRegion.create(name: medical_region_name, level: 2, code: medical_region_code)
          medical_region_list << medical_region_name
        end
        Municipality.create(name: municipality_name, code: municipality_id, medical_region_code: medical_region_code)
      end
    end
  else
    puts "対応していない形式です。エクセルファイルか確認してください。"
  end
end

=begin
#.csvの場合のロジック
CSV.foreach("db/csv/secondary_medical_region.csv").with_index do |row, i|
  #グラフタイトル飛ばす
  if i != 0
    #同じ2次医療圏は入れない
    unless medical_region_list.include?(row[6])
      MedicalRegion.create(:name => row[6], :level => 2, :code => row[5])
      medical_region_list << row[6]
    end
    Municipality.create(:name => row[8], :code => row[7], :medical_region_code => row[5])
  end
end
=end
