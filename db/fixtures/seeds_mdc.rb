require 'roo'
require 'roo-xls'

########################################################
#MDCマスタ作成用seedflie
########################################################

START_COLUMN = 1
START_LINE = 4 #Index columnをはじく
SHEET_NUM = 2
FILE_NAME = "./db/csv/（６）診断群分類毎の集計.xls"
CURRENT_YEAR = 2015

def excel_read

  #puts "Total:" + base_data.length.to_s
  excelFile  = open_spreadsheet(FILE_NAME)
  errorCount = 0
  if excelFile
    #今は最初のシートで
    excelFile.default_sheet = excelFile.sheets[SHEET_NUM]
    h = Hash.new([])
    h2 = Hash.new([])
    START_LINE.upto(excelFile.last_row) do |line|
      #番号をkey, 診断群分類名称をvalue
      str1 = excelFile.cell(line,1).to_s
      str2 = excelFile.cell(line,2).to_s
      #正しい番号か？14桁の英数　英はxのみ
      if str1 =~ /^[0-9x]+$/ then
        h.store(str1, str2)

      else
        errorCount += 1
      end
      str1 = excelFile.cell(line,4).strip
      str2 = excelFile.cell(line,5).strip
      unless h2.has_key?(str1) then
        h2.store(str1, str2)
      end
    end
    puts "診断群分類番号のフォーマット不正件数:" << errorCount.to_s
    return h, h2
  end
end
######################################
#エクセルファイルを開く
######################################
def open_spreadsheet(file)

   case File.extname(file)
     when '.xlsx' then
       Roo::Spreadsheet.open(file.path, extension: :xlsx)
     when '.ods' then
       Roo::Spreadsheet.open(file.path, extension: :ods)
     when '.csv' then
       Roo::Spreadsheet.open(file.path, extension: :csv)
     when '.xls' then
       Roo::Excel.new(file)
     else
       return false
   end
end


####################################
#データ切り出し
#parameter  key:"010020x101x0xx"
# =>        s: 切り出したい値
####################################
def get_name_insert(key, s)


  #手術・・・・あり|なしを削除
  s = s.gsub(/手術・処置等[０-９].*[(あり)|(なし)]/,"")

  #JCSの値があるか？
  m = s.match(/[(|（]\S.+?以上）/)
  if m.nil? then
    m = s.match(/[(|（]\S.+?以下）/)
    if m.nil? then
      m = s.match(/[(|（]\S.+?未満）/)
    end
  end

  #複数かっこがあるなら最初のかっこは取る
  unless m.nil? then

    if m[0].count("（") == 2 then
      n = m[0].match(/^(\S(?![（|(]))*[)|）]/)
      jcs = m[0].gsub(n.to_s,"")
    else
      jcs = m.to_s
    end
  else
    jcs = ""
  end

  #一応とっておく。
  #delete jcs value
  s.slice!(m.to_s)
  #手術サブ分類（99など）がある場合 全角、半角スペースで区切る
  ar = s.split(/　|\s/)
  if ar.length >1 then
#puts "0="+ar[0].to_s
#puts "1="+ar[1].to_s
#puts "2="+ar[2].to_s
#puts "3="+ar[3].to_s
    layer1 = ar[0] + n.to_s #最初のかっこを戻す。もっといい方法があるかも
    layer2 = ""
    #layer2がスペース区切りの場合もあり
    ar.each{|key|
      if key.match(/術|等/) then
#        puts key
        layer2 += key + " "
      end
    }

    #layer2 = ar[1]
  else
    layer1 = ar[0]
    layer2 = ""
  end

  if layer1.nil? then
    puts key
    puts "NG! No layer1 data" + s
  end
  #layer2がxxならNilでOK check機能つける
  if layer2.nil? && key[8,2] != "xx" then
    puts "NG! layer2 code doesn't match"
  end
  #key[6,2]#JCS 不使用
  layer1_code_id = key[0,2].to_s
  layer1_id = key[2,4].to_s
  layer2_id = key[8,2].to_s
  #layer3_id = key[10,4].to_s
  layer3_id1 = key[10].to_s
  layer3_id2 = key[11].to_s
  layer3_id3 = key[12].to_s
  layer3_id4 = key[13].to_s
  #Insert data
  mdc1code = MdcLayer1Code.where(year:CURRENT_YEAR).where(code:layer1_code_id)
  #mdc1 = MdcLayer1.find_by(code: layer1_id, name: layer1,mdc_layer1_code_id: layer1_code_id)
  #mdc1 = MdcLayer1.new
  mdc1 = mdc1code[0].mdc_layer1s.build()
  mdc1.attributes = {code: layer1_id, name: layer1, mdc_layer1_code_id: layer1_code_id}
  if mdc1.save!.nil? then
    puts "mdc1 Insert Error"
  end
  mdc2 = mdc1.mdc_layer2s.build()
  mdc2.attributes = {code: layer2_id, name: layer2, mdc_layer1_id: layer1_id}
  if mdc2.save!.nil? then
    puts "mdc2 Insert Error"
  end

  mdc3 = mdc2.mdc_layer3s.build()
  mdc3.attributes = {code_1: layer3_id1, code_2: layer3_id2, code_3: layer3_id3, code_4: layer3_id4, mdc_layer2_id: layer2_id}
  if mdc3.save!.nil? then
    puts "mdc3 Insert Error"
  end
end

puts "start"

#MdcLayer1Code.where(:year => CURRENT_YEAR).destroy_all
MdcLayer1Code.destroy_all(year: CURRENT_YEAR)

=begin
MdcLayer1Code.connection.execute("ALTER SEQUENCE mdc_layer1_codes_id_seq RESTART WITH 1;")
MdcLayer1.delete_all
MdcLayer2.delete_all
MdcLayer3.delete_all
MdcLayer1.connection.execute("ALTER SEQUENCE mdc_layer1s_id_seq RESTART WITH 1;")
MdcLayer2.connection.execute("ALTER SEQUENCE mdc_layer1s_id_seq RESTART WITH 1;")
MdcLayer3.connection.execute("ALTER SEQUENCE mdc_layer1s_id_seq RESTART WITH 1;")
=end

#excel読み込み
h, h2 = excel_read

#大分類INSERT
h2.each{|key,value|
  mdc1code = MdcLayer1Code.new
  mdc1code.attributes = {year: CURRENT_YEAR, code: key.to_s, name: value.to_s}
  if mdc1code.save!.nil? then
    puts "mdc1 Insert Error"
  end
}

#読み込んだデータを分割してデータ投入
h.each{|key,value|
  get_name_insert(key, value)
}
puts "TOTAL:" +  h.length.to_s
