# -*- coding: utf-8 -*-
require "csv"

=begin
csv_file = "department_category"
CSV.foreach("./db/" + csv_file + ".csv") do |raw|
  DepartmentCategory.create(name: raw[0], code: raw[1])
end


csv_file = "yourd_kanto"
CSV.foreach("./db/" + csv_file + ".csv") do |raw|
  next if $. == 1 
  Sheet.create(area: raw[0], medical_group: raw[1], work_status: raw[2], hospital_id: raw[3], hospital_name: raw[4], doctor_name: raw[5], position: raw[6], department: raw[7], university: raw[8], graduation_year: raw[9], society: raw[10], profession: raw[11], skill: raw[12], result: raw[13], link: raw[14], history: raw[15], top_or_next: raw[16], user: raw[17], created_at: raw[18], updated_at: raw[19], update_user: raw[20], comment: raw[21])
end



%w(Ⅰ群 Ⅱ群 Ⅲ群).each do |n|
  MedicalGroup.create(:name => n)
end


areas = {'北海道' =>  ['北海道'],
  '東北' =>  ['青森県','岩手県','宮城県','秋田県','山形県','福島県'],
  '関東' =>  ['茨城県','栃木県','群馬県','埼玉県','千葉県','東京都','神奈川県'],
  '中部' =>  ['新潟県','富山県','石川県','福井県','山梨県','長野県','岐阜県','静岡県','愛知県'],
  '近畿' =>  ['三重県','滋賀県','京都府','大阪府','兵庫県','奈良県','和歌山県'],
  '中国' =>  ['鳥取県','島根県','岡山県','広島県','山口県'],
  '四国' =>  ['徳島県','香川県','愛媛県','高知県'],
  '九州' =>  ['福岡県','佐賀県','長崎県','熊本県','大分県','宮崎県','鹿児島県'],
  '沖縄' =>  ['沖縄県']
}

areas.each do |area, prefectures|
   a = Area.create(:name => area)
  prefectures.each do | a_pre |
   Prefecture.create(:area => a, :name => a_pre)
  end
end

=end

def date_check(d)
  if d.present?
   Date.parse(d) rescue Date.today
  else
   Date.today
  end
end

UniversityList.delete_all
ActiveRecord::Base.connection.reset_pk_sequence!('uiversity_lists')


SocietyPositionList.delete_all
ActiveRecord::Base.connection.reset_pk_sequence!('society_position_lists')

SocietyList.delete_all
ActiveRecord::Base.connection.reset_pk_sequence!('society_lists')

DepartmentList.delete_all
ActiveRecord::Base.connection.reset_pk_sequence!('department_lists')

Department.delete_all
ActiveRecord::Base.connection.reset_pk_sequence!('departments')



Doctor.delete_all
ActiveRecord::Base.connection.reset_pk_sequence!('doctors')
=begin
Hospital.delete_all

ActiveRecord::Base.connection.reset_pk_sequence!('hospitals')


hospital_files = Dir::entries("db/csv").select {|item| /\Ah_/ =~ item }

hospital_files.each do | csv_file |
  CSV.foreach("./db/csv/" + csv_file) do |raw|
    next if $. == 1
    break if raw[0].nil?
    pre = Prefecture.where("name like '%" + raw[2] + "%'")
    if pre.present? and raw[4].present?

      type_num = 1
      if raw[11].strip! == "Ⅰ群"
        type_num = 2
      elsif  raw[11].strip! == "Ⅲ群"
        type_num = 3
      end

      Hospital.create(id: raw[0].gsub("ID-", "").to_i, medical_group_id: type_num, area_id: pre.first.area_id , prefecture_id: pre.first.id, std_region_code: raw[3], name: raw[4].gsub("　", "").gsub(" ", ""), postal_code: raw[5].gsub(/[^0-9]/, "").to_i, address: raw[6], tmp_head: raw[7], url: raw[8], bed_num: raw[9], acute_phase_bed_num: raw[10], tmp_user: raw[12], created_at: date_check(raw[13]))
    else
      puts "Error:Couldn't find Prefecture" + raw[2] + ":" + raw
    end
  end
end

=end

doctor_files = Dir::entries("db/csv").select {|item| /\Adr_/ =~ item }


doctor_files.each do | csv_file |
  puts csv_file + " dr num:" +Doctor.count.to_s
  CSV.foreach("./db/csv/" + csv_file) do |raw|
    next if $. == 1
    #break if raw[3].nil?
    unless raw[3].present? and raw[5].present?
     puts "hospital error:something wrong:" + raw.to_s
     next
    end

    hospital = Hospital.find(raw[3].gsub("ID-", "").to_i)
    if hospital.nil?
      hospital = Hospital.create(id: raw[3].gsub("ID-", "").to_i, name: raw[4])
    end

    if raw[8].present? and raw[8].strip.length > 2
      university = University.find_or_create_by(name: raw[8].strip).id
    else
      university = nil
    end


    names = raw[5].gsub(/\p{blank}/, "\s").strip.split("\s")
    if names.present?
      if names.count == 2
        first_name = names[1]
        last_name = names[0]
      else
        if names[0].length > 1 
          last_name = names[0].slice(0..1)
          first_name = names[0].gsub(last_name, "")
        else
          last_name = names[0]
          first_name = ""
        end
        puts "name error:couldn't separate:" + raw[5]
      end
      doctor = Doctor.create(hospital_id: hospital.id, first_name: first_name, last_name: last_name, position: raw[6], university_id: university, graduation_year: raw[9], society: raw[10], profession: raw[11], skill: raw[12], result: raw[13], url: raw[14], history: raw[15], top_or_next: raw[16], user: raw[17], update_user: raw[20], comment: raw[21], created_at: date_check(raw[18]), updated_at: date_check(raw[19]))


      if raw[7].present? and raw[7].length > 1
        raw[7].strip.split("\n").each do |department_name|
          department = Department.find_or_create_by(:name => department_name.strip, :hospital_id => hospital.id)
          DepartmentList.create(:department_id => department.id, :doctor_id => doctor.id)
        end
      end

 
    else
        puts "name error:something wrong:" + raw[5]
    end
  end
end

