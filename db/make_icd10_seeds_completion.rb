require 'csv'
require 'kconv'
#require 'date'

##########################################
# ログメソッド
##########################################
def log(str)
  $stdout.print str + "\n"
end

def logr(str)
  $stdout.print str + "\r"
end

BYOMEI_TXT = '/csv/byomei/nmain316.txt'
seeds_filename = ARGV.shift

### 病名TEXT読み込み＆パース
text_filename      = File.expand_path(File.dirname(__FILE__)) + BYOMEI_TXT

log "#######################"
log "Reading TEXT Files...."
log "-----------------------"

log text_filename

log "#######################"
log "Writing Seeds File...."
log "-----------------------"

seeds_file = open(seeds_filename, "w")

#前処理
seeds_file.write("ActiveRecord::Base.connection.reset_pk_sequence!('icd10s')\n")
seeds_file.write("\n")

index = 0
CSV.foreach(text_filename, row_sep: "\r\n", encoding: "SJIS") do |row|

  name         = row[2].toutf8
  replace_code = row[5].toutf8
  code         = row[6].toutf8

  if code =~ /^([A-Z][0-9][0-9][0-9])(.+)$/ || code =~ /^([A-Z][0-9][0-9])(.*)$/
    code_u = $1
    code_l = $2
    depth = 3

    if !code_l.empty? && !replace_code.empty?
      depth = 5
    elsif !code_l.nil? || !replace_code.empty?
      depth = 4
    end
    p code_u + "." + code_l + ":" + replace_code + "  " + depth.to_s

    objname = "icd_" + index.to_s

    line = ""

    unless replace_code.nil?
      line += objname + " = Icd10.find_or_initialize_by(code: '" + code.to_s + "', replace_code: '" + replace_code.to_s + "')\n"
    else
      line += objname + " = Icd10.find_or_initialize_by(code: '" + code.to_s + "', replace_code:  nil)\n"
    end
    line += "if " + objname + ".id.nil?\n"
    line += "  "  + objname + ".name = \"%s\"\n" % name
    line += "  "  + objname + ".code = '%s'\n" % code
    unless replace_code.empty?
      line += "  "  + objname + ".replace_code = '%s'\n" % replace_code
    else
      line += "  "  + objname + ".replace_code = nil\n"
    end
    line += "  "  + objname + ".depth = %d\n" % depth
  
    if depth == 5
      line += "  parent = Icd10.find_by(code:'" + code + "', depth: 4, replace_code:  nil)\n"
      line += "  if parent.nil?\n"
      line += "    pofp   = Icd10.find_by(code:'" + code_u[0, code_u.length] + "', replace_code:  nil)\n"
      line += "    unless pofp.nil?\n"
      line += "      parent = Icd10.find_or_create_by(code:'" + code + "', depth: 10, replace_code: nil, icd10_id: pofp.id )\n"
      line += "      "  + objname + ".depth = 11\n"
      line += "    end\n"
      line += "  end\n"
    elsif depth == 4
      line += "  parent = Icd10.find_by(code:'" + code_u + "', depth: 3, replace_code:  nil)\n"
    end

    line += "  unless parent.nil?\n"
    line += "    print \"#### %s %s\\n\"\n" % [name, code]
    line += "    "  + objname + ".icd10_id = parent.id\n"
    line += "    "  + objname + ".save\n"
    line += "  end\n"
    line += "end\n"
    line += "\n"
    seeds_file.write(line)

    index += 1
  end

end #CSV

#後処理
#seeds_file.write("puts '#### %s/%s'\n" % [icd10s.length, icd10s.length])
seeds_file.close
