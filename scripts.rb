# -*- coding: utf-8 -*-
#ドクター情報から学会情報を作成するスクリプト
Doctor.where.not(society: nil).each do |dr|
  next if dr.society_lists.present?
  dr.society.split("\n").each do |s|
    tmp_ary = s.gsub("　", "").gsub(" ", "").split("学会")
    if tmp_ary.count == 2
      society = Society.find_or_create_by(:name => tmp_ary[0] + "学会")
      society_list = SocietyList.find_or_create_by(doctor: dr, society: society)
      if tmp_ary[1].index("・")
        tmp_ary[1].split("・").each do | position|
          sp = SocietyPosition.find_or_create_by(society: society, :name => position)
          SocietyPositionList.create(society_list: society_list, position: sp)
        end
      elsif tmp_ary[1].index("、")
        tmp_ary[1].split("、").each do | position|
          sp = SocietyPosition.find_or_create_by(society: society, :name => position)
          SocietyPositionList.create(society_list: society_list, position: sp)
        end
      else
        sp = SocietyPosition.find_or_create_by(society: society, :name => tmp_ary[1])
        SocietyPositionList.create(society_list: society_list, position: sp)
      end
    end
  end
end


Doctor.where.not(university: nil).each do |dr|
  next if dr.university_lists.present?
  strgy = dr.graduation_year
  gy = nil
  if strgy.present?
    strgy.tr!("０-９", "0-9")
    strgy.tr!("Ａ-Ｚ", "A-Z")
    strgy.gsub!(/[\s　]/, "")
    case strgy
    when /昭和([0-9]+)/
      gy = 1925 + $1.to_i
    when /昭([0-9]+)/
      gy = 1925 + $1.to_i
    when /昭和元/
      gy = 1926
    when /昭元/
      gy = 1926
    when /平成([0-9]+)/
      gy = 1988 + $1.to_i
    when /平([0-9]+)/
      gy = 1988 + $1.to_i
    when /平成元/
      gy = 1989
    when /平元/
      gy = 1989
    when /S([0-9]+)/
      gy = 1925 + $1.to_i
    when /H([0-9]+)/
      gy = 1988 + $1.to_i
    when /[0-9][0-9][0-9][0-9]/
      gy = $1.to_i
    when /0/
      gy = nil
    else
      gy = nil
    end
  end
  ul = dr.university_lists.build(university: dr.university, graduation_year: gy)
  ul.save!
end
