# -*- coding: utf-8 -*-
require 'sqlite3'

class WhytplotDbExportJob < ActiveJob::Base
  queue_as :default

  def perform(*args)
    time = Time.now.strftime("%Y%m%d%H")
    @@errors = Array.new
    # 本番データとテストデータ分岐
    if args[0]
      @@db = SQLite3::Database.new("#{Rails.root}/tmp/datafiles/#{time}.sqlite3")
    else
      sqlite_file = "#{Rails.root}/tmp/datafiles/#{time}_beta.sqlite3"
      if File.exist?("#{sqlite_file}")
        File.delete("#{sqlite_file}")
      end
      @@db = SQLite3::Database.new("#{sqlite_file}")
    end

    whyt_plot_table_views = WhytPlotTableView.where(is_view: false)
    whyt_plot_table_views.each do |whyt_plot_table_view|
      #create_postgres_view(whyt_plot_table_view)
      create_sqlite_table(whyt_plot_table_view.name)
      insert_sqlite_record(whyt_plot_table_view.name)
    end

    sqlite_views = WhytPlotTableView.where(is_view: true)
    sqlite_views.each do |sqlite_view|
      create_sqlite_view(sqlite_view)
    end
    @@db.close
    # エラーの取得
    @@errors.each do |error|
      WhytplotExportError.create(message: error['message'], detail: error['detail'])
    end
    # s3に作成されたsqliteデータファイルアップロード
    if Rails.env.production? || Rails.env.staging?
      WhytPlotTableView.upload_db
    end
  end

  private
    def create_sqlite_table(pg_view_name)
      #カラム定義取得
      query = <<SQL
SELECT at.attname, format_type(at.atttypid, at.atttypmod)
FROM
pg_attribute as at
  left join pg_type as tp on (at.atttypid = tp.oid)
WHERE
at.attnum > 0 and
at.attrelid = (select relfilenode from pg_class where relname = '#{pg_view_name}')
ORDER BY
at.attnum;
SQL
      column_definition = ActiveRecord::Base.connection.execute(query)
      # カラム定義作成
      definition = column_definition.map.with_index { |result, i|
        '' << "#{result['attname']} #{result['format_type']}"
      }
      definition = definition.join(",\n")
      # テーブルの削除 SQLiteのupsertしっくりこなかったのでdrop tableで
      query = <<SQL
DROP TABLE IF EXISTS #{pg_view_name};
SQL
      begin
        @@db.execute(query)
      rescue => e
        @@errors += ['message' => e.message.to_s, 'detail' => "#{pg_view_name} #{e.backtrace.to_s}"]
      end
      # sqliteのテーブル作成
      query = <<SQL
CREATE TABLE IF NOT EXISTS #{pg_view_name} (
#{definition}
);
SQL
      begin
        @@db.execute(query)
      rescue => e
        @@errors += ['message' => e.message.to_s, 'detail' => "#{pg_view_name} #{e.backtrace.to_s}"]
      end
    end

    def insert_sqlite_record(pg_view_name)
      # 作成したviewのレコード取得
      begin
        query = "SELECT * FROM #{pg_view_name}"
        records = ActiveRecord::Base.connection.execute(query)
      rescue => e
        @@errors += ['message' => e.message.to_s, 'detail' => "#{pg_view_name} #{e.backtrace.to_s}"]
      end
      # レコードのinsert
      begin
        @@db.transaction do
          records.map.with_index { |record, index|
            values = Array.new
            column_names = String.new
            record.each { |key, value|
              values << value
              # upsertで使う各colume nameの文字列作成
              column_names << "#{key},";
            }
            args = String.new
            args = values.map { |e| "?" }.join(",")
            sql = "INSERT OR IGNORE INTO %s (%s) VALUES (%s)" % [pg_view_name, column_names.chop, args]
            @@db.execute(sql, values)
          }
        end
      rescue => e
        @@errors += ['message' => e.message.to_s, 'detail' => "#{pg_view_name} #{e.backtrace.to_s}"]
      end
    end

    def create_sqlite_view(sqlite_view)
      # sqliteのviewを作成
      begin
        @@db.execute(sqlite_view.create_view_sql)
      rescue => e
        @@errors += ['message' => e.message.to_s, 'detail' => "#{sqlite_view.name} #{e.backtrace.to_s}"]
      end
      # web apiとしても使用するため、sqliteのviewをpostgresにも作成
      begin
        drop_viwe_sql = "DROP VIEW IF EXISTS #{sqlite_view.name}"
        ActiveRecord::Base.connection.execute(drop_viwe_sql)
        ActiveRecord::Base.connection.execute(sqlite_view.create_view_sql)
      rescue => e
        @@errors += ['message' => e.message.to_s, 'detail' => "#{sqlite_view.name} #{e.backtrace.to_s}"]
      end
    end

end
