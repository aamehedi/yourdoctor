class ImportJob < ActiveJob::Base
  queue_as :default

  rescue_from(Exception) do |exception|
    Rails.logger.error exception
  end

  def perform(filepath, s3_filepath, year)
    result = Import::ImportResult.new
    begin
      if filepath =~ /.xls|.xlsx/
        excel = Import::RwExcel.new(filepath, year)
        p excel.file_type
        case excel.file_type
        when "施設概要表",
          "分析対象外としたデータの状況",
          "分析対象外としたデータの状況",
          "在院日数の状況",
          "退院先の状況",
          "退院時転帰の状況",
          "再入院の状況",
          "手術化学療法放射線療法全身麻酔について" then
          result = DpcHospital.import(excel)
        when "診断群分類毎の集計" then
          result = MdcLayer1Code.import(excel)
        when "化学療法のレジメン" then
          result = AgByMdcL1.import(excel)
        when "再入院再転棟_医療機関別集計" then
          result = DpcHospital.import(excel)
        when "MDC別医療機関別件数（割合）",
          "予定・救急医療入院医療機関別MDC別集計",
          "救急車による搬送の有無の医療機関別MDC別集計",
          "入院から24時間以内の死亡の有無の医療機関別MDC別集計",
          "在院日数の平均の差_MDC別" then
          result = AgByDpchXMdcL1c.import(excel)
        when "電子点数表" then
          result = Kcode.import(excel)
        when "2次医療圏_地域マスタ" then
          result = MedicalRegion.import(excel)
        when /.*[都道府県]人口/ then
          result = Population.import(excel)
        when "疾患別手術別集計" then
          result = AgByDpchXMdcL2.import(excel)
        when "疾患別手術別処置1,2集計" then
          result = AgByDpchXMdcL2Treatment.import(excel)
        when "地方自治体コード" then
          result = Municipality.import(excel)
        end
      elsif filepath =~ /.csv|.CSV/
        case filepath
        when /KEN_ALL/
          result = Address.import(filepath)
        end
      end
      datafile = Datafile.find_or_initialize_by(filepath: s3_filepath)
      datafile.status = :imported
      datafile.total_line_num    = result.total_line_num
      datafile.error_line_num    = result.error_line_num
      datafile.warning_line_num  = result.warning_line_num
      datafile.messages          = result.messages
      unless datafile.save!
        datafile.status = :failed
        datafile.save!
      end

    rescue => e
      datafile = Datafile.find_or_initialize_by(filepath: s3_filepath)
      datafile.status = :failed
      datafile.save!
      p e
    ensure
      # エクセルをS3で読み込む方法わからなかったので、ダウンロードして処理
      # s3からダウンロードしたファイルは処理終了後に削除
      if File.exist?(filepath)
        File.unlink filepath
      end
    end
  end
end
