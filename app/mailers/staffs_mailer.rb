# -*- coding: utf-8 -*-
class StaffsMailer < ApplicationMailer
  def pre_monographs_ready(name, email)
    @name = name
    mail to: email, subject: "要確認論文リストの準備ができました"
  end

  def notificate_kpi(email_address, target_date, kpi_info)
    @target_date = target_date
    @kpi_info = kpi_info
    mail to: email_address, subject: "Whytlink KPI(#{@target_date.to_s})"
  end
end

