# -*- coding: utf-8 -*-
class DoctorsMailer < ApplicationMailer
  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.doctors_mailer.email_confirmation.subject
  #
  def invitation(user, inviter, comment)
    @user    = user
    @inviter = inviter
    @comment = comment.split("https://www.whytlink.com/#/signup?uuid=oxoxoxoxoxoxoxox")
    mail to: @user.email, subject: "Whytlinkに招待されました"
  end

  def email_confirmation(user)
    @user = user
    mail to: @user.email, subject: "Whytlinkにご登録いただきありがとうございます"
  end

  def new_shine(user, shined_user, post)
    @user = user
    @shined_user = shined_user
    @post = post
    mail to: @user.email, subject: "「輝いているね!」されました"
  end

  def new_comment(user, comment, post)
    @user = user
    @comment = comment
    @post = post
    mail to: @user.email, subject: "コメントされました"
  end

  def new_recommend(skill, recommender)
    @user = skill.doctor
    @skill = skill
    @recommender = recommender
    mail to: @user.email, subject: "専門分野が推薦されました"
  end

#  belongs_to :followee, :class_name => "Doctor", :foreign_key => "followee_id" #許諾者
#  belongs_to :follower, :class_name => "Doctor", :foreign_key => "follower_id" #リンク申請者
  def new_request(followee_id, followee_name, followee_email, follower_id, follower_name, follower_email)
    @followee_id = followee_id
    @followee_name = followee_name
    @follower_id = follower_id
    @follower_name = follower_name
    mail to: followee_email, subject: "Dr.#{@follower_name}からのリンク申請です"
  end

  def new_approve_request(followee_id, followee_name, followee_email, follower_id, follower_name, follower_email)
    @followee_id = followee_id
    @followee_name = followee_name
    @follower_id = follower_id
    @follower_name = follower_name
    mail to: follower_email, subject: "Dr.#{@followee_name}と繋がりました"
  end

  def thank_you(name, email_address)
    @name = name
    mail to: email_address, subject: "【Whytlink】代表 塩飽哲生からのご挨拶"
  end

  def report_account(id, name, reporter_id, reporter_name)
    @id = id
    @name = name
    @reporter_id = reporter_id
    @reporter_name = reporter_name
    mail to: "info@whytlink.com", subject: "虚偽登録報告です"
  end

  def delete_account(name, email)
    @name = name
    mail to: email, bcc: "info@whytlink.com", subject: "Whytlinkをご利用いただきありがとうございました。"
  end

  def license_check(id, name, iseki_id, iseki_date, sex, birthdate)
    @id = id
    @name = name
    @iseki_id = iseki_id
    @iseki_date = iseki_date
    @sex = sex
    @birthdate = birthdate
    mail to: "info@whytlink.com", subject: "医籍確認の連絡です"
  end

  def all_mail(all_mail, to_doctor)
    @all_mail = all_mail
    @doctor = to_doctor
    mail to: @doctor.email, subject: "【Whytlink】#{@all_mail.title}"
  end
end
