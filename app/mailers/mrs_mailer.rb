class MrsMailer < ApplicationMailer
  def password_reset mr
    @mr = mr
    mail to: @mr.email, subject: t(".reset_password")
  end
end
