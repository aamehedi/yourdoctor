class ApplicationMailer < ActionMailer::Base
  default from: "info@whytlink.com"
  layout 'mailer'
end
