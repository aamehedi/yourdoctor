class NumberByCode < ActiveRecord::Base
  belongs_to :ag_by_mdc
  belongs_to :codeable, :polymorphic => true
  class NumberByIcd10Costly < NumberByCode
  end

  class NumberByIcd10Comorbidity < NumberByCode
  end

  class NumberByKcode < NumberByCode
  end
end
