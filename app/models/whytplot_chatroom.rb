class WhytplotChatroom < ApplicationRecord
  has_many :whytplot_mr_chatrooms, dependent: :destroy
  has_many :whytplot_messages, dependent: :destroy
  has_many :mrs, through: :whytplot_mr_chatrooms
  has_one :branch
  has_one :project

  accepts_nested_attributes_for :whytplot_mr_chatrooms

  validates :chatroom_type, presence: true, allow_blank: false

  enum chatroom_type: [:dual, :multi]

  # NO USE FOR NOW BUT MAYBE USEFUL LATER
  # after_create_commit :broadcast_chatroom_info

  def broadcast_chatroom_info
    ActionCable.server.broadcast "whytplot_rooms_info", self
  end

  def last_message_id
    WhytplotMessage.where(whytplot_chatroom_id: self.id).last.try :id
  end

  def update_participation_favorite favorite
    return if favorite.nil?
    self.whytplot_mr_chatrooms.update_all favorite: favorite
  end

  def api_update attributes, participation_favorite
    if self.multi?
      self.update_attributes! attributes
    else
      self.update_participation_favorite participation_favorite
    end
  rescue StandardError
  end

  class << self
    def find_dual_chatroom params
      chatrooms_d1 = WhytplotChatroom.where(chatroom_type: 0)
        .joins(:whytplot_mr_chatrooms)
        .where("whytplot_mr_chatrooms.mr_id IN (?)", params[0])
      chatrooms_d2 = WhytplotChatroom.where(chatroom_type: 0)
        .joins(:whytplot_mr_chatrooms)
        .where("whytplot_mr_chatrooms.mr_id IN (?)", params[1])
      (chatrooms_d1 & chatrooms_d2).last
    end

    # NO USE FOR NOW BUT MAYBE USEFUL LATER
    # def mr_chatrooms_with_missed_chat mr_id
    #   chatroom_ids = WhytplotMrChatroom.select(:whytplot_chatroom_id).where(
    #     "(last_read_message_id > (SELECT MAX(whytplot_messages.id) FROM
    #     whytplot_messages WHERE whytplot_messages.whytplot_chatroom_id =
    #     whytplot_mr_chatrooms.whytplot_chatroom_id) OR last_read_message_id
    #     IS NULL) AND mr_id = ?", mr_id)
    #   WhytplotChatroom.where("id = ANY(?)", chatroom_ids)
    # end

    def get_filtered_chatrooms mr_id, chatroom_type, favorite
      query_string = "mr_id = #{mr_id}"
      query_string << " AND favorite = #{favorite}" unless favorite.nil?
      chatroom_ids = WhytplotMrChatroom.select(:whytplot_chatroom_id)
        .where query_string
      if chatroom_type.nil?
        WhytplotChatroom.where("id = ANY(?)", chatroom_ids)
      else
        WhytplotChatroom.where("id = ANY(?)", chatroom_ids)
          .where("chatroom_type = ?", chatroom_type)
      end
    end

    def create_chatroom params
      chatroom = WhytplotChatroom.new params
      chatroom if chatroom.save
    end

    def find_or_create_chatroom params
      if params[:mr_ids].size == Settings.whytplot_chatroom
          .private_chat_participant_number
        chatroom = find_dual_chatroom params[:mr_ids]
        if chatroom
          chatroom.tap{chatroom.update_participation_favorite true}
        else
          create_chatroom params.merge(
            chatroom_type: Settings.whytplot_chatroom.private_chat)
        end
      else
        create_chatroom params.merge(
          chatroom_type: Settings.whytplot_chatroom.group_chat)
      end
    end
  end

  def is_default?
    self.branch || self.project ? true : false
  end
end
