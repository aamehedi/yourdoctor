class UniversityList < ActiveRecord::Base
  belongs_to :doctor
  belongs_to :university
  has_paper_trail
  validates :doctor_id, :university_id, presence: true

  #タイムライン
  after_create   :log_timeline_by_create
  after_update   :log_timeline_by_update
  before_destroy :log_timeline_by_destroy

  # accepts_nested_attributes_for :university
  # university_list経由でマスタであるuniversity自体は変更させない。但し新規追加はできる。
  #  -accept_nestedはしない
  #  -idのないuniversity_attributesがある場合は新規でuniversityを作成する
  def university_attributes=(attributes)
    #学会情報がない場合は特に何もしない
    return if attributes.nil?

    #IDが無い、存在しないIDが指定されている場合は同名の学会を探すか、なければ新規追加
    if attributes[:id].nil? || University.find_by(id: attributes[:id]).nil?
      university = University.find_or_create_by(name: attributes[:name])
      new_university_id = university.id
    else
      new_university_id = attributes[:id]
    end
    self.university_id = new_university_id
  end

  private
    def log_timeline_by_create
      Timeline.create(editor: Doctor.editor, target: self.doctor, model: self, action: "create", meta: self.university.name)
    end

    def log_timeline_by_update
      if self.changed?
        Timeline.create(editor: Doctor.editor, target: self.doctor, model: self, action: "update", meta: self.university.name)
      end
    end

    def log_timeline_by_destroy
      Timeline.create(editor: Doctor.editor, target: self.doctor, model: self, action: "destroy", meta: self.university.name)
    end
end
