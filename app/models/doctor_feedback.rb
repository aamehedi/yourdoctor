class DoctorFeedback < ApplicationRecord
  belongs_to :doctor
  belongs_to :emergency_request_hospital

  has_one :hospital, through: :emergency_request_hospital

  validates :doctor, presence: true

  enum status: %w(considering confirmed accepted declined)

  scope :emergency_request_feedbacks, -> doctor, emergency_request_id{
    doctor.doctor_feedbacks.joins("INNER JOIN emergency_request_hospitals on
      emergency_request_hospitals.id=doctor_feedbacks
      .emergency_request_hospital_id").where(emergency_request_hospitals:
      {emergency_request_id: emergency_request_id})
  }

  def update_doctor_feedback doctor_feedback_params
    DoctorFeedback.check_emergency_request(self.emergency_request_hospital
      .emergency_request_id) &&
      self.update_attributes(doctor_feedback_params)
  end

  class << self
    def check_emergency_request emergency_request_id
      emergency_request = EmergencyRequest.find_by_id emergency_request_id
      !emergency_request.not_editable?
    end
  end
end
