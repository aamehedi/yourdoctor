class MrProject < ApplicationRecord
  belongs_to :mr, inverse_of: :mr_projects
  belongs_to :project, inverse_of: :mr_projects

  validates :mr, presence: true, uniqueness: {scope: :project_id}
end
