class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  validates :name, presence: true

  enum role: { kanri: 10, gyousha: 30, office: 40, staff: 50, baito: 60 }
  default_scope -> { order('id desc') }

  def admin?
    admin
  end

  def active_for_authentication?
    super and self.is_active?
  end

  def is_active?
    !self.suspended
  end

#  def self.find_for_authentication(conditions)
#    find_first_by_auth_conditions(conditions, suspended: false)
#  end
end
