class StaffPost < ActiveRecord::Base
  has_many :staff_logs, as: :staff_log
  validates :body, :start_time, :end_time, presence: true
  default_scope -> { order('updated_at desc') }

  cattr_accessor :current_user

  after_create -> { create_staff_log("create") }
  after_update -> { create_staff_log("update") }
  after_destroy -> { create_staff_log("destroy") }

  def convert_to_timeline_format
    Timeline.new(action: "create", model: self, model_type: "StaffPost", updated_at: self.updated_at)
  end

  private
    def create_staff_log(type)
      StaffLog.create(user_id: self.current_user.id, staff_log_id: self.id, staff_log_type: "StaffPost", sub: type)
    end
end
