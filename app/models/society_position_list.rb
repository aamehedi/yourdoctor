class SocietyPositionList < ActiveRecord::Base
  belongs_to :society_list
  belongs_to :position, :class_name => 'SocietyPosition', :foreign_key => :society_position_id
  attr_accessor :name
  attr_accessor :society_id
  has_paper_trail  

  #タイムライン
  after_create   :log_timeline_by_create
  after_update   :log_timeline_by_update
  before_destroy :log_timeline_by_destroy

  #accepts_nested_attributes_for :position_attributes
  def position_attributes=(attributes)
    "###### position_attributes #######"
    #役職情報がない場合は特に何もしない
    return if attributes.nil?
    return if attributes[:name].blank?

    #IDが無い、存在しないIDが指定されている場合は同学会、同名の役職を探すか、なければ新規追加
    if attributes[:id].nil? || SocietyPosition.find_by(id: attributes[:id]).nil?
      if attributes[:society_id].present?
        position = SocietyPosition.find_or_create_by(name: attributes[:name], society_id: attributes[:society_id])
      else
        position = SocietyPosition.create(name: attributes[:name])
      end
    else
      position = SocietyPosition.find(attributes[:id])
    end
    self.society_position_id = position.id
  end

  private
    def log_timeline_by_create
      Timeline.create(editor: Doctor.editor, target: self.society_list.doctor, model: self, action: "create", meta: self.position.name)
    end

    def log_timeline_by_update
      if self.changed?
        Timeline.create(editor: Doctor.editor, target: self.society_list.doctor, model: self, action: "update", meta: self.position.name)
      end
    end

    def log_timeline_by_destroy
      Timeline.create(editor: Doctor.editor, target: self.society_list.doctor, model: self, action: "destroy", meta: self.position.name)
    end
end
