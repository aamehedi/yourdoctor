class MedicalMunicipality < ActiveRecord::Base
  belongs_to :medical_region, :foreign_key => :code
  has_many :hospitals, through: :municipalities

end
