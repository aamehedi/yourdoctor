# -*- coding: utf-8 -*-
class DpcHospital < ActiveRecord::Base
  belongs_to :hospital
  has_many :ag_by_dpch_x_reason_majors
  has_many :ag_by_dpch_x_mdc_l1cs
  has_many :ag_by_dpch_x_mdc_l2s
  has_many :ag_by_dpch_x_mdc_l2_treatments
  has_many :datatable_datafiles

  extend ImportUtil

  scope :where_created_year, ->(created_year) { where("created_year = ?", created_year) }
  scope :where_notice_number, ->(notice_number) { where("notice_number = ?", notice_number) }

  def self.import(excel)
    case excel.file_type
    when "施設概要表" then
      result = import_dpc(excel)
    when "分析対象外としたデータの状況", "分析対象外としたデータの状況" then
      result = import_out_of_analysis(excel)
    when "在院日数の状況" then
      result = import_hospital_days(excel)
    when "退院先の状況" then
      result = import_status_after_leaving(excel)
    when "退院時転帰の状況"
      result = import_status_outcome(excel)
    when "再入院の状況"
      result = import_rehospitalize(excel)
    when "手術化学療法放射線療法全身麻酔について"
      result = import_advanced_medi_care(excel)
    when "再入院再転棟_医療機関別集計"
      result = import_rehospitalization(excel)
    else
      result = Import::ImportResult.new
    end
    return result
  end
  #施設概要表
  def self.import_dpc(excel)
    result = Import::ImportResult.new
    excel.sheet_info.each do |sheet_info|
      sheet = excel.sheet(sheet_info.name)
      sheet_info.start_row.upto(sheet.last_row) do |index|
        row = sheet.row(index)
        if to_code(row[0]) =~ /^\d+$/
          result.total_line_num += 1
          col01 = sheet_info.start_column - 1
          col02 = excel.extensions[0][0].to_cn0
          is_serial_number = excel.extensions[1][0]          #通番あり？
          is_dpc_submitted_months_num = excel.extensions[2][0]   #提出月数あり？
          #col01 = sheet_info.start_column - 1
          notice_number                      = row[col01].to_i
          serial_number                      = is_serial_number && row[col01+1] =~ /^\d+$/ ? row[col01+1].to_i : nil
          #p row[0].to_s + row[1].to_s + row[2].to_s + row[3].to_s
          if is_serial_number then
            col01 += 1
          end
          name_org                           = row[col01+1].to_s
          name                               = row[col01+1].gsub(/[\s　]/, "")
          dpc_type                           = row[col02+0]
          dpc_calc_bed_num                   = row[col02+1].to_i
          dpc_calc_hospitalization_basic_fee = row[col02+2].to_s.tr("０-９", "0-9")
          dpc_calc_bed_rate                  = row[col02+3] #データチェック用
          dpc_mental_hospital_bed_num        = row[col02+4].to_i
          dpc_recuperation_hospital_bed_num  = row[col02+5].to_i
          dpc_tuberculosis_hospital_bed_num  = row[col02+6].to_i
          dpc_total_bed_num                  = row[col02+7].to_i
          dpc_submitted_months_num                      = is_dpc_submitted_months_num && row[col02+8].to_s =~ /\d+/ ? row[col02+8].to_i : nil
          dpch = DpcHospital.find_or_initialize_by(created_year: excel.year, notice_number: notice_number, serial_number: serial_number)
          dpch.notice_number                      = notice_number
          dpch.serial_number                      = serial_number
          dpch.name                               = name
          dpch.dpc_type                           = dpc_type
          dpch.dpc_calc_bed_num                   = dpc_calc_bed_num
          if dpc_calc_hospitalization_basic_fee =~ /(.+?)(\d.?)(.+)(\d.?)/#{/(.+)(\d.?)(.+)(\d.?)/}
            dpch.dpc_calc_hospitalization_basic_fee_type = $1
            dpch.dpc_calc_hospitalization_basic_fee_lratio = $2
            dpch.dpc_calc_hospitalization_basic_fee_rratio = $4
          end
          dpch.dpc_mental_hospital_bed_num        = dpc_mental_hospital_bed_num
          dpch.dpc_recuperation_hospital_bed_num  = dpc_recuperation_hospital_bed_num
          dpch.dpc_tuberculosis_hospital_bed_num  = dpc_tuberculosis_hospital_bed_num
          dpch.dpc_total_bed_num                  = dpc_total_bed_num
          dpch.dpc_submitted_months_num           = dpc_submitted_months_num
          dpch.created_year = excel.year
          #まだ対応する病院が決まっていない場合は関連先を探す
          unless dpch.hospital.present?
            target_hospitals = Hospital.where(name: name)
            if target_hospitals.count == 1
              dpch.hospital = target_hospitals[0]
            elsif target_hospitals.count == 0
              #再検索
              new_name = get_new_name(name_org)
              target_hospitals = Hospital.where(name: new_name)
              target_hospitals = Hospital.where("name like '%#{name}%'") unless target_hospitals.present?
              target_hospitals = Hospital.where("name like '%#{new_name}%'") unless target_hospitals.present?
              if target_hospitals.count == 1
                dpch.hospital = target_hospitals[0]
              elsif target_hospitals.count == 0
                result.warning_line_num += 1
                result.messages += message(index, 'WARNING', "対応する病院がありませんでした:" + name_org.to_s + "_" + new_name.to_s)
              elsif  target_hospitals.count > 1
              get_target_hospital(get_new_name(name_org),name,dpch,result,index)
              end
            elsif target_hospitals.count > 1
              get_target_hospital(get_new_name(name_org),name,dpch,result,index)
            end
          end
          #2012年のため
          unless equal_appro(dpc_calc_bed_num.to_f / dpc_total_bed_num.to_f, dpc_calc_bed_rate, 0.000001) || (dpc_calc_bed_num.to_f / dpc_total_bed_num.to_f).round(4) == dpc_calc_bed_rate.round(4)
            result.warning_line_num += 1
            result.messages += message(index, 'WARNING', "DPC算定病床割合が計算値と異なります")
          end
          begin
            dpch.save
          rescue => e
            result.error_line_num += 1
            result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
          end
        end
      end
    end
    return result
  end
  #xxxxx会=establisher_orgでマッチするか検索する
  def self.get_target_hospital(new_name,name,dpch,result,index)
    non_name = name.match(/[^\s|社会]+会(?![病院|社会保険])(?!($))/)
    unless non_name.present?
      target_hospitals = Hospital.where(name: new_name, establisher_org: non_name)
      target_hospitals = Hospital.where("name ='#{name}' AND establisher_org like '%#{non_name.to_s}%'") unless target_hospitals.present?
      target_hospitals = Hospital.where("name ='#{new_name}' AND establisher_org like '%#{non_name.to_s}%'") unless target_hospitals.present?
      if target_hospitals.count == 1
        dpch.hospital = target_hospitals[0]
        p index.to_s + ": establisher_org " + non_name.to_s
      elsif target_hospitals.count == 0
        result.warning_line_num += 1
        result.messages += message(index, 'WARNING', "対応する病院がありませんでした:" + name.to_s + "_" + new_name.to_s)
      elsif target_hospitals.count > 1
        result.warning_line_num += 1
        result.messages += message(index, 'WARNING', "対応する病院が一意に定まりませんでした:" + name.to_s + "_" + new_name.to_s)
      end
    else
      result.warning_line_num += 1
      result.messages += message(index, 'WARNING', "対応する病院が一意に定まりませんでした:" + name.to_s + "_" + new_name.to_s)
    end
  end
  #名前のみを取得
  def self.get_new_name(name)
    #name = name.gsub(/国立大学法人/,"")
    #name = name.gsub(/学校法人/,"")
    #name = name.gsub(/.*大学/,"")
    #name = name.gsub(/国立研究開発法人/,"")
=begin
神鋼記念病院>神鋼病院
駿河台日本大学病院>駿河台日本大学病院
国立大学法人
学校法人
国立研究開発法人
社会福祉法人
独立行政法人
医療法人
社会医療法人
地方独立行政法人
公益財団法人
一般財団法人
地域医療機能推進機構
社団カレスサッポロ
=end

    name = name.gsub(/日本郵政株式会社/,"")
    name = name.gsub(/医療法人社団/,"")
    name = name.gsub(/養生館/,"")
    name = name.gsub(/・/,"")
    name = name.gsub(/（医）/,"")
    name = name.gsub(/（財団）/,"")
    name = name.gsub(/（社団）/,"")
    name = name.gsub(/（総）/,"")
    #name = name.gsub(/公立/,"")
    name = name.gsub(/[^\s]+財団法人(?!($))/,"")
    name = name.gsub(/[^\s]+財団(?!($))/,"")
    name = name.gsub(/[^\s]+法人(?!($))/,"")
    #name = name.gsub(/[^\s]+大学(?!($))/,"")
    name = name.gsub(/[^\s]+生活協同組合(?!($))/,"")

    name = name.gsub(/[^\s|社会]+会(?![病院|社会保険])(?!($))/,"") #文末ではないxxxx会[\s\p{blank}|\s]
    name = name.gsub(/[^\s]+医療センター(?!($))/,"") #文末ではない医療センター
    name = name.gsub(/^学校法人(.*?)大学/,"") #xxxx法人xxx大学 文末ではない
    name = name.gsub(/[^\s]+機構(?!($))/,"")
    name = name.gsub(/^(健康保険)/,"")
    return name.gsub(/\s|\p{blank}/,"")
  end

  def self.find_dpc_hospital(name, name_org, dpc_hospital, result, index)
    unless dpc_hospital.id.present?
      target_hospitals = Hospital.where(name: name)
      if target_hospitals.count == 1
        dpc_hospital.hospital = target_hospitals[0]
      elsif target_hospitals.count == 0
        #再検索
        new_name = get_new_name(name_org)
        target_hospitals = Hospital.where(name: new_name)
        target_hospitals = Hospital.where("name like '%#{name}%'") unless target_hospitals.present?
        target_hospitals = Hospital.where("name like '%#{new_name}%'") unless target_hospitals.present?
        if target_hospitals.count == 1
          dpc_hospital.hospital = target_hospitals[0]
        elsif target_hospitals.count == 0
          result.warning_line_num += 1
          result.messages += message(index, 'WARNING', "対応する病院がありませんでした:" + name_org.to_s + "_" + new_name.to_s)
        elsif  target_hospitals.count > 1
          get_target_hospital(get_new_name(name_org),name,dpc_hospital,result,index)
        end
      elsif target_hospitals.count > 1
        get_target_hospital(get_new_name(name_org),name,dpc_hospital,result,index)
      end
    end
  end

  #(2)分析対象外としたデータの状況
  def self.import_out_of_analysis(excel)
    result = Import::ImportResult.new
    excel.sheet_info.each do |sheet_info|
      sheet = excel.sheet(sheet_info.name)
      sheet_info.start_row.upto(sheet.last_row) do |index|
        begin
          row = sheet.row(index)
          if to_code(row[0]) =~ /^\d+$/
            result.total_line_num += 1
            is_serial_number  = excel.extensions[0][0]          #通番あり？
            col01                             = sheet_info.start_column - 1
            notice_number                      = row[col01].to_i
            serial_number                      = is_serial_number && row[col01+1] =~ /^\d+$/ ? row[col01+1].to_i : nil
            if is_serial_number then
              col01 += 1
            end
            #９ヶ月のシートなら　3/4
            if sheet_info.name =~/(9|９)カ月/ then
              val = 12/9.to_f
            else
              val = 1
            end
            name                                            = row[col01+1].gsub(/[\s　]/, "")
            out_of_analysis_eceptions_num                   = row[col01+ 2].is_a?(Numeric)? (row[col01+ 2]*val).to_i : nil#integer 受付データ
            out_of_analysis_psychiatric_wards_num           = row[col01+ 3].is_a?(Numeric)? (row[col01+ 3]*val).to_i : nil #integer 精神病棟のみ
            out_of_analysis_general_wards_num               = row[col01+ 4].is_a?(Numeric)? (row[col01+ 4]*val).to_i : nil #integer 一般病棟に入院があったデータ
            out_of_analysis_duplicate_medical_record_num    = row[col01+ 5].is_a?(Numeric)? (row[col01+ 5]*val).to_i : nil #integer 診療録情報の重複提出
            out_of_analysis_stay_1day_or_less_num           = row[col01+ 6].is_a?(Numeric)? (row[col01+ 6]*val).to_i : nil  #integer 在院日数1日以下
            out_of_analysis_stay_sleepover_or_less_num      = row[col01+ 7].is_a?(Numeric)? (row[col01+ 7]*val).to_i : nil  #integer 外泊>=在院日数(在院日数外泊日数以下)
            out_of_analysis_age_under0_over120              = row[col01+ 8].is_a?(Numeric)? (row[col01+ 8]*val).to_i : nil  #integer 年齢0歳未満120歳超
            out_of_analysis_error_of_hospitalized_birth_num = row[col01+ 9].is_a?(Numeric)? (row[col01+ 9]*val).to_i : nil  #integer 入退院生年月日の誤り
            out_of_analysis_movement_other_general_ward_num = row[col01+10].is_a?(Numeric)? (row[col01+10]*val).to_i : nil  #integer 一般病棟以外の病棟との移動
            out_of_analysis_death_within_24hours            = row[col01+11].is_a?(Numeric)? (row[col01+11]*val).to_i : nil  #integer 24時間以内の死亡
            out_of_analysis_transplant_surgery_num          = row[col01+12].is_a?(Numeric)? (row[col01+12]*val).to_i : nil  #integer 移植手術あり
            out_of_analysis_no_insurance_num                = row[col01+13].is_a?(Numeric)? (row[col01+13]*val).to_i : nil  #integer 自費のみ
            out_of_analysis_not_applicable_dpc              = row[col01+14].is_a?(Numeric)? (row[col01+14]*val).to_i : nil  #integer DPC該当せず
            out_of_analysis_ad_apr_earlier_and_expect_dis_jun_to_dec_num  = row[col01+15].is_a?(Numeric)? (row[col01+15]*val).to_i  : nil #integer 4月以前入院7～12月退院以外
            out_of_analysis_trial_num                           = row[col01+16].is_a?(Numeric)? (row[col01+16]*val).to_i : nil  #integer 治験実施
            out_of_analysis_death_within_7days_after_birth_num  = row[col01+17].is_a?(Numeric)? (row[col01+17]*val).to_i : nil  #integer 生後7日以内に死亡
            out_of_analysis_stipulated_by_ministry_of_health    = row[col01+18].is_a?(Numeric)? (row[col01+18]*val).to_i : nil  #integer 厚生労働大臣が定めるもの
            out_of_analysis_analyzed_num                        = row[col01+19].is_a?(Numeric)? (row[col01+19]*val).to_i : nil  #integer 分析対象数
            out_of_analysis_analyzed_ratio                      = row[col01+20].is_a?(Numeric)? (row[col01+20]*val).to_f : nil  #decimal 分析対象該当率

            dpch = DpcHospital.find_or_initialize_by(created_year: excel.year, notice_number: notice_number, serial_number: serial_number)
            dpch.notice_number                      = notice_number
            dpch.serial_number                      = serial_number
            dpch.name                               = name
            dpch.out_of_analysis_eceptions_num                                  = out_of_analysis_eceptions_num
            dpch.out_of_analysis_psychiatric_wards_num                          = out_of_analysis_psychiatric_wards_num
            dpch.out_of_analysis_general_wards_num                              = out_of_analysis_general_wards_num
            dpch.out_of_analysis_duplicate_medical_record_num                   = out_of_analysis_duplicate_medical_record_num
            dpch.out_of_analysis_stay_1day_or_less_num                          = out_of_analysis_stay_1day_or_less_num
            dpch.out_of_analysis_stay_sleepover_or_less_num                     = out_of_analysis_stay_sleepover_or_less_num
            dpch.out_of_analysis_age_under0_over120                             = out_of_analysis_age_under0_over120
            dpch.out_of_analysis_error_of_hospitalized_birth_num                = out_of_analysis_error_of_hospitalized_birth_num
            dpch.out_of_analysis_movement_other_general_ward_num                = out_of_analysis_movement_other_general_ward_num
            dpch.out_of_analysis_death_within_24hours                           = out_of_analysis_death_within_24hours
            dpch.out_of_analysis_transplant_surgery_num                         = out_of_analysis_transplant_surgery_num
            dpch.out_of_analysis_no_insurance_num                               = out_of_analysis_no_insurance_num
            dpch.out_of_analysis_not_applicable_dpc                             = out_of_analysis_not_applicable_dpc
            dpch.out_of_analysis_ad_apr_earlier_and_expect_dis_jun_to_dec_num   = out_of_analysis_ad_apr_earlier_and_expect_dis_jun_to_dec_num
            dpch.out_of_analysis_trial_num                                      = out_of_analysis_trial_num
            dpch.out_of_analysis_death_within_7days_after_birth_num             = out_of_analysis_death_within_7days_after_birth_num
            dpch.out_of_analysis_stipulated_by_ministry_of_health               = out_of_analysis_stipulated_by_ministry_of_health
            dpch.out_of_analysis_analyzed_num                                   = out_of_analysis_analyzed_num
            dpch.out_of_analysis_analyzed_ratio                                 = out_of_analysis_analyzed_ratio

            dpch.created_year = excel.year
            dpch.save
          end
        rescue => e
          result.error_line_num += 1
          result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
        end
      end
    end
    return result
  end
  #(3)在院日数の状況
  def self.import_hospital_days(excel)
    result = Import::ImportResult.new
    excel.sheet_info.each_with_index do |sheet_info, sheet_no|
      sheet = excel.sheet(sheet_info.name)
      sheet_info.start_row.upto(sheet.last_row) do |index|
        begin
          row = sheet.row(index)
          if to_code(row[0]) =~ /^\d+$/
            result.total_line_num += 1
            col01                             = sheet_info.start_column - 1
            col02                             = excel.extensions[0][0].to_cn0  #対象列
            is_serial_number                  = excel.extensions[1][0]
            notice_number                     = row[col01].to_i
            serial_number                      = is_serial_number && row[col01+1] =~ /^\d+$/ ? row[col01+1].to_i : nil
            if is_serial_number then
              col01 += 1
            end
            name                                     = row[col01+1].gsub(/[\s　]/, "")
            hospital_days_num                        = row[col02+0].is_a?(Numeric)? row[col02+0].to_i : nil
            hospital_days_average_value              = row[col02+1].is_a?(Numeric)? row[col02+1].to_f : nil
            hospital_days_coefficient_of_variation   = row[col02+2].is_a?(Numeric)? row[col02+2].to_f : nil
            hospital_day_sminimum_vanue              = row[col02+3].is_a?(Numeric)? row[col02+3].to_i : nil
            hospital_days_percentile25               = row[col02+4].is_a?(Numeric)? row[col02+4].to_f : nil
            hospital_days_percentile50               = row[col02+5].is_a?(Numeric)? row[col02+5].to_f : nil
            hospital_days_percentile75               = row[col02+6].is_a?(Numeric)? row[col02+6].to_f : nil
            hospital_days_maximum_value              = row[col02+7].is_a?(Numeric)? row[col02+7].to_i : nil

            dpch = DpcHospital.find_or_initialize_by(created_year: excel.year, notice_number: notice_number, serial_number: serial_number)
            dpch.notice_number                      = notice_number
            dpch.serial_number                      = serial_number
            dpch.name                               = name
            dpch.hospital_days_num                  = hospital_days_num

            dpch.hospital_days_average_value            = hospital_days_average_value
            dpch.hospital_days_coefficient_of_variation = hospital_days_coefficient_of_variation
            dpch.hospital_day_sminimum_vanue            = hospital_day_sminimum_vanue
            dpch.hospital_days_percentile25             = hospital_days_percentile25
            dpch.hospital_days_percentile50             = hospital_days_percentile50
            dpch.hospital_days_percentile75             = hospital_days_percentile75
            dpch.hospital_days_maximum_value            = hospital_days_maximum_value
            self.find_dpc_hospital(name, row[col01+1], dpch, result, index)

            dpch.created_year = excel.year
            dpch.save
          end
        rescue => e
          result.error_line_num += 1
          result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
        end
      end
    end
    return result
  end
  #(8)退院先の状況　２０１５年のみ対応
  def self.import_status_after_leaving(excel)
    result = Import::ImportResult.new
    excel.sheet_info.each_with_index do |sheet_info, sheet_no|
      sheet = excel.sheet(sheet_info.name)
      sheet_info.start_row.upto(sheet.last_row) do |index|
        begin
          row = sheet.row(index)
          if to_code(row[0]) =~ /^\d+$/
            result.total_line_num += 1
            col02                               = excel.extensions[0][0].to_cn0  #データ開始列
            notice_number                      = row[0].to_i
            serial_number                      = row[1] =~ /^\d+$/ ? row[1].to_i : nil
            name                               = row[2]
            status_after_leaving_home_care_visiting_same_hospital = row[col02+0].is_a?(Numeric)? row[col02+0].to_f : nil
            status_after_leaving_home_care_visiting_other_hospital= row[col02+1].is_a?(Numeric)? row[col02+1].to_f : nil
            status_after_leaving_home_care_other                  = row[col02+2].is_a?(Numeric)? row[col02+2].to_f : nil
            status_after_leaving_changing_hospital                = row[col02+3].is_a?(Numeric)? row[col02+3].to_f : nil
            status_after_leaving_entering_nursing_home            = row[col02+4].is_a?(Numeric)? row[col02+4].to_f : nil
            status_after_leaving_entering_asissted_nursing_home   = row[col02+5].is_a?(Numeric)? row[col02+5].to_f : nil
            status_after_leaving_entering_residential_facility    = row[col02+6].is_a?(Numeric)? row[col02+6].to_f : nil
            status_after_leaving_death                            = row[col02+7].is_a?(Numeric)? row[col02+7].to_f : nil
            status_after_leaving_other_method                     = row[col02+8].is_a?(Numeric)? row[col02+8].to_f : nil

            dpch = DpcHospital.find_or_initialize_by(created_year: excel.year, notice_number: notice_number, serial_number: serial_number)
            dpch.notice_number                      = notice_number
            dpch.serial_number                      = serial_number
            dpch.name                               = name

            dpch.status_after_leaving_home_care_visiting_same_hospital  = status_after_leaving_home_care_visiting_same_hospital
            dpch.status_after_leaving_home_care_visiting_other_hospital = status_after_leaving_home_care_visiting_other_hospital
            dpch.status_after_leaving_home_care_other                   = status_after_leaving_home_care_other
            dpch.status_after_leaving_changing_hospital                 = status_after_leaving_changing_hospital
            dpch.status_after_leaving_entering_nursing_home             = status_after_leaving_entering_nursing_home
            dpch.status_after_leaving_entering_asissted_nursing_home    = status_after_leaving_entering_asissted_nursing_home
            dpch.status_after_leaving_entering_residential_facility     = status_after_leaving_entering_residential_facility
            dpch.status_after_leaving_death                             = status_after_leaving_other_method
            dpch.status_after_leaving_other_method                      = status_after_leaving_other_method
            dpch.created_year = excel.year
            dpch.save
          end
        rescue => e
          result.error_line_num += 1
          result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
        end
      end
    end
    return result
  end
  #(9)退院時転帰の状況
  def self.import_status_outcome(excel)
    result = Import::ImportResult.new
    excel.sheet_info.each_with_index do |sheet_info, sheet_no|
      sheet = excel.sheet(sheet_info.name)
      sheet_info.start_row.upto(sheet.last_row) do |index|
        begin
          row = sheet.row(index)
          if to_code(row[0]) =~ /^\d+$/
            result.total_line_num += 1
            col01                             = sheet_info.start_column - 1
            col02                             = excel.extensions[0][0].to_cn0  #対象列
            is_serial_number                  = excel.extensions[1][0]
            notice_number                     = row[col01].to_i
            serial_number                      = is_serial_number && row[col01+1] =~ /^\d+$/ ? row[col01+1].to_i : nil
            if is_serial_number then
              col01 += 1
            end
            name                               = row[col01+1].gsub(/[\s　]/, "")
            # & 数値以外ならnil
            status_outcome_healed                             = row[col02+0].is_a?(Numeric)? row[col02+0].to_f : nil#治癒
            status_outcome_improvement                        = row[col02+1].is_a?(Numeric)? row[col02+1].to_f : nil#軽快
            status_outcome_palliation                         = row[col02+2].is_a?(Numeric)? row[col02+2].to_f : nil#寛解
            status_outcome_unchanging                         = row[col02+3].is_a?(Numeric)? row[col02+3].to_f : nil#不変
            status_outcome_worse                              = row[col02+4].is_a?(Numeric)? row[col02+4].to_f : nil#憎悪
            status_outcome_death_of_illness_costed_most       = row[col02+5].is_a?(Numeric)? row[col02+5].to_f : nil#傷病による死亡
            status_outcome_death_of_other_illness_costed_most = row[col02+6].is_a?(Numeric)? row[col02+6].to_f : nil#傷病以外による死亡
            status_outcome_other                              = row[col02+7].is_a?(Numeric)? row[col02+7].to_f : nil #その他

            dpch = DpcHospital.find_or_initialize_by(created_year: excel.year, notice_number: notice_number, serial_number: serial_number)

            dpch.notice_number                      = notice_number
            dpch.serial_number                      = serial_number
            dpch.name                               = name

            dpch.status_outcome_healed                         =  status_outcome_healed
            dpch.status_outcome_improvement                    =  status_outcome_improvement
            dpch.status_outcome_palliation                     =  status_outcome_palliation
            dpch.status_outcome_unchanging                    = status_outcome_unchanging
            dpch.status_outcome_worse                         = status_outcome_worse
            dpch.status_outcome_death_of_illness_costed_most  = status_outcome_death_of_illness_costed_most
            dpch.status_outcome_death_of_other_illness_costed_most  = status_outcome_death_of_other_illness_costed_most
            dpch.status_outcome_other = status_outcome_other

            #dpch.created_year = excel.year iranai..
            dpch.save
          end
        rescue => e
          result.error_line_num += 1
          result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
        end
      end
    end
    return result
  end
  #(10)再入院の状況
  def self.import_rehospitalize(excel)
    result = Import::ImportResult.new
    excel.sheet_info.each_with_index do |sheet_info, sheet_no|
      sheet = excel.sheet(sheet_info.name)
      sheet_info.start_row.upto(sheet.last_row) do |index|
        begin
          row = sheet.row(index)
          if to_code(row[0]) =~ /^\d+$/
            result.total_line_num += 1
            col01                             = sheet_info.start_column - 1
            col02                             = excel.extensions[0][0].to_cn0  #対象列
            is_serial_number                  = excel.extensions[1][0]
            notice_number                     = row[col01].to_i
            serial_number                     = is_serial_number && row[col01+1] =~ /^\d+$/ ? row[col01+1].to_i : nil
            if is_serial_number then
              col01 += 1
            end
            name                               = row[col01+1].gsub(/[\s　]/, "")

            #再入院は計算で
            rehospitalize                         = row[col02+0].is_a?(Numeric)? row[col02+0].to_f : nil
            rehospitalize_within_6wks_same_cond   = row[col02+1].is_a?(Numeric)? row[col02+1].to_f : nil # 同一疾患での6週間以内の再入院
            rehospitalize_after_6wks_same_cond    = row[col02+2].is_a?(Numeric)? row[col02+2].to_f : nil # 同一疾患での6週間以降の再入院
            rehospitalize_within_6weeks_diff_cond = row[col02+3].is_a?(Numeric)? row[col02+3].to_f : nil # 異なる疾患での6週間以内の再入院
            rehospitalize_after_6wks_diff_cond    = row[col02+4].is_a?(Numeric)? row[col02+4].to_f : nil # 異なる疾患での6週間以降の再入院
            if !rehospitalize_within_6wks_same_cond.nil? && !rehospitalize_after_6wks_same_cond.nil? && !rehospitalize_within_6weeks_diff_cond.nil? && !rehospitalize_after_6wks_diff_cond.nil? && !rehospitalize.nil? then
              rehospitalize_calc = (rehospitalize_within_6wks_same_cond+rehospitalize_after_6wks_same_cond+rehospitalize_within_6weeks_diff_cond+rehospitalize_after_6wks_diff_cond)
              if rehospitalize.round(2) !=rehospitalize_calc.round(2) then
                if rehospitalize !=rehospitalize_calc.round(3) then
                  result.warning_line_num += 1
                  result.messages += message(index, 'WARNING', "再入院合計の計算が合いませんでした")
                end
              end
            end
            dpch = DpcHospital.find_or_initialize_by(created_year: excel.year, notice_number: notice_number, serial_number: serial_number)

            dpch.notice_number                      = notice_number
            dpch.serial_number                      = serial_number
            dpch.name                               = name

            dpch.rehospitalize_within_6wks_same_cond    = rehospitalize_within_6wks_same_cond
            dpch.rehospitalize_after_6wks_same_cond     = rehospitalize_after_6wks_same_cond
            dpch.rehospitalize_within_6weeks_diff_cond  = rehospitalize_within_6weeks_diff_cond
            dpch.rehospitalize_after_6wks_diff_cond     = rehospitalize_after_6wks_diff_cond

            #dpch.created_year = excel.year iranai..
            dpch.save
          end
        rescue => e
          result.error_line_num += 1
          result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
        end
      end
    end
    return result
  end
  #(15)手術化学療法放射線療法全身麻酔について
  def self.import_advanced_medi_care(excel)
    result = Import::ImportResult.new
    excel.sheet_info.each_with_index do |sheet_info, sheet_no|
      sheet = excel.sheet(sheet_info.name)
      sheet_info.start_row.upto(sheet.last_row) do |index|
        begin
          row = sheet.row(index)
          if to_code(row[0]) =~ /^\d+$/
            result.total_line_num += 1
            col01                             = sheet_info.start_column - 1
            col02                             = excel.extensions[0][0].to_cn0  #対象列
            is_serial_number                  = excel.extensions[1][0]    #通番
            is_nine_month                     = excel.extensions[2][0]    #9ヶ月分
            notice_number                     = row[col01].to_i
            serial_number                     = is_serial_number && row[col01+1] =~ /^\d+$/ ? row[col01+1].to_i : nil
            if is_serial_number then
              col01 += 1
            end
            name                               = row[col01+1].gsub(/[\s　]/, "")
            #９ヶ月のシートなら　3/4
            if is_nine_month then #if sheet_info.name =~/[9|９]カ月/ then
              val = 12/9.to_f
            else
              val = 1
            end
            advanced_medi_care_surgery              = row[col02+0].is_a?(Numeric)? (row[col02+0]*val).to_f : nil
            advanced_medi_care_chemotherapy         = row[col02+1].is_a?(Numeric)? (row[col02+1]*val).to_f : nil
            advanced_medi_care_radiation_therapy    = row[col02+2].is_a?(Numeric)? (row[col02+2]*val).to_f : nil
            advanced_medi_care_ambulance_transport  = row[col02+3].is_a?(Numeric)? (row[col02+3]*val).to_f : nil
            advanced_medi_care_any_of_these         = row[col02+4].is_a?(Numeric)? (row[col02+4]*val).to_f : nil
            advanced_medi_care_anesthesia           = row[col02+5].is_a?(Numeric)? (row[col02+5]*val).to_f : nil

            dpch = DpcHospital.find_or_initialize_by(created_year: excel.year, notice_number: notice_number, serial_number: serial_number)

            dpch.notice_number                      = notice_number
            dpch.serial_number                      = serial_number
            dpch.name                               = name

            dpch.advanced_medi_care_surgery             = advanced_medi_care_surgery
            dpch.advanced_medi_care_chemotherapy        = advanced_medi_care_chemotherapy
            dpch.advanced_medi_care_radiation_therapy   = advanced_medi_care_radiation_therapy
            dpch.advanced_medi_care_ambulance_transport = advanced_medi_care_ambulance_transport
            dpch.advanced_medi_care_any_of_these        = advanced_medi_care_any_of_these
            dpch.advanced_medi_care_anesthesia          = advanced_medi_care_anesthesia

            dpch.save
          end
        rescue => e
          result.error_line_num += 1
          result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
        end
      end
    end
    return result
  end
  #参考資料2 再入院再転棟_医療機関別集計
  def self.import_rehospitalization(excel)
    result = Import::ImportResult.new
    excel.sheet_info.each_with_index do |sheet_info, sheet_no|
      sheet = excel.sheet(sheet_info.name)
      sheet_info.start_row.upto(sheet.last_row) do |index|
        begin
          row = sheet.row(index)
          if to_code(row[0]) =~ /^\d+$/
            result.total_line_num += 1
            sheet_type    = excel.extensions[0][sheet_no]
            col           = excel.extensions[1][sheet_no].to_cn0


            col01                             = sheet_info.start_column - 1
            #col02                             = excel.extensions[0][0].to_cn0  #対象列
            is_serial_number                  = excel.extensions[2][0]
            notice_number                     = row[col01].to_i
            serial_number                     = is_serial_number && row[col01+1] =~ /^\d+$/ ? row[col01+1].to_i : nil
            if is_serial_number then
              col01 += 1
            end
            name                               = row[col01+1].gsub(/[\s　]/, "")
            dpch          = DpcHospital.find_or_initialize_by(created_year: excel.year, notice_number: notice_number, serial_number: serial_number)
            $stderr.print sheet_type + ":" + index.to_s + "/" + sheet.last_row.to_s + "\r"
            case sheet_type
            when "再入院転棟率" then
              altcol = row.count - col == 2 ? 1 : 2 #1 or 2つとばし?
              dpch.rehospitalization_ratio_rehospitalization = row[col + 0].is_a?(Numeric)? row[col + 0].to_f : nil
              dpch.rehospitalization_ratio_change_ward_again = row[col + altcol].is_a?(Numeric)? row[col + altcol].to_f : nil
            when "再入院事由" then
              AgByDpchXReasonMajor.reasons.each_with_index do |(key, value), i|
                reason_major = dpch.ag_by_dpch_x_reason_majors.find_or_initialize_by(reason: value)
                reason_major.same_reason      = row[col + i + 0].is_a?(Numeric)? row[col + i + 0].to_f : nil # 前回入院と同一
                reason_major.different_reason = row[col + i + 3].is_a?(Numeric)? row[col + i + 3].to_f : nil # 前回入院と異なる
                reason_major.reason = value
                reason_major.save!
              end
            when "計画的_事由" then
              reason_major = dpch.ag_by_dpch_x_reason_majors.find_or_initialize_by(reason: AgByDpchXReasonMajor.reasons[:planned])
              reason_num = row.count - col
              reason_num.times do |i|
                reason_minor = reason_major.ag_by_dpch_x_reason_minors.find_or_initialize_by(reason: i)
                reason_minor.ratio = row[col + i].is_a?(Numeric)? row[col + i].to_f : nil
                reason_minor.reason = i
                reason_minor.save!
              end
            when "予された_事由" then
              reason_major = dpch.ag_by_dpch_x_reason_majors.find_or_initialize_by(reason: AgByDpchXReasonMajor.reasons[:expected])
              reason_num = row.count - col
              reason_num.times do |i|
                reason_minor = reason_major.ag_by_dpch_x_reason_minors.find_or_initialize_by(reason: i)
                reason_minor.ratio = row[col + i].is_a?(Numeric)? row[col + i].to_f : nil
                reason_minor.reason = i
                reason_minor.save!
              end
            when "予せぬ_事由" then
              reason_major = dpch.ag_by_dpch_x_reason_majors.find_or_initialize_by(reason: AgByDpchXReasonMajor.reasons[:unexpected])
              reason_num = row.count - col
              reason_num.times do |i|
                reason_minor = reason_major.ag_by_dpch_x_reason_minors.find_or_initialize_by(reason: i)
                reason_minor.ratio = row[col + i].is_a?(Numeric)? row[col + i].to_f : nil
                reason_minor.reason = i
                reason_minor.save!
              end
            when "化学療法_MDC" then
              reason_num = row.count - col #18になるはず
              reason_num.times do |i|
                mdc_l1c = MdcLayer1Code.find_by(year: excel.year, code: (i+1).to_s.rjust(2, "0"))
                ag = dpch.ag_by_dpch_x_mdc_l1cs.find_or_initialize_by(mdc_layer1_code: mdc_l1c)
                ag.rehospitalization_chemical_treatment_rate = row[col + i].is_a?(Numeric)? row[col + i].to_f : nil
                ag.save! #親元でsave
              end
            when "化学療法以外_事由" then
              AgByDpchXReasonMajor.reasons.each_with_index do |(key, value), i|
                reason_major = dpch.ag_by_dpch_x_reason_majors.find_or_initialize_by(reason: value)
                reason_major.same_reason_nonchemo      = row[col + i + 0].is_a?(Numeric)? row[col + i + 0].to_f : nil # 前回入院と同一
                reason_major.different_reason_nonchemo = row[col + i + 3].is_a?(Numeric)? row[col + i + 3].to_f : nil # 前回入院と異なる
                reason_major.reason = value
                reason_major.save! #親元でsave
              end
            when "期間別再入院割合" then
              dpch.rehospitalization_length_of_hospital_days_within_3days       = row[col + 0].is_a?(Numeric)? row[col + 0].to_f : nil
              dpch.rehospitalization_length_of_hospital_days_within_4to7days    = row[col + 1].is_a?(Numeric)? row[col + 1].to_f : nil
              dpch.rehospitalization_length_of_hospital_days_within_8to14days   = row[col + 2].is_a?(Numeric)? row[col + 2].to_f : nil
              dpch.rehospitalization_length_of_hospital_days_within_15to28days  = row[col + 3].is_a?(Numeric)? row[col + 3].to_f : nil
              dpch.rehospitalization_length_of_hospital_days_within_29to42days  = row[col + 4].is_a?(Numeric)? row[col + 4].to_f : nil
            when "手術_MDC" then
              reason_num = row.count - col #18になるはず
              reason_num.times do |i|
                mdc_l1c = MdcLayer1Code.find_by(year: excel.year, code: (i+1).to_s.rjust(2, "0"))
                ag = dpch.ag_by_dpch_x_mdc_l1cs.find_or_initialize_by(mdc_layer1_code: mdc_l1c)
                ag.rehospitalization_surgery_rate = row[col + i].is_a?(Numeric)? row[col + i].to_f : nil
                ag.save! #親元でsave
              end
            when "回数別在院日数" then
              dpch.rehospitalization_length_of_stay_first_time  = row[col + 0].is_a?(Numeric)? row[col + 0].to_f : nil
              dpch.rehospitalization_length_of_stay_second_time = row[col + 1].is_a?(Numeric)? row[col + 1].to_f : nil
              dpch.rehospitalization_length_of_stay_third_time  = row[col + 2].is_a?(Numeric)? row[col + 2].to_f : nil
            when "再入院回数" then
              dpch.rehospitalization_times_of_hospitalization   = row[col]
            end
            dpch.created_year = excel.year
            dpch.save!
          end
        rescue => e
          result.error_line_num += 1
          result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
          p message(index, 'ERROR', e.message + e.backtrace.to_s)
        end
      end
      $stderr.print "\n"
    end
    return result
  end
end

