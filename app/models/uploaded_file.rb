class UploadedFile < ActiveRecord::Base
  mount_uploader :file_info, FileUploader
  belongs_to :parent, :polymorphic => true

  def url
    "#{self.file_info}"
  end
end
