class Link < ActiveRecord::Base
  belongs_to :followee, :class_name => "Doctor", :foreign_key => "followee_id" #リンク申請者
  belongs_to :follower, :class_name => "Doctor", :foreign_key => "follower_id" #リンク相手

  after_create :send_mail
  after_update :log_timeline
  has_paper_trail

  #リンクステータス
  enum state: [:request, :refusal, :linking]

  #フォロワーとフォロウィーは逆でもユニークであること
  #followee_id => 1 follower_id => 2 が有るならば
  #followee_id => 2 follower_id => 1 は許可しない
  validate :uniq_followee_follower
  def uniq_followee_follower
  	dup_link = Link.where(followee_id: self.follower_id).where(follower_id: self.followee_id).first
    if dup_link
      errors.add(:duplication_link, "can't be duplicate link")
    end
  end

  validate :cant_exist_same_doctor
  def cant_exist_same_doctor
    if self.follower_id == self.followee_id
      errors.add(:same_doctor_link, "can't link same doctor")
    end
  end


   def send_mail
     queue_id = "LinkRequest" + self.id.to_s
     DoctorsMailer.delay(:queue => queue_id).new_request(self.followee.id, self.followee.name, self.followee.email, self.follower.id, self.follower.name, self.follower.email)
   end


  def log_timeline
    if self.linking?
      Timeline.create(editor: self.follower, target: self.followee, model: self, action: "update")
     queue_id = "LinkApprovedRequest" + self.id.to_s
      DoctorsMailer.delay(:queue => queue_id).new_approve_request(self.followee.id, self.followee.name, self.followee.email, self.follower.id, self.follower.name, self.follower.email)
    end
  end
end
