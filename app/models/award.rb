class Award < ActiveRecord::Base
  belongs_to :doctor

  default_scope -> { order(:year).order('created_at desc') }

  #タイムライン
  after_create   :log_timeline_by_create
  after_update   :log_timeline_by_update
  before_destroy :log_timeline_by_destroy
  has_paper_trail  

  private
    def log_timeline_by_create
      Timeline.create(editor: Doctor.editor, target: self.doctor, model: self, action: "create", meta: self.title)
    end

    def log_timeline_by_update
      if self.changed?
        Timeline.create(editor: Doctor.editor, target: self.doctor, model: self, action: "update", meta: self.title)
      end
    end

    def log_timeline_by_destroy
      Timeline.create(editor: Doctor.editor, target: self.doctor, model: self, action: "destroy", meta: self.title)
    end
end
