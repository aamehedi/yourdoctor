class FavoriteGroup < ApplicationRecord
  belongs_to :doctor

  has_many :favorite_group_doctors, dependent: :destroy,
    inverse_of: :favorite_group
  has_many :doctors, through: :favorite_group_doctors
  has_many :hospitals, through: :favorite_group_doctors

  validates :name, presence: true
  validates :doctor_id, presence: true
  validates :favorite_group_doctors, length: {minimum: Settings
    .default_minimum_child}
  validates_associated :doctor, :favorite_group_doctors

  accepts_nested_attributes_for :favorite_group_doctors

  scope :with_hospitals_and_doctors, -> id{
    eager_load(favorite_group_doctors: [{doctor: :specialties}, :hospital])
      .where(id: id)
  }

  def update_favorite_group_doctors_params params
    favorite_group_doctors_attributes = []
    favorite_group_doctors_list = favorite_group_doctors.as_json(only:
      [:doctor_id, :hospital_id]).map(&:symbolize_keys)
    params.each do |favorite_group_doctor|
      favorite_group_doctors_attributes +=
        favorite_group_doctor[:doctor_ids].map do |doctor|
          {hospital_id: favorite_group_doctor[:hospital_id], doctor_id: doctor}
        end
    end
    favorite_group_doctors_attributes - favorite_group_doctors_list
  end

  def update_params params
    {
      name: params[:name] || name,
      favorite_group_doctors_attributes: update_favorite_group_doctors_params(
        params[:favorite_group_doctors_attributes])
    }
  end

  class << self
    def create_favorite_group_doctors_params params
      favorite_group_doctors_attributes = []
      params.map do |fgd|
        fgd[:doctor_ids].each do |doctor_id|
          favorite_group_doctors_attributes <<
            {hospital_id: fgd[:hospital_id], doctor_id: doctor_id}
        end
      end
      favorite_group_doctors_attributes
    end

    def create_params params, requested_doctor_id
      return unless params[:favorite_group_doctors_attributes]
      {
        name: params[:name],
        doctor_id: requested_doctor_id,
        favorite_group_doctors_attributes: create_favorite_group_doctors_params(
          params[:favorite_group_doctors_attributes])
      }
    end

    def create_favorite_group params, doctor_id
      favorite_group = FavoriteGroup.new create_params(params, doctor_id)
      favorite_group if favorite_group.save
    end
  end
end
