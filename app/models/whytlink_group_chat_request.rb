class WhytlinkGroupChatRequest < ApplicationRecord
  belongs_to :requested_doctor, class_name: Doctor.name, foreign_key: :doctor_id
  belongs_to :whytlink_group_chat_info

  enum status: [:requested, :accepted, :rejected]

  validates :status, presence: true
  validates :requested_doctor, presence: true
  validates_associated :requested_doctor
  validates :whytlink_group_chat_info, presence: true
  validate :validate_closed_group, :validate_doctor_not_joined,
    :validate_not_emergency_chatroom

  def validate_closed_group
    if whytlink_group_chat_info.opened
      errors.add :group, I18n.t("not_closed_group")
    end
  end

  def validate_doctor_not_joined
    if WhytlinkDoctorChatroom.where(doctor_id: doctor_id,
        whytlink_chatroom_id: whytlink_group_chat_info.whytlink_chatroom_id)
        .present?
      errors.add :joined, I18n.t("request_accepted")
    end
  end

  def validate_not_emergency_chatroom
    if EmergencyRequest.where(whytlink_chatroom_id:
      self.whytlink_group_chat_info.whytlink_chatroom_id).present?
      errors.add :emergency, I18n.t("emergency_chatroom")
    end
  end

  def accept_or_reject_request update_params
    return false unless self.requested?
    if update_params[:status] == "accepted"
      accept_request update_params[:doctor_id]
    else
      begin
        rejected!
      rescue
        return false
      end
    end
  end

  def add_doctor_to_whytlink_chatroom doctor_id
    WhytlinkDoctorChatroom.find_or_create_by!(whytlink_chatroom_id:
      whytlink_group_chat_info.whytlink_chatroom_id, doctor_id:
      doctor_id)
  end

  def accept_request doctor_id
    transaction do
      accepted!
      add_doctor_to_whytlink_chatroom doctor_id
      self
    end
  rescue
    return false
  end
end
