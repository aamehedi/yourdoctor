class EmergencyDoctorHospital < ApplicationRecord
  belongs_to :emergency_doctor, class_name: Doctor.name, foreign_key: :doctor_id
  belongs_to :emergency_hospital, class_name: Hospital.name,
    foreign_key: :hospital_id

  validates :emergency_doctor, presence: true
  validates :emergency_hospital, presence: true
end
