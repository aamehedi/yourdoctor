class EmergencyRequestHospital < ApplicationRecord
  belongs_to :emergency_request
  belongs_to :hospital

  has_many :doctor_feedbacks, dependent: :destroy
  has_many :doctors, through: :doctor_feedbacks

  validates :hospital, presence: true
end
