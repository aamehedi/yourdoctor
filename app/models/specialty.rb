class Specialty < ApplicationRecord
  has_many :doctor_specialties, dependent: :destroy
end
