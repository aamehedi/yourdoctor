# -*- coding: utf-8 -*-
module ImportUtil
  def equal_appro(val1, val2, error)
  	if val1.present? && val2.present?
  	  val1 + error > val2 and val1 - error < val2
  	else
  	  val1.nil? && val2.nil?
 	end
  end

  def message(line, level, text)
  	"LINE:" + line.to_s + " LEVEL:" + level.to_s +  " - " +  text.to_s + "\n"
  end

  def to_code(val)
    val.to_s.gsub(".0", "")
  end

  #文字列の共通部分を抜き出す
  def intersect(str1, str2)
    if str1.nil?
      str2
    else
      str1.split(//).zip(str2.split(//)).select{ |e| e.uniq.size==1 }.map{|e| e[0]}.join
    end
  end
end
