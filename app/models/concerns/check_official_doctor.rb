# -*- coding: utf-8 -*-
# 厚生労働省の'https://licenseif.mhlw.go.jp/search/ssl/isekikakuninTop.jsp'にアクセスして登録の可否をチェック
# チェックＯＫ場合の戻り値は　1
# チェックエラーの場合の戻り値は　-1
# 必要な引数要素は全て揃っていると想定：引数のバリデーションはなし
# モデルのDoctor.rb　のbefore_create/before_update で呼ばれることを想定　
# 要　mechanize gem :  gem 'mechanize', '~> 2.7.3'　
module CheckOfficialDoctor
  def check_official(last_name, first_name, doctor_num, doctor_registered_date, sex, birth_date)
    result_num = -1
    begin
      require 'mechanize'
      agent = Mechanize.new
      agent.user_agent_alias = 'Windows IE 9'
      agent.verify_mode = OpenSSL::SSL::VERIFY_NONE
      url = 'https://licenseif.mhlw.go.jp/search/ssl/isekikakuninTop.jsp'
      page = agent.get(url)
      man = true
      woman = false
      if sex == "female"
        man = false
        woman = true
      end
      result_page = page.form_with(:name => 'ISEKIKAKUNIN') do |form|
        form.radiobuttons[0].checked = true
        form.radiobuttons[1].checked = false
        form.radiobuttons[2].checked = man
        form.radiobuttons[3].checked = woman
        form.field_with(:name => 'name').value = "#{last_name}　#{first_name}"
        form.field_with(:name => 'birthG').value = 99
        form.field_with(:name => 'birthY').value = birth_date.year
        form.field_with(:name => 'birthM').value = birth_date.month
        form.field_with(:name => 'birthD').value = birth_date.day
        form.field_with(:name => 'torokuNo').value = doctor_num
        form.field_with(:name => 'torokuG').value = 99
        form.field_with(:name => 'torokuY').value = doctor_registered_date.year
        form.field_with(:name => 'torokuM').value = doctor_registered_date.month
        form.field_with(:name => 'torokuD').value = doctor_registered_date.day
      end.submit

      if result_page.body.toutf8.include?("検索条件の者は名簿に登録されています")
        result_num = 1
      end
    rescue Exception => e
      logger.info("CheckOfficialDoctor ERROR!!!!!!!!!!!!!!!!!!!" + e.message)
    end
    return result_num
  end
end
