class Mr < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :recoverable, :rememberable,
    :trackable, :validatable

  has_many :mr_projects, inverse_of: :mr, dependent: :destroy
  has_many :projects, through: :mr_projects
  has_many :charts, dependent: :destroy
  has_many :scenarios, dependent: :destroy
  has_many :whytplot_mr_chatrooms, dependent: :destroy
  has_many :dr_mr_chatrooms, dependent: :destroy
  belongs_to :branch
  belongs_to :company

  enum mr_type: %w(company_manager branch_manager project_manager staff)
  enum sex: %w(male female)

  validates :email, presence: true, uniqueness: true
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :company_id, presence: true
  validates :mr_type, presence: true

  accepts_nested_attributes_for :mr_projects, allow_destroy: true

  mount_uploader :avatar_string, AvatarImageUploader

  scope :mrs_except_manager_in_company, -> mr{
    where(company_id: mr.company.id).where.not mr_type: "company_manager"
  }
  scope :branches_of_mr, -> mr_id, branch_id{
    where id: mr_id, branch_id: branch_id
  }
  scope :name_cont, -> name{
    where("LOWER(first_name || ' ' || last_name) like ?", "%#{name.downcase}%")
  }

  def name
    "#{last_name} #{first_name}"
  end

  def password_reset
    _raw, enc = Devise.token_generator
      .generate self.class, :reset_password_token
    self.reset_password_token   = enc
    self.reset_password_sent_at = Time.now.utc
    save validate: false
    MrsMailer.password_reset(self).deliver
  end

  class << self
    def login mr_id, device_token
      MrSession.find_or_create_by mr_id: mr_id, device_token: device_token
    end

    def ransackable_scopes _auth_object = nil
      [:name_cont]
    end
  end
end
