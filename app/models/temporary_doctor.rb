class TemporaryDoctor < ActiveRecord::Base
  attr_accessor :password_confirmation
  validates :password, confirmation: true
  validate :doctor_email_must_be_uniqueness
  validates :email, :first_name, :last_name, presence: true
  has_paper_trail  

  after_create :log_timeline

  belongs_to :inviter, :class_name => "Doctor", :foreign_key => :doctor_id
  belongs_to :invitee, :class_name => "Doctor", :foreign_key => :estimate_doctor_id
  def doctor_email_must_be_uniqueness
  	if Doctor.find_by_email(self.email)
  	  errors.add(:email, :already_registered)
  	end
  end

  private
    def log_timeline
      if self.inviter
        invitee = self.invitee ? self.invitee : nil
        Timeline.create(editor: self.inviter, target: invitee, model: self, action: "create", meta: self.last_name + " " + self.first_name)
      else
        Timeline.create(editor: Doctor.editor, target: nil, model: self, action: "create", read_level: Timeline.read_levels[:staff])
      end
    end
end
