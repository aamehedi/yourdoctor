class MdcLayer2 < ActiveRecord::Base
  belongs_to :mdc_layer1
  has_many :mdc_layer3s
  has_many :ag_by_dpch_x_mdc_l2s
  has_many :ag_by_dpch_x_mdc_l2_treatments
end
