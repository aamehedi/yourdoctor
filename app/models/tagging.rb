class Tagging < ActiveRecord::Base
  belongs_to :taggable,   :polymorphic => true
  belongs_to :tagger,     :polymorphic => true
  belongs_to :tag,        :polymorphic => true
end

