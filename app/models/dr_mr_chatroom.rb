class DrMrChatroom < ApplicationRecord
  belongs_to :doctor
  belongs_to :mr
  has_many :dr_mr_messages, dependent: :destroy

  enum status: [:requested, :accepted, :rejected, :canceled]

  validates :doctor, presence: true
  validates :mr, presence: true
  validates :status, presence: true

  ALLOWED_PARAMS = [:id, :last_seen_message_id_dr, :last_seen_message_id_mr]

  def last_message_id
    DrMrMessage.where(dr_mr_chatroom_id: self.id).last.try :id
  end

  def broadcast_chatroom_info
    ActionCable.server.broadcast "dr_mr_rooms_info", self
  end

  def update_chatroom whitelisted_params, dr_or_mr_id, from_dr
    return self if
      from_dr &&
      dr_or_mr_id == doctor_id &&
      update_attributes(whitelisted_params.except(:doctor_id, :favorite))

    update_chatroom_by_mr whitelisted_params, dr_or_mr_id, from_dr
  end

  class << self
    def get_filtered_dr_mr_chatrooms params, dr_or_mr_id, from_dr
      return DrMrChatroom.where(
        doctor_id: dr_or_mr_id, status: params[:status]) if from_dr
      DrMrChatroom.where(mr_id: dr_or_mr_id, favorite: params[:favorite]) if
        params.key? :favorite
    end
  end

  class << self
    def update_dr_mr_chatroom data
      dr_mr_chatroom = DrMrChatroom.find_by_id data["id"]
      return unless dr_mr_chatroom
      dr_mr_chatroom_params = data.select do |key|
        ALLOWED_PARAMS.include? key.to_sym
      end
      if dr_mr_chatroom.update_attributes dr_mr_chatroom_params
        ActionCable.server.broadcast(
          "dr_mr_room_#{dr_mr_chatroom.id}", dr_mr_chatroom)
      end
    end
  end

  private
  def update_chatroom_by_mr whitelisted_params, dr_or_mr_id, from_dr
    if !from_dr && dr_or_mr_id == mr_id
      custom_params = whitelisted_params
      custom_params = whitelisted_params.except(:status) unless
          self.class.statuses.except(:accepted, :rejected).values
              .include? whitelisted_params[:status]
      self if update_attributes(custom_params)
    end
  end
end
