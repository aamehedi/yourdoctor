class Chart < ApplicationRecord
  belongs_to :mr
  has_many :scenario_charts, dependent: :destroy
  has_many :scenarios, through: :scenario_charts

  validates :name, presence: true
  validates :chart_type, presence: true
  validates :mr_id, presence: true

  class << self
    def retrieved_by mr, params, search_params
      limit = params[:limit] || Settings.chart.default_limit
      last_id = params[:last_id] || Chart.last.id + Settings.last_id_offset
      if search_params && params[:last_id]
        where("mr_id = ? AND id < ?", mr.id, last_id).order("id DESC")
          .limit(limit).search(search_params.to_h).result
      elsif search_params
        mr.charts.search(search_params.to_h).result
      else
        where("mr_id = ? AND id < ?", mr.id, last_id).order("id DESC")
          .limit limit
      end
    end
  end
end
