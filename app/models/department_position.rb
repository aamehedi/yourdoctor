class DepartmentPosition < ActiveRecord::Base
  belongs_to :department
  has_many :department_position_lists, dependent: :destroy
  has_many :department_lists, through: :department_position_lists
  #ドクターが診療科と役職を同時に新規追加する場合、役職を登録する段階で診療科をまだ作成していないこともある
  #validates :department_id, :name, presence: true
  validates_length_of :name, allow_blank: false  
end
