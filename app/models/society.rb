class Society < ActiveRecord::Base
  has_many :society_lists
  has_many :doctors, through: :society_lists
  has_many :society_positions, dependent: :destroy
  has_many :whytlink_group_chat_infos, as: :affliation

  validates :name, presence: true  


  def merge(src_society)
    #society_listを繋ぎ替え
    src_society.society_lists.each do | society_list |
      #同じ医師のsociety_listがある場合は
      dup_society_list = self.society_lists.where(doctor_id: society_list.doctor_id).first
      #そこにぶら下がるposition_listをマージする
      if dup_society_list
        society_list.position_lists.each do | position_list | 
          #同じ名前の役職が存在するなら重複とみなし
          #position_listは削除する
          #postionは他にだれも参照していなければ削除する
          if dup_society_list.positions.where(name: position_list.position.name).first
            position = position_list.position
            SocietyPositionList.delete(position_list.id)
            position = SocietyPosition.find(position.id)
            if position.society_position_lists.length == 0
              SocietyPosition.delete(position.id)
            end
          #存在しなければ別ものとみなし繋ぎ換える
          else
            position = position_list.position
            position.update(society_id: self.id)
            position_list.update(society_list_id: dup_society_list.id)
          end
        end
        SocietyList.delete(society_list.id)
      #同じ医師のsociety_listが無い場合は新規とみなしつなぎ先を変える
      else
        society_list.update(society_id: self.id)
      end
    end

    #上記position_list繋ぎ替えの際もpositionの繋ぎ替えを行っているが
    #誰も属していないpositionの繋ぎ替えが漏れる可能性があるのでここでも繋ぎかえる
    src_society.society_positions.each do | position | 
      if self.society_positions.where(name: position.name).first
        position.destroy
      else
        position.update(society_id: self.id)
      end
    end

    #マージ元削除
    Society.delete(src_society.id)
  end
end
