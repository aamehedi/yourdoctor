class Staff < User
  belongs_to :leader, :class_name => "Leader", :foreign_key => "user_id"

  def is_active?
    !self.suspended && self.leader && !self.leader.suspended
  end
end
