class Icd10 < ActiveRecord::Base
  belongs_to :parent,   :class_name => 'Icd10', :foreign_key => 'icd10_id'
  has_many   :children, :class_name => 'Icd10', :foreign_key => 'icd10_id', :dependent => :destroy
  has_one    :tagging, as: :tag

  #自分の所属するグループ
  #第5階層の場合は中項目グループである3階層目を返す
  #他は自分の親グループを返す
  def group
    if self.depth == 5
      group_code = self.code[0, 3]
      Icd10.where(depth: 3).where(code: group_code).first
    elsif self.depth == 3
      self  
    else
      self.parent
    end
  end

  #3〜5は3階層目を返す それ以外はnil
  def group_lv3
    case self.depth
    when 1
      nil
    when 2
      nil
    when 3
      p "DEPTH3:" + self.to_s
      self
    when 4
      p "DEPTH4:" + self.parent.to_s
      self.parent
    when 5
      group_code = self.code[0, 3]
      p "DEPTH5:" + group_code
      Icd10.where(depth: 3).where(code: group_code).first
    else
      nil
    end
  end
end
