class StaffLog < ActiveRecord::Base
  belongs_to :user
  belongs_to :staff_logs, polymorphic: true
end
