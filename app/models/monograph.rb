# -*- coding: utf-8 -*-
class Monograph < ActiveRecord::Base
  belongs_to :doctor
  paginates_per 20
  # default_scope -> { order(:year).order('created_at desc') }
  has_paper_trail
  acts_as_taggable

  #タイムライン
  after_create   :log_timeline_by_create
  after_update   :log_timeline_by_update
  before_destroy :log_timeline_by_destroy

  attr_accessor :no_timeline #タイムライン不要

  def get_page_num
    position = Monograph.where(doctor_id: self.doctor.id).where("published > ?", self.published).count
    position / 20 + 1
  end

  private
    def log_timeline_by_create
      return if no_timeline
      Timeline.create(editor: Doctor.editor, target: self.doctor, model: self, action: "create", meta: self.title)
    end

    def log_timeline_by_update
      return if no_timeline
      if self.changed?
        Timeline.create(editor: Doctor.editor, target: self.doctor, model: self, action: "update", meta: self.title)
      end
    end

    def log_timeline_by_destroy
      return if no_timeline
      Timeline.create(editor: Doctor.editor, target: self.doctor, model: self, action: "destroy", meta: self.title)
    end
end
