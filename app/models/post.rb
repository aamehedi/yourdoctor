class Post < ActiveRecord::Base
  belongs_to :doctor
  belongs_to :placeholder

  has_one  :timeline, as: :model
  has_many :taggings, as: :taggable, dependent: :destroy
  accepts_nested_attributes_for :taggings, allow_destroy: :true
  has_many :icd10s, through: :taggings, source: :tag, source_type: "Icd10"
  has_many :uploaded_files, as: :parent, dependent: :destroy
  has_many :shines, as: :parent, dependent: :destroy
  has_many :comments, dependent: :destroy

  attr_accessor :imgs
  after_create  :set_imgs, :log_timeline_by_create

  def set_imgs
    if self.imgs
      self.imgs.each do | img_id |
        UploadedFile.find(img_id).update_attributes(parent_id: self.id, parent_type: "Post")
      end
    end
  end

  #投稿テスト用
  def images
    self.uploaded_files.map{ |uf| uf.url }
    #[{id: "1", url: "https://www.whytlink.com/images/default_avatar.jpg"}, {id: "2", url: "https://www.whytlink.com/images/wl_ui_bgv01.jpg"}, {id: "3", url: "yD_hajimaru.png"}]
    #["https://www.whytlink.com/images/default_avatar.jpg", "https://www.whytlink.com/images/wl_ui_bgv01.jpg", "https://www.whytlink.com/images/yD_hajimaru.png"]
  end

  #acts_as_taggable_on :labels # post.label_list が追加される
  #acts_as_taggable            # acts_as_taggable_on :tags のエイリアス

  #タイムライン


  private
    def log_timeline_by_create
      Timeline.create(editor: Doctor.editor, target: nil, model: self, action: "create", read_level: self.read_level, no_series: true)
    end
end
