# -*- coding: utf-8 -*-
require 'aws-sdk-resources'
require 'rubygems'
require 'zip'

class WhytPlotTableView < ActiveRecord::Base
  after_create :create_view
  after_update :create_view
  before_update :drop_view_if_edited
  after_destroy :drop_view
  validates :name, presence: true, :uniqueness => true,
            format: {
              with: /\A(?!.*(delete|truncate|drop|;)).+\z/m,
              message: "'%{value}' 入力なしまたは入力された文字列が無効です"
            }
  validates :create_view_sql, presence: true,
            format: {
              with: /\A(?!.*(delete|truncate|drop|;)).+\z/m,
              message: "'%{value}' 入力なしまたは入力された文字列が無効です"
            }
  validate :name_must_be_same_view_name
  default_scope ->{ order('id') }

  def self.upload_db
    files = Dir.glob("tmp/datafiles/*.sqlite3")
    files.each do |file|
      zip_path = self.create_zip(file)
      # TODO リリース時までコメントアウト(開発で必要な場合を除く)
      filename = File.basename(zip_path)
      s3 = Aws::S3::Resource.new(region: 'ap-northeast-1')
      obj = s3.bucket('rw-rails-data').object("whytplot-db/#{filename}")
      obj.upload_file(zip_path)

      if File.exist?(zip_path)
        File.delete(zip_path)
      end
    end
  end

  def self.create_zip(file_path)
    zip_path = "#{Rails.root}/tmp/datafiles/#{File.basename(file_path)}.zip"
    File.unlink zip_path if File.file?(zip_path)
    Zip::File.open(zip_path, Zip::File::CREATE) do |z_fp|
      z_fp.add(File.basename(file_path), file_path)
    end
    if zip_path.present?
      File.unlink file_path
      zip_path
    end

  end

  def self.get_whytplot_data(timestamp, user)
    # wp_bucket = Aws::S3::Resource.new(region: 'ap-northeast-1').bucket('rw-rails-data')
    # address@reasonwhy.jpの場合テストデータ返す
    if user.email =~ /^.+@reasonwhy.jp$/
      file_name = "#{timestamp}_beta.sqlite3.zip"
    else
      file_name = "#{timestamp}.sqlite3.zip"
    end
    s3 = Aws::S3::Client.new(region: 'ap-northeast-1')
    begin
      file = s3.get_object(
        response_target: "#{Rails.root}/tmp/datafiles/#{file_name}",
        bucket: "rw-rails-data",
        key: "whytplot-db/#{file_name}"
      )
    rescue => e
      p e
      p "該当のファイルがありません"
    end
    file_name
  end

  def self.get_whytplot_sqlite_files
    s3 = Aws::S3::Resource.new(region: 'ap-northeast-1').bucket('rw-rails-data')
    client = Aws::S3::Client.new(region: 'ap-northeast-1', signature_version: 'v4')
    signer = Aws::S3::Presigner.new(client: client)
    files = Array.new
    beta_files = Array.new
    res = s3.objects(prefix: 'whytplot-db/').map { |obj|
      if obj.key =~ /.+[0-9]{10}_beta.sqlite3.zip$/
        beta_files << obj.key
      else
        files << obj.key
      end
    }
    latest_beta_file = beta_files.compact.last
    latest_beta_file_url =
      signer.presigned_url(:get_object, bucket: "rw-rails-data", key: latest_beta_file)
    latest_file = files.compact.last
    latest_file_url =
      signer.presigned_url(:get_object, bucket: "rw-rails-data", key: latest_file)

    { latest_beta_file => latest_beta_file_url, latest_file => latest_file_url }
  end

  def self.get_latest_version(user)
    wp_bucket = Aws::S3::Resource.new(region: 'ap-northeast-1').bucket('rw-rails-data')
    # address@reasonwhy.jpの場合テストデータ返す
    if user.email =~ /^.+@reasonwhy.jp$/
      objs = wp_bucket.objects(prefix: 'whytplot-db/').map { |obj|
        obj.key =~ /^.+[0-9]{10}_beta.sqlite3.zip$/ ? obj : nil }.compact.last
    else
      objs = wp_bucket.objects(prefix: 'whytplot-db/').map { |obj|
        obj.key =~ /^.+[0-9]{10}.sqlite3.zip$/ ? obj : nil }.compact.last
    end
    latest_version = objs.key =~ /[0-9]{10}/ ? $& : nil
  end

  private

    def drop_view
      #viewの削除
      query = <<SQL
DROP VIEW IF EXISTS #{self.name};
SQL
      ActiveRecord::Base.connection.execute(query)
    end

    def drop_view_if_edited
      table_view = WhytPlotTableView.find_by_id(self.id)
      query = <<SQL
DROP VIEW IF EXISTS #{table_view.name};
SQL
      begin
        ActiveRecord::Base.connection.execute(query)
      rescue => e
        p e
      end
    end

    def create_view
      ActiveRecord::Base.connection.execute(self.create_view_sql)
    end

    def name_must_be_same_view_name
      unless create_view_sql.match(/VIEW #{name} AS/i)
        errors.add(:name, "エラー: 作成するビューと同じ名前である必要があります")
        errors.add(:create_view_sql, "エラー: ビューネームはnameカラムと同じ名前である必要があります")
      end
    end

end
