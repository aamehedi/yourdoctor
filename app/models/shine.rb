class Shine < ActiveRecord::Base
  belongs_to :parent, :polymorphic => true
  belongs_to :doctor
  after_create :send_msg
  def total_point
    self.parent.shines.count
  end

  def send_msg
    p "AAAAAAAAAAvvvv!!!!!!"
    if self.parent.doctor.email_sendable? == true
      p "VVVVVV!!!!!!"
      queue_id = "Shine" + self.id.to_s
      DoctorsMailer.delay(:queue => queue_id).new_shine(self.parent.doctor, self.doctor, self.parent)
    end
  end

end
