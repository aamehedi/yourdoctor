# -*- coding: utf-8 -*-
class DuplicateDoctorList < ActiveRecord::Base
  #has_many :doctors
  #def doctor_name
  #	doctor = Doctor.find(self.doctor_ids[0])
  #	[doctor.first_name.to_s, doctor_last_name.to_s]
  #end

  has_many :staff_logs, as: :staff_log

  def merge(src_id, dst_id)
    dst_doctor = Doctor.find(dst_id)
    src_doctor = Doctor.find(src_id)
    dst_doctor.merge(src_doctor)

    #マージ完了
    self.doctor_ids.delete(src_id)
    self.merged_ids = Array.new if self.merged_ids.nil?
    self.merged_ids.push(src_id)    
    self.save
  end

  def doctor_first_name
  	doctor = Doctor.find(self.doctor_ids[0])
  	doctor.first_name.to_s
  end

  def doctor_last_name
  	doctor = Doctor.find(self.doctor_ids[0])
  	doctor.last_name.to_s
  end

  def doctors
    Doctor.where(id: self.doctor_ids)
  end
end
