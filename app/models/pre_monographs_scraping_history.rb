class PreMonographsScrapingHistory < ActiveRecord::Base
  belongs_to :doctor
  belongs_to :created_by, :class_name => 'Doctor'
end
