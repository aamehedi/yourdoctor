# -*- coding: utf-8 -*-
class Timeline < ActiveRecord::Base
  belongs_to :editor, :class_name => "Doctor", :foreign_key => :editor_id
  belongs_to :target, :class_name => "Doctor", :foreign_key => :doctor_id
  belongs_to :model, :polymorphic => true
  default_scope -> { where("series <= 0").order('updated_at desc') }

  #閲覧レベル read_level
  #all_drs:  全員
  #relation: 関連する人まで　 (スタッフ、本人、知り合い、所属関連)
  #links:    リンクする人まで (スタッフ、本人、知り合い)
  #subject:  本人まで        (スタッフ、本人)
  #staff:    スタッフのみ　　 (スタッフ)
  enum read_level: [:all_drs, :related, :linked, :subject, :staff]

  before_save :collect

  attr_accessor :relation_type
  attr_accessor :no_series

  def self.create(args={})
    timeline = Timeline.new

    timeline.set_editor(args[:editor])
    timeline.set_target(args[:target])
    timeline.model_type = args[:model].class.to_s if args[:model]
    timeline.model_id   = args[:model].id         if args[:model]
    timeline.model_type = args[:model_type]       if args[:model_type]
    timeline.model_id   = args[:model_id]         if args[:model_id]
    timeline.read_level = args[:read_level]       if args[:read_level]

    #editor = timeline.editor
    editor = Doctor.unscoped.find_by(id: timeline.editor_id)
    if editor && editor.is_staff
      timeline.read_level = Timeline.read_levels[:staff]
    end

    timeline.action     = args[:action]
    timeline.meta       = args[:meta]
    timeline.no_series  = args[:no_series] if args[:no_series]
    timeline.updated_at = Time.now
    timeline.save
  end

  def set_editor(src)
    return if src.nil?
  	self.editor_id              = src.id
  	self.editor_first_name      = src.first_name
    self.editor_last_name       = src.last_name
    self.editor_hospital_name   = src.department_lists.order(:index).first.department.hospital.name if src.department_lists.count > 0
    self.editor_hospital_id     = src.department_lists.order(:index).first.department.hospital.id   if src.department_lists.count > 0
    self.editor_university_name = src.university_lists.first.university.name            if src.university_lists.count > 0
    self.editor_university_id   = src.university_lists.first.university.id              if src.university_lists.count > 0
    self.editor_graduation_year = src.university_lists.first.graduation_year if src.university_lists.count > 0
  end

  def set_target(src)
    return if src.nil?
  	self.doctor_id              = src.id
  	self.doctor_first_name      = src.first_name
    self.doctor_last_name       = src.last_name
    self.doctor_hospital_name   = src.department_lists.order(:index).first.department.hospital.name if src.department_lists.count > 0
    self.doctor_hospital_id     = src.department_lists.order(:index).first.department.hospital.id   if src.department_lists.count > 0
    self.doctor_university_name = src.university_lists.first.university.name            if src.university_lists.count > 0
    self.doctor_university_id   = src.university_lists.first.university.id              if src.university_lists.count > 0
    self.doctor_graduation_year = src.university_lists.first.graduation_year if src.university_lists.count > 0
    self.paper_trail_id = src.versions.last.id if src.versions.count > 0
  end

  def collect
    if self.no_series
      return
    end

    if self.series == 0
      return
    end

    old_timeline = Timeline.where(editor_id: self.editor_id).where(doctor_id: self.doctor_id).where(model_type: self.model_type).order("updated_at desc").first
    unless old_timeline
      return
    end

    #先頭がdestroyの場合はシリーズ化しない(destroyはそもそも表示しないので)
    if old_timeline.series == -1 && old_timeline.action == "destroy"
      return
    end
    #一つ前のタイムラインが同一model_typeの操作だった場合シリーズ化する
    if old_timeline.updated_at >= 30.minutes.ago
      if old_timeline.series < 0
        old_timeline.series = 0
        old_timeline.save
      end
      self.series = old_timeline.series + 1
    end
=begin
    #一つ前のタイムラインで同じことをやってたらそれを削除する
    if old_timeline && old_timeline.doctor_id  == self.doctor_id  && \
       old_timeline.model_type == self.model_type && \
       old_timeline.model_id   == self.model_id   && \
       old_timeline.action     == self.action
       old_timeline.destroy
    end
=end
  end
end
