require 'csv'

class Address < ActiveRecord::Base
  belongs_to :prefecture

  extend ImportUtil

  def self.import(filepath)
    result = Import::ImportResult.new
    lines = CSV.read(filepath, encoding: 'SJIS:UTF-8')
    lines.each_with_index do |row, index|
      result.total_line_num += 1
      next unless row.length > 0

      address_details = row
      prefecture = Prefecture.find_by(name: address_details[6])
      prefecture_id = prefecture.id
      unless prefecture_id.present?
        result.warning_line_num += 1
        result.messages += message(index, 'ERROR', '該当のprefecture_idがありません')
        next
      end

      case address_details[7]
      when /^(市川市|市原市|野々市市|四日市市|廿日市市)$/ then
        city1 = address_details[7]
      when /^(余市郡|芳賀郡|中新川郡|西八代郡|神崎郡|吉野郡|高市郡)/, /(郡|市)/ then
        city1 = $` + $&
        city2 = $'
      else
        city1 = address_details[7]
      end

      city              = address_details[7]
      city_kana         = address_details[4]
      town              = address_details[8]
      town_kana         = address_details[5]
      postal_code       = address_details[2]
      municipality_id   = address_details[0]
      city1             = city1
      city2             = city2
      full_address      = address_details[7] + address_details[8]
      full_address_kana = address_details[4] + address_details[5]
      prefecture_id     = prefecture_id

      begin
        # 郵便番号は必ず1つの町名に紐づいているわけではない
        # ex. 郵便番号 950-0000
        # 新潟県 新潟市北区, 新潟市東区, 新潟市中央区, 新潟市江南区, 新潟市西区, 新潟市西蒲区
        address = Address.find_or_initialize_by(postal_code: postal_code, town: town)
        address.city              = city
        address.city_kana         = city_kana
        address.town_kana         = town_kana
        address.city1             = city1
        address.city2             = city2
        address.full_address      = full_address
        address.full_address_kana = full_address_kana
        address.prefecture_id     = prefecture_id
        address.municipality_id   = municipality_id
        address.save
      rescue => e
        p e
        result.error_line_num += 1
        result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
      end
    end
    #municipality : hospital紐付け
    begin
      municipalities = Municipality.where.not(city_name:nil).select("id")
      municipalities.each do |municipality|
        address = Address.where(municipality_id:  municipality.id).select("CAST(postal_code AS integer)")
        if address.count != 0
          hospitals = Hospital.where("postal_code IN(?)", address)
          hospitals.each do |hospital|
            hospital.update_column(:municipality_id, municipality.id)
          end
        end
      end
    rescue => e
      p e
      result.error_line_num += 1
      result.messages += message('municipality_id Update', 'ERROR', e.message + e.backtrace.to_s)
    end
    return result
  end

end
