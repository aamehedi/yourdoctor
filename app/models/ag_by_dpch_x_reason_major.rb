class AgByDpchXReasonMajor < ActiveRecord::Base
  #belongs_to :reason_for_rehospitalization
  belongs_to :dpc_hospital
  has_many   :ag_by_dpch_x_reason_minors, foreign_key: :parent_id

  enum reason: { planned: 1, expected: 2, unexpected: 3 }
end
