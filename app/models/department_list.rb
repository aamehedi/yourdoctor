class DepartmentList < ActiveRecord::Base
  belongs_to :doctor
  has_many :position_lists, :class_name => 'DepartmentPositionList', :foreign_key => :department_list_id, :dependent => :destroy
  accepts_nested_attributes_for :position_lists, allow_destroy: :true
  has_many :positions, through: :position_lists, :source => :position, :class_name => 'DepartmentPosition'
  belongs_to :department
  validates :department_id, presence: true
  has_paper_trail

  default_scope -> { order(:index).order('created_at desc') }

  #タイムライン
  after_create   :log_timeline_by_create
  after_update   :log_timeline_by_update
  before_destroy :log_timeline_by_destroy

  before_save do
    if self.active
      self.term_end = nil
    end
  end

  # accepts_nested_attributes_for :department
  # department_list経由でマスタであるdepartment自体は変更させない。但し新規追加はできる。
  #  -accept_nestedはしない
  #  -idのないdepartment_attributesがある場合は新規でdepartment,hospitalを作成する
  def department_attributes=(attributes)
    p "******* department_attributes *******"
    hospital_attributes = attributes[:hospital_attributes]
    #病院情報がない場合は特に何もしない
    return if hospital_attributes.nil?
    return if hospital_attributes[:prefecture_id].blank?

    #IDが無い、存在しないIDが指定されている場合は同病院、同診療科を探すか、なければ新規追加
  	if attributes[:id].nil? || Department.find_by(id: attributes[:id]).nil?
      #IDが無い、存在しないIDが指定されている場合は同地域、同名の病院を探すか、なければ新規追加
      if hospital_attributes[:id].nil? || Hospital.find_by(id: hospital_attributes[:id]).nil?
        hospital = Hospital.find_or_create_by(name:          hospital_attributes[:name],
                                              prefecture_id: hospital_attributes[:prefecture_id])
        new_hospital_id = hospital.id
      else
      	new_hospital_id = hospital_attributes[:id]
      end
      department = Department.find_or_create_by(name: attributes[:name], hospital_id: new_hospital_id)
      new_department_id = department.id
    else
      new_department_id = attributes[:id]
  	end
    self.department_id = new_department_id
  end

  private
    def log_timeline_by_create
      Timeline.create(editor: Doctor.editor, target: self.doctor, model: self, action: "create", meta: self.department.hospital.name + " " + self.department.name)
    end

    def log_timeline_by_update
      if self.changed?
        Timeline.create(editor: Doctor.editor, target: self.doctor, model: self, action: "update", meta: self.department.hospital.name + " " + self.department.name)
      end
    end

    def log_timeline_by_destroy
      Timeline.create(editor: Doctor.editor, target: self.doctor, model: self, action: "destroy", meta: self.department.hospital.name + " " + self.department.name)
    end
end
