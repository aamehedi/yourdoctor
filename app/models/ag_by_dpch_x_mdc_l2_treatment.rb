class AgByDpchXMdcL2Treatment < ActiveRecord::Base
  belongs_to :dpc_hospital
  belongs_to :mdc_layer2
  validates :label, presence: true
  validates :dpc_hospital_id, presence: true
  validates :mdc_layer2_id, presence: true

  extend ImportUtil
  #(9) (10)疾患別手術有無別処置1,2有無別集計
  def self.check_mdc2_master(mdc_list, mdc_layer1_id, mdc1_code, mdc2_code, result, index)
    key = mdc_list, mdc_layer1_id, mdc2_code.to_s + "_" + mdc2_code.to_s
    if mdc_list.has_key?(key)
      unless mdc_list[key]
        return nil
      else
        return mdc_list[key]
      end
    else
      mdc_l2 = MdcLayer2.where(mdc_layer1_id: mdc_layer1_id, code: mdc2_code)
      if mdc_l2.count == 0
        mdc_list[key] = nil
        result.warning_line_num += 1
        result.messages += message(index, 'ERROR', "対応するDPCコードがありません。先にMDCマスタを作成する必要があります。DPC:" + mdc1_code + " 術式:" + mdc2_code.to_s)
        return nil
      elsif mdc_l2.count == 1
        mdc_list[key] = mdc_l2
        return mdc_l2
      end
    end
  end

  def self.check_mdc_master(mdc_list, mdc_code, year, result, index)
    if mdc_list.has_key?(mdc_code)
      unless mdc_list[mdc_code]
        return nil
      else
        return mdc_list[mdc_code]
      end
    else
      mdc_l1 = MdcLayer1.name_code(mdc_code, year)
      if mdc_l1.count == 0
        mdc_list[mdc_code] = nil
        #p "error message " + mdc_code.to_s
        result.warning_line_num += 1
        result.messages += message(index, 'ERROR', "対応するMDCコードがありません。先にMDCマスタを作成する必要があります。DPC:" + mdc_code.to_s)
        return nil
      elsif mdc_l1.count == 1
        mdc_list[mdc_code] = mdc_l1
        return mdc_l1
      end
    end
  end

  def self.import(excel)
    result = Import::ImportResult.new

    mdc_code_list = Array.new #各mdc_codeを格納する
    count_list    = Array.new #件数の始めのカラム番号を格納する
    stay_day_list = Array.new #在院日数の始めのカラム番号を格納する
    mdc_list = {}
    sheetName = excel.book.sheets[0]
    sheet = excel.sheet(sheetName)
    sheet.first_row.upto(sheet.last_row) do |index|
      begin
        sheet.first_column.upto(sheet.last_column) do |column|
          #mdc_code_listを作成するためのロジック
          if index == 1
            case sheet.cell(1, column)
            when /[0-9]{4,}/
              mdc_code_list << sheet.cell(1, column)
            end
          #件数と在院日数の何列目かを判定するロジック
          elsif index == 3
            case sheet.cell(3, column)
            when /件数/ then
              count_list << column
            when /在院日数/ then
              stay_day_list << column
            end
          end
        end
        #ここから各行のデータを作成していく
        #ヘッダーは飛ばすため
        if index >= excel.sheet_info[0].start_row && (sheet.cell(index, 1) =~ /^\d+$/ || sheet.cell(index, 1).is_a?(Numeric))
          is_serial_number  = excel.extensions[0][0] #通番あり？
          col01             = excel.sheet_info[0].start_column - 1
          notice_number     = sheet.cell(index, 1).to_i
          serial_number     = is_serial_number && sheet.cell(index, 2) =~ /^\d+$/ ? sheet.cell(index, 2).to_i : nil
          if is_serial_number then
            col01 += 1
          end
          hospital_name     = sheet.cell(index, col01 + 2)
          p notice_number.to_s + ":" + serial_number.to_s + ":" + hospital_name
          dpc_hospital = DpcHospital.find_by(created_year: excel.year, notice_number: notice_number, serial_number: serial_number)
          unless dpc_hospital.present?
            result.warning_line_num += 1
            result.messages += message(index, 'WARNING', "対応する病院がありませんでした:" + hospital_name)
            next
          end
          mdc_code_list.count.times do |column_num|
            #mdcのカラム数を格納する(ex, 99,97,97（輸血以外の再掲）,01の場合は4つになる)
            mdc_column_num = stay_day_list[column_num] - count_list[column_num]
            #二度同じエラーを出さない
            unless mdc_l1 = check_mdc_master(mdc_list, mdc_code_list[column_num], excel.year, result, index)
              next
            end
            mdc_column_num.times do |num|
              mdc2_code = ( num+1 <= mdc_column_num/2 ) ? "97" : "99"
              unless mdc_l2 = check_mdc2_master(mdc_list, mdc_l1[0].id, mdc_code_list[column_num], mdc2_code, result, index)
                next
              end
              label = sheet.cell(5, count_list[column_num]+num)
              number    = sheet.cell(index, count_list[column_num]+num).is_a?(Numeric) ? sheet.cell(index, count_list[column_num]+num).to_i : nil
              stay_days = sheet.cell(index, stay_day_list[column_num]+num).is_a?(Float) ? sheet.cell(index, stay_day_list[column_num]+num).to_f : nil
              treatmented = ( num+1 <= mdc_column_num/2 ) ? true : false ; # 手術ありtrue, 手術なしfalse

              unless number.present? && stay_days.present?
                next
              end

              if /処置1/ =~ label then
                ag_by_dpch_x_mdc_l2_treatments = Treatment1.find_or_initialize_by(
                  label: label,
                  treatmented: treatmented,
                  dpc_hospital_id: dpc_hospital.id,
                  mdc_layer2_id: mdc_l2[0].id,
                  mdc_layer1_id: mdc_l1[0].id
                )
              elsif /処置2/ =~ label then
                ag_by_dpch_x_mdc_l2_treatments = Treatment2.find_or_initialize_by(
                  label: label,
                  treatmented: treatmented,
                  dpc_hospital_id: dpc_hospital.id,
                  mdc_layer2_id: mdc_l2[0].id
                )
              else
                result.warning_line_num += 1
                result.messages += message(index, 'WARNING', "処置１、２以外が見つかりました。")
                next
              end

              ag_by_dpch_x_mdc_l2_treatments.number = number
              ag_by_dpch_x_mdc_l2_treatments.days = stay_days
              ag_by_dpch_x_mdc_l2_treatments.save
              result.total_line_num += 1
            end
          end
        end
      rescue => e
        result.error_line_num += 1
        result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
      end

    end
    return result
  end
end

class Treatment1 < AgByDpchXMdcL2Treatment
end

class Treatment2 < AgByDpchXMdcL2Treatment
end
