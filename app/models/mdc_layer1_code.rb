class MdcLayer1Code < ActiveRecord::Base
  has_many :mdc_layer1s, :dependent => :destroy
  has_many :ag_by_dpch_x_mdc_l1cs
  has_many :data_datafiles

  belongs_to :hospital
  extend ImportUtil

  def self.import(excel)
    result = Import::ImportResult.new
    excel.sheet_info.each_with_index do |sheet_info,sheet_no|
      sheet = excel.sheet(sheet_info.name)
      is_prev_2011 = excel.extensions[0][sheet_no]
      unless is_prev_2011 then
        sheet_info.start_row.upto(sheet.last_row) do |index|
          begin
            row = sheet.row(index)
            #正しい番号なら読み込む
            if row[0].to_s =~ /^[0-9x]+$/ then
              result.total_line_num += 1
              is_inclusion = excel.extensions[1][sheet_no]
              i = is_inclusion ?  1 : 0
              code      = row[0].to_s
              name_org  = row[1].to_s
              mdc_code  = row[2 + i].strip
              mdc_name  = row[3 + i].strip
              mdc_l1_code = code.slice(2,4)

              p name_org

              #----------------
              #名称整形
              #----------------
              name = name_org
              #半角カッコは全て全角カッコに、半角スペースは全角スペースに
              name.gsub!("(", "（")
              name.gsub!(")", "）")
              name.gsub!(" ", "　")
              #カッコの中のスペースをsplit回避のために一時的にアンダーバーに置き換える
              name.gsub!(/[（](.+)[）]/){ "（" + $1.gsub(/[　]/, "_") + "）" }
              #始まりカッコの前にスペースがある場合はsplit回避のためスペースをなくす
              name.gsub!("　（", "（")
              #3層目は2層目を作る上では邪魔なのでここでカット。3層目のnameはname_orgをそのまま使う。
              name.gsub!(/手術・処置等[０-９].*/,"")
              name.gsub!(/定義副傷病.*/,"")

              #----------------
              #MDC 1層目作成
              #----------------
              #MDC 1層目 上位2桁
              mdc_l1c = MdcLayer1Code.find_or_create_by(code: mdc_code, year: excel.year)
              mdc_l1c.update_column(:name, mdc_name) if mdc_l1c.name.nil?
              #MDC 1層目 下位4桁
              mdc_l1  = mdc_l1c.mdc_layer1s.find_or_create_by(code: mdc_l1_code)

              #1層目の名称を抽出
              #名称の共通部分を正式名称とする
              names = name.to_s.split("　").reject(&:blank?)
              mdc_l1_name = names.shift.gsub("_", " ")

              if mdc_l1.name && mdc_l1.name != ""
                name_a1 = mdc_l1.name.split(/[（|）]/).reject(&:blank?)
                name_a2 = mdc_l1_name.split(/[（|）]/).reject(&:blank?)
                name_a3 = name_a1 & name_a2
                new_name = name_a3.map { |name|
                  name == name_a3[0] ? name : "（" + name + "）"
                }.join
              else
                new_name = mdc_l1_name
              end
              mdc_l1.update_column(:name, new_name) if mdc_l1.name != new_name

              #----------------
              #MDC 2層目作成
              #----------------
              if names.count > 0
                mdc_l2_name = names.join(" ").strip.gsub("_", " ")
              else
                mdc_l2_name = ""
              end
              mdc_l2_code = code.slice(8,2)
              mdc_l2 = mdc_l1.mdc_layer2s.find_or_create_by(code: mdc_l2_code)
              if mdc_l2.name.nil?
                mdc_l2.update_column(:name, mdc_l2_name)
              end

              #----------------
              #MDC 3層目作成
              #----------------
              mdc_l3_code1 = code.slice(10, 1)
              mdc_l3_code2 = code.slice(11, 1)
              mdc_l3_code3 = code.slice(12, 1)
              mdc_l3_code4 = code.slice(13, 1)
              mdc_l3 = mdc_l2.mdc_layer3s.find_or_create_by(code_1: mdc_l3_code1, code_2: mdc_l3_code2, code_3: mdc_l3_code3, code_4: mdc_l3_code4)
              if mdc_l3.name.nil?
                mdc_l3.update_column(:name, name_org)
              end

              #----------------
              #MDC関連テーブル
              #----------------
              import_ag_by_mdc(row, mdc_l3, result, index, is_inclusion)
            end
          rescue => e
            p e.message
            result.error_line_num += 1
            result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
          end
        end
      #2010 and prev
      else
        # save 2 years flag as update/insert
        h10 = get_mdc_layer1_code_string()
        h11 = get_mdc_layer1_code_string()
        keys = h10.keys
        sheet_info.start_row.step(sheet.last_row, 2) do |index|
          begin
            row = sheet.row(index)
            #----------------
            #MDC 1層目 下位4桁 DPC
            #----------------
            year        = row[0].to_i
            next unless (year == 2010 || year == 2011)
            next unless row[2].is_a?(Integer)
            if sheet_info.name =~ /dpc_code/
              mdc_l1_code   = row[1].to_s.rjust(6, "0").slice(2,4)
              mdc_code      = row[2].to_s.rjust(2, "0")
              name          = row[3].to_s
              #----------------
              #MDC 1
              #----------------
              i = row[2].to_i - 1
              h = year == 2010 ? h10 : h11
              if  h[keys[i]].to_s == "" then
                #p index.to_s + "  key empty" + ":" + keys[i] + "year=" + year.to_s + ": i=" + i.to_s + ":" + h[keys[i]].to_s #if (mdc_code == "18" && year == 2010)||(mdc_code == "16" && year == 2011)
                mdc_name = keys[i]
                mdc_l1c = MdcLayer1Code.find_or_create_by(code: mdc_code, year: year)
                mdc_l1c.update_column(:name, mdc_name)
                mdc_l1c_id = mdc_l1c.id
                h[keys[i]] = "x" #更新済みflag
              else
                mdc_l1c = MdcLayer1Code.where(code: mdc_code, year: year)
                mdc_l1c_id = mdc_l1c[0].id
              end
              #mdc_l1  = mdc_l1c.mdc_layer1s.find_or_create_by(code: mdc_l1_code)
              next if mdc_l1c == nil
              mdc_l1  = MdcLayer1.find_or_create_by(code: mdc_l1_code, mdc_layer1_code_id: mdc_l1c_id)
              mdc_l1.update_column(:name, name)
              result.total_line_num += 1
            #----------------
            #MDC 2層目作成
            #----------------
            elsif sheet_info.name =~ /cure_code/
              mdc_l1_code     = row[1].to_s.rjust(6, "0").slice(2,4)
              mdc_l2_code     = row[2].to_s.rjust(2, "0")
              mdc_l2_name     = row[3].to_s
              mdc_code = row[1].to_s.rjust(6, "0").slice(0,2)
              mdc_l1c = MdcLayer1Code.where(code: mdc_code, year: year)
              next if mdc_l1c.count == 0
              #mdc_l1  = mdc_l1c.mdc_layer1s.where(code: mdc_l1_code,mdc_layer1_code_id: mdc_l1c[0].id)
              mdc_l1  = MdcLayer1.where(code: mdc_l1_code, mdc_layer1_code_id: mdc_l1c[0].id)
              next if mdc_l1 == nil
              #mdc_l2 = mdc_l1.mdc_layer2s.find_or_create_by(code: mdc_l2_code)
              mdc_l2 = MdcLayer2.find_or_create_by(code: mdc_l2_code, mdc_layer1_id: mdc_l1[0].id)
              next if mdc_l2 == nil
              mdc_l2.update_column(:name, mdc_l2_name)
              result.total_line_num += 1
            end
          rescue => e
            p e.message
            result.error_line_num += 1
            result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
          end #begin
        end #sheet_info.start_row.step(sheet.last_row, 2) do |index|
      end # unless is_prev_2011 then
    end #excel.sheet_info.each_with_index do |sheet_info,sheet_no|
    return result
  end

  def self.get_mdc_layer1_code_string()
    h = {"神経系疾患"=>"","眼科系疾患"=>"","耳鼻咽喉科系疾患"=>"","呼吸器系疾患"=>"","循環器系疾患"=>"","消化器系疾患、肝臓・胆道・膵臓疾患"=>"","筋骨格系疾患"=>"","皮膚・皮下組織の疾患"=>"","乳房の疾患"=>"","内分泌・栄養・代謝に関する疾患"=>"","腎・尿路系疾患及び男性生殖器系疾患"=>"","女性生殖器系疾患及び産褥期疾患・異常妊娠分娩"=>"","血液・造血器・免疫臓器の疾患"=>"","新生児疾患、先天性奇形"=>"","小児疾患"=>"","外傷・熱傷・中毒"=>"","精神疾患"=>"","その他"=>""}
    return h
  end
  def self.import_ag_by_mdc(row, mdc_layer3, result, index, is_inclusion)
    ag_by_mdc = AgByMdc.find_or_create_by(mdc_layer3_id: mdc_layer3.id)
    ag_by_mdc.mdc_layer3 = mdc_layer3
    col = "A".to_cn0
    ag_by_mdc.code                            = row[col]
    col = 2
    if is_inclusion then
      ag_by_mdc.inclusion                       = row[col] == "○"              # 包括
      col += 1
    end
    #col = "F".to_cn0
    ag_by_mdc.dpc_num_for_the_mdc             = row[col+ 2].is_a?(Numeric)? row[col+ 2].to_i : nil  #  integer 当該MDCに含まれるDPCの数
    ag_by_mdc.cases_num_for_the_mdc           = row[col+ 3].is_a?(Numeric)? row[col+ 3].to_i : nil      # integer 当該MDCの症例数
    ag_by_mdc.number                          = row[col+ 4].is_a?(Numeric)? row[col+ 4].to_i : nil      #  integer 件数
    ag_by_mdc.dpc_ratio_for_all_cases         = row[col+ 6].is_a?(Numeric)? row[col+ 6].to_f : nil # double  全症例に対する当該DPCの比率
    ag_by_mdc.male_num                        = row[col+ 7].is_a?(Numeric)? row[col+ 7].to_i : nil # integer 男性の件数
    ag_by_mdc.female_num                      = row[col+ 9].is_a?(Numeric)? row[col+ 9].to_i : nil # integer 女性の件数
    ag_by_mdc.from0to2_num                    = row[col+11].is_a?(Numeric)? row[col+11].to_i : nil # integer ＜年齢＞0~2歳の件数
    ag_by_mdc.from3to5_num                    = row[col+13].is_a?(Numeric)? row[col+13].to_i : nil # integer 3~5歳の件数
    ag_by_mdc.from6to15_num                   = row[col+15].is_a?(Numeric)? row[col+15].to_i : nil # integer 6~15歳の件数
    ag_by_mdc.from16to20_num                  = row[col+17].is_a?(Numeric)? row[col+17].to_i : nil # integer 16~20歳の件数
    ag_by_mdc.from21to40_num                  = row[col+19].is_a?(Numeric)? row[col+19].to_i : nil # integer 21~40歳の件数
    ag_by_mdc.from41to60_num                  = row[col+21].is_a?(Numeric)? row[col+21].to_i : nil # integer 41~60歳の件数
    ag_by_mdc.from61to79_num                  = row[col+23].is_a?(Numeric)? row[col+23].to_i : nil  #  integer 61~79歳の件数
    ag_by_mdc.over80_num                      = row[col+25].is_a?(Numeric)? row[col+25].to_i : nil  #  integer 80歳以上の件数
    ag_by_mdc.introduction_num                = row[col+27].is_a?(Numeric)? row[col+27].to_i : nil  #  integer ＜経路＞他院よりの紹介
    ag_by_mdc.outpatient_num                  = row[col+29].is_a?(Numeric)? row[col+29].to_i : nil  #  integer 自院の外来からの入院
    ag_by_mdc.transported_num                 = row[col+31].is_a?(Numeric)? row[col+31].to_i : nil  #  integer 救急車による搬送
    ag_by_mdc.unexpected_hospitalization_num  = row[col+33].is_a?(Numeric)? row[col+33].to_i : nil  #  integer 予定外入院
    ag_by_mdc.emergency_hospitalization_num   = row[col+35].is_a?(Numeric)? row[col+35].to_i : nil  #  integer 救急医療入院
    ag_by_mdc.status_healed                   = row[col+37].is_a?(Numeric)? row[col+37].to_i : nil  #  integer ＜退院時転帰＞治癒
    ag_by_mdc.status_improvement              = row[col+38].is_a?(Numeric)? row[col+38].to_i : nil  #  integer 軽快
    ag_by_mdc.status_palliation               = row[col+39].is_a?(Numeric)? row[col+39].to_i : nil  #  integer 寛解
    ag_by_mdc.status_unchanging               = row[col+40].is_a?(Numeric)? row[col+40].to_i : nil  #  integer 不変
    ag_by_mdc.status_worse                    = row[col+41].is_a?(Numeric)? row[col+41].to_i : nil  #  integer 増悪
    ag_by_mdc.status_death_of_illness_costed_most       = row[col+42].is_a?(Numeric)? row[col+42].to_i : nil #  integer 死亡(医療資源病名）
    ag_by_mdc.status_death_of_other_illness_costed_most = row[col+43].is_a?(Numeric)? row[col+43].to_i : nil #  integer 死亡(医療資源病名以外）
    ag_by_mdc.status_other                    = row[col+44].is_a?(Numeric)? row[col+44].to_i : nil #book.cell(line, "AV".to_cn)  #  integer その他
    ag_by_mdc.hospital_days_average           = row[col+45].is_a?(Numeric)? row[col+45].to_f : nil #book.cell(line, "AW".to_cn)  #  double ＜在院日数＞平均値
    ag_by_mdc.hospital_days_minimum           = row[col+46].is_a?(Numeric)? row[col+46].to_i : nil #book.cell(line, "AX".to_cn)  #  integer 最小値
    ag_by_mdc.hospital_days_maximum           = row[col+47].is_a?(Numeric)? row[col+47].to_i : nil #book.cell(line, "AY".to_cn)  #  integer 最大値
    ag_by_mdc.hospital_days_coefficient_of_variation  = row[col+48].is_a?(Numeric)? row[col+48].to_f : nil #  integer 変動係数
    ag_by_mdc.hospital_days_value_of_25percentile     = row[col+49].is_a?(Numeric)? row[col+49].to_i : nil #  integer 25パーセンタイル値
    ag_by_mdc.hospital_days_value_of_50percentile     = row[col+50].is_a?(Numeric)? row[col+50].to_i : nil #  integer 50パーセンタイル値
    ag_by_mdc.hospital_days_value_of_75percentile     = row[col+51].is_a?(Numeric)? row[col+51].to_i : nil #  integer 75パーセンタイル値
    ag_by_mdc.hospital_days_value_of_90percentile     = row[col+52].is_a?(Numeric)? row[col+52].to_i : nil #  integer 90パーセンタイル値

    #col = "EQ".to_cn0

    ag_by_mdc.artificial_respiration    = row[col+ 143].is_a?(Numeric)? row[col+143].to_i : nil #  integer 人工呼吸
    ag_by_mdc.artificial_kidney         = row[col+ 145].is_a?(Numeric)? row[col+145].to_i : nil #  integer 人工腎臓
    ag_by_mdc.central_venous_injection  = row[col+ 147].is_a?(Numeric)? row[col+147].to_i : nil #  integer 中心静脈注射
    ag_by_mdc.blood_transfusion         = row[col+ 149].is_a?(Numeric)? row[col+149].to_i : nil #  integer 輸血

    #ag_by_mdc.save

    #医療資源を最も投入した傷病ICD10
    #col = "BE".to_cn0
    0.upto 9 do |i|
      icd10_code = row[col + 53 + i*3].present?  ? row[col + 53 + i*3].strip : nil
      number     = row[col + 54 + i*3]
      if icd10_code.present? && number.present?
        nbc = ag_by_mdc.number_by_icd10_costlies.find_or_create_by(code: icd10_code, codeable_type: "Icd10")
        nbc.codeable = Icd10.find_by(:code => icd10_code, :replace_code => nil)
        nbc.code     = icd10_code
        nbc.number   = number
        nbc.index    = i
        #nbc.save
        if nbc.codeable.nil?
          result.warning_line_num += 1
          result.messages += message(index, 'WARNING', "対応するICD10がありませんでした:" + icd10_code.to_s)
          p message(index, 'WARNING', "対応するICD10がありませんでした:" + icd10_code.to_s)
        end
      end
    end

    #入院時併存症及び入院後発症疾患ICD10
    #col = "CI".to_cn0
    0.upto 9 do |i|
      icd10_code = row[col + 83 + i*3].present?  ? row[col + 83 + i*3].strip : nil
      number     = row[col + 84 + i*3]
      if icd10_code.present? && number.present?
        nbc = ag_by_mdc.number_by_icd10_comorbidities.find_or_create_by(code: icd10_code, codeable_type: "Icd10")
        nbc.codeable = Icd10.find_by(:code => icd10_code, :replace_code => nil)
        nbc.code     = icd10_code
        nbc.number   = number
        nbc.index    = i
        #nbc.save
        if nbc.codeable.nil?
          result.warning_line_num += 1
          result.messages += message(index, 'WARNING', "対応するICD10がありませんでした:" + icd10_code.to_s)
          p message(index, 'WARNING', "対応するICD10がありませんでした:" + icd10_code.to_s)
        end
      end
    end

    #手術Kコード
    #col = "DM".to_cn0
    0.upto 9 do |i|
      kcode  = row[col + 113 + i*3].present?  ? row[col + 113 + i*3].strip : nil
      number = row[col + 114 + i*3]
      if kcode.present? && number.present?
        nbc = ag_by_mdc.number_by_kcodes.find_or_create_by(code: kcode, codeable_type: "Kcode")
        nbc.codeable = Kcode.find_or_create_by(:code => kcode)
        nbc.code     = kcode
        nbc.number   = number
        nbc.index    = i
        #nbc.save
        if nbc.codeable.nil?
          result.warning_line_num += 1
          result.messages += message(index, 'WARNING', "対応するKコードがありませんでした:" + kcode.to_s)
          p message(index, 'WARNING', "対応するKコードがありませんでした:" + kcode.to_s)
        end
      end
    end
    ag_by_mdc.save
  end
end
