class Population < ActiveRecord::Base
  validates :year, uniqueness: {
    scope: :municipality_id,
    message: "yearとmunicipality_idは同じ組み合わせのレコードが既に存在します。"
  }
  extend ImportUtil
  #xx県人口
  def self.import(excel)
    result = Import::ImportResult.new
    #都道府県名取得
    prefecture_name = excel.sheet_info[0].name.match(/.*[都道府県]/).to_s
    p prefecture_name
    prefecture = Prefecture.find_by(name: prefecture_name)
    unless prefecture.present?
      result.warning_line_num += 1
      result.messages += message(excel.sheet_info[0].name, 'ERROR', '不正な都道府県なので処理を終了します')
      return
    end
    excel.book.sheets.each do |sheet_info|
      next if sheet_info == 'rw_info'
      sheet = excel.sheet(sheet_info)
      next unless sheet.first_column.present?
      municipality = sheet.cell(1, 1)
      municipality_id = municipality =~ /[0-9]{4,6}/ ? $& : nil;

      sheet.first_column.upto(sheet.last_column) do |index|
        begin
          next if index == 1

          if prefecture_name == '福島県'
            year = sheet.cell(3, index).to_i
          else
            year = sheet.cell(55, index)
            year = year =~ /[0-9]{4,6}/ ? $& : nil;
          end
          unless year.present?
            result.warning_line_num += 1
            result.messages += message("シート名[#{sheet_info}] #{index}", 'ERROR', '年がありません')
            next
          end

          unless municipality_id.present?
            result.warning_line_num += 1
            result.messages += message("シート名[#{sheet_info}] #{index}", 'ERROR', '地方自治体コードがありません')
            next
          end

          # 福島県だけフォーマット違うので個別対応
          if prefecture_name == '福島県'
            male_0to4         = sheet.cell(4, index)
            female_0to4       = sheet.cell(29, index)
            male_5to9         = sheet.cell(5, index)
            female_5to9       = sheet.cell(30, index)
            male_10to14       = sheet.cell(6, index)
            female_10to14     = sheet.cell(31, index)
            male_15to19       = sheet.cell(7, index)
            female_15to19     = sheet.cell(32, index)
            male_20to24       = sheet.cell(8, index)
            female_20to24     = sheet.cell(33, index)
            male_25to29       = sheet.cell(9, index)
            female_25to29     = sheet.cell(34, index)
            male_30to34       = sheet.cell(10, index)
            female_30to34     = sheet.cell(35, index)
            male_35to39       = sheet.cell(11, index)
            female_35to39     = sheet.cell(36, index)
            male_40to44       = sheet.cell(12, index)
            female_40to44     = sheet.cell(37, index)
            male_45to49       = sheet.cell(13, index)
            female_45to49     = sheet.cell(37, index)
            male_50to54       = sheet.cell(14, index)
            female_50to54     = sheet.cell(39, index)
            male_55to59       = sheet.cell(15, index)
            female_55to59     = sheet.cell(40, index)
            male_60to64       = sheet.cell(16, index)
            female_60to64     = sheet.cell(41, index)
            male_65to69       = sheet.cell(17, index)
            female_65to69     = sheet.cell(42, index)
            male_70to74       = sheet.cell(18, index)
            female_70to74     = sheet.cell(43, index)
            male_75to79       = sheet.cell(19, index)
            female_75to79     = sheet.cell(44, index)
            male_80to84       = sheet.cell(20, index)
            female_80to84     = sheet.cell(45, index)
            male_85to89       = sheet.cell(21, index)
            female_85to89     = sheet.cell(46, index)
            male_over90       = sheet.cell(22, index)
            female_over90     = sheet.cell(47, index)
          else
            male_0to4         = sheet.cell(31, index)
            female_0to4       = sheet.cell(57, index)
            male_5to9         = sheet.cell(32, index)
            female_5to9       = sheet.cell(58, index)
            male_10to14       = sheet.cell(33, index)
            female_10to14     = sheet.cell(59, index)
            male_15to19       = sheet.cell(34, index)
            female_15to19     = sheet.cell(60, index)
            male_20to24       = sheet.cell(35, index)
            female_20to24     = sheet.cell(61, index)
            male_25to29       = sheet.cell(36, index)
            female_25to29     = sheet.cell(62, index)
            male_30to34       = sheet.cell(37, index)
            female_30to34     = sheet.cell(63, index)
            male_35to39       = sheet.cell(38, index)
            female_35to39     = sheet.cell(64, index)
            male_40to44       = sheet.cell(39, index)
            female_40to44     = sheet.cell(65, index)
            male_45to49       = sheet.cell(40, index)
            female_45to49     = sheet.cell(66, index)
            male_50to54       = sheet.cell(41, index)
            female_50to54     = sheet.cell(67, index)
            male_55to59       = sheet.cell(42, index)
            female_55to59     = sheet.cell(68, index)
            male_60to64       = sheet.cell(43, index)
            female_60to64     = sheet.cell(69, index)
            male_65to69       = sheet.cell(44, index)
            female_65to69     = sheet.cell(70, index)
            male_70to74       = sheet.cell(45, index)
            female_70to74     = sheet.cell(71, index)
            male_75to79       = sheet.cell(46, index)
            female_75to79     = sheet.cell(72, index)
            male_80to84       = sheet.cell(47, index)
            female_80to84     = sheet.cell(73, index)
            male_85to89       = sheet.cell(48, index)
            female_85to89     = sheet.cell(74, index)
            male_over90       = sheet.cell(49, index)
            female_over90     = sheet.cell(75, index)
          end
          dpc_male_0to2     = (male_0to4/5)*3
          dpc_female_0to2   = (female_0to4/5)*3
          dpc_male_3to5	    = ((male_0to4 + male_5to9)/10)*3
          dpc_female_3to5	  = ((female_0to4 + female_5to9)/10)*3
          dpc_male_6to15    = ((male_5to9 + male_10to14 + male_15to19)/15)*10
          dpc_female_6to15  = ((female_5to9 + female_10to14 + female_15to19)/15)*10
          dpc_male_16to20	  = ((male_15to19 + male_20to24)/10)*5
          dpc_female_16to20 =	((female_15to19 + female_20to24)/10)*5
          dpc_male_21to40   =
            ((male_20to24 + male_25to29 + male_30to34 + male_35to39 + male_40to44)/25)*20
          dpc_female_21to40 =
            ((female_20to24 + female_25to29 + female_30to34 + female_35to39 + female_40to44)/25)*20
          dpc_male_41to60   =
            ((male_40to44 + male_45to49 + male_50to54 + male_55to59 + male_60to64)/25)*20
          dpc_female_41to60 =
            ((female_40to44 + female_45to49 + female_50to54 + female_55to59 + female_60to64)/25)*20
          dpc_male_61to79   =
            ((male_60to64 + male_65to69 + male_70to74 + male_75to79)/20)*19
          dpc_female_61to79 =
            ((female_60to64 + female_65to69 + female_70to74 + female_75to79)/20)*19
          dpc_male_over80	  = (male_80to84 + male_85to89 + male_over90)
          dpc_female_over80 = (female_80to84 + female_85to89 + female_over90)

          population = Population.find_or_initialize_by(
            year: year,
            municipality_id: municipality_id
          )

          population.year              = year
          population.male_0to4         = male_0to4.to_i
          population.female_0to4       = female_0to4.to_i
          population.male_5to9         = male_5to9.to_i
          population.female_5to9       = female_5to9.to_i
          population.male_10to14       = male_10to14.to_i
          population.female_10to14     = female_10to14.to_i
          population.male_15to19       = male_15to19.to_i
          population.female_15to19     = female_15to19.to_i
          population.male_20to24       = male_20to24.to_i
          population.female_20to24     = female_20to24.to_i
          population.male_25to29       = male_25to29.to_i
          population.female_25to29     = female_25to29.to_i
          population.male_30to34       = male_30to34.to_i
          population.female_30to34     = female_30to34.to_i
          population.male_35to39       = male_35to39.to_i
          population.female_35to39     = female_35to39.to_i
          population.male_40to44       = male_40to44.to_i
          population.female_40to44     = female_40to44.to_i
          population.male_45to49       = male_45to49.to_i
          population.female_45to49     = female_45to49.to_i
          population.male_50to54       = male_50to54.to_i
          population.female_50to54     = female_50to54.to_i
          population.male_55to59       = male_55to59.to_i
          population.female_55to59     = female_55to59.to_i
          population.male_60to64       = male_60to64.to_i
          population.female_60to64     = female_60to64.to_i
          population.male_65to69       = male_65to69.to_i
          population.female_65to69     = female_65to69.to_i
          population.male_70to74       = male_70to74.to_i
          population.female_70to74     = female_70to74.to_i
          population.male_75to79       = male_75to79.to_i
          population.female_75to79     = female_75to79.to_i
          population.male_80to84       = male_80to84.to_i
          population.female_80to84     = female_80to84.to_i
          population.male_85to89       = male_85to89.to_i
          population.female_85to89     = female_85to89.to_i
          population.male_over90       = male_over90.to_i
          population.female_over90     = female_over90.to_i
          population.municipality_id   = municipality_id
          population.dpc_male_0to2     = dpc_male_0to2
          population.dpc_female_0to2   = dpc_female_0to2
          population.dpc_male_3to5	   = dpc_male_3to5
          population.dpc_female_3to5	 = dpc_female_3to5
          population.dpc_male_6to15    = dpc_male_6to15
          population.dpc_female_6to15  = dpc_female_6to15
          population.dpc_male_16to20	 = dpc_male_16to20
          population.dpc_female_16to20 = dpc_female_16to20
          population.dpc_male_21to40   = dpc_male_21to40
          population.dpc_female_21to40 = dpc_female_21to40
          population.dpc_male_41to60   = dpc_male_41to60
          population.dpc_female_41to60 = dpc_female_41to60
          population.dpc_male_61to79   = dpc_male_61to79
          population.dpc_female_61to79 = dpc_female_61to79
          population.dpc_male_over80	 = dpc_male_over80
          population.dpc_female_over80 = dpc_female_over80
          population.prefecture_id     = prefecture.id
          population.save
          result.total_line_num += 1
        rescue => e
          result.error_line_num += 1
          result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
        end
      end
    end
    return result
  end

end
