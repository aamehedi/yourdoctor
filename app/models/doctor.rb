# -*- coding: utf-8 -*-
class Doctor < ActiveRecord::Base
  include CheckOfficialDoctor
  devise :database_authenticatable, :recoverable, :validatable
  has_paper_trail

  def self.default_scope
    if RequestStore.store[:is_staff]
      all
    else
      where("is_staff = ?", false)
    end
  end

  #ヘルパーoverride
  def email_changed?
    self.email_was != self.email && self.email_was.present?
  end

  def email_required?
    self.email_changed?
  end

  module ClassMethods
    Devise::Models.config(self, :email_regexp, :password_length)
  end

  SEARCH_PARAMS = ["first_name_or_last_name_cont", "departments_name_cont",
    "hospitals_name_cont"]

  #before_save :update_access_token!

  before_create :check_official_doctor
  before_update :check_official_doctor

  after_create :log_timeline_by_create
  after_create :send_thank_you_msg
  after_update :log_timeline_by_update
  after_destroy :log_timeline_by_destroy

  #has_many のものは、要dependent: :destroy　但し、mergeを行う際に指定されているとうまく行かないので、しばらくはそのままなしで
  has_many :taggings, as: :tagger
  belongs_to :hospital
  belongs_to :current_hospital, class_name: Hospital.name,
    foreign_key: :hospital_id
  #belongs_to :university #削除予定
  #accepts_nested_attributes_for :university
  has_many :department_lists, :dependent => :destroy
  has_many :departments, through: :department_lists
  accepts_nested_attributes_for :department_lists, :allow_destroy => true

  has_many :hospitals, through: :departments
  has_many :prefectures, through: :hospitals

  has_many :society_lists, :dependent => :destroy
  accepts_nested_attributes_for :society_lists, allow_destroy: :true
  has_many :societies, through: :society_lists, :dependent => :destroy
  has_many :awards, :dependent => :destroy
  accepts_nested_attributes_for :awards, allow_destroy: :true
  has_many :monographs, ->{ order(published: :DESC).order(created_at: :DESC) }, :dependent => :destroy
  accepts_nested_attributes_for :monographs, allow_destroy: :true

  has_many :university_lists, :dependent => :destroy
  accepts_nested_attributes_for :university_lists, allow_destroy: :true
  has_many :universities, through: :university_lists

  has_many :skills, :dependent => :destroy
  accepts_nested_attributes_for :skills, allow_destroy: :true
  has_many :icd10s, through: :skills

  has_many :recommends, :dependent => :destroy
  accepts_nested_attributes_for :recommends, allow_destroy: :true

  mount_uploader :avatar_image, AvatarImageUploader
  mount_uploader :cover_image,  CoverImageUploader

  has_many :invitations, :class_name => 'TemporaryDoctor', :foreign_key => :doctor_id, :dependent => :destroy
  has_many :invitees, through: :invitations, :source => :invitee, :class_name => 'Doctor'

  #has_many :links
  has_many :follower_links, :class_name => 'Link', :foreign_key => :followee_id, :dependent => :destroy
  has_many :followee_links, :class_name => 'Link', :foreign_key => :follower_id, :dependent => :destroy
  has_many :followers, through: :follower_links, :foreign_key => :follower_id, :class_name => 'Doctor'
  has_many :followees, through: :followee_links, :foreign_key => :followee_id, :class_name => 'Doctor'

  #has_many :timeline_editors, :class_name => 'Timeline', :foreign_key => :editor_id, :dependent => :destroy
  #has_many :timeline_targets, :class_name => 'Timeline', :foreign_key => :doctor_id, :dependent => :destroy

  has_many :posts, :dependent => :destroy
  has_many :shines, :dependent => :destroy
  has_many :comments, :dependent => :destroy
  has_many :mail_logs, :dependent => :destroy
  has_many :dr_mr_chatrooms, dependent: :destroy
  has_many :emergency_doctor_hospitals, dependent: :destroy
  has_many :emergency_hospitals, through: :emergency_doctor_hospitals
  has_many :emergency_requests, dependent: :destroy
  has_many :favorite_groups, dependent: :destroy
  has_many :doctor_specialties, dependent: :destroy
  has_many :doctor_feedbacks, dependent: :destroy
  has_many :specialties, through: :doctor_specialties
  has_many :whytlink_doctor_chatrooms, dependent: :destroy
  has_many :whytlink_group_chat_requests, dependent: :destroy

  validates_associated :doctor_specialties, :emergency_doctor_hospitals

  #建前リンクステータス(links.stateが実ステータス)
  attr_accessor :link_state, :paginate_info
  enum link_state: [:nolink, :request, :requested, :refusal, :linking]
  enum sex: [:not_known, :male, :female, :not_applicable] #ISO準拠

  attr_accessor :is_yourself
  attr_accessor :no_timeline #タイムライン不要

  scope :emergency_doctors_with_specialties, -> doctor, hospitals, specialties{
    where.not(id: doctor.id).joins(:emergency_doctor_hospitals)
      .where(emergency_doctor_hospitals: {hospital_id: hospitals})
      .joins(:doctor_specialties)
      .where(doctor_specialties: {specialty_id: specialties})
  }

  def self.editor=(editor)
    RequestStore.store[:editor] = editor
  end

  def self.editor
    RequestStore.store[:editor]
  end


  #T.B.D
  #validate :uniq_society

  def name
    last_name + " " + first_name
  end

  def avatar_url
    self.avatar_image.url.present? ? self.avatar_image.url : default_avatar_url
  end

  def default_avatar_url
    "https://www.whytlink.com/images/default_avatar.jpg"
  end

  def cover_url
    self.cover_image.url.present? ? self.cover_image.url : default_cover_url
  end

  def default_cover_url
    self.male? ? "https://www.whytlink.com/images/wl_ui_bgv01.jpg" : "https://www.whytlink.com/images/wl_ui_bgv02.jpg"
  end

  def is_official?
    self.identity_verification == 1 || self.email == 'info@reasonwhy.jp'
    #true #テスト用
  end

  def requested_count
    self.follower_links.where(state: 0).count
  end

  def university_attributes=(university_attrs)
    if university_attrs[:id].present?
      self.university = University.find(university_attrs[:id])
    else
      self.university = University.create(name: university_attrs[:name])
    end
  end

  def department_lists_attributes=(attributes)
    super(attributes)
    #departmentとpositionを同時に新規登録した場合、
    #departmentと同じ入れ子階層にあるposition_list配下のpositionは
    #attributesメソッドで新規追加した段階でdepartmentのIDを確定出来ないので
    #ここでdepartmentのIDを与える
    #self.save
    self.department_lists.each do | department_list |
      department_list.positions.each do |position|
        position.update(department: department_list.department)
      end
    end
  end

  def society_lists_attributes=(attributes)
    super(attributes)
    #department_lists_attributesと同じ処理
    #self.save
    self.society_lists.each do | society_list |
      society_list.positions.each do |position|
        position.update(society: society_list.society)
      end
    end
  end

  #メンバーなら真
  def is_member
    if Doctor.editor.present? and Doctor.editor.is_staff
      return false
    else
      return email.present?
    end
  end

  #ユーザ(閲覧側の医師)とこの医師の関連性を調べ、各モデルに関連性を設定する
  def check_relation(user)
    #---------------------
    # 自分自身か
    #---------------------
    self.is_yourself = self.id == user.id
    unless self.is_yourself
      #---------------------
      # リンクチェック
      #---------------------
      check_link_state(user)

      #---------------------
      # スキル推薦チェック
      #---------------------
      self.skills.each do | skill |
        skill.recommended = skill.recommends.where(doctor_id: user.id).length > 0
      end
    end
  end

  def check_link_state(user)
    #閲覧医師から見たリンクステート
    #[申請側]
    #何もしてない:    :notlink
    #申請中:         :request
    #拒否された:      :request(拒否された事に気がつかせない優しさ)
    #承認された:      :linking
    #[被申請側]
    #何もされていない: :notlink
    #申請中された:    :requested
    #拒否した:       :refusal
    #承認した:       :linking

    #自分がリンク申請している場合
    if link = self.follower_links.where(follower_id: user.id).first
      if link.request? || link.refusal?
        self.link_state = :request
      elsif link.linking?
        self.link_state = :linking
      else
        self.link_state = :notlink
      end
    #自分がリンク申請されている場合
    elsif link = self.followee_links.where(followee_id: user.id).first
      if link.request?
        self.link_state = :requested
      elsif link.refusal?
        self.link_state = :refusal
      elsif link.linking?
        self.link_state = :linking
      else
        self.link_state = :notlink
      end
    else
      self.link_state = :notlink
    end
    self.link_state
  end

  def link_doctors(page = nil, limit = nil)
    arel = self.followers.joins(:followee_links).where("links.state = ?", Link.states[:linking]).union(
      self.followees.joins(:follower_links).where("links.state = ?", Link.states[:linking])
    )
    #arel = self.followees.union(self.followers)
    sql = arel.to_sql.gsub('$1', self.id.to_s).gsub('$2', self.id.to_s)
    sql += (' offset %d ' % (page * limit)) if page  && limit
    sql += (' limit  %d ' %  limit        ) if limit
    Doctor.find_by_sql(sql)
  end

  def link_doctors_count
    self.followee_links.select("state").where(state: Link.states[:linking]).size + self.follower_links.select("state").where(state: Link.states[:linking]).size
  end
=begin
  def society_lists_attributes=(society_lists_attributes)
    logger.info "society_lists_attributes"

    society_lists_attributes.each do | attr1 |
      # 学会が存在しない場合は作成
      society_attributes   = attr1[:society_attributes]
      Society.find_or_create_by(society_attributes[:name])
      #役職が存在しない場合は作成
      positions_attributes = attr1[:positions_attributes]
      positions_attributes.each do | attr2 |
        position_attributes   = attr1[:positions_attributes]
      end
    super
  end
=end
  def update_access_token!
    self.access_token = "#{self.id}:#{Devise.friendly_token}"
  end

  def merge(src_doctor)
    #############
    # 複数項目のマージ
    #############
    #---------------------
    # DepartmentListのマージ
    #---------------------
    #マージの定義:重複するdepartment_listを存在させない。重複していないものはマージ元からマージ先に繋ぎかえる。
    #department_list重複の定義: 参照先の診療科が同じである。または参照先の診療科の内容が重複している。
    #departmentの内容重複の定義: 参照先の病院と診療科名称が同一である。

    #重複の削除
    dst_department_ids         = self.department_lists.pluck(:department_id)
    duplicate_department_lists = src_doctor.department_lists.select{|i| dst_department_ids.include?(i.department_id)}
    src_doctor.department_lists.destroy(duplicate_department_lists)
    #重複以外の参照変更
    src_doctor.department_lists.each do | department_list |
      department_list.update_columns(:doctor_id => self.id)
    end

    # 診療科レコードの整理
    # ・病院と名称が重複している診療科は削除
    # ・但し同レコードを参照している医師がいる場合は削除しない(できない)
    checked_records = Array.new #チェック済みのレコード格納用
    self.departments.each do | department |
      #1.重複departmentは
      duplicates = self.departments.where(:hospital_id => department.hospital_id).where(:name => department.name).where.not(:id => department.id)
      duplicates.each do | duplicate_department |
        next if checked_records.include?(duplicate_department.id)
        #2.他に参照している医師がいなければ
        if duplicate_department.department_lists.where.not(:doctor_id => self.id).count == 0
          #3.削除
          duplicate_department.destroy
        else
          #3'.でなければ関連だけ削除
          duplicate_department.department_lists.where(:doctor_id => self.id).each {|i| i.destroy }
        end
      end
      checked_records.push(department.id)
    end

    #---------------------
    # SocietyListのマージ
    #---------------------
    #マージの定義:重複するsociety_listを存在させない。それ以外はマージ元から繋ぎ返る。
    #           ある学会内において同じ役職を存在させない。それ以外はマージ元から繋ぎ返る。
    #society_list重複の定義: 参照先の学会が同一である。
    #society_position_list重複の定義:参照先の役職が同一である。または参照先の役職の内容が重複している。
    #society_position内容重複の定義:参照先の学会と役職名が同一である。

    #ひとまず重複は考えずにマージ元のsociety_listをマージ先にぶら下げる
    src_doctor.society_lists.each do | society_list |
      society_list.update_columns(:doctor_id => self.id)
    end

    # society_listの重複削除
    checked_records = Array.new #チェック済みレコードは操作しないためのチェック済みレコード格納配列
    self.society_lists.each do | society_list |
      #1.重複しているsociety_listは、
      duplicates = self.society_lists.where(:society_id => society_list.society_id).where.not(:id => society_list.id)
      duplicates.each do | duplicate_society_list |
        next if checked_records.include?(duplicate_society_list.id)
        #2.position_listを、重複してたら削除、
        dst_positions_ids          = society_list.position_lists.pluck(:society_position_id)
        duplicate_position_lists   = duplicate_society_list.position_lists.select{|i| dst_positions_ids.include?(i.society_position_id)}
        society_list.position_lists.destroy(duplicate_position_lists)
        #3.それ以外は片方に移し替えて、
        duplicate_society_list.position_lists.each do | position_list |
          position_list.update_columns(:society_list_id => society_list.id) unless position_list.destroyed?
        end
        #4.削除。
        duplicate_society_list.destroy
      end
      checked_records.push(society_list.id)
    end

    # 学会レコードの整理
    # ・学会と役職名が重複している役職は削除
    # ・但し同レコードを参照している医師がいる場合は削除しない(できない)
    self.society_lists.each do | society_list |
      checked_records = Array.new
      society_list.positions.each do | position |
        #1.重複positionは
        duplicates = society_list.positions.where(:name => position.name).where.not(:id => position.id)
        duplicates.each do | duplicate_position |
          next if checked_records.include?(duplicate_position.id)
          #2.他に参照している医師がいなければ
          if duplicate_position.society_position_lists.where.not(:society_list_id => society_list.id).count == 0
            #3.削除
            duplicate_position.destroy
          else
            #3'.でなければ関連だけ削除
            duplicate_position.society_position_lists.where(:society_list_id => society_list.id).each {|i| i.destroy }
          end
        end
        checked_records.push(position.id)
      end
    end

    #---------------------
    # UniversityListsのマージ
    #---------------------
    #マージの定義:重複するuniversity_listを存在させない。それ以外はマージ元から繋ぎ返る。
    #university_list重複の定義: 参照先のuniversityが同一である。

    #idが重複しない場合は、マージ元のuniversity_listをマージ先にぶら下げる
    university_ids = self.university_lists.pluck(:university_id)
    src_doctor.university_lists.each do | university_list |
      university_list.update_columns(:doctor_id => self.id) if university_ids.index(university_list.id).nil?
    end

    #############
    # 単体項目のマージ
    #############
    #マージ先のデータを優先する(テキストは追加、マージ先に無ければマージ元のデータを使う)
    self.position         = merge_text(self.position,    src_doctor.position)
    self.graduation_year  = merge_text(self.graduation_year,  src_doctor.graduation_year)
    self.society          = merge_text(self.society,  src_doctor.society)
    self.profession       = merge_text(self.profession,  src_doctor.profession)
    self.skill            = merge_text(self.skill,       src_doctor.skill)
    self.result           = merge_text(self.result,      src_doctor.result)
    self.url              = merge_text(self.url,         src_doctor.url)
    self.history          = merge_text(self.history,     src_doctor.history)
    self.user             = merge_text(self.user,        src_doctor.user)
    self.update_user      = merge_text(self.update_user, src_doctor.update_user)
    self.comment          = merge_text(self.comment,     src_doctor.comment)
    self.top_or_next      = src_doctor.top_or_next if self.top_or_next.blank?
    self.university_id    = src_doctor.university_id if self.university_id.blank?

    #マージ先保存
    self.no_timeline = true
    self.save(:validate => false)

    #マージ元削除
    src_doctor.no_timeline = true
    src_doctor.save(:validate => false)
    src_doctor.destroy
  end

=begin societyモデル関連変更のためsociety_listに移動
  def society_position_lists_attributes=(society_position_lists_attributes)
    logger.info "society_position_lists_attributes"
    # 学会の地位society_positionが存在しない場合は作成
    society_position_lists_attributes.each do | attr |
      if !attr[1][:society_position_id].present? and attr[1][:name].present?
        logger.info "aaaaaa"
        society_position = SocietyPosition.create(society_id: attr[1][:society_id], name: attr[1][:name])
        attr[1][:society_position_id] = society_position.id
      end
    end
    super
  end
=end

  #同じ学会を指すsociety_listは持たない事
  def uniq_society
    if self.society_lists
      irregular_societies = self.society_lists.map{|i| i.id != self.id}
      #異なるsocietyが含まれている場合はエラー
      if irregular_societies.count > 0
        errors.add(:invalid_uniq_society, "同じ学会が複数設定されています")
      end
    end
  end

  def email_sendable?
    p Time.now - self.mail_sent_time unless self.mail_sent_time.nil?
    if self.mail_sent_time.nil? or Time.now - self.mail_sent_time > 60*60*24
      self.update_column(:mail_sent_time, Time.now)
      p "mail送信可能状態"
      true
    else
      p "前回から時間が１日以上経っていないのでメールNG"
      false
    end
  end

  def timelines
    Timeline.where("doctor_id = ? OR editor_id = ?", self.id, self.id)
  end

  def timeline_editors
    timelines.select do |timeline|
      timeline.editor_id == self.id
    end
  end

  def timeline_targets
    timelines.select do |timeline|
      timeline.doctor_id == self.id
    end
  end

  def authorized_to_get_messages? message_id
    WhytlinkMessage.joins(whytlink_chatroom: :whytlink_doctor_chatrooms)
      .where(id: message_id)
      .where("whytlink_doctor_chatrooms.doctor_id = #{self.id}").present?
  end

  def authorized_to_create_messages? chatroom_id
    WhytlinkDoctorChatroom.where(doctor_id: self.id, whytlink_chatroom_id:
      chatroom_id).present?
  end

  def detect_nearby_emergency_doctors params
    params[:specialties] ||= Specialty.all
    hospital_ids = detect_nearby_emergency_hospitals(params[:longitude],
      params[:latitude], params[:radius]).select(:id).map &:id
    doctor_ids = Doctor.emergency_doctors_with_specialties(self, hospital_ids,
      params[:specialties]).pluck :id
    Hospital.emergency_hospital_doctors hospital_ids, doctor_ids
  end

  def detect_nearby_emergency_hospitals longitude, latitude, radius
    Hospital.nearby_hospitals_of_doctor self, latitude, longitude,
      radius
  end

  def detect_nearby_hospitals_with_radius longitude, latitude
    Settings.geocoder.radius_list.each do |radius|
      hospitals = Hospital.nearby_hospitals_of_doctor self, latitude, longitude,
        radius
      return [radius, hospitals] if hospitals.size >= Settings.geocoder
        .minimum_hospital
    end
    [0,Hospital.none]
  end

  def update_current_hospital! params
    current_hospital = Hospital.near([params[:latitude], params[:longitude]],
      Settings.geocoder.current_hospital_search_radius).first
    current_hospital = nil unless hospitals.include?(current_hospital)
    self.update_attributes hospital_id: current_hospital.try(:id)
  end

  def update_emergency_doctor params
    if params[:latitude]
      update_current_hospital! params
    elsif params.key? :emergency
      update_attributes emergency: params[:emergency]
    elsif params.key? :rest_mode
      update_attributes rest_mode: params[:rest_mode],
        rest_time_start: params[:rest_time_start],
        rest_time_end: params[:rest_time_end]
    end
  end

  class << self
    def get_doctors_with_dr_mr_chatrooms params, mr_id
      doctor_id = params[:doctor_id] || Settings.doctor.default_id
      limit = params[:limit] || Settings.doctor.default_limit
      search_params = params.select{|k,_v| SEARCH_PARAMS.include?(k)}
      Doctor.joins(%{LEFT OUTER JOIN "dr_mr_chatrooms" ON
        "dr_mr_chatrooms"."doctor_id" = "doctors"."id" AND
        "dr_mr_chatrooms"."mr_id" = #{mr_id}})
        .ransack(search_params).result
        .where('"doctors"."id" >= ?', doctor_id).group(:id).limit(limit)
        
    end
  end

  private
    def merge_text(dst, src)
      dst.to_s.split(",").map{|i| i.gsub("0", "")}.reject(&:blank?).concat(src.to_s.split(",").reject(&:blank?)).uniq.join(',')
    end

    def check_official_doctor
      if self.identity_verification != 1
        if self.last_name_changed? || self.first_name_changed? || self.registration_number_changed? || self.registration_date_changed? || self.sex_changed? || self.birthdate_changed?
          result = check_official(self.last_name, self.first_name, self.registration_number, self.registration_date, self.sex, self.birthdate)
          self.identity_verification = result
        end
      end
    end

    def send_thank_you_msg
      queue_id = "ThankYouMail" + self.id.to_s
      DoctorsMailer.delay(:queue => queue_id, run_at: 131.minutes.from_now).thank_you(self.name, self.email)
    end

    def log_timeline_by_create
      return if no_timeline
      Timeline.create(editor: self, target: self, model: self, action: "create")
    end

    def log_timeline_by_update
      return if no_timeline
      # メールアドレスが登録されたら新規扱い
      if (self.email_was.nil? || self.email_was.empty?) && self.email_change.present?
        log_timeline_by_create
      elsif self.changed?
        if self.avatar_image_changed?
          Timeline.create(editor: Doctor.editor, target: self, model_type: "Avatar", action: "update", meta: self.avatar_url)
        elsif self.cover_image_changed?
          Timeline.create(editor: Doctor.editor, target: self, model_type: "Cover",  action: "update", meta: self.cover_url)
        else
          Timeline.create(editor: Doctor.editor, target: self, model: self, action: "update")
        end
      end
    end

    def log_timeline_by_destroy
      Timeline.create(editor: Doctor.editor, target: self, model: self, action: "destroy")
    end
end
