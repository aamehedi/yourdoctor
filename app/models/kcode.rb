class Kcode < ActiveRecord::Base
  extend ImportUtil

  def self.import(excel)
    result = Import::ImportResult.new
    excel.sheet_info.each_with_index do |sheet_info,sheet_no|
      file_type = excel.extensions[0][sheet_no]
      sheet = excel.sheet(sheet_info.name)
      sheet_info.start_row.upto(sheet.last_row) do |index|
        begin
          if file_type == "kcode"
            row = sheet.row(index)
            name = row[sheet_info.start_column - 1]
            code = row[sheet_info.start_column + 0]
            if code.to_s =~ /^K\w+([ｦ-ﾟ]|\-\w+[ｦ-ﾟ]|\-\w+|)$/ then
              result.total_line_num += 1
            	kcode = Kcode.find_or_create_by(code: code)
            	kcode.update_column(:name, name)
            end
          #mdc-icd10の紐付け
          elsif file_type == "icd10"
            row = sheet.row(index)
            mdc_layer1_code = row[sheet_info.start_column - 1].to_s
            mdc_layer1      = row[sheet_info.start_column + 0].to_s
            icd10_code      = row[sheet_info.start_column + 2].to_s.gsub("$","")
            icd10 = Icd10.find_by(code:icd10_code,replace_code:nil)
            if icd10 != nil
              mdc1code = MdcLayer1Code.find_by(year: excel.year,code:mdc_layer1_code)
              if mdc1code != nil
                mdc1 = MdcLayer1.find_by(code: mdc_layer1, mdc_layer1_code_id: mdc1code.id)
                if mdc1 != nil
                  mdc1.icd10_id = icd10.id
                  mdc1.save!
                  result.total_line_num += 1
                else
                  result.warning_line_num += 1
                  result.messages += message(index, 'WARNING', "対応するmdc_layer1がありませんでした:" + mdc_layer1_code + "/" + mdc_layer1)
                  p message(index, 'WARNING', "対応するmdc_layer1がありませんでした:" + mdc_layer1_code + "/" + mdc_layer1)
                end
              else
                result.warning_line_num += 1
                result.messages += message(index, 'WARNING', "対応するmdc_layer1_codeがありませんでした:" + mdc_layer1_code)
                p message(index, 'WARNING', "対応するmdc_layer1_codeがありませんでした:" + mdc_layer1_code)
              end
            else
              result.warning_line_num += 1
              result.messages += message(index, 'WARNING', "対応するicd10がありませんでした:" + icd10_code)
              p message(index, 'WARNING', "対応するicd10がありませんでした:" + icd10_code)
            end
          end
        rescue => e
          p e.message
          result.error_line_num += 1
          result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
        end
      end
    end
    return result
  end
end
