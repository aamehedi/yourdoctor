class SocietyPosition < ActiveRecord::Base
  belongs_to :society
  has_many :society_position_lists, dependent: :destroy
  has_many :society_lists, through: :society_position_lists
  #ドクターが学会と役職を同時に新規追加する場合、役職を登録する段階で学会をまだ作成していないこともある
  #validates :society_id, presence: true
  validates :name, presence: true  
end
