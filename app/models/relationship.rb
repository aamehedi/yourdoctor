class Relationship < ApplicationRecord
  belongs_to :follower, class_name: Mr.name
  belongs_to :followed, class_name: Mr.name
  validates :follower_id, presence: true
  validates :followed_id, presence: true
end
