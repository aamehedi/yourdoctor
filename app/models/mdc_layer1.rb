class MdcLayer1 < ActiveRecord::Base
  belongs_to :mdc_layer1_code
  has_many :mdc_layer2s
  has_many :ag_by_mdc_l1s
  has_many :icd10s
  scope :name_code, ->(code,year) { joins(:mdc_layer1_code).where("mdc_layer1_codes.code = '"+code.slice(0,2)+"'").where("mdc_layer1s.code = '"+code.slice(2,4)+"'").where("mdc_layer1_codes.year = "+year.to_s) }
end
