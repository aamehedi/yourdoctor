class PreMonographsModeratedScrapingHistory < ActiveRecord::Base
  belongs_to :doctor

  serialize :page_info
end
