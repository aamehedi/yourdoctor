class Comment < ActiveRecord::Base
  belongs_to :doctor
  belongs_to :post

  after_create :send_msg

  def send_msg
    if self.post.doctor.id != self.doctor.id
      p "ooo投稿とコメントの作成者は同一人物ではないのでメールを送信ooo"
      if self.post.doctor.email_sendable? == true
        p "ooo前回送信から時間を経ているので送信可能ooo"
        queue_id = "Comment" + self.id.to_s
        DoctorsMailer.delay(:queue => queue_id).new_comment(self.post.doctor, self, self.post)
        #DoctorsMailer.new_comment(self.post.doctor, self, self.post).deliver_now
      else
        p "xxx前回送信から時間を経ているので送信不可xxx"
      end
    else
      p "xxx投稿とコメントの作成者が同一人物なのでメールは不要xxx"
    end
  end
end
