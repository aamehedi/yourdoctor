class Scenario < ApplicationRecord
  belongs_to :mr
  has_many :scenario_charts, dependent: :destroy
  has_many :charts, through: :scenario_charts

  validates :name, presence: true
  validates :mr_id, presence: true

  def ordered_charts
    Chart.joins(:scenario_charts).where("scenario_charts.scenario_id =
      #{self.id}").order("scenario_charts.chart_order")
  end

  def save_scenario? scenario_chart_attributes
    if scenario_chart_attributes
      scenario_chart_attributes.each do |chart|
        self.scenario_charts.build chart
      end
    end
    self.save
  end

  def update_scenario_chart scenario_chart_attributes
    return self.charts.destroy_all unless scenario_chart_attributes
    old_charts = self.charts.pluck :id
    new_charts = scenario_chart_attributes.pluck :chart_id
    deleted_charts = old_charts - new_charts
    ScenarioChart.where(chart_id: deleted_charts, scenario_id: self.id)
      .destroy_all
    scenario_chart_attributes.each do |chart|
      sc_param = {scenario_id: self.id, chart_id: chart[:chart_id]}
      sc = ScenarioChart.find_or_initialize_by sc_param
      sc.chart_order = chart[:chart_order]
      sc.save!
    end
  end

  def update_scenario? scenario_params
    self.class.transaction do
      update_attributes!(scenario_params.except :scenario_charts_attributes)
      update_scenario_chart scenario_params[:scenario_charts_attributes]
    end
  rescue
    ActiveRecord::Rollback
    return false
  end

  class << self
    def get_scenarios mr_id, limit, last_id
      limit ||= Settings.scenario.default_limit
      last_id ||= Scenario.last.id + Settings.last_id_offset
      Scenario.where(mr_id: mr_id).where("id < #{last_id}").limit(limit)
        .order "id DESC"
    end
  end
end
