# -*- coding: utf-8 -*-
class AgByDpchXMdcL1c < ActiveRecord::Base
  belongs_to :dpc_hospital
  belongs_to :mdc_layer1_code
  extend ImportUtil

  def self.import(excel)
    case excel.file_type
    when "MDC別医療機関別件数（割合）" then
      result = import_patient_number(excel)
    when "予定・救急医療入院医療機関別MDC別集計" then
      result = import_hospitalization(excel)
    when "救急車による搬送の有無の医療機関別MDC別集計" then
      result = import_ambulance_transport(excel)
    when "入院から24時間以内の死亡の有無の医療機関別MDC別集計" then
      result = import_death_within_24hours(excel)
    when "在院日数の平均の差_MDC別" then
      result = import_avg_hospital_days(excel)
    end
    return result
  end

  def self.get_mdc_row(sheet,sheet_info,result)
    if  sheet_info.start_row > 2 then
      sheet_info.start_row.downto(0) do |i|
        if sheet.row(i).include?("01") then
          return sheet.row(i)
        end
      end
    else
      result.warning_line_num += 1
      result.messages += message(sheet_info.name, 'WARNING', "infoシートの情報が不正です 開始行: " + sheet_info.start_row.to_s)
      return
    end
  end
  #（２）MDC別医療機関別件数（割合）
  def self.import_patient_number(excel)
    result = Import::ImportResult.new
    #まず全部のMDCを取っておこう
    mdc = MdcLayer1Code.where(year: excel.year)
    h = mdc.map{|mdc| [mdc.code, mdc.id]}
    h = Hash[h]

    excel.sheet_info.each_with_index do |sheet_info, sheet_no|
      sheet = excel.sheet(sheet_info.name)
      rowmdc = get_mdc_row(sheet,sheet_info,result)#Array.new #MDC codeのある行
      sheet_info.start_row.step(sheet.last_row, 2) do |index|
        begin
          row1 = sheet.row(index)    #データ行
          row2 = sheet.row(index+1) #データ行2

          if to_code(row1[0]) =~ /^\d+$/
            result.total_line_num += 1
            col04                              = excel.extensions[0][0].to_cn0  #開始列
            if sheet.cell(2,2).to_s =~ /通番/ then    #通番あり
              notice_number           = row1[0].to_i
              serial_number           = row1[1].to_i
              name                    = row1[2].to_s
            elsif sheet.cell(2,2).to_s =~ /施設名/ then #通番なし
              notice_number         = row1[0].to_i
              serial_number         = nil
              name                  = row1[1].to_s
            else
              result.warning_line_num += 1
              result.messages += message(index, 'WARNING', "告示番号または通番が不正です　開始行: " + sheet_info.start_row.to_s)
              next
            end

            patient_number_surgery01            = row2[col04+0 ].is_a?(Numeric) ? row2[col04+0 ].to_i : nil#MDC01
            patient_number_surgery02            = row2[col04+1 ].is_a?(Numeric) ? row2[col04+1 ].to_i : nil
            patient_number_surgery03            = row2[col04+2 ].is_a?(Numeric) ? row2[col04+2 ].to_i : nil
            patient_number_surgery04            = row2[col04+3 ].is_a?(Numeric) ? row2[col04+3 ].to_i : nil
            patient_number_surgery05            = row2[col04+4 ].is_a?(Numeric) ? row2[col04+4 ].to_i : nil
            patient_number_surgery06            = row2[col04+5 ].is_a?(Numeric) ? row2[col04+5 ].to_i : nil
            patient_number_surgery07            = row2[col04+6 ].is_a?(Numeric) ? row2[col04+6 ].to_i : nil
            patient_number_surgery08            = row2[col04+7 ].is_a?(Numeric) ? row2[col04+7 ].to_i : nil
            patient_number_surgery09            = row2[col04+8 ].is_a?(Numeric) ? row2[col04+8 ].to_i : nil
            patient_number_surgery10            = row2[col04+9 ].is_a?(Numeric) ? row2[col04+9 ].to_i : nil
            patient_number_surgery11            = row2[col04+10].is_a?(Numeric) ? row2[col04+10].to_i : nil
            patient_number_surgery12            = row2[col04+11].is_a?(Numeric) ? row2[col04+11].to_i : nil
            patient_number_surgery13            = row2[col04+12].is_a?(Numeric) ? row2[col04+12].to_i : nil
            patient_number_surgery14            = row2[col04+13].is_a?(Numeric) ? row2[col04+13].to_i : nil
            patient_number_surgery15            = row2[col04+14].is_a?(Numeric) ? row2[col04+14].to_i : nil
            patient_number_surgery16            = row2[col04+15].is_a?(Numeric) ? row2[col04+15].to_i : nil
            patient_number_surgery17            = row2[col04+16].is_a?(Numeric) ? row2[col04+16].to_i : nil
            patient_number_surgery18            = row2[col04+17].is_a?(Numeric) ? row2[col04+17].to_i : nil
            #手術なし
            patient_number_no_surgery01            = row1[col04+0 ].is_a?(Numeric) ? row1[col04+0 ].to_i : nil
            patient_number_no_surgery02            = row1[col04+1 ].is_a?(Numeric) ? row1[col04+1 ].to_i : nil
            patient_number_no_surgery03            = row1[col04+2 ].is_a?(Numeric) ? row1[col04+2 ].to_i : nil
            patient_number_no_surgery04            = row1[col04+3 ].is_a?(Numeric) ? row1[col04+3 ].to_i : nil
            patient_number_no_surgery05            = row1[col04+4 ].is_a?(Numeric) ? row1[col04+4 ].to_i : nil
            patient_number_no_surgery06            = row1[col04+5 ].is_a?(Numeric) ? row1[col04+5 ].to_i : nil
            patient_number_no_surgery07            = row1[col04+6 ].is_a?(Numeric) ? row1[col04+6 ].to_i : nil
            patient_number_no_surgery08            = row1[col04+7 ].is_a?(Numeric) ? row1[col04+7 ].to_i : nil
            patient_number_no_surgery09            = row1[col04+8 ].is_a?(Numeric) ? row1[col04+8 ].to_i : nil
            patient_number_no_surgery10            = row1[col04+9 ].is_a?(Numeric) ? row1[col04+9 ].to_i : nil
            patient_number_no_surgery11            = row1[col04+10].is_a?(Numeric) ? row1[col04+10].to_i : nil
            patient_number_no_surgery12            = row1[col04+11].is_a?(Numeric) ? row1[col04+11].to_i : nil
            patient_number_no_surgery13            = row1[col04+12].is_a?(Numeric) ? row1[col04+12].to_i : nil
            patient_number_no_surgery14            = row1[col04+13].is_a?(Numeric) ? row1[col04+13].to_i : nil
            patient_number_no_surgery15            = row1[col04+14].is_a?(Numeric) ? row1[col04+14].to_i : nil
            patient_number_no_surgery16            = row1[col04+15].is_a?(Numeric) ? row1[col04+15].to_i : nil
            patient_number_no_surgery17            = row1[col04+16].is_a?(Numeric) ? row1[col04+16].to_i : nil
            patient_number_no_surgery18            = row1[col04+17].is_a?(Numeric) ? row1[col04+17].to_i : nil

            dpch = DpcHospital.find_by(created_year: excel.year, notice_number: notice_number, serial_number: serial_number)
            dpch = DpcHospital.find_by(created_year: excel.year, name: name) unless dpch.present?
            unless dpch.present? then

              result.warning_line_num += 1
              result.messages += message(index, 'WARNING', "対応する病院がありませんでした: " + name)
              next
            end
            dpc_hospital_id = dpch.id
            #MDC01
            upd_patient_number(index,rowmdc[col04+0].to_s.gsub(".0",""),h,dpc_hospital_id,result,patient_number_surgery01,patient_number_no_surgery01)
            upd_patient_number(index,rowmdc[col04+1].to_s.gsub(".0",""),h,dpc_hospital_id,result,patient_number_surgery02,patient_number_no_surgery02)
            upd_patient_number(index,rowmdc[col04+2].to_s.gsub(".0",""),h,dpc_hospital_id,result,patient_number_surgery03,patient_number_no_surgery03)
            upd_patient_number(index,rowmdc[col04+3].to_s.gsub(".0",""),h,dpc_hospital_id,result,patient_number_surgery04,patient_number_no_surgery04)
            upd_patient_number(index,rowmdc[col04+4].to_s.gsub(".0",""),h,dpc_hospital_id,result,patient_number_surgery05,patient_number_no_surgery05)
            upd_patient_number(index,rowmdc[col04+5].to_s.gsub(".0",""),h,dpc_hospital_id,result,patient_number_surgery06,patient_number_no_surgery06)
            upd_patient_number(index,rowmdc[col04+6].to_s.gsub(".0",""),h,dpc_hospital_id,result,patient_number_surgery07,patient_number_no_surgery07)
            upd_patient_number(index,rowmdc[col04+7].to_s.gsub(".0",""),h,dpc_hospital_id,result,patient_number_surgery08,patient_number_no_surgery08)
            upd_patient_number(index,rowmdc[col04+8].to_s.gsub(".0",""),h,dpc_hospital_id,result,patient_number_surgery09,patient_number_no_surgery09)
            upd_patient_number(index,rowmdc[col04+9].to_s.gsub(".0",""),h,dpc_hospital_id,result,patient_number_surgery10,patient_number_no_surgery10)
            upd_patient_number(index,rowmdc[col04+10].to_s.gsub(".0",""),h,dpc_hospital_id,result,patient_number_surgery11,patient_number_no_surgery11)
            upd_patient_number(index,rowmdc[col04+11].to_s.gsub(".0",""),h,dpc_hospital_id,result,patient_number_surgery12,patient_number_no_surgery12)
            upd_patient_number(index,rowmdc[col04+12].to_s.gsub(".0",""),h,dpc_hospital_id,result,patient_number_surgery13,patient_number_no_surgery13)
            upd_patient_number(index,rowmdc[col04+13].to_s.gsub(".0",""),h,dpc_hospital_id,result,patient_number_surgery14,patient_number_no_surgery14)
            upd_patient_number(index,rowmdc[col04+14].to_s.gsub(".0",""),h,dpc_hospital_id,result,patient_number_surgery15,patient_number_no_surgery15)
            upd_patient_number(index,rowmdc[col04+15].to_s.gsub(".0",""),h,dpc_hospital_id,result,patient_number_surgery16,patient_number_no_surgery16)
            upd_patient_number(index,rowmdc[col04+16].to_s.gsub(".0",""),h,dpc_hospital_id,result,patient_number_surgery17,patient_number_no_surgery17)
            upd_patient_number(index,rowmdc[col04+17].to_s.gsub(".0",""),h,dpc_hospital_id,result,patient_number_surgery18,patient_number_no_surgery18)
          end
        rescue => e
          result.error_line_num += 1
          result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
        end
      end
    end
    return result
  end
  def self.upd_patient_number(index,mdc_code,h,dpc_hospital_id,result,patient_number_surgery,patient_number_no_surgery)
    if h.has_key?(mdc_code) then
      mdc_layer1_code_id = h[mdc_code]
    else
      result.warning_line_num += 1
      result.messages += message(index, 'WARNING', "対応するMDCコードがありませんでした: " + mdc_code)
      return
    end
    ag = AgByDpchXMdcL1c.find_or_initialize_by(mdc_layer1_code_id: mdc_layer1_code_id, dpc_hospital_id: dpc_hospital_id)
    ag.patient_number_surgery       = patient_number_surgery
    ag.patient_number_no_surgery    = patient_number_no_surgery
    unless ag.save! then
      result.warning_line_num += 1
      result.messages += message(index, 'WARNING', "登録失敗 MDC:" + mdc_code + "施設ID:" + dpc_hospital_id)
    end
  end

  #(3)予定・救急医療入院医療機関別MDC別集計
  def self.import_hospitalization(excel)
    result = Import::ImportResult.new
    #まず全部のMDCを取っておこう
    mdc = MdcLayer1Code.where(year: excel.year)
    h = mdc.map{|mdc| [mdc.code, mdc.id]}
    h = Hash[h]

    excel.sheet_info.each_with_index do |sheet_info, sheet_no|
      sheet = excel.sheet(sheet_info.name)
      rowmdc = get_mdc_row(sheet,sheet_info,result)
      sheet_info.start_row.upto(sheet.last_row) do |index|
        begin
          row1 = sheet.row(index)    #データ行

          if to_code(row1[0]) =~ /^\d+$/
            result.total_line_num += 1
            #MDCの出現する行数
            i = rowmdc.index("01")
            tmp = rowmdc.slice(i..rowmdc.count)
            s = tmp.index("02")

            col04                              = excel.extensions[0][0].to_cn0  #開始列
            if sheet.cell(1,2).to_s =~ /通番/ then    #通番あり
              notice_number           = row1[0].to_i
              serial_number           = row1[1].to_i
              name                    = row1[2].to_s
            elsif sheet.cell(1,2).to_s =~ /施設名/ then #通番なし
              notice_number         = row1[0].to_i
              serial_number         = nil
              name                  = row1[1].to_s
            else
              result.warning_line_num += 1
              result.messages += message(index, 'WARNING', "告示番号または通番が不正です　開始行: " + sheet_info.start_row.to_s)
            end
            case s
              when 2 then
                hospitalization_planned_num01       = row1[col04+0 ].is_a?(Numeric) ? row1[col04+0 ].to_i : nil
                hospitalization_chemo_num01         = nil
                hospitalization_unplanned_num01     = nil
                hospitalization_emergency_num01     = row1[col04+1 ].is_a?(Numeric) ? row1[col04+1 ].to_i : nil
                hospitalization_planned_num02       = row1[col04+2 ].is_a?(Numeric) ? row1[col04+2 ].to_i : nil
                hospitalization_chemo_num02         = nil
                hospitalization_unplanned_num02     = nil
                hospitalization_emergency_num02     = row1[col04+3 ].is_a?(Numeric) ? row1[col04+3 ].to_i : nil
                hospitalization_planned_num03       = row1[col04+4 ].is_a?(Numeric) ? row1[col04+4 ].to_i : nil
                hospitalization_chemo_num03         = nil
                hospitalization_unplanned_num03     = nil
                hospitalization_emergency_num03     = row1[col04+5 ].is_a?(Numeric) ? row1[col04+5 ].to_i : nil
                hospitalization_planned_num04       = row1[col04+6 ].is_a?(Numeric) ? row1[col04+6 ].to_i : nil
                hospitalization_chemo_num04         = nil
                hospitalization_unplanned_num04     = nil
                hospitalization_emergency_num04     = row1[col04+7 ].is_a?(Numeric) ? row1[col04+7 ].to_i : nil
                hospitalization_planned_num05       = row1[col04+8 ].is_a?(Numeric) ? row1[col04+8 ].to_i : nil
                hospitalization_chemo_num05         = nil
                hospitalization_unplanned_num05     = nil
                hospitalization_emergency_num05     = row1[col04+9 ].is_a?(Numeric) ? row1[col04+9 ].to_i : nil
                hospitalization_planned_num06       = row1[col04+10].is_a?(Numeric) ? row1[col04+10].to_i : nil
                hospitalization_chemo_num06         = nil
                hospitalization_unplanned_num06     = nil
                hospitalization_emergency_num06     = row1[col04+11].is_a?(Numeric) ? row1[col04+11].to_i : nil
                hospitalization_planned_num07       = row1[col04+12].is_a?(Numeric) ? row1[col04+12].to_i : nil
                hospitalization_chemo_num07         = nil
                hospitalization_unplanned_num07     = nil
                hospitalization_emergency_num07     = row1[col04+13].is_a?(Numeric) ? row1[col04+13].to_i : nil
                hospitalization_planned_num08       = row1[col04+14].is_a?(Numeric) ? row1[col04+14].to_i : nil
                hospitalization_chemo_num08         = nil
                hospitalization_unplanned_num08     = nil
                hospitalization_emergency_num08     = row1[col04+15].is_a?(Numeric) ? row1[col04+15].to_i : nil
                hospitalization_planned_num09       = row1[col04+16].is_a?(Numeric) ? row1[col04+16].to_i : nil
                hospitalization_chemo_num09         = nil
                hospitalization_unplanned_num09     = nil
                hospitalization_emergency_num09     = row1[col04+17].is_a?(Numeric) ? row1[col04+17].to_i : nil
                hospitalization_planned_num10       = row1[col04+18].is_a?(Numeric) ? row1[col04+18].to_i : nil
                hospitalization_chemo_num10         = nil
                hospitalization_unplanned_num10     = nil
                hospitalization_emergency_num10     = row1[col04+19].is_a?(Numeric) ? row1[col04+19].to_i : nil
                hospitalization_planned_num11       = row1[col04+20].is_a?(Numeric) ? row1[col04+20].to_i : nil
                hospitalization_chemo_num11         = nil
                hospitalization_unplanned_num11     = nil
                hospitalization_emergency_num11     = row1[col04+21].is_a?(Numeric) ? row1[col04+21].to_i : nil
                hospitalization_planned_num12       = row1[col04+22].is_a?(Numeric) ? row1[col04+22].to_i : nil
                hospitalization_chemo_num12         = nil
                hospitalization_unplanned_num12     = nil
                hospitalization_emergency_num12     = row1[col04+23].is_a?(Numeric) ? row1[col04+23].to_i : nil
                hospitalization_planned_num13       = row1[col04+24].is_a?(Numeric) ? row1[col04+24].to_i : nil
                hospitalization_chemo_num13         = nil
                hospitalization_unplanned_num13     = nil
                hospitalization_emergency_num13     = row1[col04+25].is_a?(Numeric) ? row1[col04+25].to_i : nil
                hospitalization_planned_num14       = row1[col04+26].is_a?(Numeric) ? row1[col04+26].to_i : nil
                hospitalization_chemo_num14         = nil
                hospitalization_unplanned_num14     = nil
                hospitalization_emergency_num14     = row1[col04+27].is_a?(Numeric) ? row1[col04+27].to_i : nil
                hospitalization_planned_num15       = row1[col04+28].is_a?(Numeric) ? row1[col04+28].to_i : nil
                hospitalization_chemo_num15         = nil
                hospitalization_unplanned_num15     = nil
                hospitalization_emergency_num15     = row1[col04+29].is_a?(Numeric) ? row1[col04+29].to_i : nil
                hospitalization_planned_num16       = row1[col04+30].is_a?(Numeric) ? row1[col04+30].to_i : nil
                hospitalization_chemo_num16         = nil
                hospitalization_unplanned_num16     = nil
                hospitalization_emergency_num16     = row1[col04+31].is_a?(Numeric) ? row1[col04+31].to_i : nil
                hospitalization_planned_num17       = row1[col04+32].is_a?(Numeric) ? row1[col04+32].to_i : nil
                hospitalization_chemo_num17         = nil
                hospitalization_unplanned_num17     = nil
                hospitalization_emergency_num17     = row1[col04+33].is_a?(Numeric) ? row1[col04+33].to_i : nil
                hospitalization_planned_num18       = row1[col04+34].is_a?(Numeric) ? row1[col04+34].to_i : nil
                hospitalization_chemo_num18         = nil
                hospitalization_unplanned_num18     = nil
                hospitalization_emergency_num18     = row1[col04+35].is_a?(Numeric) ? row1[col04+35].to_i : nil
              when 3 then
                hospitalization_planned_num01       = row1[col04+0 ].is_a?(Numeric) ? row1[col04+0 ].to_i : nil
                hospitalization_chemo_num01         = nil
                hospitalization_unplanned_num01     = row1[col04+1 ].is_a?(Numeric) ? row1[col04+1 ].to_i : nil
                hospitalization_emergency_num01     = row1[col04+2 ].is_a?(Numeric) ? row1[col04+2 ].to_i : nil
                hospitalization_planned_num02       = row1[col04+3 ].is_a?(Numeric) ? row1[col04+3 ].to_i : nil
                hospitalization_chemo_num02         = nil
                hospitalization_unplanned_num02     = row1[col04+4 ].is_a?(Numeric) ? row1[col04+4 ].to_i : nil
                hospitalization_emergency_num02     = row1[col04+5 ].is_a?(Numeric) ? row1[col04+5 ].to_i : nil
                hospitalization_planned_num03       = row1[col04+6 ].is_a?(Numeric) ? row1[col04+6 ].to_i : nil
                hospitalization_chemo_num03         = nil
                hospitalization_unplanned_num03     = row1[col04+7 ].is_a?(Numeric) ? row1[col04+7 ].to_i : nil
                hospitalization_emergency_num03     = row1[col04+8 ].is_a?(Numeric) ? row1[col04+8 ].to_i : nil
                hospitalization_planned_num04       = row1[col04+9 ].is_a?(Numeric) ? row1[col04+9 ].to_i : nil
                hospitalization_chemo_num04         = nil
                hospitalization_unplanned_num04     = row1[col04+10].is_a?(Numeric) ? row1[col04+10].to_i : nil
                hospitalization_emergency_num04     = row1[col04+11].is_a?(Numeric) ? row1[col04+11].to_i : nil
                hospitalization_planned_num05       = row1[col04+12].is_a?(Numeric) ? row1[col04+12].to_i : nil
                hospitalization_chemo_num05         = nil
                hospitalization_unplanned_num05     = row1[col04+13].is_a?(Numeric) ? row1[col04+13].to_i : nil
                hospitalization_emergency_num05     = row1[col04+14].is_a?(Numeric) ? row1[col04+14].to_i : nil
                hospitalization_planned_num06       = row1[col04+15].is_a?(Numeric) ? row1[col04+15].to_i : nil
                hospitalization_chemo_num06         = nil
                hospitalization_unplanned_num06     = row1[col04+16].is_a?(Numeric) ? row1[col04+16].to_i : nil
                hospitalization_emergency_num06     = row1[col04+17].is_a?(Numeric) ? row1[col04+17].to_i : nil
                hospitalization_planned_num07       = row1[col04+18].is_a?(Numeric) ? row1[col04+18].to_i : nil
                hospitalization_chemo_num07         = nil
                hospitalization_unplanned_num07     = row1[col04+19].is_a?(Numeric) ? row1[col04+19].to_i : nil
                hospitalization_emergency_num07     = row1[col04+20].is_a?(Numeric) ? row1[col04+20].to_i : nil
                hospitalization_planned_num08       = row1[col04+21].is_a?(Numeric) ? row1[col04+21].to_i : nil
                hospitalization_chemo_num08         = nil
                hospitalization_unplanned_num08     = row1[col04+22].is_a?(Numeric) ? row1[col04+22].to_i : nil
                hospitalization_emergency_num08     = row1[col04+23].is_a?(Numeric) ? row1[col04+23].to_i : nil
                hospitalization_planned_num09       = row1[col04+24].is_a?(Numeric) ? row1[col04+24].to_i : nil
                hospitalization_chemo_num09         = nil
                hospitalization_unplanned_num09     = row1[col04+25].is_a?(Numeric) ? row1[col04+25].to_i : nil
                hospitalization_emergency_num09     = row1[col04+26].is_a?(Numeric) ? row1[col04+26].to_i : nil
                hospitalization_planned_num10       = row1[col04+27].is_a?(Numeric) ? row1[col04+27].to_i : nil
                hospitalization_chemo_num10         = nil
                hospitalization_unplanned_num10     = row1[col04+28].is_a?(Numeric) ? row1[col04+28].to_i : nil
                hospitalization_emergency_num10     = row1[col04+29].is_a?(Numeric) ? row1[col04+29].to_i : nil
                hospitalization_planned_num11       = row1[col04+30].is_a?(Numeric) ? row1[col04+30].to_i : nil
                hospitalization_chemo_num11         = nil
                hospitalization_unplanned_num11     = row1[col04+31].is_a?(Numeric) ? row1[col04+31].to_i : nil
                hospitalization_emergency_num11     = row1[col04+32].is_a?(Numeric) ? row1[col04+32].to_i : nil
                hospitalization_planned_num12       = row1[col04+33].is_a?(Numeric) ? row1[col04+33].to_i : nil
                hospitalization_chemo_num12         = nil
                hospitalization_unplanned_num12     = row1[col04+34].is_a?(Numeric) ? row1[col04+34].to_i : nil
                hospitalization_emergency_num12     = row1[col04+35].is_a?(Numeric) ? row1[col04+35].to_i : nil
                hospitalization_planned_num13       = row1[col04+36].is_a?(Numeric) ? row1[col04+36].to_i : nil
                hospitalization_chemo_num13         = nil
                hospitalization_unplanned_num13     = row1[col04+37].is_a?(Numeric) ? row1[col04+37].to_i : nil
                hospitalization_emergency_num13     = row1[col04+38].is_a?(Numeric) ? row1[col04+38].to_i : nil
                hospitalization_planned_num14       = row1[col04+39].is_a?(Numeric) ? row1[col04+39].to_i : nil
                hospitalization_chemo_num14         = nil
                hospitalization_unplanned_num14     = row1[col04+40].is_a?(Numeric) ? row1[col04+40].to_i : nil
                hospitalization_emergency_num14     = row1[col04+41].is_a?(Numeric) ? row1[col04+41].to_i : nil
                hospitalization_planned_num15       = row1[col04+42].is_a?(Numeric) ? row1[col04+42].to_i : nil
                hospitalization_chemo_num15         = nil
                hospitalization_unplanned_num15     = row1[col04+43].is_a?(Numeric) ? row1[col04+43].to_i : nil
                hospitalization_emergency_num15     = row1[col04+44].is_a?(Numeric) ? row1[col04+44].to_i : nil
                hospitalization_planned_num16       = row1[col04+45].is_a?(Numeric) ? row1[col04+45].to_i : nil
                hospitalization_chemo_num16         = nil
                hospitalization_unplanned_num16     = row1[col04+46].is_a?(Numeric) ? row1[col04+46].to_i : nil
                hospitalization_emergency_num16     = row1[col04+47].is_a?(Numeric) ? row1[col04+47].to_i : nil
                hospitalization_planned_num17       = row1[col04+48].is_a?(Numeric) ? row1[col04+48].to_i : nil
                hospitalization_chemo_num17         = nil
                hospitalization_unplanned_num17     = row1[col04+49].is_a?(Numeric) ? row1[col04+49].to_i : nil
                hospitalization_emergency_num17     = row1[col04+50].is_a?(Numeric) ? row1[col04+50].to_i : nil
                hospitalization_planned_num18       = row1[col04+51].is_a?(Numeric) ? row1[col04+51].to_i : nil
                hospitalization_chemo_num18         = nil
                hospitalization_unplanned_num18     = row1[col04+52].is_a?(Numeric) ? row1[col04+52].to_i : nil
                hospitalization_emergency_num18     = row1[col04+53].is_a?(Numeric) ? row1[col04+53].to_i : nil
              when 4 then
                hospitalization_planned_num01       = row1[col04+0 ].is_a?(Numeric) ? row1[col04+0 ].to_i : nil
                hospitalization_chemo_num01         = row1[col04+1 ].is_a?(Numeric) ? row1[col04+1 ].to_i : nil
                hospitalization_unplanned_num01     = row1[col04+2 ].is_a?(Numeric) ? row1[col04+2 ].to_i : nil
                hospitalization_emergency_num01     = row1[col04+3 ].is_a?(Numeric) ? row1[col04+3 ].to_i : nil
                hospitalization_planned_num02       = row1[col04+4 ].is_a?(Numeric) ? row1[col04+4 ].to_i : nil
                hospitalization_chemo_num02         = row1[col04+5 ].is_a?(Numeric) ? row1[col04+5 ].to_i : nil
                hospitalization_unplanned_num02     = row1[col04+6 ].is_a?(Numeric) ? row1[col04+6 ].to_i : nil
                hospitalization_emergency_num02     = row1[col04+7 ].is_a?(Numeric) ? row1[col04+7 ].to_i : nil
                hospitalization_planned_num03       = row1[col04+8 ].is_a?(Numeric) ? row1[col04+8 ].to_i : nil
                hospitalization_chemo_num03         = row1[col04+9 ].is_a?(Numeric) ? row1[col04+9 ].to_i : nil
                hospitalization_unplanned_num03     = row1[col04+10].is_a?(Numeric) ? row1[col04+10].to_i : nil
                hospitalization_emergency_num03     = row1[col04+11].is_a?(Numeric) ? row1[col04+11].to_i : nil
                hospitalization_planned_num04       = row1[col04+12].is_a?(Numeric) ? row1[col04+12].to_i : nil
                hospitalization_chemo_num04         = row1[col04+13].is_a?(Numeric) ? row1[col04+13].to_i : nil
                hospitalization_unplanned_num04     = row1[col04+14].is_a?(Numeric) ? row1[col04+14].to_i : nil
                hospitalization_emergency_num04     = row1[col04+15].is_a?(Numeric) ? row1[col04+15].to_i : nil
                hospitalization_planned_num05       = row1[col04+16].is_a?(Numeric) ? row1[col04+16].to_i : nil
                hospitalization_chemo_num05         = row1[col04+17].is_a?(Numeric) ? row1[col04+17].to_i : nil
                hospitalization_unplanned_num05     = row1[col04+18].is_a?(Numeric) ? row1[col04+18].to_i : nil
                hospitalization_emergency_num05     = row1[col04+19].is_a?(Numeric) ? row1[col04+19].to_i : nil
                hospitalization_planned_num06       = row1[col04+20].is_a?(Numeric) ? row1[col04+20].to_i : nil
                hospitalization_chemo_num06         = row1[col04+21].is_a?(Numeric) ? row1[col04+21].to_i : nil
                hospitalization_unplanned_num06     = row1[col04+22].is_a?(Numeric) ? row1[col04+22].to_i : nil
                hospitalization_emergency_num06     = row1[col04+23].is_a?(Numeric) ? row1[col04+23].to_i : nil
                hospitalization_planned_num07       = row1[col04+24].is_a?(Numeric) ? row1[col04+24].to_i : nil
                hospitalization_chemo_num07         = row1[col04+25].is_a?(Numeric) ? row1[col04+25].to_i : nil
                hospitalization_unplanned_num07     = row1[col04+26].is_a?(Numeric) ? row1[col04+26].to_i : nil
                hospitalization_emergency_num07     = row1[col04+27].is_a?(Numeric) ? row1[col04+27].to_i : nil
                hospitalization_planned_num08       = row1[col04+28].is_a?(Numeric) ? row1[col04+28].to_i : nil
                hospitalization_chemo_num08         = row1[col04+29].is_a?(Numeric) ? row1[col04+29].to_i : nil
                hospitalization_unplanned_num08     = row1[col04+30].is_a?(Numeric) ? row1[col04+30].to_i : nil
                hospitalization_emergency_num08     = row1[col04+31].is_a?(Numeric) ? row1[col04+31].to_i : nil
                hospitalization_planned_num09       = row1[col04+32].is_a?(Numeric) ? row1[col04+32].to_i : nil
                hospitalization_chemo_num09         = row1[col04+33].is_a?(Numeric) ? row1[col04+33].to_i : nil
                hospitalization_unplanned_num09     = row1[col04+34].is_a?(Numeric) ? row1[col04+34].to_i : nil
                hospitalization_emergency_num09     = row1[col04+35].is_a?(Numeric) ? row1[col04+35].to_i : nil
                hospitalization_planned_num10       = row1[col04+36].is_a?(Numeric) ? row1[col04+36].to_i : nil
                hospitalization_chemo_num10         = row1[col04+37].is_a?(Numeric) ? row1[col04+37].to_i : nil
                hospitalization_unplanned_num10     = row1[col04+38].is_a?(Numeric) ? row1[col04+38].to_i : nil
                hospitalization_emergency_num10     = row1[col04+39].is_a?(Numeric) ? row1[col04+39].to_i : nil
                hospitalization_planned_num11       = row1[col04+40].is_a?(Numeric) ? row1[col04+40].to_i : nil
                hospitalization_chemo_num11         = row1[col04+41].is_a?(Numeric) ? row1[col04+41].to_i : nil
                hospitalization_unplanned_num11     = row1[col04+42].is_a?(Numeric) ? row1[col04+42].to_i : nil
                hospitalization_emergency_num11     = row1[col04+43].is_a?(Numeric) ? row1[col04+43].to_i : nil
                hospitalization_planned_num12       = row1[col04+44].is_a?(Numeric) ? row1[col04+44].to_i : nil
                hospitalization_chemo_num12         = row1[col04+45].is_a?(Numeric) ? row1[col04+45].to_i : nil
                hospitalization_unplanned_num12     = row1[col04+46].is_a?(Numeric) ? row1[col04+46].to_i : nil
                hospitalization_emergency_num12     = row1[col04+47].is_a?(Numeric) ? row1[col04+47].to_i : nil
                hospitalization_planned_num13       = row1[col04+48].is_a?(Numeric) ? row1[col04+48].to_i : nil
                hospitalization_chemo_num13         = row1[col04+49].is_a?(Numeric) ? row1[col04+49].to_i : nil
                hospitalization_unplanned_num13     = row1[col04+50].is_a?(Numeric) ? row1[col04+50].to_i : nil
                hospitalization_emergency_num13     = row1[col04+51].is_a?(Numeric) ? row1[col04+51].to_i : nil
                hospitalization_planned_num14       = row1[col04+52].is_a?(Numeric) ? row1[col04+52].to_i : nil
                hospitalization_chemo_num14         = row1[col04+53].is_a?(Numeric) ? row1[col04+53].to_i : nil
                hospitalization_unplanned_num14     = row1[col04+54].is_a?(Numeric) ? row1[col04+54].to_i : nil
                hospitalization_emergency_num14     = row1[col04+55].is_a?(Numeric) ? row1[col04+55].to_i : nil
                hospitalization_planned_num15       = row1[col04+56].is_a?(Numeric) ? row1[col04+56].to_i : nil
                hospitalization_chemo_num15         = row1[col04+57].is_a?(Numeric) ? row1[col04+57].to_i : nil
                hospitalization_unplanned_num15     = row1[col04+58].is_a?(Numeric) ? row1[col04+58].to_i : nil
                hospitalization_emergency_num15     = row1[col04+59].is_a?(Numeric) ? row1[col04+59].to_i : nil
                hospitalization_planned_num16       = row1[col04+60].is_a?(Numeric) ? row1[col04+60].to_i : nil
                hospitalization_chemo_num16         = row1[col04+61].is_a?(Numeric) ? row1[col04+61].to_i : nil
                hospitalization_unplanned_num16     = row1[col04+62].is_a?(Numeric) ? row1[col04+62].to_i : nil
                hospitalization_emergency_num16     = row1[col04+63].is_a?(Numeric) ? row1[col04+63].to_i : nil
                hospitalization_planned_num17       = row1[col04+64].is_a?(Numeric) ? row1[col04+64].to_i : nil
                hospitalization_chemo_num17         = row1[col04+65].is_a?(Numeric) ? row1[col04+65].to_i : nil
                hospitalization_unplanned_num17     = row1[col04+66].is_a?(Numeric) ? row1[col04+66].to_i : nil
                hospitalization_emergency_num17     = row1[col04+67].is_a?(Numeric) ? row1[col04+67].to_i : nil
                hospitalization_planned_num18       = row1[col04+68].is_a?(Numeric) ? row1[col04+68].to_i : nil
                hospitalization_chemo_num18         = row1[col04+69].is_a?(Numeric) ? row1[col04+69].to_i : nil
                hospitalization_unplanned_num18     = row1[col04+70].is_a?(Numeric) ? row1[col04+70].to_i : nil
                hospitalization_emergency_num18     = row1[col04+71].is_a?(Numeric) ? row1[col04+71].to_i : nil
            end

            dpch = DpcHospital.find_by(created_year: excel.year, notice_number: notice_number, serial_number: serial_number)
            dpch = DpcHospital.find_by(created_year: excel.year, name: name) unless dpch.present?
            unless dpch.present? then
              result.warning_line_num += 1
              result.messages += message(index, 'WARNING', "対応する病院がありませんでした: " + name)
              next
            end
            dpc_hospital_id = dpch.id
            upd_hospitalization(index,rowmdc[col04+0*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,hospitalization_planned_num01,hospitalization_chemo_num01,hospitalization_unplanned_num01,hospitalization_emergency_num01)
            upd_hospitalization(index,rowmdc[col04+1*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,hospitalization_planned_num02,hospitalization_chemo_num02,hospitalization_unplanned_num02,hospitalization_emergency_num02)
            upd_hospitalization(index,rowmdc[col04+2*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,hospitalization_planned_num03,hospitalization_chemo_num03,hospitalization_unplanned_num03,hospitalization_emergency_num03)
            upd_hospitalization(index,rowmdc[col04+3*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,hospitalization_planned_num04,hospitalization_chemo_num04,hospitalization_unplanned_num04,hospitalization_emergency_num04)
            upd_hospitalization(index,rowmdc[col04+4*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,hospitalization_planned_num05,hospitalization_chemo_num05,hospitalization_unplanned_num05,hospitalization_emergency_num05)
            upd_hospitalization(index,rowmdc[col04+5*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,hospitalization_planned_num06,hospitalization_chemo_num06,hospitalization_unplanned_num06,hospitalization_emergency_num06)
            upd_hospitalization(index,rowmdc[col04+6*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,hospitalization_planned_num07,hospitalization_chemo_num07,hospitalization_unplanned_num07,hospitalization_emergency_num07)
            upd_hospitalization(index,rowmdc[col04+7*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,hospitalization_planned_num08,hospitalization_chemo_num08,hospitalization_unplanned_num08,hospitalization_emergency_num08)
            upd_hospitalization(index,rowmdc[col04+8*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,hospitalization_planned_num09,hospitalization_chemo_num09,hospitalization_unplanned_num09,hospitalization_emergency_num09)
            upd_hospitalization(index,rowmdc[col04+9*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,hospitalization_planned_num10,hospitalization_chemo_num10,hospitalization_unplanned_num10,hospitalization_emergency_num10)
            upd_hospitalization(index,rowmdc[col04+10*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,hospitalization_planned_num11,hospitalization_chemo_num11,hospitalization_unplanned_num11,hospitalization_emergency_num11)
            upd_hospitalization(index,rowmdc[col04+11*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,hospitalization_planned_num12,hospitalization_chemo_num12,hospitalization_unplanned_num12,hospitalization_emergency_num12)
            upd_hospitalization(index,rowmdc[col04+12*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,hospitalization_planned_num13,hospitalization_chemo_num13,hospitalization_unplanned_num13,hospitalization_emergency_num13)
            upd_hospitalization(index,rowmdc[col04+13*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,hospitalization_planned_num14,hospitalization_chemo_num14,hospitalization_unplanned_num14,hospitalization_emergency_num14)
            upd_hospitalization(index,rowmdc[col04+14*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,hospitalization_planned_num15,hospitalization_chemo_num15,hospitalization_unplanned_num15,hospitalization_emergency_num15)
            upd_hospitalization(index,rowmdc[col04+15*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,hospitalization_planned_num16,hospitalization_chemo_num16,hospitalization_unplanned_num16,hospitalization_emergency_num16)
            upd_hospitalization(index,rowmdc[col04+16*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,hospitalization_planned_num17,hospitalization_chemo_num17,hospitalization_unplanned_num17,hospitalization_emergency_num17)
            upd_hospitalization(index,rowmdc[col04+17*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,hospitalization_planned_num18,hospitalization_chemo_num18,hospitalization_unplanned_num18,hospitalization_emergency_num18)
          end
        rescue => e
          result.error_line_num += 1
          result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
        end
      end
    end
    return result
  end
  def self.upd_hospitalization(index,mdc_code,h,dpc_hospital_id,result,hospitalization_planned_num,hospitalization_chemo_num,hospitalization_unplanned_num,hospitalization_emergency_num)
    if h.has_key?(mdc_code) then
      mdc_layer1_code_id = h[mdc_code]
    else
      result.warning_line_num += 1
      result.messages += message(index, 'WARNING', "対応するMDCコードがありませんでした: " + mdc_code)
      return
    end
    ag = AgByDpchXMdcL1c.find_or_initialize_by(mdc_layer1_code_id: mdc_layer1_code_id, dpc_hospital_id: dpc_hospital_id)
    ag.hospitalization_planned_num   = hospitalization_planned_num
    ag.hospitalization_chemo_num     = hospitalization_chemo_num
    ag.hospitalization_unplanned_num = hospitalization_unplanned_num
    ag.hospitalization_emergency_num = hospitalization_emergency_num
    unless ag.save! then
      result.warning_line_num += 1
      result.messages += message(index, 'WARNING', "登録失敗 MDC:" + mdc_code + "施設ID:" + dpc_hospital_id)
    end
  end
  #(4)救急車による搬送の有無の医療機関別MDC別集計
  def self.import_ambulance_transport(excel)
    result = Import::ImportResult.new
    #まず全部のMDCを取っておこう
    mdc = MdcLayer1Code.where(year: excel.year)
    h = mdc.map{|mdc| [mdc.code, mdc.id]}
    h = Hash[h]

    excel.sheet_info.each_with_index do |sheet_info, sheet_no|
      sheet = excel.sheet(sheet_info.name)
      rowmdc = get_mdc_row(sheet,sheet_info,result)
      sheet_info.start_row.upto(sheet.last_row) do |index|
        begin
          row1 = sheet.row(index)    #データ行

          if to_code(row1[0]) =~ /^\d+$/
            result.total_line_num += 1
            col04                             = excel.extensions[0][0].to_cn0  #開始列
            #告示番号、通番、施設名
            if sheet.cell(1,2).to_s =~ /通番/ then    #通番あり
              notice_number           = row1[0].to_i
              serial_number           = row1[1].to_i
              name                    = row1[2].to_s
            elsif sheet.cell(1,2).to_s =~ /施設名/ then #通番なし
              notice_number         = row1[0].to_i
              serial_number         = nil
              name                  = row1[1].to_s
            else
              result.warning_line_num += 1
              result.messages += message(index, 'WARNING', "告示番号または通番が不正です　開始行: " + sheet_info.start_row.to_s)
            end

            ambulance_transport01               = row1[col04+0 ].is_a?(Numeric) ? row1[col04+0 ].to_i : nil
            ambulance_transport_total01         = row1[col04+1 ].is_a?(Numeric) ? row1[col04+1 ].to_i : nil
            ambulance_transport02               = row1[col04+2 ].is_a?(Numeric) ? row1[col04+2 ].to_i : nil
            ambulance_transport_total02         = row1[col04+3 ].is_a?(Numeric) ? row1[col04+3 ].to_i : nil
            ambulance_transport03               = row1[col04+4 ].is_a?(Numeric) ? row1[col04+4 ].to_i : nil
            ambulance_transport_total03         = row1[col04+5 ].is_a?(Numeric) ? row1[col04+5 ].to_i : nil
            ambulance_transport04               = row1[col04+6 ].is_a?(Numeric) ? row1[col04+6 ].to_i : nil
            ambulance_transport_total04         = row1[col04+7 ].is_a?(Numeric) ? row1[col04+7 ].to_i : nil
            ambulance_transport05               = row1[col04+8 ].is_a?(Numeric) ? row1[col04+8 ].to_i : nil
            ambulance_transport_total05         = row1[col04+9 ].is_a?(Numeric) ? row1[col04+9 ].to_i : nil
            ambulance_transport06               = row1[col04+10].is_a?(Numeric) ? row1[col04+10].to_i : nil
            ambulance_transport_total06         = row1[col04+11].is_a?(Numeric) ? row1[col04+11].to_i : nil
            ambulance_transport07               = row1[col04+12].is_a?(Numeric) ? row1[col04+12].to_i : nil
            ambulance_transport_total07         = row1[col04+13].is_a?(Numeric) ? row1[col04+13].to_i : nil
            ambulance_transport08               = row1[col04+14].is_a?(Numeric) ? row1[col04+14].to_i : nil
            ambulance_transport_total08         = row1[col04+15].is_a?(Numeric) ? row1[col04+15].to_i : nil
            ambulance_transport09               = row1[col04+16].is_a?(Numeric) ? row1[col04+16].to_i : nil
            ambulance_transport_total09         = row1[col04+17].is_a?(Numeric) ? row1[col04+17].to_i : nil
            ambulance_transport10               = row1[col04+18].is_a?(Numeric) ? row1[col04+18].to_i : nil
            ambulance_transport_total10         = row1[col04+19].is_a?(Numeric) ? row1[col04+19].to_i : nil
            ambulance_transport11               = row1[col04+20].is_a?(Numeric) ? row1[col04+20].to_i : nil
            ambulance_transport_total11         = row1[col04+21].is_a?(Numeric) ? row1[col04+21].to_i : nil
            ambulance_transport12               = row1[col04+22].is_a?(Numeric) ? row1[col04+22].to_i : nil
            ambulance_transport_total12         = row1[col04+23].is_a?(Numeric) ? row1[col04+23].to_i : nil
            ambulance_transport13               = row1[col04+24].is_a?(Numeric) ? row1[col04+24].to_i : nil
            ambulance_transport_total13         = row1[col04+25].is_a?(Numeric) ? row1[col04+25].to_i : nil
            ambulance_transport14               = row1[col04+26].is_a?(Numeric) ? row1[col04+26].to_i : nil
            ambulance_transport_total14         = row1[col04+27].is_a?(Numeric) ? row1[col04+27].to_i : nil
            ambulance_transport15               = row1[col04+28].is_a?(Numeric) ? row1[col04+28].to_i : nil
            ambulance_transport_total15         = row1[col04+29].is_a?(Numeric) ? row1[col04+29].to_i : nil
            ambulance_transport16               = row1[col04+30].is_a?(Numeric) ? row1[col04+30].to_i : nil
            ambulance_transport_total16         = row1[col04+31].is_a?(Numeric) ? row1[col04+31].to_i : nil
            ambulance_transport17               = row1[col04+32].is_a?(Numeric) ? row1[col04+32].to_i : nil
            ambulance_transport_total17         = row1[col04+33].is_a?(Numeric) ? row1[col04+33].to_i : nil
            ambulance_transport18               = row1[col04+34].is_a?(Numeric) ? row1[col04+34].to_i : nil
            ambulance_transport_total18         = row1[col04+35].is_a?(Numeric) ? row1[col04+35].to_i : nil

            dpch = DpcHospital.find_by(created_year: excel.year, notice_number: notice_number, serial_number: serial_number)
            dpch = DpcHospital.find_by(created_year: excel.year, name: name) unless dpch.present?
            unless dpch.present? then
              result.warning_line_num += 1
              result.messages += message(index, 'WARNING', "対応する病院がありませんでした: " + name)
              next
            end
            dpc_hospital_id = dpch.id
            s = 2 #2列とばし
            upd_ambulance_transport(index,rowmdc[col04+0*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,ambulance_transport01,ambulance_transport_total01)
            upd_ambulance_transport(index,rowmdc[col04+1*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,ambulance_transport02,ambulance_transport_total02)
            upd_ambulance_transport(index,rowmdc[col04+2*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,ambulance_transport03,ambulance_transport_total03)
            upd_ambulance_transport(index,rowmdc[col04+3*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,ambulance_transport04,ambulance_transport_total04)
            upd_ambulance_transport(index,rowmdc[col04+4*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,ambulance_transport05,ambulance_transport_total05)
            upd_ambulance_transport(index,rowmdc[col04+5*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,ambulance_transport06,ambulance_transport_total06)
            upd_ambulance_transport(index,rowmdc[col04+6*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,ambulance_transport07,ambulance_transport_total07)
            upd_ambulance_transport(index,rowmdc[col04+7*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,ambulance_transport08,ambulance_transport_total08)
            upd_ambulance_transport(index,rowmdc[col04+8*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,ambulance_transport09,ambulance_transport_total09)
            upd_ambulance_transport(index,rowmdc[col04+9*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,ambulance_transport10,ambulance_transport_total10)
            upd_ambulance_transport(index,rowmdc[col04+10*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,ambulance_transport11,ambulance_transport_total11)
            upd_ambulance_transport(index,rowmdc[col04+11*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,ambulance_transport12,ambulance_transport_total12)
            upd_ambulance_transport(index,rowmdc[col04+12*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,ambulance_transport13,ambulance_transport_total13)
            upd_ambulance_transport(index,rowmdc[col04+13*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,ambulance_transport14,ambulance_transport_total14)
            upd_ambulance_transport(index,rowmdc[col04+14*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,ambulance_transport15,ambulance_transport_total15)
            upd_ambulance_transport(index,rowmdc[col04+15*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,ambulance_transport16,ambulance_transport_total16)
            upd_ambulance_transport(index,rowmdc[col04+16*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,ambulance_transport17,ambulance_transport_total17)
            upd_ambulance_transport(index,rowmdc[col04+17*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,ambulance_transport18,ambulance_transport_total18)

          end
        rescue => e
          result.error_line_num += 1
          result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
        end
      end
    end
    return result
  end
  def self.upd_ambulance_transport(index,mdc_code,h,dpc_hospital_id,result,ambulance_transport,ambulance_transport_total)
    if h.has_key?(mdc_code) then
      mdc_layer1_code_id = h[mdc_code]
    else
      result.warning_line_num += 1
      result.messages += message(index, 'WARNING', "対応するMDCコードがありませんでした: " + mdc_code)
      return
    end
    ag = AgByDpchXMdcL1c.find_or_initialize_by(mdc_layer1_code_id: mdc_layer1_code_id, dpc_hospital_id: dpc_hospital_id)
    ag.ambulance_transport          = ambulance_transport
    ag.ambulance_transport_total    = ambulance_transport_total
    unless ag.save! then
      result.warning_line_num += 1
      result.messages += message(index, 'WARNING', "登録失敗 MDC:" + mdc_code + "施設ID:" + dpc_hospital_id)
    end
  end
  #(5)入院から24時間以内の死亡の有無の医療機関別MDC別集計
  def self.import_death_within_24hours(excel)
    result = Import::ImportResult.new
    #まず全部のMDCを取っておこう
    mdc = MdcLayer1Code.where(year: excel.year)
    h = mdc.map{|mdc| [mdc.code, mdc.id]}
    h = Hash[h]
    excel.sheet_info.each_with_index do |sheet_info, sheet_no|
      sheet = excel.sheet(sheet_info.name)
      rowmdc = get_mdc_row(sheet,sheet_info,result)
      sheet_info.start_row.upto(sheet.last_row) do |index|
        begin
          row1 = sheet.row(index)    #データ行

          if to_code(row1[0]) =~ /^\d+$/
            result.total_line_num += 1
            col04                             = excel.extensions[0][0].to_cn0  #開始列
            #告示番号、通番、施設名
            if sheet.cell(1,2).to_s =~ /通番/ then    #通番あり
              notice_number           = row1[0].to_i
              serial_number           = row1[1].to_i
              name                    = row1[2].to_s
            elsif sheet.cell(1,2).to_s =~ /施設名/ then #通番なし
              notice_number         = row1[0].to_i
              serial_number         = nil
              name                  = row1[1].to_s
            else
              result.warning_line_num += 1
              result.messages += message(index, 'WARNING', "告示番号または通番が不正です　開始行: " + sheet_info.start_row.to_s)
            end
            death_within_24hours_num01               = row1[col04+0 ].is_a?(Numeric) ? row1[col04+0 ].to_i : nil
            death_within_24hours_num_total01         = row1[col04+1 ].is_a?(Numeric) ? row1[col04+1 ].to_i : nil
            death_within_24hours_num02               = row1[col04+2 ].is_a?(Numeric) ? row1[col04+2 ].to_i : nil
            death_within_24hours_num_total02         = row1[col04+3 ].is_a?(Numeric) ? row1[col04+3 ].to_i : nil
            death_within_24hours_num03               = row1[col04+4 ].is_a?(Numeric) ? row1[col04+4 ].to_i : nil
            death_within_24hours_num_total03         = row1[col04+5 ].is_a?(Numeric) ? row1[col04+5 ].to_i : nil
            death_within_24hours_num04               = row1[col04+6 ].is_a?(Numeric) ? row1[col04+6 ].to_i : nil
            death_within_24hours_num_total04         = row1[col04+7 ].is_a?(Numeric) ? row1[col04+7 ].to_i : nil
            death_within_24hours_num05               = row1[col04+8 ].is_a?(Numeric) ? row1[col04+8 ].to_i : nil
            death_within_24hours_num_total05         = row1[col04+9 ].is_a?(Numeric) ? row1[col04+9 ].to_i : nil
            death_within_24hours_num06               = row1[col04+10].is_a?(Numeric) ? row1[col04+10].to_i : nil
            death_within_24hours_num_total06         = row1[col04+11].is_a?(Numeric) ? row1[col04+11].to_i : nil
            death_within_24hours_num07               = row1[col04+12].is_a?(Numeric) ? row1[col04+12].to_i : nil
            death_within_24hours_num_total07         = row1[col04+13].is_a?(Numeric) ? row1[col04+13].to_i : nil
            death_within_24hours_num08               = row1[col04+14].is_a?(Numeric) ? row1[col04+14].to_i : nil
            death_within_24hours_num_total08         = row1[col04+15].is_a?(Numeric) ? row1[col04+15].to_i : nil
            death_within_24hours_num09               = row1[col04+16].is_a?(Numeric) ? row1[col04+16].to_i : nil
            death_within_24hours_num_total09         = row1[col04+17].is_a?(Numeric) ? row1[col04+17].to_i : nil
            death_within_24hours_num10               = row1[col04+18].is_a?(Numeric) ? row1[col04+18].to_i : nil
            death_within_24hours_num_total10         = row1[col04+19].is_a?(Numeric) ? row1[col04+19].to_i : nil
            death_within_24hours_num11               = row1[col04+20].is_a?(Numeric) ? row1[col04+20].to_i : nil
            death_within_24hours_num_total11         = row1[col04+21].is_a?(Numeric) ? row1[col04+21].to_i : nil
            death_within_24hours_num12               = row1[col04+22].is_a?(Numeric) ? row1[col04+22].to_i : nil
            death_within_24hours_num_total12         = row1[col04+23].is_a?(Numeric) ? row1[col04+23].to_i : nil
            death_within_24hours_num13               = row1[col04+24].is_a?(Numeric) ? row1[col04+24].to_i : nil
            death_within_24hours_num_total13         = row1[col04+25].is_a?(Numeric) ? row1[col04+25].to_i : nil
            death_within_24hours_num14               = row1[col04+26].is_a?(Numeric) ? row1[col04+26].to_i : nil
            death_within_24hours_num_total14         = row1[col04+27].is_a?(Numeric) ? row1[col04+27].to_i : nil
            death_within_24hours_num15               = row1[col04+28].is_a?(Numeric) ? row1[col04+28].to_i : nil
            death_within_24hours_num_total15         = row1[col04+29].is_a?(Numeric) ? row1[col04+29].to_i : nil
            death_within_24hours_num16               = row1[col04+30].is_a?(Numeric) ? row1[col04+30].to_i : nil
            death_within_24hours_num_total16         = row1[col04+31].is_a?(Numeric) ? row1[col04+31].to_i : nil
            death_within_24hours_num17               = row1[col04+32].is_a?(Numeric) ? row1[col04+32].to_i : nil
            death_within_24hours_num_total17         = row1[col04+33].is_a?(Numeric) ? row1[col04+33].to_i : nil
            death_within_24hours_num18               = row1[col04+34].is_a?(Numeric) ? row1[col04+34].to_i : nil
            death_within_24hours_num_total18         = row1[col04+35].is_a?(Numeric) ? row1[col04+35].to_i : nil

            dpch = DpcHospital.find_by(created_year: excel.year, notice_number: notice_number, serial_number: serial_number)
            dpch = DpcHospital.find_by(created_year: excel.year, name: name) unless dpch.present?
            unless dpch.nil? then
              dpc_hospital_id = dpch.id
            else
              result.warning_line_num += 1
              result.messages += message(index, 'WARNING', "対応する病院がありませんでした: " + name)
              next
            end
            #MDC01
            mdc_code = rowmdc[col04+0].to_s
            s = 2 #2列とばし
            upd_death_within_24hours(index,rowmdc[col04+0*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,death_within_24hours_num01,death_within_24hours_num_total01)
            upd_death_within_24hours(index,rowmdc[col04+1*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,death_within_24hours_num02,death_within_24hours_num_total02)
            upd_death_within_24hours(index,rowmdc[col04+2*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,death_within_24hours_num03,death_within_24hours_num_total03)
            upd_death_within_24hours(index,rowmdc[col04+3*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,death_within_24hours_num04,death_within_24hours_num_total04)
            upd_death_within_24hours(index,rowmdc[col04+4*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,death_within_24hours_num05,death_within_24hours_num_total05)
            upd_death_within_24hours(index,rowmdc[col04+5*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,death_within_24hours_num06,death_within_24hours_num_total06)
            upd_death_within_24hours(index,rowmdc[col04+6*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,death_within_24hours_num07,death_within_24hours_num_total07)
            upd_death_within_24hours(index,rowmdc[col04+7*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,death_within_24hours_num08,death_within_24hours_num_total08)
            upd_death_within_24hours(index,rowmdc[col04+8*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,death_within_24hours_num09,death_within_24hours_num_total09)
            upd_death_within_24hours(index,rowmdc[col04+9*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,death_within_24hours_num10,death_within_24hours_num_total10)
            upd_death_within_24hours(index,rowmdc[col04+10*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,death_within_24hours_num11,death_within_24hours_num_total11)
            upd_death_within_24hours(index,rowmdc[col04+11*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,death_within_24hours_num12,death_within_24hours_num_total12)
            upd_death_within_24hours(index,rowmdc[col04+12*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,death_within_24hours_num13,death_within_24hours_num_total13)
            upd_death_within_24hours(index,rowmdc[col04+13*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,death_within_24hours_num14,death_within_24hours_num_total14)
            upd_death_within_24hours(index,rowmdc[col04+14*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,death_within_24hours_num15,death_within_24hours_num_total15)
            upd_death_within_24hours(index,rowmdc[col04+15*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,death_within_24hours_num16,death_within_24hours_num_total16)
            upd_death_within_24hours(index,rowmdc[col04+16*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,death_within_24hours_num17,death_within_24hours_num_total17)
            upd_death_within_24hours(index,rowmdc[col04+17*s].to_s.gsub(".0",""),h,dpc_hospital_id,result,death_within_24hours_num18,death_within_24hours_num_total18)

          end
        rescue => e
          result.error_line_num += 1
          result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
        end
      end
    end
    return result
  end
  def self.upd_death_within_24hours(index,mdc_code,h,dpc_hospital_id,result,death_within_24hours_num,death_within_24hours_num_total)
    if h.has_key?(mdc_code) then
      mdc_layer1_code_id = h[mdc_code]
    else
      result.warning_line_num += 1
      result.messages += message(index, 'WARNING', "対応するMDCコードがありませんでした: " + mdc_code)
      return
    end
    ag = AgByDpchXMdcL1c.find_or_initialize_by(mdc_layer1_code_id: mdc_layer1_code_id, dpc_hospital_id: dpc_hospital_id)
    ag.death_within_24hours_num          = death_within_24hours_num
    ag.death_within_24hours_num_total    = death_within_24hours_num_total
    unless ag.save! then
      result.warning_line_num += 1
      result.messages += message(index, 'WARNING', "登録失敗 MDC:" + mdc_code + "施設ID:" + dpc_hospital_id)
    end
  end
  #(14)在院日数の平均の差_MDC別
  def self.import_avg_hospital_days(excel)
    result = Import::ImportResult.new
    #まず全部のMDCを取っておこう
    mdc = MdcLayer1Code.where(year: excel.year)
    h = mdc.map{|mdc| [mdc.code, mdc.id]}
    h = Hash[h]

    excel.sheet_info.each_with_index do |sheet_info, sheet_no|
      sheet = excel.sheet(sheet_info.name)
        sheet_info.start_row.upto(sheet.last_row) do |index|
        begin
          row1 = sheet.row(index)    #データ行
          if to_code(row1[0]) =~ /^\d+$/
            result.total_line_num += 1
            #告示番号、通番、施設名
            if sheet.cell(2,2).to_s =~ /通番/ then    #通番あり
              notice_number           = row1[0].to_i
              serial_number           = row1[1].to_i
              name                    = row1[2].to_s
            elsif sheet.cell(3,'B').to_s =~ /施設名/ || sheet.cell(2,'B').to_s =~ /施設名/ then #通番なし
              notice_number         = row1[0].to_i
              serial_number         = nil
              name                  = row1[1].to_s
            else
              result.warning_line_num += 1
              result.messages += message(index, 'WARNING', "告示番号または通番が不正です　開始行: " + sheet_info.start_row.to_s)
            end
            col04                             = excel.extensions[0][0].to_cn0  #開始列
            #Get MDC from sheet name
            if /(\d{2})/=~sheet_info.name then
              mdc_code = $1
            else
              result.warning_line_num += 1
              result.messages += message(index, 'WARNING', "シート名からMDCコードが読み取れません　シート名:" + sheet_info.name)
              return
            end
            avg_hospital_days_number	                       =	row1[col04+0 ].is_a?(Numeric) ? row1[col04+0 ].to_i : nil
            avg_hospital_days_by_hopital	                   =	row1[col04+1 ].is_a?(Numeric) ? row1[col04+1 ].to_f : nil
            avg_hospital_days_whole_country	                 =	row1[col04+2 ].is_a?(Numeric) ? row1[col04+2 ].to_f : nil
            avg_hospital_days_index_patient_format	         =	row1[col04+3 ].is_a?(Numeric) ? row1[col04+3 ].to_f : nil
            avg_hospital_days_whole_country_patient_format	 =	row1[col04+4 ].is_a?(Numeric) ? row1[col04+4 ].to_f : nil
            avg_hospital_days_index	                         =	row1[col04+5 ].is_a?(Numeric) ? row1[col04+5 ].to_f : nil

            dpch = DpcHospital.find_by(created_year: excel.year, notice_number: notice_number, serial_number: serial_number)
            dpch = DpcHospital.find_by(created_year: excel.year, name: name) unless dpch.present?

            unless dpch.present? then
              result.warning_line_num += 1
              result.messages += message(index, 'WARNING', "対応する病院がありませんでした: " + name)
              next
            end
            dpc_hospital_id = dpch.id

            upd_avg_hospital_days(index,mdc_code,h,dpc_hospital_id,result,avg_hospital_days_number,avg_hospital_days_by_hopital,avg_hospital_days_whole_country,avg_hospital_days_index_patient_format,avg_hospital_days_whole_country_patient_format,avg_hospital_days_index)

          end
        rescue => e
          result.error_line_num += 1
          result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
        end
      end
    end
    return result
  end
  def self.upd_avg_hospital_days(index,mdc_code,h,dpc_hospital_id,result,avg_hospital_days_number,avg_hospital_days_by_hopital,avg_hospital_days_whole_country,avg_hospital_days_index_patient_format,avg_hospital_days_whole_country_patient_format,avg_hospital_days_index)
    if h.has_key?(mdc_code) then
      mdc_layer1_code_id = h[mdc_code]
    else
      result.warning_line_num += 1
      result.messages += message(index, 'WARNING', "対応するMDCコードがありませんでした: " + mdc_code)
      return
    end
    ag = AgByDpchXMdcL1c.find_or_initialize_by(mdc_layer1_code_id: mdc_layer1_code_id, dpc_hospital_id: dpc_hospital_id)

    ag.avg_hospital_days_number                       = avg_hospital_days_number
    ag.avg_hospital_days_by_hopital                   = avg_hospital_days_by_hopital
    ag.avg_hospital_days_whole_country                = avg_hospital_days_whole_country
    ag.avg_hospital_days_index_patient_format         = avg_hospital_days_index_patient_format
    ag.avg_hospital_days_whole_country_patient_format = avg_hospital_days_whole_country_patient_format
    ag.avg_hospital_days_index                        = avg_hospital_days_index
    unless ag.save! then
      result.warning_line_num += 1
      result.messages += message(index, 'WARNING', "登録失敗 MDC:" + mdc_code + "施設ID:" + dpc_hospital_id)
    end
  end


end
