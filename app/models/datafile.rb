class Datafile < ActiveRecord::Base
  after_initialize :set_default_value, if: :new_record?
  enum status: { not_yet_import: 0, importing: 1, imported: 2, cannot_imoprt: 3, failed: 4}

  private
  def set_default_value
    if filepath =~ /.xls|.xlsx|.csv|.CSV/
      self.status = :not_yet_import
    else
      self.status = :cannot_imoprt
    end
  end

end
