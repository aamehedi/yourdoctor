class WhytPlotApi < ActiveRecord::Base
  validates :name, presence: true, :uniqueness => true
  validates :api_sql, presence: true,
            format: {
              with: /\A(?!.*(delete|truncate|drop|;)).+\z/m,
              message: "api '%{value}' 入力なし、または該当の文字列は無効です"
            }
  validates :version, presence: true,
            format: {
              with: /\A([1-9]\d*|0)(\.\d+)?\z/i,
              message: 'バージョンを入力してください'
            }

  default_scope ->{ order('id') }

  def params
    match = api_sql.scan(/\[(.*)\]/).to_a
    match.present? ? match.join(",").split(",") : Array.new
  end

end
