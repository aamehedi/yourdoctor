class Municipality < ActiveRecord::Base
  has_many :medical_municipalities
  has_many :addresses
  has_many :populations
  has_many :hospitals

  extend ImportUtil
  #地方自治体コード
  def self.import(excel)
    result = Import::ImportResult.new

    excel.book.sheets.each do |sheet_info|
      sheet = excel.sheet(sheet_info)
      if sheet_info == 'rw_info'
        break
      end
      sheet.first_row.upto(sheet.last_row) do |index|
        result.total_line_num += 1

        content = sheet.row(index)
        municipality_id = content[0].to_s.gsub(".0","")
        #地方自治体コードを整形
        if municipality_id =~ /^0[0-9]{5,6}/ then
          municipality_id.slice!(0)
          municipality_id = municipality_id.chop!
        elsif municipality_id =~ /^(?!0)\d{4,6}/ then
          municipality_id = municipality_id.chop!
        else
          next
        end

        unless municipality_id.present?
          result.warning_line_num += 1
          result.messages += message(index, 'ERROR', '地方自治体コードがありません')
          next
        end

        #政令指定都市を分けるロジック
        if content[1] =~ /市/ then
          p city_name = content[1]
          city_name_kana = content[2]
          is_ordinance_designated_city = true

          #政令指定都市に県がはいるようにする
          if municipality_id.length == 4
            pref_id = municipality_id[0,1].to_i
          elsif municipality_id.length == 5
            pref_id =  municipality_id[0,2].to_i
          end
          prefecture = Prefecture.where(id: pref_id)
          if prefecture.count == 0
            result.warning_line_num += 1
            result.messages += message(index, 'ERROR', '地方自治体コードが不正です')
          else
            prefecture_name = prefecture[0].name
          end
        else
          prefecture_name = content[1]
          p city_name = content[2]
          prefecture_name_kana = content[3]
          city_name_kana = content[4]
        end

        begin
          municipality = Municipality.find_or_initialize_by(
            id: municipality_id,
            city_name: city_name
          )
          if is_ordinance_designated_city then
            municipality.is_ordinance_designated_city = true
          end

          municipality.prefecture_name = prefecture_name
          municipality.prefecture_name_kana = prefecture_name_kana
          municipality.city_name_kana = city_name_kana
          municipality.save!

        rescue => e
          result.error_line_num += 1
          result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
        end
      end
    end
    return result
  end

end
