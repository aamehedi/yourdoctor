class EmergencyRequest < ApplicationRecord
  belongs_to :doctor
  belongs_to :emergency_request_hospital
  belongs_to :whytlink_chatroom

  has_one :hospital, through: :emergency_request_hospital
  has_many :emergency_request_hospitals, dependent: :destroy
  has_many :hospitals, through: :emergency_request_hospitals
  has_many :doctor_feedbacks, through: :emergency_request_hospitals

  alias_attribute :accepted_hospital, :hospital

  enum status: %w(requested confirmed canceled)
  enum closing_reason: %w(patient's_condition_changed found_new_hospital others)

  validates :time, presence: true
  validates :emergency_request_hospitals, length: {minimum: Settings
    .default_minimum_child}
  validates_associated :doctor, :emergency_request_hospitals
  validates :whytlink_chatroom, presence: true

  accepts_nested_attributes_for :emergency_request_hospitals

  CONFIRM_PARAMS = [:emergency_request_hospital_id, :status]
  CANCEL_PARAMS = [:status, :closing_reason, :closing_comment]

  scope :emergency_request_and_feedbacks, -> emergency_request_id, status{
    where(id: emergency_request_id).includes(emergency_request_hospitals:
      {doctor_feedbacks: :doctor}).where(doctor_feedbacks:
      {status: status || DoctorFeedback.statuses.values})
  }

  def not_editable?
    self.canceled? || self.confirmed? && self.updated_at < 3.hours.ago
  end

  def doctor_accepted? doctor_id
    EmergencyRequestHospital.where(emergency_request_id: id)
      .joins(:doctor_feedbacks).where(doctor_feedbacks:
      {doctor_id: doctor_id, status: DoctorFeedback.statuses[:confirmed]})
      .count > 0
  end

  def can_add_member? current_doctor_id
    return requested? if doctor_id == current_doctor_id
    confirmed? && doctor_accepted?(current_doctor_id)
  end

  def emergency_request_hospital_update_params params
    emergency_request_hospital = self.emergency_request_hospitals
      .find_by_hospital_id params[:hospital_id]
    if emergency_request_hospital
      {
        id: emergency_request_hospital.id,
        hospital_id: params[:hospital_id],
        doctor_ids: emergency_request_hospital.doctor_ids + params[:doctor_ids]
      }
    else
      {hospital_id: params[:hospital_id], doctor_ids: params[:doctor_ids]}
    end
  end

  def add_member_params params
    emergency_request_hospitals = []
    doctor_ids = []
    params[:emergency_request_hospitals_attributes].each do |erh|
      doctor_ids += erh[:doctor_ids]
      emergency_request_hospitals <<
        emergency_request_hospital_update_params(erh)
    end
    {
      emergency_request_hospitals: emergency_request_hospitals,
      whytlink_chatroom_params: (whytlink_chatroom.doctor_ids + doctor_ids).uniq
    }
  end

  def add_member_in_emergency_request params
    self.class.transaction do
      update_params = add_member_params params
      update_attributes! emergency_request_hospitals_attributes:
        update_params[:emergency_request_hospitals]
      whytlink_chatroom.update_attributes! doctor_ids:
        update_params[:whytlink_chatroom_params]
    end
  rescue
    return false
  end

  def update_emergency_request params, request_creator_id
    return add_member_in_emergency_request params if can_add_member?(
      request_creator_id) && params[:emergency_request_hospitals_attributes]
    update_params = EmergencyRequest.update_params params
    update_params && update_attributes(update_params)
  end

  class << self
    def confirm_emergency_request_params params
      return unless params[:emergency_request_hospital_id]
      params.select do |key|
        CONFIRM_PARAMS.include? key.to_sym
      end
    end

    def cancel_emergency_request_params params
      return unless params[:closing_reason]
      params.select do |key|
        CANCEL_PARAMS.include? key.to_sym
      end
    end

    def update_params params
      if params[:status] == EmergencyRequest.statuses.keys[1]
        DoctorFeedback.where(emergency_request_hospital_id:
        params[:emergency_request_hospital_id])
          .update_all(status: params[:status])
        confirm_emergency_request_params params
      elsif params[:status] == EmergencyRequest.statuses.keys[2]
        cancel_emergency_request_params params
      end
    end

    def create_params params, request_creator_id
      doctor_ids = []
      hospital_ids = []
      emergency_request_hospitals = []
      params[:emergency_request_hospitals_attributes].each do |erh|
        doctor_ids << erh[:doctor_ids]
        hospital_ids << erh[:hospital_id]
        emergency_request_hospitals <<
          {hospital_id: erh[:hospital_id], doctor_ids: erh[:doctor_ids]}
      end
      doctor_ids.flatten!.uniq!
      return unless EmergencyDoctorHospital.exists? doctor_id: doctor_ids
      return unless EmergencyDoctorHospital.exists? hospital_id: hospital_ids
      messages_attributes = params[:whytlink_messages_attributes].map do |item|
        item.merge(doctor_id: request_creator_id)
      end
      {
        chatroom_type: :multi,
        doctor_ids: doctor_ids.push(request_creator_id),
        whytlink_messages_attributes: messages_attributes,
        emergency_request_attributes: {
          doctor_id: request_creator_id,
          time: params[:time], status: :requested,
          emergency_request_hospitals_attributes: emergency_request_hospitals
        }
      }
    end

    def create_emergency_request params, request_creator_id
      create_params = EmergencyRequest.create_params(params, request_creator_id)
      whytlink_chatroom = WhytlinkChatroom.new create_params
      WhytlinkChatroom.transaction do
        whytlink_chatroom.save!
        whytlink_chatroom.whytlink_doctor_chatrooms.find_by_doctor_id(
          request_creator_id).update_attributes! admin: true
        whytlink_chatroom.emergency_request.reload
      end
    rescue
      return false
    end
  end
end
