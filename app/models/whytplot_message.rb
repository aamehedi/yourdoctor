class WhytplotMessage < ApplicationRecord
  belongs_to :mr
  belongs_to :whytplot_chatroom

  enum message_type: [:text, :chart, :scenario, :image, :file]

  after_create_commit :broadcast_message

  MESSAGE_LIMIT = Settings.whytlink_message.message_limit

  scope :unread, ->(whytplot_chatroom_id, last_read_message_id) {
    where(whytplot_chatroom_id: whytplot_chatroom_id)
      .where "id > ?", last_read_message_id.to_i
  }

  class << self
    def get_messages last_seen_message, limit, last_message_excluded = "false"
      last_message_excluded = (last_message_excluded == "true") ? true : false
      operator = last_message_excluded ? "<" : "<="
      where("whytplot_chatroom_id = ?",
        last_seen_message.whytplot_chatroom_id).order("id DESC")
        .where("id #{operator} ?", last_seen_message.id).limit limit
    end

    def check_authorization_for_get_messages? id, mr_id
      WhytplotMessage.joins(whytplot_chatroom: :whytplot_mr_chatrooms)
        .where(id: id)
        .where("whytplot_mr_chatrooms.mr_id = #{mr_id}").present?
    end
  end

  private
  def broadcast_message
    ActionCable.server.broadcast("whytplot_room_#{self.whytplot_chatroom_id}",
      self)
  end
end
