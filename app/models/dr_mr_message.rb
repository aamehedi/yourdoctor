class DrMrMessage < ApplicationRecord
  belongs_to :doctor
  belongs_to :mr
  belongs_to :dr_mr_chatroom

  enum message_type: [:text, :image, :video]

  after_create_commit :broadcast_message

  validates :dr_mr_chatroom, presence: true
  validates :content, presence: true, allow_blank: false, if: :text?
  validates :attachment, presence: true, unless: :text?
  validate :file_size_validation, unless: :text?

  mount_uploader :attachment, AttachmentUploader

  scope :unread, ->(dr_mr_chatroom_id, last_read_message_id) {
    where(dr_mr_chatroom_id: dr_mr_chatroom_id)
      .where "id > ?", last_read_message_id.to_i
  }

  def image_size_invalid?
    attachment.file.size > Settings.dr_mr_message.image_max_file_size
  end

  def video_size_invalid?
    attachment.file.size > Settings.dr_mr_message.video_max_file_size
  end

  def file_size_validation
    unless attachment.file
      return errors.add :attachment, I18n.t("whytlink_no_file_attached_error")
    end
    if image? && image_size_invalid?
      errors.add :attachment, I18n.t("dr_mr_message_image_size_error")
    elsif video? && video_size_invalid?
      errors.add :attachment, I18n.t("dr_mr_message_video_size_error")
    end
  end

  class << self
    def get_messages last_seen_message, limit, last_message_excluded = "false"
      last_message_excluded = (last_message_excluded == "true") ? true : false
      operator = last_message_excluded ? "<" : "<="
      where("dr_mr_chatroom_id = ?", last_seen_message.dr_mr_chatroom_id)
        .order("id DESC").where("id #{operator} ?", last_seen_message.id)
        .limit limit
    end
  end
  private
  def broadcast_message
    ActionCable.server.broadcast("dr_mr_room_#{dr_mr_chatroom_id}", self)
  end
end
