class WhytlinkGroupChatInfo < ApplicationRecord
  belongs_to :whytlink_chatroom
  belongs_to :affliation, polymorphic: true

  has_many :whytlink_group_chat_requests, dependent: :destroy

  enum affliation_type: [:Hospital, :University, :Society]

  VALID_ID_NUM = 0

  mount_uploader :avatar, GroupChatAvatarUploader

  validates :name, presence: true
  validates :affliation_id, presence: true
  validates :affliation_type, presence: true
  validates_associated :whytlink_group_chat_requests
end
