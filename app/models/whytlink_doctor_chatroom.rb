class WhytlinkDoctorChatroom < ApplicationRecord
  belongs_to :whytlink_chatroom
  belongs_to :doctor

  validates :doctor, presence: true

  scope :admin, ->{where admin: true}

  class << self
    def update_whytlink_doctor_chatroom data
      whytlink_doctor_chatroom = WhytlinkDoctorChatroom.find data["id"]
      if whytlink_doctor_chatroom.update_attributes data
        ActionCable.server.broadcast(
          "whytlink_room_#{whytlink_doctor_chatroom.whytlink_chatroom_id}",
          whytlink_doctor_chatroom)
      end
    end
  end
end
