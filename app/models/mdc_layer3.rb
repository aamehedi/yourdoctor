class MdcLayer3 < ActiveRecord::Base
  belongs_to :mdc_layer2
  has_one :ag_by_mdc
  delegate :mdc_layer1, to: :mdc_layer2
  delegate :mdc_layer1_code, to: :mcd_layer1
end
