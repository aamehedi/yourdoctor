class Leader < User
  has_many :staffs, :class_name => "Staff", :foreign_key => "user_id", :dependent => :destroy
  accepts_nested_attributes_for :staffs
end
