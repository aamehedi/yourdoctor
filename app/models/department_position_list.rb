class DepartmentPositionList < ActiveRecord::Base
  belongs_to :department_list
  belongs_to :position, :class_name => 'DepartmentPosition', :foreign_key => :department_position_id
  #validates :department_list_id, :department_position_id, presence: true
  has_paper_trail  

  #タイムライン
  after_create   :log_timeline_by_create
  after_update   :log_timeline_by_update
  before_destroy :log_timeline_by_destroy

  #accepts_nested_attributes_for :position_attributes
  def position_attributes=(attributes)
    p "******* position_attributes *******"
    if attributes[:name].strip.present?
      #IDが無い、存在しないIDが指定されている場合は同学会、同名の役職を探すか、なければ新規追加
      if attributes[:id].nil? || DepartmentPosition.find_by(id: attributes[:id]).nil?
        if attributes[:department_id].present?
          position = DepartmentPosition.find_or_create_by(name: attributes[:name].strip, department_id: attributes[:department_id])
        else
          position = DepartmentPosition.create(name: attributes[:name].strip)
        end
      else
        position = DepartmentPosition.find(attributes[:id])
      end
      self.department_position_id = position.id
    else
      #self.destroy
    end
  end

  private
    def log_timeline_by_create
      Timeline.create(editor: Doctor.editor, target: self.department_list.doctor, model: self, action: "create", meta: self.position.name)
    end

    def log_timeline_by_update
      if self.changed?
        Timeline.create(editor: Doctor.editor, target: self.department_list.doctor, model: self, action: "update", meta: self.position.name)
      end
    end

    def log_timeline_by_destroy
      Timeline.create(editor: Doctor.editor, target: self.department_list.doctor, model: self, action: "destroy", meta: self.position.name)
    end
end
