class University < ActiveRecord::Base
  has_many :university_lists
  has_many :doctors, through: :university_lists
  has_many :whytlink_group_chat_infos, as: :affliation

  accepts_nested_attributes_for :university_lists, allow_destroy: :true

  def merge(src_university)
    src_university.university_lists.each do | university_list | 
      if UniversityList.where(university_id: self.id).where(doctor_id: university_list.doctor_id).first
      	university_list.destroy
      else
        university_list.update(university_id: self.id)
      end
    end
    src_university.destroy
  end

end
