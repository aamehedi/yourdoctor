class Company < ApplicationRecord
  has_many :projects, dependent: :destroy
  has_many :branches, dependent: :destroy
  validates :name, presence: true

  def count_mr_by_type type
    Mr.where(company_id: self.id).send(type).count
  end
end
