class WhytplotMrChatroom < ApplicationRecord
  belongs_to :whytplot_chatroom
  belongs_to :mr

  class << self
    def update_whytplot_mr_chatroom data
      whytplot_mr_chatroom = WhytplotMrChatroom.find data["id"]
      if whytplot_mr_chatroom.update_attributes data
        ActionCable.server.broadcast(
          "whytplot_room_#{whytplot_mr_chatroom.whytplot_chatroom_id}",
          whytplot_mr_chatroom)
      end
    end
  end
end
