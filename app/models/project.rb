class Project < ApplicationRecord
  belongs_to :project_manager, class_name: Mr.name, foreign_key: :mr_id
  belongs_to :company
  has_many :mr_projects, dependent: :destroy
  has_many :mrs, through: :mr_projects
  belongs_to :whytplot_chatroom

  validates :name, presence: true, uniqueness: {scope: :company_id}
  validates :mr_id, presence: true
  validates :company_id, presence: true

  after_commit :find_or_create_default_chatroom, on: :create
  before_commit :update_default_chatroom, on: :update
  after_commit :destroy_default_chatroom, on: :destroy

  attr_accessor :param_mr_ids

  def find_or_create_default_chatroom
    unless self.whytplot_chatroom
      whytplot_chatroom = WhytplotChatroom.new(name: self.name,
        chatroom_type: Settings.whytplot_chatroom.group_chat,
        mr_ids: self.mr_ids)
      self.whytplot_chatroom = whytplot_chatroom
      self.save
    end
  end

  def update_default_chatroom
    unless whytplot_chatroom.mr_ids == param_mr_ids
      self.whytplot_chatroom.mr_ids = param_mr_ids
    end
  end

  def destroy_default_chatroom
    self.whytplot_chatroom.try &:destroy
  end
end
