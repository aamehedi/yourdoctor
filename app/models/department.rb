class Department < ActiveRecord::Base
  has_many :department_lists, dependent: :destroy
  has_many :doctors, through: :department_lists
  belongs_to :hospital
  acts_as_taggable_on :department_types, :urls

  validates :hospital_id, :name, presence: true
  validates_length_of :name, allow_blank: false

  def categories
    self.department_type_list.map{|id| DepartmentCategory.find(id) }
  end
end
