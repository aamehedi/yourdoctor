class AllMail < ActiveRecord::Base
  has_many :mail_logs
  before_create :add_br_to_content
  after_create :setup_mails

  def add_br_to_content
    self.content = self.content.gsub(/(\r\n|\r|\n)/, "<br />")
  end
  def setup_mails
    Doctor.where.not(email: nil).each do | d |
      MailLog.create(all_mail: self, doctor: d)
    end
    self.delay.send_mails
  end

  def send_mails
    mail_logs = self.mail_logs.where(done: false)
    if mail_logs.present?
      t = mail_logs.first
      p t.id
      p "メール送信"
      t.update_column(:done, true)
      DoctorsMailer.delay.all_mail(self, t.doctor)
      self.delay.send_mails
    else
      self.update_column(:done, true)
    end
  end

  def all_done?
  end


end
