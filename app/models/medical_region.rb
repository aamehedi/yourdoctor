class MedicalRegion < ActiveRecord::Base
  has_many :medical_municipalities, primary_key: "code", :foreign_key => :medical_region_code
  validates :code, presence: true, uniqueness: true

  accepts_nested_attributes_for :medical_municipalities
  extend ImportUtil
  #2次医療圏_地域マスタ
  def self.import(excel)
    result = Import::ImportResult.new
    medical_region_list = Array.new
    excel.sheet_info.each do |sheet_info|
      sheet = excel.sheet(sheet_info.name)
      sheet_info.start_row.upto(sheet.last_row) do |index|
        result.total_line_num += 1

        row = sheet.row(index)
        if to_code(row[1]) =~ /^\d+$/

          level = 2 #TODO 1次,3次のため仕組み
          area_id           = row[1].to_s =~ /^\d+$/ ? row[1].to_i : nil
          prefecture_id     = row[3].to_s =~ /^\d+$/ ? row[3].to_i : nil
          code              = row[5].to_s
          name              = row[6].to_s
          municipality_id   = row[7].to_s =~ /^\d+$/ ? row[7].to_i : nil
          municipality_name = row[8].to_s

          unless name.present? && code.present?
            result.warning_line_num += 1
            result.messages += message(index, 'WARNING', '2次医療圏コードまたは2次医療圏名が存在しません')
          end

          begin
            medical_region = MedicalRegion.find_or_initialize_by(name: name, code: code)
            medical_region.level = level
            medical_region.area_id = area_id
            medical_region.prefecture_id = prefecture_id
            # medical_minicipalitiesレコード作成・更新
            medical_region.medical_municipalities.find_or_initialize_by(name: municipality_name, municipality_id: municipality_id)
            medical_region.save
            medical_region_list << medical_region.name
          rescue => e
          result.error_line_num += 1
          result.messages += message(index, 'ERROR', e.message + e.backtrace.to_s)
          end
        end
      end
    end
    return result
  end

end
