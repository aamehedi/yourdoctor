# -*- coding: utf-8 -*-
class PreMonograph < ActiveRecord::Base
  belongs_to :doctor
  paginates_per 20
  has_paper_trail
  acts_as_taggable
end
