class Skill < ActiveRecord::Base
  belongs_to :doctor
  belongs_to :icd10
  has_many :recommends, :dependent => :destroy
  accepts_nested_attributes_for :recommends
  has_many :recommenders, through: :recommends, :source => :doctor, :class_name => 'Doctor'
  has_paper_trail  

  scope :rank, -> { includes(:recommends).group('recommends.skill_id').order('count_recommends_id desc').count('recommends.id') }

  attr_accessor :recommended

  #タイムライン
  after_create   :log_timeline_by_create
  after_update   :log_timeline_by_update
  before_destroy :log_timeline_by_destroy

  validates :doctor_id,
    uniqueness: {
      message: "既に登録済みの専門分野です",
      scope: [:icd10_id]
  }

  def total_point
  	self.recommends.count
  end

  private
    def log_timeline_by_create
      Timeline.create(editor: Doctor.editor, target: self.doctor, model: self, action: "create", meta: self.icd10.group_lv3.name)
    end

    def log_timeline_by_update
      if self.changed?
        Timeline.create(editor: Doctor.editor, target: self.doctor, model: self, action: "update", meta: self.icd10.group_lv3.name)
      end
    end

    def log_timeline_by_destroy
      Timeline.create(editor: Doctor.editor, target: self.doctor, model: self, action: "destroy", meta: self.icd10.group_lv3.name)
    end
end
