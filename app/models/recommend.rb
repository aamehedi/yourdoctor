class Recommend < ActiveRecord::Base
  belongs_to :doctor
  belongs_to :skill
  has_paper_trail
  after_create :log_timeline, :send_msg

  validates :point, numericality: {only_integer: true, greater_than_or_equal_to: 1, less_than_or_equal_to: 5}


  #何度も推薦はできない
  validates :doctor_id, uniqueness: {
      message: "既に推薦しています",
      scope: [:skill_id]
    }

  #自画自賛は出来ない
  validate :dont_praise_oneself
  def dont_praise_oneself
    if self.skill.doctor == self.doctor
      errors.add(:dont_praise_oneself, "自分で推薦はできません")
    end
  end

  def log_timeline
    Timeline.create(editor: self.doctor, target: self.skill.doctor, model: self.skill.icd10, action: "create", meta: self.skill.icd10.group_lv3.name)
  end

  def send_msg
    p "AAAAAAAAAAvvvv!!!!!!"
    if self.skill.doctor.email_sendable? == true
      p "VVVVVV!!!!!!"
      queue_id = "Recommedn" + self.id.to_s
      DoctorsMailer.delay(:queue => queue_id).new_recommend(self.skill, self.doctor)
    end
  end

end
