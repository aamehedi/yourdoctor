class AgByMdcL1 < ActiveRecord::Base
  belongs_to :mdc_layer1
  has_many :regimen_pharmacy_rankings
  has_many :regimen_regimen_rankings
  has_many :datatable_datafiles

  extend ImportUtil
  #化学療法のレジメン
  def self.import(excel)
    result = Import::ImportResult.new
    excel.book.sheets.each do |sheet_name|
      #next if sheet_name == "rw_info"
      next if sheet_name == "rw_info"
      sheet = excel.sheet(sheet_name)

      code = sheet.cell(1, 1).to_s.strip
      if code.length == 5 then
        code = "0"+ code
      end

      unless code =~ /^\d+(x)*/ then
        result.warning_line_num += 1
        result.messages += message(sheet_name, 'ERROR', "MDCコードが不正です")
        next
      end
      #mdc_layer1 = MdcLayer1.name_code(code).where("mdc_layer1_codes.year = " + excel.year.to_s).first
      mdc_layer1 = MdcLayer1.name_code(code, excel.year)
      unless mdc_layer1.count == 1 then
        result.warning_line_num += 1
        result.messages += message(sheet_name, 'ERROR', "対応するDPCコードがありません。先にMDCマスタを作成する必要があります。DPC:" + code.to_s)
      	next
      end
      result.total_line_num += 1
      ag = AgByMdcL1.find_or_initialize_by(mdc_layer1_id: mdc_layer1[0].id)
      #ag.mdc_layer1 = mdc_layer1#?? data is set already..

      section = 0
      sub_section = 1

      pharmacies = Array.new
      3.step(sheet.last_row,2) do |index|
        begin
          row = sheet.row(index)
          if row[0].nil? then
            next #nilならとばす
          end
          case section
          when 0 then #ヘッダ
          	case row[0].to_s.gsub(" ","").gsub("　","")
          	when /使用薬剤数/ then
          	  ag.drugs_num     = row[1].is_a?(Integer) ? row[1].to_i : nil
          	when /レジメン数/ then
          	  ag.regimens_num  = row[1].is_a?(Integer) ? row[1].to_i : nil
          	when /症例数/ then
          	  ag.cases_num     = row[1].is_a?(Integer) ? row[1].to_i : nil
          	when /施設数/ then
          	  ag.hospitals_num = row[1].is_a?(Integer) ? row[1].to_i : nil
          	  section = 1
          	end
          when 1 then #製薬ランキング regimen_pharmacy_rankings
          	1.upto (row.count-1) do |i|
          	  pharmacies[i-1] ||= Hash.new
          	  case row[0].to_s.gsub(" ","").gsub("　","")
          	  when /症例数/        then pharmacies[i-1][:cases_num]       = row[i].is_a?(Integer) ? row[i].to_i : nil
          	  when /↑%/ then
          	    case sub_section
                  when 1          then pharmacies[i-1][:cases_ratio]     = row[i].is_a?(Float) ? row[i].to_f : nil
                  when 2          then pharmacies[i-1][:regimens_ratio]  = row[i].is_a?(Float) ? row[i].to_f : nil
                  when 3          then pharmacies[i-1][:hospitals_ratio] = row[i].is_a?(Float) ? row[i].to_f : nil
                end
                #sub_section change
                if i == row.count - 1 then
                  if sub_section < 3 then
                    sub_section += 1
                  else
                    sub_section = 1
                  end
                end
          	  when /使用レジメン数/ then pharmacies[i-1][:regimens_num]    = row[i].is_a?(Integer) ? row[i].to_i : nil
          	  when /施設数/        then pharmacies[i-1][:hospitals_num]   = row[i].is_a?(Integer) ? row[i].to_i : nil
          	  when /順位/ then
          	    section = 2
          	  end
          	end
          when 2 then #レジメンランキング regimen_regimen_rankings
          	no = row[0].to_i
          	if no > 0
              regi = ag.regimen_regimen_rankings.find_or_initialize_by(no: no)#,ag_by_mdc_l1_id:ag_by_mdc_l1_id)
              regi.cases_num               = row[1].is_a?(Integer) ? row[1].to_i : nil #症例数
              regi.cases_ratio             = row[2].is_a?(Float) ? row[2].to_f : nil
              regi.cases_cumulative_ratio  = row[3].is_a?(Float) ? row[3].to_f : nil
              regi.hospitals_num           = row[4].is_a?(Integer) ? row[4].to_i : nil
              regi.hospitals_ratio         = row[5].is_a?(Float) ? row[5].to_f : nil
              regi.average_hospital_days   = row[6].is_a?(Float) ? row[6] : row[6].to_f
              regi.regimen                 = row[7].is_a?(String) ? row[7].to_s : nil
              regi.save!
          	end
          end
        rescue => e
          p e.message
          result.error_line_num += 1
          result.messages += message(sheet_name, 'ERROR', e.message + e.backtrace.to_s)
        end
      end
      #製薬ランキング
      pharmacies.each_with_index do |pharmacy, index|#index 変数名おなじでOK？
        ranking = ag.regimen_pharmacy_rankings.find_or_initialize_by(no: index+1)
        ranking.cases_num       = pharmacy[:cases_num]
        ranking.regimens_num    = pharmacy[:regimens_num]
        ranking.hospitals_num   = pharmacy[:hospitals_num]
        ranking.cases_ratio     = pharmacy[:cases_ratio]
        ranking.regimens_ratio  = pharmacy[:regimens_ratio]
        ranking.hospitals_ratio = pharmacy[:hospitals_ratio]
        ranking.save!
      end
      #化学療法のレジメンMAIN
      unless ag.save! then
        result.error_line_num += 1
        result.messages += message(sheet_name, 'ERROR', "main error")
      end
    end
    return result
  end

end
