class WhytlinkChatroom < ApplicationRecord
  has_many :whytlink_doctor_chatrooms, dependent: :destroy
  has_many :whytlink_messages, dependent: :destroy
  has_one :emergency_request, dependent: :destroy, inverse_of:
    :whytlink_chatroom
  has_many :doctors, through: :whytlink_doctor_chatrooms
  has_one :whytlink_group_chat_info, dependent: :destroy
  has_many :whytlink_group_chat_requests, through: :whytlink_group_chat_info
  has_many :requested_doctors, through: :whytlink_group_chat_requests

  enum chatroom_type: [:dual, :multi]

  validates :chatroom_type, presence: true
  validates :whytlink_group_chat_info, presence: true, if: :not_emergency_multi?
  validates :whytlink_group_chat_info, presence: false, if: :dual_or_emergency?
  validates :whytlink_doctor_chatrooms, length: {is: Settings
    .whytlink_chatroom.private_chat_participant_number}, if: :dual?
  validates_associated :whytlink_doctor_chatrooms, :whytlink_group_chat_info,
    :whytlink_messages, :emergency_request

  accepts_nested_attributes_for :whytlink_messages, :emergency_request,
    :whytlink_group_chat_info, :whytlink_doctor_chatrooms

  def broadcast_chatroom_info
    ActionCable.server.broadcast "whytlink_rooms_info", self
  end

  def last_message_id
    WhytlinkMessage.where(whytlink_chatroom_id: self.id).last.try :id
  end

  def unread_message_count doctor_id
    last_read_message_id = whytlink_doctor_chatrooms
      .find{|e| e.doctor_id == doctor_id}.try :last_read_message_id
    return 0 if last_read_message_id == last_message_id
    WhytlinkMessage.unread(id, last_read_message_id).count
  end

  class << self
    def chatrooms_with_missed_chat doctor_id, chatroom_type
      chatroom_ids = WhytlinkDoctorChatroom.select(:whytlink_chatroom_id).where(
        "(last_read_message_id > (SELECT MAX(whytlink_messages.id) FROM
        whytlink_messages WHERE whytlink_messages.whytlink_chatroom_id =
        whytlink_doctor_chatrooms.whytlink_chatroom_id) OR last_read_message_id
        IS NULL) AND doctor_id = ?",
        doctor_id)
      if chatroom_type.present?
        WhytlinkChatroom.where("id = ANY(?)", chatroom_ids)
          .where(chatroom_type: chatroom_type)
      else
        WhytlinkChatroom.where("id = ANY(?)", chatroom_ids)
      end
    end

    def find_dual_chatroom params
      chatrooms_d1 = WhytlinkChatroom.where(chatroom_type: 0)
        .joins(:whytlink_doctor_chatrooms)
        .where("whytlink_doctor_chatrooms.doctor_id IN (?)", params[0])
      chatrooms_d2 = WhytlinkChatroom.where(chatroom_type: 0)
        .joins(:whytlink_doctor_chatrooms)
        .where("whytlink_doctor_chatrooms.doctor_id IN (?)", params[1])
      (chatrooms_d1 & chatrooms_d2).last
    end

    def find_all_chatrooms doctor_id, chatroom_type
      whytlink_chatroom_ids = WhytlinkDoctorChatroom.where(doctor_id: doctor_id)
        .pluck(:whytlink_chatroom_id)
      closed_emergency_chatroom_ids = WhytlinkChatroom.where(id:
        whytlink_chatroom_ids).joins(:emergency_request).where(%(
        "emergency_requests"."status" <> ? AND "emergency_requests"."updated_at"
         < ?), 0, 3.hours.ago).pluck(:id)
      whytlink_chatroom_ids -= closed_emergency_chatroom_ids
      WhytlinkChatroom.eager_load({whytlink_doctor_chatrooms: :doctor},
        :emergency_request, whytlink_group_chat_info:
        :whytlink_group_chat_requests).where(id: whytlink_chatroom_ids,
        chatroom_type: chatroom_type || WhytlinkChatroom.chatroom_types.values)
    end

    def find_all_non_emergency_chatrooms doctor_id, chatroom_type
      whytlink_chatroom_ids = WhytlinkDoctorChatroom.where(doctor_id: doctor_id)
        .pluck(:whytlink_chatroom_id)
      WhytlinkChatroom.eager_load({whytlink_doctor_chatrooms: :doctor},
        :emergency_request, :whytlink_group_chat_requests,
        :whytlink_group_chat_info).where(id: whytlink_chatroom_ids,
        chatroom_type: chatroom_type || WhytlinkChatroom.chatroom_types.values)
        .where(emergency_requests: {whytlink_chatroom_id: nil})
    end

    def create_chatroom params, admin_id
      chatroom = WhytlinkChatroom.new(params)
      WhytlinkChatroom.transaction do
        chatroom.save!
        if admin_id
          chatroom.whytlink_doctor_chatrooms.find_by_doctor_id(admin_id)
            .update_attributes! admin: true
          chatroom.whytlink_doctor_chatrooms.find_by_doctor_id(admin_id).reload
        end
        chatroom
      end
    rescue
      return false
    end

    def find_or_create_chatroom params, admin_id
      if params[:chatroom_type] == WhytlinkChatroom.chatroom_types.keys[0]
        doctor_ids = params[:whytlink_doctor_chatrooms_attributes].map do |item|
          item[:doctor_id]
        end
        chatroom = find_dual_chatroom doctor_ids
        return chatroom if chatroom
      end
      create_chatroom params, admin_id
    end
  end

  private
  def not_emergency_multi?
    multi? && emergency_request.nil?
  end

  def dual_or_emergency?
    dual? || emergency_request
  end
end
