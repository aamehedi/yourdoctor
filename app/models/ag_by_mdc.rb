class AgByMdc < ActiveRecord::Base
  belongs_to :mdc_layer3
  has_many :number_by_icd10_costlies,      :class_name => "NumberByCode::NumberByIcd10Costly"
  has_many :number_by_icd10_comorbidities, :class_name => "NumberByCode::NumberByIcd10Comorbidity"
  has_many :number_by_kcodes,              :class_name => "NumberByCode::NumberByKcode"
end
