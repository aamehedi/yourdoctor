class SocietyList < ActiveRecord::Base
  belongs_to :doctor
  belongs_to :society
  has_many :position_lists, :class_name => 'SocietyPositionList', :foreign_key => :society_list_id, :dependent => :destroy
  accepts_nested_attributes_for :position_lists, allow_destroy: :true
  has_many :positions, through: :position_lists, :source => :position, :class_name => 'SocietyPosition'
  has_paper_trail  
  validates :society_id, presence: true

  #タイムライン
  after_create   :log_timeline_by_create
  after_update   :log_timeline_by_update
  before_destroy :log_timeline_by_destroy

  attr_accessor :name

  # accepts_nested_attributes_for :sosiety
  # society_list経由でマスタであるsosiety自体は変更させない。但し新規追加はできる。
  #  -accept_nestedはしない
  #  -idのないsosiety_attributesがある場合は新規でsosietyを作成する
  def society_attributes=(attributes)
    #学会情報がない場合は特に何もしない
    return if attributes.nil?
    return if attributes[:name].blank?

    #IDが無い、存在しないIDが指定されている場合は同名の学会を探すか、なければ新規追加
    if attributes[:id].nil? || Society.find_by(id: attributes[:id]).nil?
      society = Society.find_or_create_by(name: attributes[:name])
      new_society_id = society.id
    else
      new_society_id = attributes[:id]
    end
    self.society_id = new_society_id
  end

  private
    def log_timeline_by_create
      Timeline.create(editor: Doctor.editor, target: self.doctor, model: self, action: "create", meta: self.society.name)
    end

    def log_timeline_by_update
      if self.changed?
        Timeline.create(editor: Doctor.editor, target: self.doctor, model: self, action: "update", meta: self.society.name)
      end
    end

    def log_timeline_by_destroy
      Timeline.create(editor: Doctor.editor, target: self.doctor, model: self, action: "destroy", meta: self.society.name)
    end
end
