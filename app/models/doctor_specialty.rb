class DoctorSpecialty < ApplicationRecord
  belongs_to :doctor
  belongs_to :specialty

  validates :specialty, presence: true
  validates :doctor, presence: true
end
