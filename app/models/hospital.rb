# -*- coding: utf-8 -*-
class Hospital < ActiveRecord::Base
  belongs_to :medical_group
  belongs_to :area
  belongs_to :prefecture
  belongs_to :municipality
  #belongs_to :admin_user
  belongs_to :municipality

  has_many :doctors
  has_many :departments
  has_many :emergency_doctor_hospitals, dependent: :destroy
  has_many :emergency_doctors, through: :emergency_doctor_hospitals
  has_many :favorite_group_doctors, dependent: :destroy
  has_many :whytlink_group_chat_infos, as: :affliation

  validates :prefecture_id, presence: true

  has_paper_trail
  geocoded_by :address
  after_validation :geocode

  scope :emergency_hospital_doctors, -> hospital_ids, doctor_ids{
    where(id: hospital_ids).includes(:emergency_doctors)
      .where(emergency_doctor_hospitals: {doctor_id: doctor_ids})
  }

  scope :nearby_hospitals_of_doctor, -> doctor, latitude, longitude, radius{
    where.not(id: doctor.hospital_id).near([latitude, longitude],radius)
  }


  def merge(src_hospital)
  	#hospitals

 	self.name                         = self.name                          ? self.name                         : src_hospital.name
 	self.medical_group_id             = self.medical_group_id              ? self.medical_group_id             : src_hospital.medical_group_id
 	self.area_id                      = self.area_id                       ? self.area_id                      : src_hospital.area_id
 	self.prefecture_id                = self.prefecture_id                 ? self.prefecture_id                : src_hospital.prefecture_id
  	self.std_region_code              = self.std_region_code               ? self.std_region_code              : src_hospital.std_region_code
 	self.postal_code                  = self.postal_code                   ? self.postal_code                  : src_hospital.postal_code
 	self.address                      = self.address                       ? self.address                      : src_hospital.address
 	self.tmp_head                     = self.tmp_head                      ? self.tmp_head                     : src_hospital.tmp_head
 	self.url                          = self.url                           ? self.url                          : src_hospital.url
 	self.bed_num                      = self.bed_num                       ? self.bed_num                      : src_hospital.bed_num
 	self.acute_phase_bed_num          = self.acute_phase_bed_num           ? self.acute_phase_bed_num          : src_hospital.acute_phase_bed_num
 	self.tmp_user                     = self.tmp_user                      ? self.tmp_user                     : src_hospital.tmp_user
 	self.user_id                      = self.user_id                       ? self.user_id                      : src_hospital.user_id
 	self.code_region                  = self.code_region                   ? self.code_region                  : src_hospital.code_region
 	self.code_number                  = self.code_number                   ? self.code_number                  : src_hospital.code_number
 	self.code_checkdigit              = self.code_checkdigit               ? self.code_checkdigit              : src_hospital.code_checkdigit
 	self.phone                        = self.phone                         ? self.phone                        : src_hospital.phone
 	self.number_of_fulltime_medicine  = self.number_of_fulltime_medicine   ? self.number_of_fulltime_medicine  : src_hospital.number_of_fulltime_medicine
 	self.number_of_fulltime_dentistry = self.number_of_fulltime_dentistry  ? self.number_of_fulltime_dentistry : src_hospital.number_of_fulltime_dentistry
 	self.number_of_fulltime_pharmacy  = self.number_of_fulltime_pharmacy   ? self.number_of_fulltime_pharmacy  : src_hospital.number_of_fulltime_pharmacy
 	self.number_of_parttime_medicine  = self.number_of_parttime_medicine   ? self.number_of_parttime_medicine  : src_hospital.number_of_parttime_medicine
 	self.number_of_parttime_dentistry = self.number_of_parttime_dentistry  ? self.number_of_parttime_dentistry : src_hospital.number_of_parttime_dentistry
 	self.number_of_parttime_pharmacy  = self.number_of_parttime_pharmacy   ? self.number_of_parttime_pharmacy  : src_hospital.number_of_parttime_pharmacy
 	self.establisher_org              = self.establisher_org               ? self.establisher_org              : src_hospital.establisher_org
  	self.establisher_first_name       = self.establisher_first_name        ? self.establisher_first_name       : src_hospital.establisher_first_name
 	self.establisher_last_name        = self.establisher_last_name         ? self.establisher_last_name        : src_hospital.establisher_last_name
 	self.director_first_name          = self.director_first_name           ? self.director_first_name          : src_hospital.director_first_name
 	self.director_last_name           = self.director_last_name            ? self.director_last_name           : src_hospital.director_last_name
 	self.designation_date             = self.designation_date              ? self.designation_date             : src_hospital.designation_date
 	self.designation_reason           = self.designation_reason            ? self.designation_reason           : src_hospital.designation_reason
 	self.designation_start            = self.designation_start             ? self.designation_start            : src_hospital.designation_start
 	self.state                        = self.state                         ? self.state                        : src_hospital.state
 	self.version_year                 = self.version_year                  ? self.version_year                 : src_hospital.version_year
 	self.beds_hash         |= src_hospital.beds_hash         if self.beds_hash         && src_hospital.beds_hash
 	self.departments_array |= src_hospital.departments_array if self.departments_array && src_hospital.departments_array

    #department_listを繋ぎ替え
    src_hospital.departments.each do | department |
      dup_department = self.departments.where(name: department.name).first
      #--------------------
      #departmentのマージ
      #--------------------
      if dup_department
        department.department_lists.each do | department_list |
          #--------------------
          #department_listのマージ
          #--------------------
          #同じ医師のdepartment_listがある場合は
          dup_department_list = dup_department.department_lists.where(doctor_id: department_list.doctor_id).first
          #そこにぶら下がるposition_listをマージする
          if dup_department_list
            department_list.position_lists.each do | position_list |
              #--------------------
              #department_listのマージ
              #--------------------
              #同じ名前の役職が存在するなら重複とみなし
              #position_listは削除する
              #postionは他にだれも参照していなければ削除する
              if dup_department_list.positions.where(name: position_list.position.name).first
                position = position_list.position
                position_list.destroy
                position = DepartmentPosition.find(position.id)
                if position.department_position_lists.length == 0
                  DepartmentPosition.delete(position.id)
                end
              #存在しなければ別ものとみなし繋ぎ換える
              else
                position = position_list.position
                position.update(department_id: dup_department.id)
                position_list.update(department_list_id: dup_department_list.id)
              end
            end
            DepartmentList.delete(department_list.id)
          #同じ医師のdepartment_listが無い場合は新規とみなしつなぎ先を変える
          else
            department_list.update(department_id: dup_department.id)
          end
        end
        Department.delete(department.id)
      else
      	department.update(hospital_id: self.id)
      end
  	end

    #マージ先保存
    self.save!
    #マージ元削除
    Hospital.delete(src_hospital.id)
  end
end
