class FavoriteGroupDoctor < ApplicationRecord
  belongs_to :doctor
  belongs_to :favorite_group, inverse_of: :favorite_group_doctors
  belongs_to :hospital

  validates :hospital, presence: true
  validates :favorite_group, presence: true
  validates :doctor, presence: true
end
