class MrSession < ApplicationRecord
  belongs_to :mr

  before_create :generate_access_token!

  def generate_access_token!
    self.access_token = loop do
      random_token = "#{self.mr.id}:#{Devise.friendly_token}"
      break random_token unless MrSession.exists? access_token: random_token
    end
  end
end
