$(document).on "click", ".alert", ()->
  confirm "本当に未定にしてもよろしいですか？"


@setup_typeahead = (id, url, max_num, init_data, add_func, no_delete = null) ->
    pre_populate = null    
    if init_data.length > 0
      console.log init_data
      #if init_data[0].name > 0
      pre_populate = init_data
    $("#" + id).tokenInput url,
      theme: "facebook"
      prePopulate: pre_populate
      searchingText: '検索中'
      hintText: '入力してください'
      noResultsText: '登録なし：新規入力'
      tokenLimit: max_num
      allowFreeTagging: true
      onFreeTaggingAdd: (r) ->
        console.log "onFreeTaggingAdd"
        return r + "[新規登録]"
      onAdd: (r) ->
        add_func(r, id)
      noDelete: no_delete 
$ ->

  if $("#department_department_type_list").length > 0
    $("#department_department_type_list").tokenInput "/department_official_category",
      theme: "facebook"
      searchingText: '検索中'
      hintText: '入力してください'
      noResultsText: '対応する項目はありません'
      allowFreeTagging: false
        
  if $(".department_list_position").length > 0
    $(".department_list_position").each ->   
      $(this).tokenInput "/department_position_names",
        theme: "facebook"
        searchingText: '検索中'
        hintText: '入力してください'
        noResultsText: '対応する項目はありません'
        allowFreeTagging: true

  if $("#university_typeahead_list").length > 0
    add_func = (r, id) ->
        console.log "on add"
        if r.name.indexOf("[新規登録]") > 0
          console.log "新規！"
          $('#university_typeahead li.token-input-token-facebook').addClass("new")
          $('#doctor_university_attributes_id').val("")
          $('#doctor_university_attributes_name').prop('disabled',false).val(r.name.replace("[新規登録]",""))
        else
          $('#doctor_university_attributes_id').val(r.id)
          $('#doctor_university_attributes_name').val("").prop('disabled',true)        
        console.log r.name  
    #setup_typeahead("university_typeahead_list", "/typeaheads/university/", "", add_func, null)


  society_list_add_func = (r, id) ->
        console.log "on add"
        attr_name = "#doctor_society_lists_attributes_" + id.replace("society_lists_", "")
        console.log attr_name
        if r.name.indexOf("[新規登録]") > 0
          console.log "新規！" + id.replace("lists_", "")
          $('#' + id.replace("lists_", "") + ' li.token-input-token-facebook').addClass("new")
          $(attr_name + '_id').val("")
          $(attr_name + '_society_id').val("")
          $(attr_name + '_name').prop('disabled',false).val(r.name.replace("[新規登録]",""))
        else
          console.log "あて！" + attr_name + r.id
          $('#' + id.replace("lists_", "") + ' li.token-input-token-facebook').addClass("add")
          $(attr_name + '_id').val("")
          $(attr_name + '_society_id').val(r.id)
          $(attr_name + '_name').val("").prop('disabled',true)

        
  if $(".society_list_typeahead_list").length > 0
    $(".society_list_typeahead_list").each ->
      setup_typeahead($(this).attr("id"), "/typeaheads/society/", 1, $(this).data("pre_data"), society_list_add_func, true)

  # 医者に学会を追加するボタンの動作
  $(document).on "click", '#add_society_for_dr_btn', (e) ->
    id_num = getUnixTime()    
    add_obj = $('#society_0').clone()
    add_obj.find('ul').remove()
    console.log add_obj.attr("id")
    id_str = add_obj.attr("id").replace("_0", "_" + id_num)
    console.log id_str
    add_obj.attr("id", id_str)
    #add_obj.attr("data-society_id", "")
    add_obj.find("input").each ->
      console.log $(this).attr("name")
      if $(this).attr("id") and $(this).attr("name")
        $(this).attr("id", $(this).attr("id").replace("_0", "_" + id_num))
        $(this).attr("name", $(this).attr("name").replace("[0]", "[" + id_num + "]")) 
        $(this).val("")
      
    $('#society_list').append(add_obj)
    #setup_typeahead(id_str.replace("society", "society_lists"), "/typeaheads/society/", "", society_list_add_func, true)
    setup_typeahead(id_str.replace("society", "society_lists"), "/typeaheads/society/", 1, [], society_list_add_func, true)

  society_position_add_func = (r, id) ->
        console.log "on add" + r["input_id"]
        attr_name = "#doctor_society_position_lists_attributes_0"
        change_id = "doctor_society_position_lists_attributes_" + r["input_id"].replace("input_id_", "")
        change_name = "doctor[society_position_lists_attributes][" + r["input_id"].replace("input_id_", "") + "]"
        $('#' + r["input_id"]).append($(attr_name + '_id').clone().attr("id", change_id + '_id').attr("name", change_name + '[id]').val(""))

        society_id = $('#' + r["input_id"]).closest(".society_list_top").data('society_id')
        if r.name.indexOf("[新規登録]") > 0
          console.log "新規！" + id.replace("lists_", "")
          $('#' + r["input_id"]).addClass("new")
          $('#' + r["input_id"]).append($(attr_name + '_society_position_id').clone().attr("id", change_id + '_society_id').attr("name", change_name + '[society_id]').val(society_id))
          $('#' + r["input_id"]).append($(attr_name + '_name').clone().attr("id", change_id + '_name').attr("name", change_name + '[name]').prop('disabled',false).val(r.name.replace("[新規登録]","")))
        else
          console.log "既存のものを割当！" + r.name + ";" + r.id
          $(attr_name + '_id').val("")
          $('#' + r["input_id"]).addClass("add")
          $('#' + r["input_id"]).append($(attr_name + '_society_position_id').clone().attr("id", change_id + '_society_position_id').attr("name", change_name + '[society_position_id]').val(r.id))
          $('#' + r["input_id"]).append($(attr_name + '_name').clone().attr("id", change_id + '_name').attr("name", change_name + '[name]').val("").prop('disabled',true))

  # 学会の認定情報
  if $(".society_positions").length > 0
    $(".society_positions").each ->
      setup_typeahead($(this).attr("id"), "/typeaheads/society_position/" + $(this).data("pid"), null, $(this).data("pre_data"), society_position_add_func, true)