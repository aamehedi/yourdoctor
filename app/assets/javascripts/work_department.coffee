help_sotable = (e, tr) ->
  $helper = undefined
  $originals = undefined
  $originals = tr.children()
  $helper = tr.clone()
  $helper.children().each (index) ->
    $(this).width $originals.eq(index).width() * 1.05
    return
  $helper

update_sotable = (ev, ui) ->
  index = 1
  $('.order').each ->
    $(this).val index
    index++
    return
  return

$ ->
  $('.sortable > tbody').sortable
    helper: help_sotable
    update: update_sotable
    cursor: 'move'
    opacity: 0.5
  #$('form').on 'keypress', (e) ->
  #  if e.keyCode == 13
  #    return false
  #  return
