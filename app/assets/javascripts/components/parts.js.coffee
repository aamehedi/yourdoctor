###
$ ->

  style =
    just_check:
      backgroundColor: '#fff'
      border: "1px solid #000"
      minWidth: 13
      height: 13
      display: 'inline-block'
    just_checked:
      backgroundColor: 'red'
      minWidth: 13
      height: 13
      display: 'inline-block'    
        
  JustChecker = React.createClass
    getInitialState: ->
        name: "World"
        check: false
    handleClick: ->
        this.setState({check: !this.state.check})
    render: ->
      cname = if @state.check then 'just_checked' else 'just_check'
      React.DOM.div
        className: 'just-check'
        style: style[cname]
        onClick: this.handleClick

        
  component = React.createFactory(JustChecker)
  $('.tmp_check').each () ->        
    componentInstance = React.render(component(), $(this)[0])

###