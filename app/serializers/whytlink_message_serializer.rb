class WhytlinkMessageSerializer < ActiveModel::Serializer
  attributes :id, :doctor_id, :whytlink_chatroom_id, :content, :created_at,
    :attachment, :message_type
end
