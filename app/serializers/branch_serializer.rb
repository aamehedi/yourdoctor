class BranchSerializer < ActiveModel::Serializer
  include ActiveModel::TimeBeautification

  has_many :mrs
  attributes :id, :name, :branch_manager_id, :branch_manager_name, :created_at,
    :updated_at, :this_month_request, :next_month_request, :mrs

  def branch_manager_id
    object.mr_id.to_s
  end

  def branch_manager_name
    object.branch_manager.try(:name).to_s
  end

  def this_month_request
    object.this_month_request.to_s
  end

  def next_month_request
    object.next_month_request.to_s
  end
end
