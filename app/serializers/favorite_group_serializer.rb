class FavoriteGroupSerializer < ActiveModel::Serializer
  attributes :id, :name, :doctor_id, :favorite_group_hospitals

  def favorite_group_hospitals
    object.favorite_group_doctors.group_by(&:hospital).map do |hospital, fgd|
      {
        hospital_id: hospital.id,
        hospital_name: hospital.name,
        doctors: favorite_doctors(fgd)
      }
    end
  end

  def favorite_doctors fgd
    fgd.map do |fgd|
      {
        id: fgd.doctor.id,
        name: fgd.doctor.name,
        avatar_url: fgd.doctor.avatar_url,
        departments: fgd.doctor.specialties.map(&:name).join(","),
        status: true
      }
    end
  end
end
