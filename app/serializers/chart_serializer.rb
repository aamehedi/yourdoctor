class ChartSerializer < ActiveModel::Serializer
  has_one :mr

  attributes :id, :name, :description, :favorite, :raw_query_param_values,
    :query_param_values, :option, :chart_type, :mr

  def description
    object.description.to_s
  end

  def raw_query_param_values
    object.raw_query_param_values.to_s
  end

  def query_param_values
    object.query_param_values.to_s
  end
end
