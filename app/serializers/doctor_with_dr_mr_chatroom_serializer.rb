class DoctorWithDrMrChatroomSerializer < ActiveModel::Serializer
  attributes :id, :first_name, :middle_name, :last_name, :first_name_reading,
    :middle_name_reading, :last_name_reading, :url, :history, :birthdate, :sex,
    :comment, :registration_number, :avatar_url, :cover_url, :link_state,
    :is_member, :is_yourself, :link_doctors_count, :paginate_info, :is_staff,
    :dr_mr_chatrooms
  has_many :department_lists_attributes, serializer:
    V1::DepartmentListSerializer
  has_many :society_lists_attributes, serializer: V1::SocietyListSerializer
  has_many :university_lists_attributes, serializer:
    V1::UniversityListSerializer
  has_many :skills_attributes, serializer: V1::SkillSerializer
  has_many :awards_attributes, serializer: V1::AwardSerializer
  has_many :monographs_attributes, serializer: V1::MonographSerializer


  def university_lists_attributes
    # setup_has_many "university_lists"
    object.university_lists
  end

  def department_lists_attributes
    # setup_has_many "department_lists"
    object.department_lists
  end

  def society_lists_attributes
    # setup_has_many "society_lists"
    object.society_lists
  end

  def skills_attributes
    # setup_has_many "skills"
    object.skills
  end

  def awards_attributes
    # setup_has_many "awards"
    object.awards
  end

  def monographs_attributes
    # setup_has_many "monographs"
    object.monographs
  end

  def paginate_info
    if object.paginate_info
      {
        page_type: object.paginate_info[:page_type],
        page_num: object.paginate_info[:page_num],
        tagged_with: object.paginate_info[:tagged_with],
        tagged_count: object.monographs.tagged_with(
          object.paginate_info[:tagged_with]).count
      }
    else
      {}
    end
  end

  def dr_mr_chatrooms
    object.dr_mr_chatrooms.map do |dmc|
      {
        id: dmc.id,
        status: dmc.status,
        favorite: dmc.favorite,
        mr: dmc.mr
      }
    end
  end
end
