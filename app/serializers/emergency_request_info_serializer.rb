class EmergencyRequestInfoSerializer < ActiveModel::Serializer
  attributes :id, :doctor_id, :emergency_request_hospital_id,
    :whytlink_chatroom_id, :status, :time, :closing_reason, :closing_comment,
    :created_at, :updated_at
end
