class MrSessionSerializer < ActiveModel::Serializer
  attributes :access_token, :mr

  def mr
    MrInfoSerializer.new object.mr, root: false
  end
end
