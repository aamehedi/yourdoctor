class EmergencyDoctorHospitalSerializer < ActiveModel::Serializer
  attributes :id, :name, :emergency_hospital

  def emergency_hospital
    options[:current_doctor_hospitals].include? object.id
  end
end
