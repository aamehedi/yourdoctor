class WhytlinkGroupChatRequestSerializer < ActiveModel::Serializer
  attributes :id, :doctor_id, :whytlink_group_chat_info_id, :status, :created_at,
    :updated_at
end
