class WhytlinkGroupChatInfoSerializer < ActiveModel::Serializer
  attributes :id, :name, :whytlink_chatroom_id, :affliation_id,
    :affliation_type, :avatar, :opened, :created_at, :updated_at,
    :affliation_name

  def affliation_name
    object.affliation.name
  end
end
