class WhytlinkDoctorChatroomSerializer < ActiveModel::Serializer
  attributes :id, :doctor_id, :whytlink_chatroom_id, :last_read_message_id,
    :admin, :doctor_first_name, :doctor_last_name, :doctor_avatar_url,
    :hospital_name, :department_name

  def doctor_first_name
    object.doctor.first_name
  end

  def doctor_last_name
    object.doctor.last_name
  end

  def doctor_avatar_url
    object.doctor.avatar_url
  end

  def department_name
    object.doctor.departments.to_a.first.try :name
  end

  def hospital_name
    object.doctor.hospitals.to_a.first.try :name
  end
end
