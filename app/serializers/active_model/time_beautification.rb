require "active_support/concern"

module ActiveModel::TimeBeautification
  extend ActiveSupport::Concern
  include AbstractController::Translation

  included do
    [:created_at, :updated_at].each do |time|
      define_method(time) do
        object.send(time).strftime t("datetime.formats.default")
      end
    end
  end
end