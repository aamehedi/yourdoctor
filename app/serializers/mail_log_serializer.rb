class MailLogSerializer < ActiveModel::Serializer
  attributes :id, :done
  has_one :doctor
  has_one :all_mail
end
