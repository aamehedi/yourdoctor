class DoctorFeedbackSerializer < ActiveModel::Serializer
  attributes :id, :doctor_id, :hospital, :status

  def hospital
    {
      hospital_id: object.hospital.id,
      hospital_name: object.hospital.name
    }
  end
end
