class EmergencyRequestHospitalSerializer < ActiveModel::Serializer
  attributes :id, :hospital, :doctors
  has_many :doctors, serializer: EmergencyDoctorSerializer

  def hospital
    {
      hospital_id: object.hospital.id,
      hospital_name: object.hospital.name
    }
  end

  def doctors
    object.doctor_feedbacks.map{|feedback| feedback.doctor}
  end
end
