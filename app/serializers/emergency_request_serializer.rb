class EmergencyRequestSerializer < ActiveModel::Serializer
  attributes :id, :doctor_id, :time, :closing_reason, :status, :closing_comment,
    :accepted_hospital
  has_one :whytlink_chatroom
  has_many :emergency_request_hospitals

  def accepted_hospital
    {
      accepted_hospital_id: object.accepted_hospital.try(:id),
      accepted_hospital_name: object.accepted_hospital.try(:name)
    }
  end
end
