class EmergencyDoctorSerializer < ActiveModel::Serializer
  attributes :id, :name, :avatar_url, :departments, :status, :rest_mode,
    :rest_time_start, :rest_time_end

  def departments
    object.specialties.pluck(:name).join(",")
  end

  def status
    true
  end

  def rest_time_start
    object.rest_time_start.to_formatted_s :time if object.rest_time_start
  end

  def rest_time_end
    object.rest_time_end.to_formatted_s :time if object.rest_time_end
  end

  def doctor_info
    {
      id: object.id,
      name: object.name,
      hospital_id: object.hospital_id,
      hospital_name: object.hospital.try(:name),
      emergency: object.emergency,
      rest_mode: object.rest_mode,
      rest_time_start: rest_time_start,
      rest_time_end: rest_time_end
    }
  end
end
