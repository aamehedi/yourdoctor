class MrSerializer < ActiveModel::Serializer
  attributes :id, :name, :mr_type, :avatar_string, :project_list,
    :company_image, :company_name, :branch_name

  def project_list
    object.projects.map{|project| {name: project.name}}
  end

  def avatar_string
    object.avatar_string.try(:url).to_s
  end

  def company_image
    # TODO: fix it when implement uploader for company_image
    object.company.try(:company_image).to_s
  end

  def company_name
    object.company.name
  end

  def branch_name
    object.branch.try(:name)
  end
end
