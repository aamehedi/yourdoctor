class HospitalAndDoctorSerializer < ActiveModel::Serializer
  attributes :id, :name

  has_many :emergency_doctors, serializer: EmergencyDoctorSerializer
end
