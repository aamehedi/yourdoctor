class CompanySerializer < ActiveModel::Serializer
  attributes :name, :using_chat, :max_request, :bonus_request

  def company_info
    {
      about_company: CompanySerializer.new(object).about_company,
      number_of_user: CompanySerializer.new(object).number_of_user,
      number_of_request: CompanySerializer.new(object).number_of_request
    }
  end

  def about_company
    {
      id: object.id.to_s,
      name: object.name,
      using_chat: object.using_chat
    }
  end

  def number_of_user
    {
      company_manager_number: object.count_mr_by_type(:company_manager),
      project_manager_number: object.count_mr_by_type(:project_manager),
      branch_manager_number: object.count_mr_by_type(:branch_manager),
      staff_number: object.count_mr_by_type(:staff)
    }
  end

  def number_of_request
    {
      max_request: object.max_request,
      sent_request: object.max_request - object.bonus_request,
      bonus_request: object.bonus_request
    }
  end
end
