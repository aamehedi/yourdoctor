class WhytplotChatroomSerializer < ActiveModel::Serializer
  attributes :id, :name, :chatroom_type, :last_message_id, :unread_messages,
    :is_default?, :created_at
  has_many :whytplot_mr_chatrooms

  def unread_messages
    # TODO: Consider asynchronous processing if API comes slow
    last_read_message_id = whytplot_mr_chatrooms
      .find{|e| e.mr_id == options[:current_mr_id]}
      .try(:last_read_message_id)
    if last_read_message_id == object.last_message_id
      0
    else
      WhytplotMessage.unread(object.id, last_read_message_id).count
    end
  end
end
