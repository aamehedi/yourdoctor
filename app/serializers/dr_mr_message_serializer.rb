class DrMrMessageSerializer < ActiveModel::Serializer
  attributes :id, :doctor_id, :mr_id, :dr_mr_chatroom_id, :content, :created_at,
    :message_type, :attachment
end
