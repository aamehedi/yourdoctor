class PostSerializer < ActiveModel::Serializer
  attributes :id, :memo, :read_level
  has_one :doctor
  has_one :placeholder
end
