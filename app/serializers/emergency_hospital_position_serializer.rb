class EmergencyHospitalPositionSerializer < ActiveModel::Serializer
  attributes :id, :name, :longitude, :latitude
end
