class WhytplotMessageSerializer < ActiveModel::Serializer
  include ActiveModel::TimeBeautification

  attributes :id, :mr_id, :content, :message_type, :object_id, :created_at

  def object_id
    object.object_id.to_i
  end
end
