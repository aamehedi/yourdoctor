class WhytlinkChatroomSerializer < ActiveModel::Serializer
  attributes :id, :chatroom_type, :last_message, :unread_messages, :created_at,
    :pending_requests

  has_many :whytlink_doctor_chatrooms
  has_one :whytlink_group_chat_info

  def unread_messages
    if options[:current_doctor_id].present?
      object.unread_message_count options[:current_doctor_id]
    end
  end

  def last_message
    object.whytlink_messages.last
  end

  def pending_requests
    if options[:current_doctor_id].present? && object.whytlink_doctor_chatrooms
      .where(doctor_id: options[:current_doctor_id], admin: true)
      object.whytlink_group_chat_requests.requested
    end
  end
end
