class ScenarioSerializer < ActiveModel::Serializer
  has_one :mr
  has_many :chart_list

  attributes :id, :name, :description, :favorite, :mr, :chart_list

  def description
    object.description.to_s
  end

  def chart_list
    object.ordered_charts
  end
end
