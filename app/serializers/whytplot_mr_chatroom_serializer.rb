class WhytplotMrChatroomSerializer < ActiveModel::Serializer
  attributes :id, :mr_id, :mr_name, :mr_avatar, :last_read_message_id, :favorite

  def mr_name
    object.mr.name
  end

  def mr_avatar
    object.mr.avatar_string.try(:url).to_s
  end
end
