class MrInfoSerializer < ActiveModel::Serializer
  include ActiveModel::TimeBeautification

  attributes :id, :email, :name, :branch_id, :branch_name, :mr_type,
    :avatar_string, :created_at, :updated_at, :project_list, :company

  def project_list
    object.projects.map{|project| {name: project.name}}
  end

  def branch_id
    object.branch_id.to_s
  end

  def branch_name
    object.branch.try(:name).to_s
  end

  def avatar_string
    object.avatar_string.try(:url).to_s
  end

  def company
    CompanySerializer.new(object.company).about_company
  end
end
