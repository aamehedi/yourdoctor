module V1
  class DoctorSerializer < ActiveModel::Serializer
    attributes :id, :email, :first_name, :middle_name, :last_name, :first_name_reading, :middle_name_reading, :last_name_reading, :url, :history, :birthdate, :sex, :comment, :registration_number, :avatar_url, :cover_url, :link_state, :is_member, :is_yourself, :is_official, :link_doctors_count, :totals, :paginate_info, :is_staff
    has_many   :department_lists_attributes, serializer: V1::DepartmentListSerializer
    has_many   :society_lists_attributes, serializer: V1::SocietyListSerializer
    has_many   :monographs_attributes, serializer: V1::MonographSerializer
    has_many   :awards_attributes, serializer: V1::AwardSerializer
    has_many   :university_lists_attributes, serializer: V1::UniversityListSerializer
    has_many   :skills_attributes, serializer: V1::SkillSerializer

    @@PREFERRED_TAG_NAMES = ['査読論文', '論文', '口頭発表', 'ポスター', '寄稿', '講演会', '国内', '海外']

    def university_lists_attributes
      # setup_has_many "university_lists"
      object.university_lists
    end

    def department_lists_attributes
      # setup_has_many "department_lists"
      object.department_lists
    end

    def society_lists_attributes
      # setup_has_many "society_lists"
      object.society_lists
    end

    def awards_attributes
      setup_has_many "awards"
    end

    def monographs_attributes
      setup_has_many "monographs"
    end

    def skills_attributes
      setup_has_many "skills"
    end

    def setup_has_many model_name
      if object.paginate_info
        if object.paginate_info[:page_type] == model_name
          if object.paginate_info[:page_num] && object.paginate_info[:tagged_with]
            object.send(model_name).tagged_with(object.paginate_info[:tagged_with]).page(object.paginate_info[:page_num])
          else
            if object.paginate_info[:page_num]
              object.send(model_name).page(object.paginate_info[:page_num])
            elsif object.paginate_info[:tagged_with]
              object.send(model_name).tagged_with(object.paginate_info[:tagged_with])
            else
              object.send(model_name)
            end
          end
        else
          []
        end
      else
        if model_name == "monographs"
          object.send(model_name).limit(3)
        else
          object.send(model_name)
        end
      end
    end

    def is_official
      object.is_official?
    end

    def totals
      tags_counts = []
      @@PREFERRED_TAG_NAMES.each do |tag_name|
        tags_counts << { name: tag_name, count: object.monographs.tagged_with(tag_name).count }
      end
      { university_lists: object.university_lists.count, department_lists: object.department_lists.count,  society_lists: object.society_lists.count, awards: object.awards.count,  monographs: object.monographs.count, skills: object.skills.count, monograph_tags: tags_counts, timelines: object.timelines.count }
    end

    def paginate_info
      if object.paginate_info
        { page_type: object.paginate_info[:page_type], page_num: object.paginate_info[:page_num], tagged_with: object.paginate_info[:tagged_with], tagged_count: object.monographs.tagged_with(object.paginate_info[:tagged_with]).count }
      else
        { }
      end
    end
  end
end
