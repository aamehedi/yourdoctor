 module V1
  class DepartmentPositionListSerializer < ActiveModel::Serializer
    attributes :id, :department_list_id, :department_position_id
    has_one :position_attributes, serializer: V1::NameAndIDSerializer
    def position_attributes
      object.position
    end
  end
end
