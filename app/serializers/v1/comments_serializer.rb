module V1
  class CommentsSerializer < ActiveModel::Serializer
    attributes :id, :doctor_id, :name, :url, :content, :created_at

    def name
      object.doctor.name
    end

    def url
      object.doctor.avatar_url
    end
  end
end
