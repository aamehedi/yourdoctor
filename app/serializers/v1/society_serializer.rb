module V1
  class SocietySerializer < ActiveModel::Serializer
    attributes :id, :name
  end
end
