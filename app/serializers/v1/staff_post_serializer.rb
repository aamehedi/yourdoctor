module V1
  class StaffPostSerializer < ActiveModel::Serializer
    attributes :id, :memo, :images, :shines, :icd10s, :comments
    def memo
      object.body
    end

    def images
      []
    end

    def shines
      []
    end

    def icd10s
      []
    end

    def comments
      []
    end
  end
end
