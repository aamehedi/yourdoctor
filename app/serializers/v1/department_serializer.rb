module V1
  class DepartmentSerializer < ActiveModel::Serializer
    attributes :id, :name, :comment, :hospital_id
    has_one   :hospital_attributes, serializer: V1::HospitalSerializer

    def hospital_attributes
      object.hospital
    end
  end
end
