module V1
  class ThumbnailSerializer < ActiveModel::Serializer
    attributes :id, :first_name, :middle_name, :last_name, :avatar_url, :cover_url, :link_state, :is_member, :is_official
    #has_many   :department_lists_attributes, serializer: V1::DepartmentListSerializer

    #def department_lists_attributes
    #  object.department_lists
    #end

    has_one :department_attributes, serializer: V1::DepartmentSerializer

    def department_attributes
      department_list = object.department_lists.first
      department_list ? department_list.department : nil
    end

    def is_official
      object.is_official?
    end
  end
end
