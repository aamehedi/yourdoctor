module V1
  class HospitalSerializer < ActiveModel::Serializer
    attributes :id, :name, :area_id, :prefecture_id
    has_one :prefecture, serializer: V1::PrefectureSerializer
  end
end
