module V1
  class SkillSerializer < ActiveModel::Serializer
    attributes :id, :comment, :total_point, :recommended
    has_one :icd10_attributes, serializer: V1::Icd10ViewSerializer

    def icd10_attributes
      object.icd10
    end
  end
end
 
