module V1
  class PostSerializer < ActiveModel::Serializer
    attributes :id, :memo, :images, :shines
    has_many :icd10s, serializer: V1::NameAndIDSerializer
    has_many :comments, serializer: V1::CommentsSerializer

    def shines
      #object.shines.pluck(:doctor_id)
      object.shines.map{ |s| { id: s.doctor_id, name: s.doctor.name } }
    end
  end
end
