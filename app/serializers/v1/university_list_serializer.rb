module V1
  class UniversityListSerializer < ActiveModel::Serializer
    attributes :id, :graduation_year
    has_one :university_attributes, serializer: V1::NameAndIDSerializer

    def university_attributes
      object.university
    end
  end
end
