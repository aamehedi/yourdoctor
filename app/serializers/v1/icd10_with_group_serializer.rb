module V1
  class Icd10WithGroupSerializer < ActiveModel::Serializer
    attributes :id, :name, :code, :replace_code, :depth, :index, :group
  end
end
 
