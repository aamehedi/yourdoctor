module V1
  class LinkRequestSerializer < ActiveModel::Serializer
    attributes :id
    has_many   :followers_in_request, serializer: V1::ThumbnailSerializer
    has_many   :followees_in_request, serializer: V1::ThumbnailSerializer

    def followers_in_request
      object.followers.where("links.state = ?", Link.states[:request])
    end

    def followees_in_request
      object.followees.where("links.state = ?", Link.states[:request])
    end
  end
end
