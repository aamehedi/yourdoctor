module V1
  class Icd10ViewSerializer < ActiveModel::Serializer
    attributes :id, :name, :depth
    has_one :group, serializer: V1::NameAndIDSerializer
  end
end
 
