module V1
  class SkillWithRecommendersSerializer < ActiveModel::Serializer
    attributes :id, :comment, :total_point, :icd10
    has_one :icd10_attributes, serializer: V1::Icd10ViewSerializer
    has_many :recommenders_attributes, serializer: V1::ThumbnailSerializer

    def icd10_attributes
      object.icd10
    end
    
    def recommenders_attributes
      object.recommenders
    end
  end
end
 
