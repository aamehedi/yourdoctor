module V1
  class ShineSerializer < ActiveModel::Serializer
    attributes :total_point, :doctors

    def doctors
      object.shines.map{ |s| { id: s.doctor_id, name: s.name } }
    end

  end
end
