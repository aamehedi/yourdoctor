module V1
  class AwardSerializer < ActiveModel::Serializer
    attributes :id, :title, :year, :description
  end
end
 
