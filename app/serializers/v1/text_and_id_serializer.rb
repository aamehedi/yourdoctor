module V1
  class TextAndIDSerializer < ActiveModel::Serializer
    attributes :id, :text
  end
end
