module V1
  class NameAndIDSerializer < ActiveModel::Serializer
    attributes :id, :name
  end
end
