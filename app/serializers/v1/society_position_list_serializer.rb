 module V1
  class SocietyPositionListSerializer < ActiveModel::Serializer
    attributes :id, :society_list_id, :society_position_id
    has_one :position_attributes, serializer: V1::NameAndIDSerializer
    def position_attributes
      object.position
    end    
  end
end
