module V1
  class SkillWithDoctorSerializer < ActiveModel::Serializer
    attributes :id, :comment, :total_point, :icd10, :recommended
    has_one :icd10_attributes, serializer: V1::Icd10ViewSerializer
    has_one :doctor, serializer: V1::ThumbnailSerializer

    def icd10_attributes
      object.icd10
    end
  end
end
 
