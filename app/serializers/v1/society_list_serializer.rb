module V1
  class SocietyListSerializer < ActiveModel::Serializer
    attributes :id
    has_one :society_attributes, serializer: V1::NameAndIDSerializer
    has_many :position_lists_attributes, serializer: V1::SocietyPositionListSerializer

    def society_attributes
      object.society
    end

    def position_lists_attributes
      object.position_lists
    end
  end
end
