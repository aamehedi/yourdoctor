module V1
  class PrefectureSerializer < ActiveModel::Serializer
    attributes :id, :name
    has_one :area, serializer: V1::NameAndIDSerializer
  end
end
 