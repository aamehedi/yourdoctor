module V1
  class TimelineSerializer < ActiveModel::Serializer
    attributes :id, :action, :meta, :model_type, :model_id, :editor_id, :editor_first_name, :editor_last_name, :editor_hospital_name, :editor_hospital_id, :editor_university_name, :editor_graduation_year, :doctor_id, :doctor_first_name, :doctor_last_name, :doctor_hospital_name, :doctor_hospital_id, :doctor_university_name, :doctor_graduation_year, :updated_at, :relation_type, :series
	has_one :editor, serializer: V1::ThumbnailSerializer
	has_one :target, serializer: V1::ThumbnailSerializer
	has_one :data

	def data
	  if object.model.kind_of?(Post)
            V1::PostSerializer.new(object.model)
          elsif object.model.kind_of?(StaffPost)
            V1::StaffPostSerializer.new(object.model)
          else
            nil
	  end
	end
  end
end
 
