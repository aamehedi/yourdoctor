module V1
  class Icd10Serializer < ActiveModel::Serializer
    attributes :id, :name, :code, :replace_code, :depth, :index
  end
end
 
