module V1
  class MonographSerializer < ActiveModel::Serializer
    attributes :id, :title, :year, :description, :url, :writers, :media, :page, :published, :published_type, :tag_list
  end
end
