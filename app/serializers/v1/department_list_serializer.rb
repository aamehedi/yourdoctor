module V1
  class DepartmentListSerializer < ActiveModel::Serializer
    attributes :id, :active, :position, :term_start, :term_end, :department_id, :part_time, :index
    has_one  :department_attributes, serializer: V1::DepartmentSerializer
    has_many :position_lists_attributes, serializer: V1::DepartmentPositionListSerializer

    def department_attributes
    	object.department
    end

    def position_lists_attributes
      object.position_lists
    end
  end
end
