module V1
  class UserSerializer < DoctorSerializer
    attributes :email, :token_type, :user_id, :registration_number, :registration_date, :access_token, :is_official?, :default_avatar_url, :default_cover_url, :requested_count
    #has_many   :followers, serializer: V1::ThumbnailSerializer
    #has_many   :followees, serializer: V1::ThumbnailSerializer
    def user_id
      object.id
    end

    def token_type
      'Bearer'
    end
  end
end
