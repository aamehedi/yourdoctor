module V1
  class URLAndIDSerializer < ActiveModel::Serializer
    attributes :id, :url
  end
end
