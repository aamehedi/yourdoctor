class SpecialtyDoctorSerializer < ActiveModel::Serializer
  attributes :id, :name, :doctor_specialty

  def doctor_specialty
    options[:current_doctor_specialties].include? object.id
  end
end
