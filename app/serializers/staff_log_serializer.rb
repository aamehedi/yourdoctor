class StaffLogSerializer < ActiveModel::Serializer
  attributes :id, :staff_log_id, :staff_log_type
  has_one :user
end
