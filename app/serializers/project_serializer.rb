class ProjectSerializer < ActiveModel::Serializer
  include ActiveModel::TimeBeautification

  has_many :mrs
  attributes :id, :name, :created_at, :updated_at, :project_owner_id,
    :project_owner_name, :this_month_request, :next_month_request, :mrs

  def project_owner_id
    object.mr_id.to_s
  end

  def project_owner_name
    object.project_manager.name
  end

  def description
    object.description.to_s
  end

  def this_month_request
    object.this_month_request.to_s
  end

  def next_month_request
    object.next_month_request.to_s
  end
end
