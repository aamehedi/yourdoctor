class DrMrChatroomSerializer < ActiveModel::Serializer
  has_one :doctor
  has_one :mr

  attributes :id, :status, :favorite, :last_message, :last_seen_message_id_dr,
    :last_seen_message_id_mr, :unread_messages, :created_at

  def unread_messages
    last_read_message_id = if options[:from_dr]
      object.last_seen_message_id_dr
    else
      object.last_seen_message_id_mr
    end
    return 0 if last_read_message_id == object.last_message_id
    DrMrMessage.unread(object.id, last_read_message_id).count
  end

  def last_message
    object.dr_mr_messages.last
  end
end
