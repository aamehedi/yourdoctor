class SocietiesController < InheritedResources::Base
  before_action :redirect_root_if_not_login  
  layout "work_layout"
  def index
    if params[:search]
      word = '%' + params[:search] + '%'
      @societies = Society.where('name LIKE ?', word).order(:id).page(params[:page]).per(100)
    else
      @societies = Society.all.order(:id).page(params[:page]).per(100)
    end
  end

  def merge
    params[:societies].each do | item |
      if item[:target].present?
        src = Society.find(item[:id])
        dst = Society.find(item[:target])
        dst.merge(src)
      end
    end
    get_param = params[:search].present? ? "?search=" + params[:search] : ""
    redirect_to societies_path + get_param
  end

  private
    def society_params
      params.require(:society).permit(:name, :english_name, :address, :email)
    end
end
