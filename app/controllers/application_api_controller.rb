# -*- coding: utf-8 -*-
class ApplicationApiController < ActionController::Base
  include AbstractController::Translation

  ERROR_CODES = {
    invalid_mr_access_token: 600
  }

  before_action :authenticate_user_from_token!
  before_action :authenticate_user_from_identity
  before_action :set_current_user_type

  respond_to :json

  # User Authentication
  # Authenticates the user with OAuth2 Resource Owner Password Credentials Grant
  def authenticate_user_from_token!
    auth_token = request.headers["Authorization"]
    if auth_token
      authenticate_with_auth_token auth_token
    else
      authentication_error
    end
  end

  def authenticate_user_from_identity
    user = login_user
    if user && user.is_official?
      # success
    else
      render json: {error: t("api_error.unofficial")}, status: 401
    end
  end

  def invalid_login_attempt
    warden.custom_failure!
    render json: {error: t("api_error.invalid_login_attempt")}, status:
      :unprocessable_entity
  end

  def validate_mr_access_token
    return authentication_error unless params[:access_token]
    authentication_error unless MrSession.find_by_access_token
    params[:access_token]
  end

  def authenticate_mr
    @current_mr ||= MrSession.find_by_access_token(request
      .headers["Authorization"]).try(:mr) || invalid_mr_access_token
  end

  def invalid_mr_access_token
    warden.custom_failure!
    render json: {
      error_code: ERROR_CODES[:invalid_mr_access_token],
      error: t("invalid_mr_access_token")
    }, status: :unprocessable_entity
  end

  def unauthorized_mr
    warden.custom_failure!
    render json: {error: t("not_accessible")}, status: :unprocessable_entity
  end

  def unauthorized_doctor
    warden.custom_failure!
    render json: {error: t("unauthorized_doctor")},
      status: :unprocessable_entity
  end

  def authenticate_company_manager
    return unauthorized_mr unless @current_mr.company_manager?
  end

  def check_valid_mr
    return unauthorized_mr unless authenticate_mr
  end

  private

  def authenticate_with_auth_token auth_token
    unless auth_token.include? ":"
      authentication_error
      return
    end
    user_id = auth_token.split(":").first
    @user = Doctor.unscoped.find user_id
    if @user && Devise.secure_compare(@user.access_token, auth_token)
      # User can access
      sign_in @user, store: false
    else
      authentication_error
    end
  end

  # Authentication Failure
  # Renders a 401 error
  def authentication_error
    # User"s token is either invalid or not in the right format
    # Authentication timeout
    render json: {error: t("api_error.unauthorized")}, status: 401
  end

  def login_user
    auth_token = request.headers["Authorization"]
    return nil if auth_token.nil?
    return nil unless auth_token.include? ":"
    user_id = auth_token.split(":").first
    Doctor.unscoped.find user_id
  end

  def set_current_user_type
    user = login_user
    RequestStore.store[:is_staff] = user.is_staff if user
  end
end
