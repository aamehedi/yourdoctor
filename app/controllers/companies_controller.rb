class CompaniesController < ApplicationApiController
  skip_before_action :authenticate_user_from_token!
  skip_before_action :authenticate_user_from_identity
  skip_before_action :set_current_user_type
  before_action :authenticate_mr
  before_action :authenticate_company_manager

  def show
    if @current_mr.company.id == params[:id].to_i
      render json: CompanySerializer.new(@current_mr.company).company_info
    else
      render json: {error: t(".access_error")}, status: :unprocessable_entity
    end
  end
end
