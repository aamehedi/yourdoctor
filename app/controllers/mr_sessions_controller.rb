class MrSessionsController < ApplicationApiController
  skip_before_action :authenticate_user_from_token!
  skip_before_action :authenticate_user_from_identity
  skip_before_action :set_current_user_type

  respond_to :json

  def create
    mr = Mr.find_for_database_authentication email:
      mr_info_params[:email]
    if mr && mr.valid_password?(mr_info_params[:password])
      mr_session = Mr.login mr.id, mr_info_params[:device_token]
      if mr_session
        render json: mr_session
      else
        invalid_login_attempt
      end
    else
      invalid_login_attempt
    end
  end

  def destroy
    mr_session = MrSession.find_by access_token:
      mr_session_info_params[:access_token]
    if mr_session.present? && mr_session.destroy
      render json: {status: "success"}
    else
      render json: {status: "error"}
    end
  end

  private
  def mr_info_params
    params.require(:mr_info).permit :email, :password, :device_token
  end

  def mr_session_info_params
    params.require(:mr_session_info).permit :access_token
  end
end
