class HospitalsController < InheritedResources::Base
  before_action :redirect_root_if_not_login  
  layout "work_layout"
  def index
    if params[:search]
      word = '%' + params[:search] + '%'
      @hospitals = Hospital.where('name LIKE ?', word).order(:id).page(params[:page]).per(100)
    else
      @hospitals = Hospital.all.order(:id).page(params[:page]).per(100)
    end
  end

  def merge
    params[:hospitals].each do | item |
      if item[:target].present?
        src = Hospital.find(item[:id])
        dst = Hospital.find(item[:target])
        dst.merge(src)
      end
    end
    get_param = params[:search].present? ? "?search=" + params[:search] : ""
    redirect_to hospitals_path + get_param
  end
  private

    def hospital_params
      params.require(:hospital).permit(:name)
    end
end
