class WhytPlotApisController < ApplicationController
  layout "work_layout"
  before_action :set_whyt_plot_api

  def index
    @whyt_plot_apis = WhytPlotApi.all
    @chart_names = @whyt_plot_apis.map { |e|
      chart_type = ChartType.find_by_id(e.chart_type_id)
      chart_type.present? ? chart_type['name'] : 'nothing'
    }
  end

  def new
    @whyt_plot_api = WhytPlotApi.new
  end

  def create
    @whyt_plot_api = WhytPlotApi.new(whyt_plot_api_params)
    if @whyt_plot_api.save
      redirect_to whyt_plot_apis_path
    else
      redirect_to whyt_plot_apis_path,
      flash: { errors: @whyt_plot_api.errors.full_messages }
    end
  end

  def show
    @whyt_plot_api
  end

  def edit
    @whyt_plot_api
  end

  def update
    if @whyt_plot_api.update_attributes(whyt_plot_api_params)
      redirect_to whyt_plot_apis_path
    else
      redirect_to edit_whyt_plot_api_path(@whyt_plot_api),
      flash: { errors: @whyt_plot_api.errors.full_messages }
    end
  end

  def destroy
    @whyt_plot_api.destroy
    redirect_to whyt_plot_apis_path
  end

  private

    def set_whyt_plot_api
      @whyt_plot_api = params[:id] ? WhytPlotApi.find(params[:id]) : nil
    end

    def whyt_plot_api_params
      params.require(:whyt_plot_api).permit(
        :name, :purpose_jp, :purpose_en, :api_sql, :chart_type_id, :meta_data, :version
      )
    end

end
