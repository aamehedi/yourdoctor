require 'uri'

class DuplicateHospitalsController < InheritedResources::Base
  before_action :redirect_root_if_not_login  
  layout "work_layout"
  def index
    if params[:search]
      query = params[:search].strip
      @hospitals = Hospital.where('name LIKE ?', '%' + query + '%').order(:id).page(params[:page]).per(100)
    else
      @hospitals = []
    end
  end

  def merge
    duplicate_hospitals = duplicate_hospitals_params
    dst = Hospital.find(duplicate_hospitals[:dst_id])
    duplicate_hospitals[:listed_ids].each do | listed_id |
      next if listed_id.to_i == dst.id
      src = Hospital.find(listed_id)
      dst.merge(src)
    end
    redirect_to duplicate_hospitals_path + "?search=" + dst.name
  end
  private

    def duplicate_hospitals_params
      params.require(:duplicate_hospitals).permit(:dst_id, listed_ids: [])
    end
end
