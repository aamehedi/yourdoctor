class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  skip_before_action :verify_authenticity_token

  def redirect_root_if_not_leader
    unless (current_user && current_user.type == 'Leader')
      redirect_to "https:www.reasonwhy.jp"
    end
  end

  def redirect_root_if_not_login
    unless current_user
      redirect_to "https:www.reasonwhy.jp"
    end
  end
end
