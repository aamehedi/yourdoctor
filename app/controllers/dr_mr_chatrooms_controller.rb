class DrMrChatroomsController < ApplicationApiController
  skip_before_action :authenticate_user_from_token!
  skip_before_action :authenticate_user_from_identity
  skip_before_action :set_current_user_type
  before_action :authenticate_mr, only: :create
  before_action :authenticate_dr_or_mr, only: [:index, :update]

  def index
    unless @from_dr || params.key?(:favorite)
      return render json: Doctor.get_doctors_with_dr_mr_chatrooms(
        params, @dr_or_mr_id),
        each_serializer: DoctorWithDrMrChatroomSerializer, root: "doctors"
    end

    render json: DrMrChatroom.get_filtered_dr_mr_chatrooms(
      params, @dr_or_mr_id, @from_dr), from_dr: @from_dr
  end

  def show
    if dr_mr_chatroom = DrMrChatroom.find_by_id(params[:id])
      render json: dr_mr_chatroom
    else
      render_error t(".access_error")
    end
  end

  def create
    chatroom = DrMrChatroom.find_by_mr_id_and_doctor_id(
      @current_mr.id, dr_mr_chatroom_params[:doctor_id])
    chatroom ||= DrMrChatroom.new(dr_mr_chatroom_params
        .merge(mr_id: @current_mr.id).except :status, :favorite)
    return render json: chatroom if chatroom.save
  end

  def update
    if dr_mr_chatroom = DrMrChatroom.find_by_id(params[:id])
      return render json: dr_mr_chatroom if dr_mr_chatroom.update_chatroom(
        dr_mr_chatroom_params, @dr_or_mr_id, @from_dr)
    end
    render json: {error: t(".error")}, status: :unprocessable_entry
  end

  private
  def dr_mr_chatroom_params
    params.require(:dr_mr_chatroom).permit :doctor_id, :status, :favorite
  end

  def authenticate_dr_or_mr
    @from_dr = ActiveRecord::Type::Boolean.new.cast params[:from_dr]
    @dr_or_mr_id = if @from_dr
                     authenticate_user_from_token!
                     @user.id
                   else
                     authenticate_mr
                     @current_mr.id
                   end
  end
end
