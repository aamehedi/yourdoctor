class MailLogsController < InheritedResources::Base

  private

    def mail_log_params
      params.require(:mail_log).permit(:doctor_id, :all_mail_id, :done)
    end
end

