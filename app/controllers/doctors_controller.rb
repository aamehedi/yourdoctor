class DoctorsController < ApplicationController
  layout "work_layout"
  before_action :redirect_root_if_not_login  
  before_action :set_doctor, only: [:show, :edit, :update, :destroy, :plural_doctors]

  respond_to :html

  def index
    @doctors = Doctor.all
    respond_with(@doctors)
  end

  def show
    respond_with(@doctor)
  end

  def new
    @doctor = Doctor.new
    @doctor.department_lists.build(department_id: params[:department_id])
    @doctor.build_university
    @doctor.society_lists.build
    respond_with(@doctor)
  end

  def plural_doctors
    @drs = Doctor.where(first_name: @doctor.first_name, last_name: @doctor.last_name)
  end

  def edit

    @drs = Doctor.where(first_name: @doctor.first_name, last_name: @doctor.last_name)
    if @doctor.university.nil?
      @doctor.build_university
    end
    unless @doctor.society_lists.present?
      @doctor.society_lists.build
    end

    @duplicate_doctor_list = DuplicateDoctorList.where('%d = ANY(doctor_ids)' % @doctor.id).first
  end

  def create
    @doctor = Doctor.new(doctor_params)
    @doctor.save
    respond_with(@doctor)
  end

  def update
    @doctor.update(doctor_params)
    # respond_with(@doctor)
    respond_to do |format|
      format.html { redirect_to edit_doctor_path(@doctor) }
    end
  end

  def destroy
    @doctor.destroy
    respond_with(@doctor)
  end

  private
    def set_doctor
      @doctor = Doctor.find(params[:id])
    end

    def doctor_params
      params.require(:doctor).permit(:hospital_id, :first_name, :last_name, :middle_name, :last_name_initial, :first_name_initial, :middle_name_initial,:position, :university_id, :graduation_year, :society, :profession, :skill, :result, :url, :history, :top_or_next, :user, :update_user, :comment, :created_at, :updated_at, university_attributes:[:id, :name],  society_lists_attributes:[:id, :society_id, :name, :_destroy], society_position_lists_attributes:[:id, :society_position_id, :society_id, :name, :_destroy])
    end
end
