class StaffsController < InheritedResources::Base
  layout "work_layout"

  #スタッフはリーダーしか作れない
  before_action :redirect_root_if_not_leader, only: [:new, :create]
  
  #アップデートはスタッフ本人しかできない
  before_action :set_staff, only: [:update]
  before_action :redirect_root_if_not_self, only: [:update]

  def new  	
    @staff = current_user.staffs.build
  end

  def create
  	@staff = current_user.staffs.build
    @staff.assign_attributes(staff_params)
    if @staff.save
      redirect_to home_leader_url
    else
      respond_with(@staff)
    end
  end

  def update
  	@staff.assign_attributes(staff_params)
    if @staff.save
      flash[:notice] = '更新しました'
    else
      flash[:alert] = '更新に失敗しました'
    end
    redirect_to database_work_path
  end

  private
    def set_staff
      @staff = params[:id] ? Staff.find(params[:id]) : nil
    end

    def staff_params
      params.require(:staff).permit(:email, :name, :password, :password_confirmation)
    end

    def redirect_root_if_not_leader
      unless current_user && (current_user.type == 'Leader')
      	redirect_to root_path
      end
    end

    def redirect_root_if_not_self
      unless current_user && (current_user.id == @staff.id)
        redirect_to root_path
      end      
    end
end
