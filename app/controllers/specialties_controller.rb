class SpecialtiesController < ApplicationApiController
  skip_before_action :authenticate_user_from_identity

  def index
    render json: Specialty.all
  end
end
