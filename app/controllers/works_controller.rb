# -*- coding: utf-8 -*-
class WorksController < ApplicationController
  layout "work_layout"

  before_action :set_department, only: [:update_department_lists]
  before_action :redirect_root_if_not_login, only: [:top]
  def index

  end

  def top
  end

  def prefecture_select
     @hospitals = Prefecture.find(params[:id]).hospitals
  end

  def department_select
     @departments = Hospital.find(params[:id]).departments.uniq
  end

  def department
    if params[:id] =~ /\Anew_/
      @department = Department.new(hospital_id: params[:id].sub("new_department_", ""))
    else
      @department = Department.find(params[:id])
    end
  end

  def department_list_json
    render json: DepartmentCategory.all.to_a.as_json
  end

  def department_position_names
    #render json: [{1: "院長"} { 2:"副院長"},  {3: "センター長"}].as_json

  end

  def update_department_lists
    if @department.update_attributes(department_params)
      flash[:success] = "情報を更新しました"
    else
      flash[:alert]   = "登録に失敗しました"
    end
    redirect_to work_department_path
  end

  private
    def set_department
      @department = Department.find(params[:id])
    end

    def department_params
      params.require(:department).permit(department_lists_attributes:[:id, :order_num, :active])
    end

end
