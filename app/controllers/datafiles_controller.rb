require 'aws-sdk-resources'

class DatafilesController < ApplicationController
  layout "work_layout"
  before_action :redirect_root_if_not_login
  # before_action :set_duplicate_docter_list, only: [:show, :update, :merge]

  respond_to :html

  def index
    wp_bucket = Aws::S3::Resource.new(region: 'ap-northeast-1').bucket('rw-rails-data')
    @s3_folder = wp_bucket.objects.map { |obj|
      obj.key =~ /^whytplot-data\/([0-9]{4,})\/$/ ? obj : nil;
    }.compact

    @years = @s3_folder.map do |dir|
      if dir.key =~ /[0-9]{4,}/ then $& end
    end
  end

  def show
    @year = params[:year]
    @datafile = Datafile.find(params[:id])
  end

  def list
    wp_bucket = Aws::S3::Resource.new(region: 'ap-northeast-1').bucket('rw-rails-data')
    @datafiles = Array.new
    wp_bucket.objects(prefix: "whytplot-data/#{params[:year]}/").each do |obj|
      if obj.key =~ /^whytplot-data\/([0-9]{4})\/.+/ && File.extname(obj.key).present?
        @datafiles.push(Datafile.find_or_initialize_by(filepath: "#{obj.key}"))
      end
    end
  end

  def edit
  end

  def new
  end

  def create
  end

  def update
  end

  def destroy
  end

  def import
    import_params = params[:import]

    if import_params.nil?
      redirect_to datafiles_path, alert: "パラメータが不正です", status: 505
      return
    end

    import_params.each do | key, item |
      #チェックがついているものだけ実行
      if item[:do]
        s3_filepath  = item[:filepath]
        file_name = s3_filepath =~ /^whytplot-data\/([0-9]{4,}|.+)\// ? $' : nil

        # s3のファイル一旦public/にダウンロードする
        s3 = Aws::S3::Client.new(region: 'ap-northeast-1')
        File.open("#{Rails.root}/tmp/datafiles/#{file_name}", "wb") do |file|
          s3.get_object(bucket: 'rw-rails-data', key: "#{s3_filepath}") do |chunk|
            file.write chunk
          end
        end

        year = s3_filepath =~ /\/[0-9]{4,}\// ? $&.delete("/") : nil
        filepath = "#{Rails.root}/tmp/datafiles/#{file_name}"
        datafile = Datafile.find_or_initialize_by(filepath: s3_filepath)
        datafile.status = :importing
        datafile.filepath = s3_filepath
        datafile.save!

        # TODO 本番ではコメントアウト必要
        ImportJob.perform_later(filepath, s3_filepath, year)
        # ImportJob.new.perform(filepath, s3_filepath, year)
      end
    end
    redirect_to :back, notice: "インポートを実行しました"
  end
end
