class DepartmentsController < ApplicationController
  layout "work_layout"
  before_action :redirect_root_if_not_login  
  before_action :set_department, only: [:show, :edit, :update, :destroy]

  respond_to :html

  def index
    @departments = Department.all.page(params[:page]).per(20)
    respond_with(@departments)
  end

  def show
    respond_with(@department)
  end

  def new
    @department = Department.new
    respond_with(@department)
  end

  def edit
    if @department.university.nil?
      @department.build_university
    end

    unless @department.society_lists.present?
      @department.society_lists.build
    end
  end

  def create
    @department = Department.new(department_params)
    @department.save
    redirect_to "/work_department/" + @department.id.to_s
  end

  def update
    @department.update(department_params)
    # respond_with(@department)
    respond_to do |format|
      format.html { redirect_to edit_department_path(@department) }
    end
  end

  def destroy
    @department.destroy
    respond_with(@department)
  end

  private
    def set_department
      @department = Department.find(params[:id])
    end

    def department_params
      params.require(:department).permit(:hospital_id, :name, :name_kana, :department_type_list, :url_list, :no_name, :comment)
    end
end
