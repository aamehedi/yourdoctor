class Mrs::WhytplotChatroomsController < ApplicationApiController
  skip_before_action :authenticate_user_from_token!
  skip_before_action :authenticate_user_from_identity
  skip_before_action :set_current_user_type
  before_action :authenticate_mr
  before_action :check_authorization, only: [:update, :show]

  def index
    render json: WhytplotChatroom.get_filtered_chatrooms(@current_mr.id,
      chatroom_ids_chatroom_params[:chatroom_type],
      chatroom_ids_chatroom_params[:favorite]),
      current_mr_id: @current_mr.id
  end

  def show
    if whytplot_chatroom = WhytplotChatroom.find_by_id(params[:id])
      render json: whytplot_chatroom, current_mr_id: @current_mr.id
    else
      render_error t(".access_error")
    end
  end

  def create
    chatroom = WhytplotChatroom.find_or_create_chatroom chatroom_params
    return render json: chatroom if chatroom
    render_error  t(".access_error")
  end

  def update
    chatroom = WhytplotChatroom.find_by_id params[:id]
    if chatroom &&
      chatroom.api_update(chatroom_params, params[:participation_favorite])
      render json: chatroom, current_mr_id: @current_mr.id
    else
      render_error t(".error")
    end
  end

  private
  def chatroom_params
    params.require(:whytplot_chatroom).permit :name, :chatroom_type, mr_ids: []
  rescue StandardError
  end

  def chatroom_ids_chatroom_params
    params.permit :missed_chat, :favorite, :chatroom_type
  end

  def check_authorization
    unauthorized_mr unless WhytplotMrChatroom.exists?(
      whytplot_chatroom_id: params[:id], mr_id: @current_mr.id)
  end

  def render_error error_message
    # TODO: We should define this in base controller in another refactoring task
    render json: {error: error_message}, status: :unprocessable_entity
  end
end
