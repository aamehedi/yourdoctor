class Mrs::WhytplotMrChatroomsController < ApplicationApiController
  skip_before_action :authenticate_user_from_token!
  skip_before_action :authenticate_user_from_identity
  skip_before_action :set_current_user_type
  before_action :authenticate_mr

  def update
    if @mr_chatroom &&
       @mr_chatroom.update_attributes!(whytplot_mr_chatroom_params)
      render json: @mr_chatroom
    else
      render json: {error: t(".error")}
    end
  end

  private
  def whytplot_mr_chatroom_params
    params.require(:whytplot_mr_chatroom).permit :last_read_message_id,
      :favorite
  end

  def check_authorization
    @mr_chatroom = WhytplotMrChatroom.find_by(
      whytplot_chatroom_id: params[:id], mr_id: @current_mr.id)
    unauthorized_mr if @mr_chatroom.nil? || @mr_chatroom.id != params[:id]
  end
end
