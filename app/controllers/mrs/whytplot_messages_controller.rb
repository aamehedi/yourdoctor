class Mrs::WhytplotMessagesController < ApplicationApiController
  skip_before_action :authenticate_user_from_token!
  skip_before_action :authenticate_user_from_identity
  skip_before_action :set_current_user_type
  before_action :authenticate_mr
  before_action :check_authorization

  def index
    message_limit = WhytplotMessage::MESSAGE_LIMIT
    message_limit = whytplot_message_params[:limit] if
      whytplot_message_params[:limit].present?
    last_message_excluded = whytplot_message_params[:last_message_excluded] ||
                            false
    whytplot_messages = WhytplotMessage.get_messages(@message,
      message_limit, last_message_excluded)
      .sort_by &:id
    render json: whytplot_messages
  end

  private
  def whytplot_message_params
    params.permit :id, :limit, :last_message_excluded
  end

  def check_authorization
    return unauthorized_mr unless
      WhytplotMessage.check_authorization_for_get_messages?(
        whytplot_message_params[:id], @current_mr.id)
    @message = WhytplotMessage.find_by_id whytplot_message_params[:id]
  end
end
