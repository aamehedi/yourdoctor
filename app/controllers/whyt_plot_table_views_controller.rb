# -*- coding: utf-8 -*-
class WhytPlotTableViewsController < ApplicationController
  layout "work_layout"
  before_action :set_whyt_plot_table_view

  def index
    @whyt_plot_table_views = WhytPlotTableView.all
    @sqlite_files = WhytPlotTableView.get_whytplot_sqlite_files
  end

  def new
    @whyt_plot_table_view = WhytPlotTableView.new
  end

  def create
    @whyt_plot_table_view = WhytPlotTableView.new(whyt_plot_table_view_params)
    if @whyt_plot_table_view.save
      redirect_to whyt_plot_table_views_path,
      flash: { notice: 'レコードが追加されました' } and return
    else
      redirect_to new_whyt_plot_table_view_path,
      flash: { errors: @whyt_plot_table_view.errors.full_messages } and return
    end
  end

  def show
    @whyt_plot_table_view
  end

  def edit
    @whyt_plot_table_view
  end

  def update
    @whyt_plot_table_view.update_attributes(whyt_plot_table_view_params)
    redirect_to edit_whyt_plot_table_view_path(@whyt_plot_table_view),
    flash: { errors: @whyt_plot_table_view.errors.full_messages }
  end

  def destroy
    whytplot_table_view = WhytPlotTableView.find(params[:id])
    @whyt_plot_table_view.destroy
    redirect_to whyt_plot_table_views_path
  end

  def export
    # TODO 本番ではコメントアウト必要
    WhytplotDbExportJob.perform_later(params[:is_production])
    # WhytplotDbExportJob.new.perform(params[:is_production])
    redirect_to whyt_plot_table_views_path
  end

  def download
    timestamp = params[:timestamp]
    user_id = params[:user_id]
    user = User.find_by_id(user_id)
    unless user.present? then
      render json: { message: 'user not found' }, :status => 404 and return
    end
    filename = WhytPlotTableView.get_whytplot_data(timestamp, user)
    filepath = Dir.glob("tmp/datafiles/#{filename}")
    unless filepath.present? then
      render json: { message: 'file not found'}, :status => 404 and return
    end

    stat = File::stat(filepath[0])
    if filepath.present?
      send_file(filepath[0], :filename => filename, :length => stat.size)
    end
  end

  def latest
    user_id = params[:user_id]
    user = User.find_by_id(user_id)
    result = WhytPlotTableView.get_latest_version(user)
    unless result.present?
      render json: { message: 'file not found' }, :status => 404
    else
      render json: { timestamp: result }
    end
  end

  private

    def set_whyt_plot_table_view
      @whyt_plot_table_view =
        params[:id] ? WhytPlotTableView.find(params[:id]) : nil
    end

    def whyt_plot_table_view_params
      params.require(:whyt_plot_table_view).permit(
        :name, :create_view_sql, :is_view, :description
      )
    end

end
