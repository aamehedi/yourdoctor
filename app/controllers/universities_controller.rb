class UniversitiesController < InheritedResources::Base
  before_action :redirect_root_if_not_login  
  layout "work_layout"
  def index
  	if params[:search]
  	  word = '%' + params[:search] + '%'
      @universities = University.where('name LIKE ?', word).order(:id).page(params[:page]).per(100)
    else
      @universities = University.all.order(:id).page(params[:page]).per(100)
    end
  end

  def merge
    params[:universities].each do | item |
      if item[:target].present?
        src = University.find(item[:id])
        dst = University.find(item[:target])
        dst.merge(src)
      end
    end
    get_param = params[:search].present? ? "?search=" + params[:search] : ""
    redirect_to universities_path + get_param
  end

  private
    def universitiy_params
      params.require(:university).permit(:name)
    end
end
