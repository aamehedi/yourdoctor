class ChartsController < ApplicationApiController
  skip_before_action :authenticate_user_from_token!
  skip_before_action :authenticate_user_from_identity
  skip_before_action :set_current_user_type
  before_action :authenticate_mr

  def index
    render json: Chart.retrieved_by(@current_mr, params,
      chart_search_params[:search])
  end

  def show
    chart = Chart.find_by id: params[:id]
    return render json: chart if chart.present?
    render json: {error: t("no_such_chart")}, status: :unprocessable_entity
  end

  def create
    chart_info = chart_params.merge mr: @current_mr
    chart = Chart.new chart_info
    chart.favorite = chart_params[:favorite] || false
    return success_response chart if chart.save
    render json: {error: t("not_created")}, status: :unprocessable_entity
  end

  def update
    chart = @current_mr.charts.find_by id: params[:id]
    if chart.present? && chart.update_attributes(chart_params)
      return success_response chart
    end
    render json: {error: t("update_fail")}, status: :unprocessable_entity
  end

  private
  def chart_params
    params.require(:chart_info).permit :name, :description, :favorite,
      :raw_query_param_values, :query_param_values,
      :option, :chart_type
  end

  def chart_search_params
    params.permit search: [:name_cont, :chart_type_eq]
  end

  def success_response chart
    render json: {
      success: t(".success"),
      chart_info: ChartSerializer.new(chart, root: false)
    }
  end
end
