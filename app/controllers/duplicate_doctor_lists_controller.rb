# -*- coding: utf-8 -*-
class DuplicateDoctorListsController < ApplicationController
  layout "work_layout"
  before_action :redirect_root_if_not_login  
  before_action :set_duplicate_docter_list, only: [:show, :update, :merge]

  respond_to :html

  def index
  	case params[:search]
  	when 'all'
  		@duplicate_doctor_list = DuplicateDoctorList.all.order(:id).page(params[:page]).per(20)
  	when 'confirmed'
  		@duplicate_doctor_list = DuplicateDoctorList.where(:check => true).order(:id).page(params[:page]).per(20)
  	when 'unconfrimed'
  		@duplicate_doctor_list = DuplicateDoctorList.where(:check => false).order(:id).page(params[:page]).per(20)
   	else
  		@duplicate_doctor_list = DuplicateDoctorList.all.order(:id).page(params[:page]).per(20)
  	end
  end

  def show

  end

  def update
    before_check = @duplicate_docter_list.check
    @duplicate_docter_list.update_attributes(duplicate_doctor_list_params)
    if before_check != @duplicate_docter_list.check
      StaffLog.create(user_id: current_user.id, staff_log_id: @duplicate_docter_list.id, staff_log_type: "DuplicateDoctorList", sub: "確認済")
    end
    redirect_to duplicate_doctor_lists_path
  end

  def merge
    if params[:src_id].blank? || params[:dst_id].blank?
      return redirect_to duplicate_doctor_list_path(@duplicate_docter_list), :alert => "IDを指定してください"
    end
    #マージ作業
    if @duplicate_docter_list.merge(params[:src_id].to_i, params[:dst_id].to_i)
      StaffLog.create(user_id: current_user.id, staff_log_id: @duplicate_docter_list.id, staff_log_type: "DuplicateDoctorList", sub: "マージ")
      redirect_to duplicate_doctor_list_path(@duplicate_docter_list)
    else
      redirect_to duplicate_doctor_list_path(@duplicate_docter_list), :alert => "失敗しました"
    end
  end

private
    def set_duplicate_docter_list
      @duplicate_docter_list = DuplicateDoctorList.find(params[:id])
    end

    def duplicate_doctor_list_params
      params.require(:duplicate_doctor_list).permit(:check, :comment)
    end
end
