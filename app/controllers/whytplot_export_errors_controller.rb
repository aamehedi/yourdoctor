class WhytplotExportErrorsController < ApplicationController
  layout "work_layout"

  def index
    @WhytplotExportError = WhytplotExportError.all
  end

  def destroy
      whytplot_export_error = WhytplotExportError.find(params[:id])
      whytplot_export_error.destroy
      redirect_to whytplot_export_errors_path
  end
end
