class SocietyPositionsController < InheritedResources::Base

  private

    def society_position_params
      params.require(:society_position).permit(:name, :comment, :society_id)
    end
end

