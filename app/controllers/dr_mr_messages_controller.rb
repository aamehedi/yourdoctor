class DrMrMessagesController < ApplicationApiController
  skip_before_action :authenticate_user_from_token!
  skip_before_action :authenticate_user_from_identity
  skip_before_action :set_current_user_type
  before_action :check_authentication
  before_action :check_authorization_index, only: :index
  before_action :check_authorization_create, only: :create

  def index
    message_limit = Settings.dr_mr_message.message_limit
    message_limit = dr_mr_message_params[:limit] if
        dr_mr_message_params[:limit].present?
    dr_mr_messages = DrMrMessage.get_messages(@message,
      message_limit, dr_mr_message_params[:last_message_excluded]).sort_by &:id
    render json: {
      dr_mr_messages: dr_mr_messages,
      last_message_excluded: dr_mr_message_params[:last_message_excluded]
    }
  end

  def create
    dr_mr_message = DrMrMessage.new dr_mr_message_create_params
    if dr_mr_message.save
      render json: dr_mr_message
    else
      render json: {error: dr_mr_message.errors.messages}, status:
        :unprocessable_entry
    end
  end

  private
  def dr_mr_message_params
    params.permit :id, :limit, :last_message_excluded, :from_dr
  end

  def dr_mr_message_create_params
    custom_params = if @from_dr
      {doctor_id: @user_id}
    else
      {mr_id: @user_id}
    end
    params.require(:dr_mr_message).permit(:dr_mr_chatroom_id, :message_type,
      :attachment).merge custom_params
  end

  def check_authentication
    @from_dr = ActiveRecord::Type::Boolean.new.cast(
      dr_mr_message_params[:from_dr])
    @user_id = if @from_dr
      authenticate_user_from_token!
      @user.id
    else
      authenticate_mr
      @current_mr.id
    end
  end

  def check_authorization_index
    @message = DrMrMessage.find_by_id dr_mr_message_params[:id]
    throw_error unless @message && @message.dr_mr_chatroom.accepted?
    throw_error if @from_dr && @user_id != @message.dr_mr_chatroom.doctor_id
    throw_error unless @from_dr || @user_id == @message.dr_mr_chatroom.mr_id
  end

  def check_authorization_create
    condition = {id: dr_mr_message_create_params[:dr_mr_chatroom_id]}
    if @from_dr
      condition.merge doctor_id: @user_id
    else
      condition.merge mr_id: @user_id
    end
    throw_error unless DrMrChatroom.exists? condition
  end

  def throw_error
    @from_dr ? unauthorized_doctor : unauthorized_mr
  end
end
