class Doctors::WhytlinkMessagesController < ApplicationApiController
  skip_before_action :authenticate_user_from_identity
  before_action :check_authorization_index, only: :index
  before_action :check_authorization_create, only: :create

  def index
    message_limit = WhytlinkMessage::MESSAGE_LIMIT
    message_limit = whytlink_message_params[:limit] if
      whytlink_message_params[:limit].present?
    last_message_excluded = whytlink_message_params[:last_message_excluded] ||
                            false
    whytlink_messages = WhytlinkMessage.get_messages(@message,
      message_limit, last_message_excluded)
      .sort_by &:id
    render json: {
      whytlink_messages: whytlink_messages,
      last_message_excluded: last_message_excluded
    }
  end

  def create
    whytlink_message = WhytlinkMessage.new whytlink_message_create_params
    if whytlink_message.save
      render json: whytlink_message
    else
      render json: {error: whytlink_message.errors.messages}, status:
        :unprocessable_entry
    end
  end

  private
  def whytlink_message_params
    params.permit :id, :limit, :last_message_excluded
  end

  def whytlink_message_create_params
    params.require(:whytlink_message).permit(:content, :whytlink_chatroom_id,
      :message_type, :attachment).merge doctor_id: login_user.id
  end

  def check_authorization_index
    if login_user.authorized_to_get_messages? whytlink_message_params[:id]
      @message = WhytlinkMessage.find_by_id whytlink_message_params[:id]
    else
      return unauthorized_doctor
    end
  end

  def check_authorization_create
    unless login_user
        .authorized_to_create_messages? whytlink_message_create_params[
        :whytlink_chatroom_id]
      return unauthorized_doctor
    end
  end
end
