module Doctors
  class TemporaryDoctorsController < ApplicationApiController
    #skip_before_action :authenticate_user_from_token!
    skip_before_action :authenticate_user_from_identity

    def index
      @user = login_user
      render json: @user.invitees, each_serializer: V1::InviteeSerializer, root: "invitees"
    end

    def test
      @user = Doctor.find(params[:id])
      render json: @user.invitees, each_serializer: V1::InviteeSerializer, root: "invitees"
    end
  end
end
