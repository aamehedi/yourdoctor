module Doctors
  class PrefecturesController < ApplicationApiController
    skip_before_action :authenticate_user_from_token!
    skip_before_action :authenticate_user_from_identity

  	def index
  	  prefectures = Prefecture.all
  	  render json: prefectures, each_serializer: V1::NameAndIDSerializer, root: nil
  	end
  end
end
