module Doctors
  class UniversitiesController < ApplicationApiController
    skip_before_action :authenticate_user_from_token!
    skip_before_action :authenticate_user_from_identity

  	def index
      if params[:name].present?
        society = University.where("name LIKE ?", '%' + params[:name] + '%').limit(20)
      else
        society = University.all.limit(20)
      end
  	  render json: society, each_serializer: V1::NameAndIDSerializer, root: nil
  	end
  end
end
