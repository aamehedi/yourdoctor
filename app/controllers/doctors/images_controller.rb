module Doctors
  class ImagesController < ApplicationApiController    
    #skip_before_action :authenticate_user_from_token!
    skip_before_action :authenticate_user_from_identity
    before_action :set_editor

    def create
      @user = login_user
      case params[:type]
      when "avatar"
        @user.avatar_image = params[:file]
        if @user.save!
          render json: {type: :avatar_url, url: @user.avatar_url}, root: nil
        else
          render json: { error: t('avatar_upload_error') }, status: :unprocessable_entity
        end
      when "cover"        
        @user.cover_image  = params[:file]
        if @user.save!
          render json: {type: :cover_url, url: @user.cover_url}, root: nil
        else
          render json: { error: t('avatar_upload_error') }, status: :unprocessable_entity
        end
      end
    end

    #現状未使用 Doctor::UsersControllerから削除フラグを立てて消している
    def destroy
      @user = login_user
      case params[:type]
      when "avatar"        
        @user.remove_avatar_image!
        render json: {type: :avatar_url, url: @user.avatar_url}, root: nil
      when "cover"
        @user.remove_avatar_image!
        render json: {type: :cover_url, url: @user.cover_url}, root: nil
      end        
    end

    private
      def set_editor
        Doctor.editor = login_user
      end 
  end  
end

 
