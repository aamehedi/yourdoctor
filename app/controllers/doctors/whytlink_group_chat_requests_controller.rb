class Doctors::WhytlinkGroupChatRequestsController < ApplicationApiController
  skip_before_action :authenticate_user_from_identity
  before_action :check_admin, only: :update

  def create
    whytlink_group_chat_request = WhytlinkGroupChatRequest
      .new whytlink_group_chat_request_create_params
    if whytlink_group_chat_request.save
      render json: whytlink_group_chat_request
    else
      render json: {error: whytlink_group_chat_request.errors.messages},
        status: :unprocessable_entry
    end
  end

  def update
    if @whytlink_group_chat_request.accept_or_reject_request(
      whytlink_group_chat_request_update_params)
      render json: @whytlink_group_chat_request
    else
      render json: {failed: t("already_updated")}
    end
  end

  private
  def whytlink_group_chat_request_create_params
    params.require(:whytlink_group_chat_request).permit :doctor_id,
      :whytlink_group_chat_info_id
  end

  def whytlink_group_chat_request_update_params
    params.require(:whytlink_group_chat_request).permit :doctor_id, :status
  end

  def check_admin
    @whytlink_group_chat_request = WhytlinkGroupChatRequest.find_by_id(
      params[:id])
    if login_user.whytlink_doctor_chatrooms.admin.find_by_whytlink_chatroom_id(
      @whytlink_group_chat_request.whytlink_group_chat_info
      .whytlink_chatroom_id).nil?
      unauthorized_doctor
    else
      @whytlink_group_chat_request
    end
  end
end
