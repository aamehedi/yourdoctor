class Doctors::EmergencyDoctorHospitalsController < ApplicationApiController
  skip_before_action :authenticate_user_from_identity

  def index
    if params[:longitude]
      render json: login_user.detect_nearby_emergency_doctors(
        emergency_doctor_hospital_params),
        each_serializer: HospitalAndDoctorSerializer
    else
      render json: login_user.hospitals,
        each_serializer: EmergencyDoctorHospitalSerializer,
        root: Settings.hospitals,
        current_doctor_hospitals: login_user.emergency_hospital_ids
    end
  end

  def create
    login_user.emergency_hospital_ids =
      emergency_doctor_hospital_create_params[:hospital_ids]
    render json: login_user.hospitals,
      each_serializer: EmergencyDoctorHospitalSerializer,
      root: Settings.hospitals,
      current_doctor_hospitals: login_user.emergency_hospital_ids
  end

  private
  def emergency_doctor_hospital_params
    params.permit :longitude, :latitude, :radius, :available, specialties: []
  end

  def emergency_doctor_hospital_create_params
    params.require(:emergency_doctor_hospital_info).permit hospital_ids: []
  end
end
