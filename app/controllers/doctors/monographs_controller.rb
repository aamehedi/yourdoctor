# -*- coding: utf-8 -*-
module Doctors
  class MonographsController < ApplicationApiController
    before_action :set_monograph, only: [:create, :update]
    before_action :set_editor, only: [:create, :update]

    def create
      #会員でないdoctor or 自分自身を書き換えられる
      if (!@monograph.doctor.is_member) || (@monograph.doctor.id == login_user.id)
        if @monograph.save()
          @monograph.doctor.paginate_info = { page_type: "monographs", page_num: @monograph.get_page_num }
          render json: @monograph.doctor, serializer: V1::DoctorSerializer, root: nil
        else
          render json: { error: t('user_update_error') }, status: :unprocessable_entity
        end
      else
        render json: { error: t('user_update_error') }, status: :unprocessable_entity
      end
    end

    def update
      #会員でないdoctor or 自分自身を書き換えられる
      if (!@monograph.doctor.is_member) || (@monograph.doctor.id == login_user.id)
        if @monograph.update_attributes(monograph_params)
          @monograph.doctor.paginate_info = { page_type: "monographs", page_num: @monograph.get_page_num }
          render json: @monograph.doctor, serializer: V1::DoctorSerializer, root: nil
        else
          render json: { error: t('user_update_error') }, status: :unprocessable_entity
        end
      else
        render json: { error: t('user_update_error') }, status: :unprocessable_entity
      end
    end

    private
      def set_monograph
        if params.key?(:id)
          @monograph = Monograph.find(params[:id])
        else
          @monograph = Monograph.new(monograph_params)
          @monograph.doctor = Doctor.find(monograph_params[:doctor_id])
        end
      end

      def set_editor
        Doctor.editor = login_user
      end

      def monograph_params
        if (self.action_name == "create")
          params.require(:monograph).permit(:doctor_id,:title,:year,:description,:url,:writers,:media,:page,:published,:published_type,:tag_list)
        elsif (self.action_name == "update")
          params.require(:monograph).permit(:id,:title,:year,:description,:url,:writers,:media,:page,:published,:published_type,:tag_list)
        else
          { }
        end
      end
  end
end
