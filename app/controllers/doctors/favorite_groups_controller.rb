class Doctors::FavoriteGroupsController < ApplicationApiController
  skip_before_action :authenticate_user_from_identity
  before_action :check_favorite_group, only: [:show, :update]

  def index
    render json: FavoriteGroup.with_hospitals_and_doctors(login_user
      .favorite_group_ids)
  end

  def show
    render json: FavoriteGroup.with_hospitals_and_doctors(params[:id]).to_a
      .first
  end

  def create
    if favorite_group = FavoriteGroup.create_favorite_group(
      favorite_group_params, login_user.id)
      render json: FavoriteGroup.with_hospitals_and_doctors(favorite_group.id)
        .to_a.first
    else
      render json: {error: t("not_created")}, status: :unprocessable_entity
    end
  end

  def update
    if @favorite_group.update_attributes @favorite_group.update_params(
      favorite_group_params)
      render json: FavoriteGroup.with_hospitals_and_doctors(@favorite_group.id)
        .to_a.first
    else
      render json: {error: t("update_fail")}, status: :unprocessable_entity
    end
  end

  private
  def favorite_group_params
    params.require(:favorite_group_info).permit :name,
      favorite_group_doctors_attributes: [:hospital_id, doctor_ids: []]
  end

  def check_favorite_group
    @favorite_group = login_user.favorite_groups.find_by_id params[:id]
    return render json: {error: t("not_available")},
      status: :unprocessable_entity unless @favorite_group
  end
end
