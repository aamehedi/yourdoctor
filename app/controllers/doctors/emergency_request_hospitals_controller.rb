class Doctors::EmergencyRequestHospitalsController < ApplicationApiController
  skip_before_action :authenticate_user_from_identity
  before_action :check_emergency_request

  def update
    if @emergency_request.add_member_in_emergency_request(
      emergency_request_add_member_params)
      render json: @emergency_request,
        serializer: EmergencyRequestInfoSerializer, root: nil
    else
      render json: {error: t("update_fail")}, status: :unprocessable_entity
    end
  end

  private
  def emergency_request_add_member_params
    params.require(:emergency_request_info).permit :emergency_request_id,
      emergency_request_hospitals_attributes: [:hospital_id, doctor_ids: []]
  end

  def check_emergency_request
    @emergency_request = EmergencyRequest.find_by_id(
      emergency_request_add_member_params[:emergency_request_id])
    unless @emergency_request && @emergency_request.can_add_member?(
      login_user.id)
      return render json: {error: t("not_available")},
        status: :unprocessable_entity
    end
  end
end
