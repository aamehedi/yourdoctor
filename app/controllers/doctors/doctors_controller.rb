# -*- coding: utf-8 -*-
module Doctors
  class DoctorsController < ApplicationApiController
    skip_before_action :authenticate_user_from_token!
    #本人確認ができていなくても医師情報の一覧だけは取得出来る(詳細は取得で来ない)
    skip_before_action :authenticate_user_from_identity, only: [:index, :test, :resign, :license_check]
    before_action :set_doctor, only: [:show, :update]
    before_action :set_editor, only: [:show, :update]

    def index
      p params
      search(login_user)
    end

    def test
      search(Doctor.find(params[:id]))
    end

    def custom_search
      search(login_user)
    end

    def show
      logger.info("shhhh〜〜〜〜〜〜〜〜〜〜〜〜〜")      
      @user   = login_user
      if @user
        @doctor.check_relation(@user)
      end

      if params[:page_type]
        @doctor.paginate_info = { page_type: params[:page_type], page_num: params[:page_num], tagged_with: params[:tagged_with] }
      end

      if @doctor
        render json: @doctor, serializer: V1::DoctorSerializer, root: nil
      else
        render json: { error: t('user_show_error') }, status: :unprocessable_entity
      end
    end

    def update
      #会員でない医師情報だけ書き換えられる
      unless @doctor.is_member
        if @doctor.update_attributes(doctor_params)
          render json: @doctor, serializer: V1::DoctorSerializer, root: nil
        else
          render json: { error: t('user_update_error') }, status: :unprocessable_entity
        end
      else
        render json: { error: t('user_update_error') }, status: :unprocessable_entity
      end
    end

    def resign
      @dr = Doctor.find(params[:target_id])
      if @dr.id == login_user.id
        @dr.delay().destroy
        DoctorsMailer.delay().delete_account(@dr.name, @dr.email)
        render json: { status: "success" }
      else
        render json: { status: "error" }
      end
    end

    def wrong
      @dr = Doctor.find(params[:target_id])
      DoctorsMailer.delay().report_account(@dr.id, @dr.name, login_user.id, login_user.name)
      render json: { status: "success" }
    end

    def license_check
      @dr = Doctor.find(params[:target_id])
      if @dr.last_name.present? && @dr.first_name.present? && @dr.registration_number.present? && @dr.registration_date.present? && @dr.sex.present? && @dr.birthdate.present?
        DoctorsMailer.delay().license_check(@dr.id, @dr.name, @dr.registration_number, @dr.registration_date, @dr.sex, @dr.birthdate)
        render json: { status: "success" }
      else
        render json: { status: "error" }
      end
    end

    #グループ検索・      ・・大学/病院/学会のどれか一つを選択、それに所属する医師を抽出し、その中からフリーワードで検索。 検索者は医籍登録が確認できている必要がある。
    #お知り合いでは検索   ・・ユーザと同じ大学/病院/学会のいずれかに所属する医師を抽出し、その中からフリーワードで検索。リンク済みユーザは抽出しない。検索者は医籍登録が確認できている必要がある。
    #ランダムお知り合い表示・・ユーザの所属する大学/病院/学会のどれか一つを選択、そこから固定人数分医師を抽出。フリーワード無し。リンク済みユーザは抽出しない。検索者の医師登録は不要。
    #フリーワード検索・・・・・フリーワードで検索。は検索者は医籍登録が確認できている必要がある。
    #なお、上記における「フリーワード」の対象は大学名、病院名、診療科名、学会名、氏名である。
    private
    def search(user)
        limit  = 20
        page   = params[:page].to_i
        offset = page * limit

        word_org = params[:word] ? params[:word].force_encoding('UTF-8') : ""
        word_org = word_org.encode("UTF-16BE", "UTF-8", :invalid => :replace, :undef => :replace, :replace => '?').encode("UTF-8")

        if word_org.present?
          word = "%" + word_org + "%"
          word_sql = "coalesce(last_name, '') || coalesce(middle_name, '') || coalesce(first_name, '') LIKE '%s' OR departments.name LIKE '%s' OR hospitals.name LIKE '%s' OR universities.name LIKE '%s' OR societies.name LIKE '%s' " % [word.gsub(/[　|\s]/, ""), word, word, word, word]
        end

        #グループ検索-大学
        if params[:university]
          if login_user && login_user.is_official?
            if word_org.present?
              count   = Doctor.where.not(id: user.id).includes(:departments).includes(:hospitals).includes(:universities).includes(:societies).references(:departments).references(:hospitals).references(:universities).references(:societies)
                        .where("universities.id = ?", params[:university])
                        .where(word_sql).uniq.count
              doctors = Doctor.where.not(id: user.id).includes(:departments).includes(:hospitals).includes(:universities).includes(:societies).references(:departments).references(:hospitals).references(:universities).references(:societies)
                        .where("universities.id = ?", params[:university])
                        .where(word_sql).uniq.offset(offset).limit(limit)
            else
              count   = Doctor.where.not(id: user.id).joins(:universities).where("universities.id = ?", params[:university]).uniq.count
              doctors = Doctor.where.not(id: user.id).joins(:universities).where("universities.id = ?", params[:university]).uniq.offset(offset).limit(limit)
            end
            doctors.each {|doctor| doctor.check_link_state(user)}
            render json: doctors, each_serializer: V1::ThumbnailSerializer, meta: { page: page, limit: limit, total: count, type: "university", word: word_org }
          else
            render json: {error: t('api_error.unofficial')}, status: 401
          end
        #グループ検索-病院
        elsif params[:hospital]
          if login_user && login_user.is_official?
            if word_org.present?
              count   = Doctor.where.not(id: user.id).includes(:departments).includes(:hospitals).includes(:universities).includes(:societies).references(:departments).references(:hospitals).references(:universities).references(:societies)
                        .where("departments.hospital_id = ?", params[:hospital])
                        .where(word_sql).uniq.count
              doctors = Doctor.where.not(id: user.id).includes(:departments).includes(:hospitals).includes(:universities).includes(:societies).references(:departments).references(:hospitals).references(:universities).references(:societies)
                        .where("departments.hospital_id = ?", params[:hospital])
                        .where(word_sql).offset(offset).uniq.limit(limit)
            else
              count   = Doctor.where.not(id: user.id).joins(:departments).where("departments.hospital_id = ?", params[:hospital]).uniq.count
              doctors = Doctor.where.not(id: user.id).joins(:departments).where("departments.hospital_id = ?", params[:hospital]).uniq.offset(offset).limit(limit)
            end
            doctors.each {|doctor| doctor.check_link_state(user)}
            render json: doctors, each_serializer: V1::ThumbnailSerializer, meta: { page: page, limit: limit, total: count, type: "hospital", word: word_org}
          else
            render json: {error: t('api_error.unofficial')}, status: 401
          end
        #グループ検索-学会
        elsif params[:society]
          if login_user && login_user.is_official?
            if word_org.present?
              count   = Doctor.where.not(id: user.id).includes(:departments).includes(:hospitals).includes(:universities).includes(:societies).references(:departments).references(:hospitals).references(:universities).references(:societies)
                        .where("societies.id = ?", params[:society])
                        .where(word_sql).uniq.count
              doctors = Doctor.where.not(id: user.id).includes(:departments).includes(:hospitals).includes(:universities).includes(:societies).references(:departments).references(:hospitals).references(:universities).references(:societies)
                        .where("societies.id = ?", params[:society])
                        .where(word_sql).uniq.offset(offset).limit(limit)
            else
              count   = Doctor.where.not(id: user.id).joins(:societies).where("societies.id = ?", params[:society]).uniq.count
              doctors = Doctor.where.not(id: user.id).joins(:societies).where("societies.id = ?", params[:society]).uniq.offset(offset).limit(limit)
            end
            doctors.each {|doctor| doctor.check_link_state(user)}
            render json: doctors, each_serializer: V1::ThumbnailSerializer, meta: { page: page, limit: limit, total: count, type: "society", word: word_org}
          else
            render json: {error: t('api_error.unofficial')}, status: 401
          end
        #お知り合い検索
        elsif params[:mayknow]
=begin
          if params[:mayknow].present?
            doctor = Doctor.find(params[:mayknow])
          else
            doctor = user
          end
=end
          #今のところ他の医師公開ページを開いても自分のお知り合いを表示する
          doctor = user

          if login_user && login_user.is_official?
            links_table   = Link.arel_table
            user_table    = Doctor.arel_table.alias('user')
            doctors_table = Doctor.arel_table
            condition_follower =
              links_table
                .project(Arel.star)
                .join(user_table)
                .on(links_table[:follower_id].eq(user.id))
                .where(doctors_table[:id].eq(links_table[:followee_id]))
            condition_followee =
              links_table
                .project(Arel.star)
                .join(user_table)
                .on(links_table[:followee_id].eq(user.id))
                .where(doctors_table[:id].eq(links_table[:follower_id]))
            where_sql = Array.new
            where_sql.push ("universities.id         IN (" + doctor.universities.map{|university| university.id          }.join(",") + ")") if doctor.universities.length > 0
            where_sql.push ("departments.hospital_id IN (" + doctor.departments.map {|department| department.hospital_id }.join(",") + ")") if doctor.departments.length  > 0
            where_sql.push ("societies.id            IN (" + doctor.societies.map   {| societiy | societiy.id            }.join(",") + ")") if doctor.societies.length    > 0
            #条件によって分岐し過ぎ 要リファクタリング
            if where_sql.length > 0
              if word_org.present?
                count   = Doctor.where.not(id: doctor.id).includes(:departments).includes(:hospitals).includes(:universities).includes(:societies).includes(:followee_links).includes(:follower_links).references(:departments).references(:hospitals).references(:universities).references(:societies)
                                .where(condition_follower.exists.not)
                                .where(condition_followee.exists.not)
                                .where(where_sql.join(" OR "))
                                .where(word_sql)
                                .uniq.count
                doctors = Doctor.where.not(id: doctor.id).includes(:departments).includes(:hospitals).includes(:universities).includes(:societies).includes(:followee_links).includes(:follower_links).references(:departments).references(:hospitals).references(:universities).references(:societies)
                                .where(condition_follower.exists.not)
                                .where(condition_followee.exists.not)
                                .where(where_sql.join(" OR "))
                                .where(word_sql)
                                .uniq.offset(offset).limit(limit)
              else
                count   = Doctor.where.not(id: doctor.id).includes(:departments).includes(:hospitals).includes(:universities).includes(:societies).includes(:followee_links).includes(:follower_links).references(:departments).references(:hospitals).references(:universities).references(:societies)
                                .where(condition_follower.exists.not)
                                .where(condition_followee.exists.not)
                                .where(where_sql.join(" OR "))
                                .uniq.count
                doctors = Doctor.where.not(id: doctor.id).includes(:departments).includes(:hospitals).includes(:universities).includes(:societies).includes(:followee_links).includes(:follower_links).references(:departments).references(:hospitals).references(:universities).references(:societies)
                                .where(condition_follower.exists.not)
                                .where(condition_followee.exists.not)
                                .where(where_sql.join(" OR "))
                                .uniq.offset(offset).limit(limit)
              end
            else
              #病院、大学、学会のいずれにも所属しない人は関連なし
              count   = 0
              doctors = Doctor.where(id: 0)
            end
            render json: doctors, each_serializer: V1::ThumbnailSerializer, meta: { page: page, limit: limit, total: count, type: "mayknow", word: word_org}
          else
            render json: {error: t('api_error.unofficial')}, status: 401
          end
        #お知り合いランダム表示
        elsif params[:rmayknow]
          sample = 10
          #今のところ他の医師公開ページを開いても自分のお知り合いを表示する
          doctor = user

          #リンク済みのユーザは表示しない
          links_table   = Link.arel_table
          user_table    = Doctor.arel_table.alias('user')
          doctors_table = Doctor.arel_table
          condition_follower =
            links_table
              .project(Arel.star)
              .join(user_table)
              .on(links_table[:follower_id].eq(user.id))
              .where(doctors_table[:id].eq(links_table[:followee_id]))
          condition_followee =
            links_table
              .project(Arel.star)
              .join(user_table)
              .on(links_table[:followee_id].eq(user.id))
              .where(doctors_table[:id].eq(links_table[:follower_id]))

          #所属している病院、大学、学会の数を取得
          hNum = user.department_lists.count
          uNum = user.university_lists.count
          sNum = user.society_lists.count

          #病院、大学、学会の所属数をテーブル化
          categories = Array.new
          categories.push(:hospital)   if user.department_lists.count > 0
          categories.push(:university) if user.university_lists.count > 0
          categories.push(:society)    if user.society_lists.count    > 0

          if categories.length > 0
            #カテゴリ別処理
            case categories.sample
            when :hospital
              doctors = Doctor.where.not(id: doctor.id).includes(:department_lists).includes(:departments).includes(:followee_links).includes(:follower_links).references(:department_lists).references(:departments)
                              .where(condition_follower.exists.not)
                              .where(condition_followee.exists.not)
                              .where("departments.hospital_id = ?", doctor.departments.sample(1).first.hospital_id)
                              .uniq.sample(sample)
            when :university
              doctors = Doctor.where.not(id: doctor.id).includes(:university_lists).includes(:followee_links).includes(:follower_links).references(:university_lists)
                              .where(condition_follower.exists.not)
                              .where(condition_followee.exists.not)
                              .where("university_lists.university_id = ?", doctor.university_lists.sample(1).first.university_id)
                              .uniq.sample(sample)
            when :society
              doctors = Doctor.where.not(id: doctor.id).includes(:society_lists).includes(:followee_links).includes(:follower_links).references(:society_lists)
                              .where(condition_follower.exists.not)
                              .where(condition_followee.exists.not)
                              .where("society_lists.society_id = ?", doctor.society_lists.sample(1).first.society_id)
                              .uniq.sample(sample)
            else
              doctors = Doctor.where(id: 0)
            end
          else
            #病院、大学、学会のいずれにも所属しない人はお知り合いなし
            doctors = Doctor.where(id: 0)
          end
          doctors.each {|d| d.check_link_state(user)}
          render json: doctors, each_serializer: V1::ThumbnailSerializer, meta: {sample: sample, type: "rmayknow" }
        #カスタム検索
        elsif params[:custom]
          logger.info("カスタム〜〜〜〜〜〜〜〜〜〜〜〜〜")
          queries = Array.new

          if params[:sex]
            queries.push("sex = " + params[:sex])
          end

          if params[:medical_group] == "1"
            queries.push("hospitals.medical_group_id is not null")
          elsif params[:medical_group] == "0"
            queries.push("hospitals.id is not null AND hospitals.medical_group_id is null")
          end

          if params[:address]
            params[:address].each do |address|

              address_txt = address[:name]
              Prefecture.all.each do |pref|
                short_pref_name = pref.name.gsub("県", "").gsub("都", "") #神奈川県→神奈川
                if address_txt.match(short_pref_name)
                  queries.push("prefectures.id      = " + pref.id.to_s)
                  address_txt = address_txt.gsub(pref.name, "").gsub(short_pref_name, "") #住所から県名を削除
                  break
                end
              end

              if address_txt.length > 0
                queries.push("hospitals.address LIKE '%" + address_txt + "%'")
              end
            end
          end


          if params[:areas]
            queries2 = Array.new
            params[:areas].each do |area|
              if area[:prefecture_id]
                queries2.push("prefectures.id      = " + area[:prefecture_id])
              elsif area[:id]
                queries2.push("prefectures.area_id = " + area[:id]           )
              end
            end
            queries.push(queries2.join(" OR "))
          end

          if params[:doctor_names]
            queries2 = Array.new
            params[:doctor_names].each do |doctor_name|
              queries3 = Array.new
              if doctor_name[:last_name]
                queries3.push("doctors.last_name  LIKE '%" + doctor_name[:last_name] + "%'")
              end
              if doctor_name[:first_name]
                queries3.push("doctors.first_name LIKE '%" + doctor_name[:first_name]+ "%'")
              end
              queries2.push(queries3.join(" AND "))
            end
            queries.push(queries2.join(" OR "))
          end

          if params[:universities]
            queries2 = Array.new
            params[:universities].each do |university|
              queries3 = Array.new
              if university[:id]
                queries3.push("universities.id  = " + university[:id].to_s)
              elsif university[:name]
                queries3.push("universities.name  LIKE '%" + university[:name] + "%'")
              end
              if university[:graduation_year]
                queries3.push("university_lists.graduation_year = " + university[:graduation_year])
              end
              queries2.push(queries3.join(" AND "))
            end
            queries.push(queries2.join(" OR "))
          end

          if params[:societies]
            queries2 = Array.new
            params[:societies].each do |society|
              if society[:id]
                queries2.push("societies.id  = " + society[:id].to_s)
              elsif society[:name]
                queries2.push("societies.name  LIKE '%" + society[:name] + "%'")
              end
            end
            queries.push(queries2.join(" OR "))
          end

          if params[:hospitals]
            queries2 = Array.new
            params[:hospitals].each do |hospital|
              if hospital[:id]
                queries2.push("hospitals.id  = " + hospital[:id].to_s)
              elsif hospital[:name]
                queries2.push("hospitals.name  LIKE '%" + hospital[:name] + "%'")
              end
            end
            queries.push(queries2.join(" OR "))
          end

          if params[:departments]
            queries2 = Array.new
            params[:departments].each do |department|
              queries2.push("departments.name  LIKE '%" + department + "%'")
            end
            queries.push(queries2.join(" OR "))
          end

          if params[:skills]
            queries2 = Array.new
            params[:skills].each do |skill_icd10|
              #IDありの場合は合致するID以下の階層のスキルが対象
              if skill_icd10[:id]
                icd10s = Icd10.find_by(id: skill_icd10[:id])
                icd10_ids = get_nest_icd10_ids(icd10s)
                queries2.push("icd10s.id IN (" +  icd10_ids.join(",") + ")")
              #IDなしの場合はワードで検索
              elsif skill_icd10[:name]
                queries2.push("icd10s.name  LIKE '%" + skill_icd10[:name] + "%'")
              end
            end
            queries.push(queries2.join(" OR "))
          end
          count   = Doctor.where.not(id: user.id).includes(:departments).includes(:hospitals).includes(:prefectures).includes(:university_lists).includes(:universities).includes(:societies).includes(:skills).includes(:icd10s).references(:departments).references(:hospitals).references(:prefectures).references(:university_lists).references(:universities).references(:societies).references(:skills).references(:icd10s)
                          .where(queries.join(" AND "))
                          .uniq.count
          doctors = Doctor.where.not(id: user.id).includes(:departments).includes(:hospitals).includes(:prefectures).includes(:university_lists).includes(:universities).includes(:societies).includes(:skills).includes(:icd10s).references(:departments).references(:hospitals).references(:prefectures).references(:university_lists).references(:universities).references(:societies).references(:skills).references(:icd10s)
                          .where(queries.join(" AND "))
                          .uniq.offset(offset).limit(limit)
          doctors.each {|doctor| doctor.check_link_state(user)}
          render json: doctors, each_serializer: V1::ThumbnailSerializer, meta: { page: page, limit: limit, total: count, type: "custom"}
        #フリーワード検索
        elsif word_org
          logger.info("フリーワード")
          word = "%" + word_org + "%"
          word_sql = "coalesce(last_name, '') || coalesce(middle_name, '') || coalesce(first_name, '') LIKE '%s' OR departments.name LIKE '%s' OR hospitals.name LIKE '%s' OR universities.name LIKE '%s' OR societies.name LIKE '%s' OR icd10s.name LIKE '%s' " % [word.gsub(/[　|\s]/, ""), word, word, word, word, word]
          if login_user && login_user.is_official?
            if word_org.present?
              count   = Doctor.select("id").where.not(id: user.id).includes(:departments).includes(:hospitals).includes(:university_lists).includes(:universities).includes(:societies).includes(:skills).includes(:icd10s).references(:departments).references(:hospitals).references(:university_lists).references(:universities).references(:societies).references(:skills).references(:icd10s)
                        .where(word_sql).uniq.count
              doctors = Doctor.where.not(id: user.id).includes(:departments).includes(:hospitals).includes(:university_lists).includes(:universities).includes(:societies).includes(:skills).includes(:icd10s).references(:departments).references(:hospitals).references(:university_lists).references(:universities).references(:societies).references(:skills).references(:icd10s)
                        .where(word_sql).uniq.offset(offset).limit(limit)
              doctors.each {|doctor| doctor.check_link_state(user)}
              render json: doctors, each_serializer: V1::ThumbnailSerializer, meta: { page: page, limit: limit, total: count, type: "word", word: word_org}
            else
              render json: {doctors: Array.new, meta: {page: 0, limit: 0, total: 0}}
            end
          else
            render json: {error: t('api_error.unofficial')}, status: 401
          end
        end
      end

      def get_nest_icd10_ids(parent)
        result = Array.new
        result.push(parent.id)
        parent.children.each do |child|
          result.concat(get_nest_icd10_ids(child))
        end
        result
      end

      def set_doctor
        @doctor = Doctor.find(params[:id])
      end

      def set_editor
        Doctor.editor = login_user
      end

      def doctor_params
        params.require(:doctor).permit(:first_name, :last_name, :middle_name, :first_name_reading, :last_name_reading, :middle_name_reading, :last_name_initial, :first_name_initial, :middle_name_initial, :profession, :result, :url, :history, :top_or_next, :user, :update_user, :birthdate, :sex, :comment,
                                      university_lists_attributes: [:id, :graduation_year, :_destroy,
                                        university_attributes: [:id, :name]
                                      ],
                                      society_lists_attributes: [:id, :name, :_destroy,
                                        society_attributes: [:id, :name],
                                        position_lists_attributes:[:id, :society_list_id, :society_position_id, :_destroy,
                                          position_attributes: [:id, :name, :society_id]
                                        ]
                                      ],
                                      skills_attributes: [:id, :icd10_id, :comment, :_destroy],
                                      department_lists_attributes: [:id, :_destroy, :term_start, :term_end, :active, :department_id, :part_time, :index,
                                        department_attributes: [:id, :_destroy, :name, :hospital_id,
                                          hospital_attributes:[:id, :name, :area_id, :prefecture_id]
                                        ],
                                        position_lists_attributes: [:id, :department_list_id, :department_position_id, :_destroy,
                                          position_attributes: [:id, :name, :department_id]
                                        ]
                                      ],
                                      awards_attributes: [:id, :_destroy, :title, :year, :description],
                                      monographs_attributes: [:id, :_destroy,:title,:year,:description,:url,:writers,:media,:page,:published,:published_type,:tag_list]
                                    )
      end
  end
end
