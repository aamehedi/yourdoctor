module Doctors
  class AreasController < ApplicationApiController
    skip_before_action :authenticate_user_from_token!
    skip_before_action :authenticate_user_from_identity
   
  	def index
  	  areas = Area.all
  	  render json: areas, each_serializer: V1::NameAndIDSerializer, root: nil
  	end

  	def show
  	  prefectures = Prefecture.where(area_id: params[:id])
  	  render json: prefectures, each_serializer: V1::NameAndIDSerializer, root: "prefectures"
  	end

  end
end
