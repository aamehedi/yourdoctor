class Doctors::EmergencyDoctorsController < ApplicationApiController
  skip_before_action :authenticate_user_from_identity

  def update
    if login_user.update_emergency_doctor doctor_update_params
      render json: EmergencyDoctorSerializer.new(login_user).doctor_info
    else
      render json: {error: t("update_fail")}, status: :unprocessable_entity
    end
  end

  private
  def doctor_update_params
    params.require(:doctor).permit :latitude, :longitude, :emergency,
      :rest_mode, :rest_time_start, :rest_time_end
  end
end
