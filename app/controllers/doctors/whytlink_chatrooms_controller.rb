class Doctors::WhytlinkChatroomsController < ApplicationApiController
  skip_before_action :authenticate_user_from_identity
  before_action :check_authorization, only: [:show, :update]

  def index
    if params[:group_search].nil?
      get_filtered_chatrooms
    else
      render json: WhytlinkGroupChatInfo.ransack(group_search_params.to_h)
        .result
    end
  end

  def show
    render json: @chatroom, current_doctor_id: @user.id
  end

  def create
    chatroom = WhytlinkChatroom.find_or_create_chatroom(chatroom_params,
      login_user.id)
    return render json: chatroom, current_doctor_id: @user.id if chatroom
    render json: {error: t(".access_error")}, status: :unprocessable_entry
  end

  def update
    if @chatroom.update_attributes chatroom_params.except(:chatroom_type)
      return render json: @chatroom, current_doctor_id: @user.id
    else
      render json: {error: t(".error")}, status: :unprocessable_entry
    end
  end

  private
  def chatroom_params
    params.require(:whytlink_chatroom).permit(:chatroom_type,
      whytlink_doctor_chatrooms_attributes: [:doctor_id, :admin],
      whytlink_group_chat_info_attributes: [:name, :avatar, :affliation_type,
      :affliation_id, :opened])
  end

  def chatroom_ids_chatroom_params
    params.permit :chatroom_type, :emergency
  end

  def check_authorization
    @chatroom = WhytlinkChatroom.find_by_id params[:id]
    unauthorized_doctor unless WhytlinkDoctorChatroom.exists?(
      whytlink_chatroom_id: params[:id], doctor_id: login_user.id, admin: true)
  end

  def group_search_params
    params.require(:group_search).permit :name_cont
  end

  def get_filtered_chatrooms
    if chatroom_ids_chatroom_params[:emergency]
      render json: WhytlinkChatroom.find_all_non_emergency_chatrooms(login_user
        .id, chatroom_ids_chatroom_params[:chatroom_type]),
        current_doctor_id: @user.id
    else
      render json: WhytlinkChatroom.find_all_chatrooms(login_user.id,
        chatroom_ids_chatroom_params[:chatroom_type]),
        current_doctor_id: @user.id,
        each_serializer: WhytlinkChatroomInfoSerializer
    end
  end
end
