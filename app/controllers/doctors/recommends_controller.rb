module Doctors
  class RecommendsController < ApplicationApiController
    #本人確認ができていない場合は推薦は使えない
    #skip_before_action :authenticate_user_from_token!
    #skip_before_action :authenticate_user_from_identity

  	def create
      recommend = Recommend.new(doctor_id: @user.id, skill_id: params[:skill_id], point: params[:point])
      if recommend.save
        render json: {success: true, skill_id: params[:skill_id], recommended: true, total_point: recommend.skill.total_point }
      else
        render json: { error: recommend.errors }
      end
  	end

  	def destroy
      recommend = Recommend.where(doctor_id: @user.id).where(skill_id: params[:skill_id]).first
      skill = recommend.skill
      if recommend.present?
        if recommend.destroy
          render json: {success: true, skill_id: params[:skill_id], recommended: false, total_point: skill.total_point}
        else
          render json: { error: recommend.errors }
        end
      else
        render json: { error: "not found" }
      end
  	end
  end
end
