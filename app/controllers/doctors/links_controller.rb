module Doctors
  class LinksController < ApplicationApiController
    #skip_before_action :authenticate_user_from_token!
    skip_before_action :authenticate_user_from_identity

    def index
      if @user
        if params[:mode] == "linking"
          #follower/followee区別なく全て取得
          render json: @user.link_doctors(params[:page].to_i, 20), each_serializer: V1::ThumbnailSerializer, root: nil
        elsif params[:mode] == "request"
          #follower/followeeを個別に取得
          render json: @user, serializer: V1::LinkRequestSerializer, root: nil
        end
      else
        render json: { error: t('user_show_error') }, status: :unprocessable_entity
      end
    end

  	def create
      #createはリンク申請者(follower)が行う
      link = Link.new(follower_id: @user.id, followee_id: params[:doctor_id], state: Link.states[:request])
      if link.save
        render json: {success: true, doctor_id: params[:doctor_id], link_state: link.followee.check_link_state(@user)}
      else
        render json: { error: link.errors }
      end
  	end

    def update
      #update(許可/拒否)は申請を受けた側が行う
      link = Link.where(followee_id: @user.id).where(follower_id: params[:doctor_id]).first
      if link
        case params[:answer]
        when "approval" then link.state = :linking
        when "refusal"  then link.state = :refusal
        when "request"  then link.state = :request #拒否を解除して申請状態に戻したいとき
        end
        if link.save
          render json: {success: true, doctor_id: params[:doctor_id], link_state: link.follower.check_link_state(@user), requested_count: @user.requested_count}
        else
          render json: { error: link.errors }          
        end
      else
        render json: { error:  "not found"}
      end
    end

  	def destroy
      #リンク解除は申請者、申請を受けた側どちらでも可能
      link = Link.where(followee_id: @user.id).where(follower_id: params[:doctor_id]).first
      if link.nil?
        link = Link.where(follower_id: @user.id).where(followee_id: params[:doctor_id]).first
      end
      if link.present?
        if link.destroy
          render json: {success: true, doctor_id: params[:doctor_id], link_state: ""}
        else
          render json: { error: link.errors }
        end
      else
        render json: { error: "not found" }
      end
  	end
  end
end
