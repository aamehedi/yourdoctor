module Doctors
  class SocietiesController < ApplicationApiController
    skip_before_action :authenticate_user_from_token!
    skip_before_action :authenticate_user_from_identity

  	def index
      if params[:name].present?
        society = Society.where("name LIKE ?", '%' + params[:name] + '%').limit(20)
      else
        society = Society.all
      end
  	  render json: society, each_serializer: V1::NameAndIDSerializer, root: nil
  	end

  	def show
      if params[:name].present?
        society_position = SocietyPosition.where(society_id: params[:id]).where("name LIKE ?", '%' + params[:name] + '%').limit(20)
      else
        society_position = SocietyPosition.where(society_id: params[:id]).limit(20)
      end

  	  render json: society_position, each_serializer: V1::NameAndIDSerializer, root: "society_positions"
  	end
  end
end
