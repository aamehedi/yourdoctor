class Doctors::SpecialtiesController < ApplicationApiController
  skip_before_action :authenticate_user_from_identity

  def index
    render json: Specialty.all, each_serializer: SpecialtyDoctorSerializer,
      current_doctor_specialties: login_user.specialty_ids
  end
end
