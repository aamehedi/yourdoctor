module Doctors
  class TimelinesController < ApplicationApiController
    skip_before_action :authenticate_user_from_token!
    skip_before_action :authenticate_user_from_identity

    def index
      if params[:doctor_id]
        render_timelines(Doctor.find(params[:doctor_id]), "individual", params[:result_count])
      else
        render_timelines(login_user, params[:filter])
      end
    end

    def destroy
      t = Timeline.find(params[:id])
      if(t.editor_id == login_user.id)
        t.destroy
      end

      render_timelines(login_user, params[:filter])
    end

    def render_timelines(doctor, type, limit=20)
      limit = 20 if !limit
      limit = limit.to_i

      doctor_ids = login_user.link_doctors.map{|doctor| doctor.id}
      doctor_ids.push(login_user.id)
      filters = Array.new
      #Read Levelによるフィルタ
      if type == "all"
        if login_user.is_staff
        else
          filters.push("read_level = #{Timeline.read_levels[:all_drs]} OR (read_level = #{Timeline.read_levels[:linked]} AND (editor_id IN (#{doctor_ids.join(',')}) OR doctor_id IN (#{doctor_ids.join(',')})))")
        end
      elsif type == "linked"
        if login_user.is_staff
        else
          filters.push("read_level < #{Timeline.read_levels[:staff]}")
        end
        filters.push("editor_id IN (#{doctor_ids.join(',')}) OR doctor_id IN (#{doctor_ids.join(',')})")
      elsif type == "individual"
        if login_user.is_staff
        else
          filters.push("read_level = #{Timeline.read_levels[:all_drs]} OR (read_level = #{Timeline.read_levels[:linked]} AND (editor_id IN (#{doctor_ids.join(',')}) OR doctor_id IN (#{doctor_ids.join(',')})))")
        end
        filters.push("editor_id = #{doctor.id} OR doctor_id = #{doctor.id}")
      else # 自分だけの表示
        if login_user.is_staff
        else
          filters.push("read_level < #{Timeline.read_levels[:staff]}")
        end
        filters.push("editor_id = #{login_user.id} OR doctor_id = #{login_user.id}")
      end

      #"もっと読む" の場合のフィルタ
      filters.push("id < " + params[:last_id]) if params[:last_id]

      #移籍チェックがされていない場合は東京を非表示
      filters.push("model_type != 'Post'") unless login_user.identity_verification == 1

      filters = filters.map{|filter| "(" + filter + ")"}
      timelines = Timeline.where("action != 'destroy'").where(filters.join(" AND ")).order("updated_at DESC").limit(limit)

      state = params[:last_id] ? "more" : "first"

      #関連性を洗い出す
      user = login_user
      user_hospital_ids   = user.department_lists.map{|item| item.department.hospital_id   }
      user_society_ids    = user.society_lists.map   {|item| item.society_id    }
      user_university_ids = user.university_lists.map{|item| item.university_id }
      user_skill_ids      = user.skills.map          {|item| item.icd10.id      }

      timelines.each do | timeline |
        #自分のリンクしてるドクター
        if doctor_ids.index(timeline.editor_id) || doctor_ids.index(timeline.doctor_id)
          timeline.relation_type = :linked
        #自分の関連しているドクター
        elsif user_hospital_ids.index(timeline.editor_hospital_id)   || \
           user_university_ids.index(timeline.editor_university_id)  || \
           user_hospital_ids.index(timeline.doctor_hospital_id)      || \
           user_university_ids.index(timeline.doctor_university_id)  || \
           timeline.model_type == "Icd10" && user_skill_ids.index(timeline.model_id)
          timeline.relation_type = :related
        #全く関係がない
        else
          timeline.relation_type = :none
        end
      end

      ended = timelines.count < limit

      #事務局投稿をprepend
      if (type == "all" && state == "first")
        staff_posts = StaffPost.where("start_time < ?", Time.now).where("end_time > ?", Time.now)
        staff_posts = staff_posts.map{|x| x.convert_to_timeline_format}
        timelines = staff_posts.concat(timelines)
      end

      render json: timelines, each_serializer: V1::TimelineSerializer, root: nil, meta: { state: state, ended: ended }
    end
  end
end
