module Doctors
  class HospitalsController < ApplicationApiController
    skip_before_action :authenticate_user_from_token!
    skip_before_action :authenticate_user_from_identity

    #病院一覧を取得する
  	def index
      if params[:prefecture].present?
        hospitals = Hospital.where(prefecture_id: params[:prefecture])
          .where("name LIKE ?", '%' + params[:name].to_s + '%').limit(20)
      elsif params[:radius]
        return render json: login_user.detect_nearby_emergency_hospitals(
          hospital_params[:longitude], hospital_params[:latitude],
          hospital_params[:radius]),
          each_serializer: EmergencyHospitalPositionSerializer
      elsif params[:longitude]
        radius, hospitals = login_user.detect_nearby_hospitals_with_radius(
          hospital_params[:longitude], hospital_params[:latitude])
        return render json: {
          radius: radius,
          hospitals: ActiveModel::ArraySerializer.new(hospitals,
            each_serializer: EmergencyHospitalPositionSerializer)
        }
      else
        hospitals = Hospital.where("name LIKE ?",
          '%' + params[:name].to_s + '%').limit(20)
      end
  	  render json: hospitals, each_serializer: V1::NameAndIDSerializer,
        root: nil
  	end

    #指定IDに従属する診療科一覧を取得する
  	def show
      if params[:name].present?
        departments = Department.where(hospital_id: params[:id])
          .where("name LIKE ?", '%' + params[:name] + '%').limit(20)
      else
        departments = Department.where(hospital_id: params[:id]).limit(20)
      end
  	  render json: departments, each_serializer: V1::NameAndIDSerializer,
        root: "departments"
  	end

    private
    def hospital_params
      params.permit :longitude, :latitude, :radius
    end
  end
end
