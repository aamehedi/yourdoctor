class Doctors::DoctorSpecialtiesController < ApplicationApiController
  skip_before_action :authenticate_user_from_identity

  def create
    login_user.specialty_ids = doctor_specialty_parmas[:specialty_ids]
    render json: Specialty.all, each_serializer: SpecialtyDoctorSerializer,
      root: Settings.specialties, current_doctor_specialties: login_user
      .specialty_ids
  end

  private
  def doctor_specialty_parmas
    params.require(:doctor_specialty_info).permit specialty_ids: []
  end
end
