class Doctors::DoctorFeedbacksController < ApplicationApiController
  skip_before_action :authenticate_user_from_identity

  def index
    if DoctorFeedback.check_emergency_request params[:emergency_request_id]
      render json: DoctorFeedback.emergency_request_feedbacks(login_user,
        params[:emergency_request_id])
    else
      render json: {error: t("not_available")}, status: :unprocessable_entity
    end
  end

  def update
    doctor_feedback = login_user.doctor_feedbacks.find_by_id params[:id]
    if doctor_feedback &&
       doctor_feedback.update_doctor_feedback(doctor_feedback_params)
      render json: doctor_feedback
    else
      render json: {error: t("update_fail")}, status: :unprocessable_entity
    end
  end

  private
  def doctor_feedback_params
    params.require(:doctor_feedback_info).permit :status
  end
end
