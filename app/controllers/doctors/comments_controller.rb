module Doctors
  class CommentsController < ApplicationApiController
    require "no_staff_create"
    include NoStaffCreate
    #skip_before_action :authenticate_user_from_token!
    #skip_before_action :authenticate_user_from_identity

    def show
      logger.info(params[:id])
      @Post = Post.find(params[:id])
      render json: @Post.comments.map{|s| s.doctor }, each_serializer: V1::ThumbnailSerializer, root: nil
    end

  	def create
      comment = Comment.new(doctor_id: login_user.id, post_id: params[:post_id], content: params[:content]);
      if comment.save
        render json: {success: true, comment: V1::CommentsSerializer.new(comment), timeline_id: comment.post.timeline.id }
      else
        render json: { error: comment.errors }
      end
  	end

    def del
      comment = Comment.find(params[:comment_id])

      if comment.present? and (comment.doctor_id == login_user.id || comment.post.doctor_id == login_user.id)
        success = { timeline_id: comment.post.timeline.id, comment_id: comment.id }
        if comment.destroy
          render json: success
        else
          render json: { error: comment.errors }
        end
      else
        render json: { error: "not found" }
      end
    end
  end
end
