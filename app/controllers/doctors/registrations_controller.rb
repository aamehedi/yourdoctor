module Doctors
  class RegistrationsController < ApplicationApiController
    #登録処理はログインも本人確認もしてない段階なので全部使える
    skip_before_action :authenticate_user_from_token!
    skip_before_action :authenticate_user_from_identity
    
    #このアクションでは医師ユーザを作成する。
    #但しこの段階ではメールアドレス確認の為の仮登録テーブルのレコードが生成される。
    def create
      if params[:invitate]
        @temp_user      = TemporaryDoctor.find_or_create_by(email: user_params_email[:email], doctor_id: login_user.id)
      else
        @temp_user      = TemporaryDoctor.find_or_create_by(email: user_params_email[:email])
      end
      @temp_user.attributes = user_params_email
      @temp_user.uuid = SecureRandom.uuid
      if params[:invitate] #invitateパラメータが有るときは招待とみなす
        @temp_user.inviter            = login_user 
        @temp_user.estimate_doctor_id = params[:doctor_id]
      end
      if @temp_user.save
        #招待
        if @temp_user.inviter #招待者が確認できれば招待メールを送る
          DoctorsMailer.invitation(@temp_user, @temp_user.inviter, params[:comment]).deliver_later(in: 1.minutes)
        #サインアップ
        else
          DoctorsMailer.email_confirmation(@temp_user).deliver_later(in: 1.minutes)
        end
        render json: {success: true, email: @temp_user.email}, root: nil
      else
        render json: { error: @temp_user.errors }, status: :unprocessable_entity
      end
    end

    #このアクションでは仮登録情報を返すが、メールアドレスの確認として使用される。
    #指定したUUIDで仮登録ユーザが見つかればsuccessを返す。
    def show
      @temp_user = TemporaryDoctor.find_by_uuid(params[:uuid])
      if @temp_user
        render json: {success: true, email: @temp_user.email, first_name: @temp_user.first_name, last_name: @temp_user.last_name,  password_min_length: 8, invitate: @temp_user.inviter.present?}, root: nil
      else
        render json: {error: t('uuid_error') }, status: :unprocessable_entity
      end
    end

    #このアクションでは医師情報の登録を行う。
    #医師登録は数段階に分けられるUIのため、バックエンド側もSTEPで分けられている。
    #最後のSTEPで仮登録レコードから本登録レコードに情報が移し替えられる。
    def update
      #UUID確認
      step = params[:step].to_i
      @temp_user = TemporaryDoctor.find_by_uuid(params[:uuid])
      unless @temp_user
        render json: {error: t('uuid_error') }, status: :unprocessable_entity
        return
      end

      #登録
      case step
      #STEP1 パスワードと医籍番号の登録(招待時は名前もあり)
      when 1 then
        @temp_user.attributes = user_params_base_info
        @same_name_doctors = Doctor.where(:email => nil).where(:first_name => @temp_user.first_name).where(:last_name => @temp_user.last_name)
        #同姓同名の医師がいる場合はSTEP2へ
        if @same_name_doctors.count > 0
          if @temp_user.save
            #Go to STEP2
            render json: {success: true, next_step: step + 1, same_name_doctors: @same_name_doctors.to_json(:only => [:id, :first_name, :last_name], :include => [{:departments => {:only => :name, :include => {:hospital => {:only => :name}}}}, :university_lists => {:only => :graduation_year, :include => {:university => {:only => :name}}}])}, root: nil
          else
            #Retry STEP1
            render json: {error: t('update_error')}, status: :unprocessable_entity
          end
        #同姓同名がいない場合は登録完了
        else
          #仮登録レコード->本登録レコードに移し替える
          @user = Doctor.new
          make_user_and_respond(@temp_user, @user)
        end
      #STEP2 同姓同名医師のデータを統合  
      when 2 then 
        doctorsIDList = params[:user][:doctors]
        #複数ある場合は0番目を基準にマージ
        @user = nil
        #統合
        if doctorsIDList
          baseDoctorsID = doctorsIDList.shift
          @user = Doctor.find(baseDoctorsID)
          doctorsIDList.each do |id|
            @doctor = Doctor.find(id)
            @user.merge(@doctor)
          end
        #統合しない(同姓同名の医師を選択していない)
        else
          @user = Doctor.new
        end
        make_user_and_respond(@temp_user, @user)
      #STEP3 今のところは無い。
      when 3 then
      else
      end
    end

    private

    def user_params_email
      params.require(:user).permit(:email, :first_name, :last_name, :uuid)
    end

    def user_params_base_info
      params.require(:user).permit(:password, :password_confirmation, :registration_number, :registration_date, :first_name, :last_name, :sex, :birthdate)
    end

    #仮ユーザの情報を本ユーザに移し替えて保存->render応答
    def make_user_and_respond(temp_user, user)
      user.email                 = temp_user.email
      user.first_name            = temp_user.first_name
      user.last_name             = temp_user.last_name
      user.sex                   = temp_user.sex
      user.registration_number   = temp_user.registration_number
      user.registration_date     = temp_user.registration_date
      user.birthdate             = temp_user.birthdate
      user.password              = temp_user.password
      user.password_confirmation = temp_user.password
      if user.save
        user.update_access_token!
        user.no_timeline = true
        user.save
        #もし招聘者がいるならリンクを貼る
        if temp_user.inviter
          Link.create(followee_id: user.id, follower_id: temp_user.inviter.id, state: Link.states[:linking])
        end             
        TemporaryDoctor.destroy_all(email: @temp_user.email)
        render json: user, serializer: V1::SessionSerializer, root: nil
      else
        render json: { error: t('user_create_error') }, status: :unprocessable_entity
      end 
    end
  end
end

