module Doctors
  class UsersController < ApplicationApiController
    skip_before_action :authenticate_user_from_token!, only: [:show]
    skip_before_action :authenticate_user_from_identity
    before_action :set_editor, only: [:update]

    def index
      if @user
        if params[:page_type]
          @user.paginate_info = { page_type: params[:page_type], page_num: params[:page_num], tagged_with: params[:tagged_with] }
        end
        if !@user.current_sign_in_to_app_at || @user.current_sign_in_to_app_at < Date.today
          @user.no_timeline = true
          @user.update(current_sign_in_to_app_at: Time.now)
        end
        render json: @user, serializer: V1::UserSerializer, root: nil
      else
        render json: { error: t('user_show_error') }, status: :unprocessable_entity
      end
      #@user.to_json(:only => [:id, :first_name, :last_name], :include => [{:departments => {:only => :name, :include => {:hospital => {:only => :name}}}}, :university => {:only => :name}]
    end

    def update
      if @user.update_attributes(user_params)
        render json: @user, serializer: V1::UserSerializer, root: nil
      else
        render json: { error: t('user_show_error') }, status: :unprocessable_entity
      end
    end

    private

      def user_params
        params.require(:user).permit(:registration_number, :email, :registration_date, :first_name, :remove_avatar_image, :remove_cover_image, :last_name, :middle_name, :first_name_reading, :last_name_reading, :middle_name_reading, :last_name_initial, :first_name_initial, :middle_name_initial, :profession, :result, :url, :history, :top_or_next, :user, :update_user, :birthdate, :sex, :comment,
                                      university_lists_attributes: [:id, :graduation_year, :_destroy,
                                        university_attributes: [:id, :name]
                                      ],
                                      society_lists_attributes: [:id, :name, :_destroy,
                                        society_attributes: [:id, :name],
                                        position_lists_attributes:[:id, :society_list_id, :society_position_id, :_destroy,
                                          position_attributes: [:id, :name, :society_id]
                                        ]
                                      ],
                                      skills_attributes: [:id, :icd10_id, :comment, :_destroy],
                                      department_lists_attributes: [:id, :_destroy, :term_start, :term_end, :active, :department_id, :part_time, :index,
                                        department_attributes: [:id, :_destroy, :name, :hospital_id,
                                          hospital_attributes:[:id, :name, :area_id, :prefecture_id]
                                        ],
                                        position_lists_attributes: [:id, :department_list_id, :department_position_id, :_destroy,
                                          position_attributes: [:id, :name, :department_id]
                                        ]
                                      ],
                                      awards_attributes: [:id, :_destroy, :title, :year, :description],
                                      monographs_attributes: [:id, :_destroy, :title, :year, :description, :url, :writers, :media,:page,:published,:published_type,:tag_list]
                                    )
      end

      def set_editor
        Doctor.editor = login_user
      end
  end
end
