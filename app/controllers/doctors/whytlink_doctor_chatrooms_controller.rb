class Doctors::WhytlinkDoctorChatroomsController < ApplicationApiController
  skip_before_action :authenticate_user_from_identity
  before_action :validate_doctor

  def destroy
    @whytlink_doctor_chatroom.destroy
    render json: {
      success: t("successfully_leaved")
    }
  end

  private
  def validate_doctor
    @whytlink_doctor_chatroom = WhytlinkDoctorChatroom.find_by(id: params[:id],
      doctor_id: login_user.id)
    if @whytlink_doctor_chatroom.nil?
      return render json: {
        failed: t("leave_failed")
      }
    end
  end
end
