module Doctors
  class PostsController < ApplicationApiController
    require "no_staff_create"
    include NoStaffCreate
    skip_before_action :authenticate_user_from_token!
    skip_before_action :authenticate_user_from_identity
    before_action :set_editor, only: [:create]


    def new
      #投稿テキストボックスに表示するサジェスションワードを返す
      render json: Suggestion.offset(rand(Suggestion.count)).limit(1).first, serializer: V1::TextAndIDSerializer, :root => 'suggestion'
    end

    def create
      post = Post.new(post_params)
      post.doctor_id = login_user.id
      post.taggings.each do |t|
        t.context = 'manual'
      end
      if post.save
        #保存が終わったら投稿タイムラインを返す
        post.timeline.relation_type = :linked
        render json: post.timeline, serializer: V1::TimelineSerializer
      else
        render json: { error: post.errors }
      end
    end

    private
      def post_params
        params.require(:post).permit(:doctor_id, :memo, :read_level, :placeholder_id, :suggestion_id, :imgs => [], taggings_attributes: [:tag_id, :tag_type])
      end
      def set_editor
        Doctor.editor = login_user
      end

  end
end
