# -*- coding: utf-8 -*-
require 'uri'
require 'open-uri'
require 'nokogiri'

module Doctors
  class PreMonographsController < ApplicationApiController
    @@SOURCE_ID_LIST = { researchmap: 1, pubmed: 2 }
    @@proxy_url = { top: "http://proxyhmz.herokuapp.com", path: "/proxy/" }
    before_action :authenticate_staff

    def scrape
      begin
        doctor = Doctor.find(params[:doctor_id])
        if Delayed::Job.where(queue: "PreMonograph_#{doctor.id.to_s}_#{@@SOURCE_ID_LIST[:researchmap]}").count > 0
          # 重複して操作された場合
          render json: { status: "already processed"}
        else
          # ResearchMapのsourceは1を指定
          scraping_history = PreMonographsScrapingHistory.create!(doctor_id: doctor.id, created_by_id: @user.id, source: @@SOURCE_ID_LIST[:researchmap])
          # doctor・@userをそのまま渡すと、scopeがis_staff: falseなのでエラーになる
          PreMonographsController.delay(queue: "PreMonograph_#{doctor.id.to_s}_#{@@SOURCE_ID_LIST[:researchmap]}").scrape_monographs({ id: doctor.id, name: doctor.name }, { id: @user.id, name: @user.name, email: @user.email }, scraping_history)
          render json: { status: "success" }
        end
      rescue => e
        render json: { status: "error" }
      end
    end

    def scrape_html
      begin
        doctor = Doctor.find(params[:doctor_id])
        pre_monographs_html = pre_monographs_html_params
        monographs = []
        pre_monographs_html[:htmls].each do |item_html|
          monographs.push(PreMonographsController.extract_monograph_info({ id: doctor.id, name: doctor.name }, { url: pre_monographs_html[:url], block_name: pre_monographs_html[:page_info][:block_name] }, Nokogiri::HTML.parse(item_html)))
        end
        PreMonographsModeratedScrapingHistory.create!(doctor_id: doctor.id, source: @@SOURCE_ID_LIST[:researchmap], page_info: pre_monographs_html[:page_info], url: pre_monographs_html[:url])
        PreMonograph.create!(monographs)
        render json: { status: "success" }
      rescue => e
        render json: { status: "error" }
      end
    end

    def show
      begin
        pre_monographs = PreMonograph.where(doctor_id: params[:id], is_confirmed: false).limit(100).order("updated_at ASC")
        render json: pre_monographs, each_serializer: V1::PreMonographSerializer, root: nil
      rescue => e
        render json: { status: "error" }
      end
    end

    def confirm
      begin
        pre_monograph = PreMonograph.find(params[:id])
        pre_monograph.is_confirmed = true
        pre_monograph.save!
        if params[:monograph]
          monograph = Monograph.create!(monograph_params.merge(doctor_id: pre_monograph.doctor.id, no_timeline: true))
          render json: monograph, serializer: V1::MonographSerializer, root: nil
        else
          # monographパラメータがなければ、Monograph.createはしない
          render json: { status: "success" }
        end
      rescue => e
        render json: { status: "error" }
      end
    end

    def status
      begin
        doctor = Doctor.find(params[:doctor_id])
        scraping_histories = PreMonographsScrapingHistory.order("created_at desc").where(doctor_id: doctor.id).limit(1)
        moderated_scraping_histories = PreMonographsModeratedScrapingHistory.order("created_at desc").where(doctor_id: doctor.id, source: @@SOURCE_ID_LIST[:researchmap])
        # 最終スクレイピング日時を取得
        queued_ats = []
        queued_ats.push(scraping_histories.first.created_at) if !scraping_histories.empty?
        queued_ats.push(moderated_scraping_histories.first.created_at) if !moderated_scraping_histories.empty?
        queued_at = queued_ats.empty? ? nil : queued_ats.max

        rendered = { doctor: doctor, queued_at: queued_at, histories: moderated_scraping_histories }
        # 確認済みでないpre_monographが存在する
        if (count = PreMonograph.where(doctor_id: doctor.id, is_confirmed: false).count) > 0
          render json: rendered.merge(status: "found", count: count)
        # Delayed::Jobのqueueにジョブが存在する
        elsif Delayed::Job.where(queue: "PreMonograph_#{doctor.id.to_s}_#{@@SOURCE_ID_LIST[:researchmap]}").count > 0
          render json: rendered.merge(status: "processing")
        else
          queues_count = PreMonographsScrapingHistory.where(created_by_id: @user.id, completed_at: nil).count
          render json: rendered.merge(status: "none", queues_count: queues_count)
        end
      rescue => e
        render json: { status: "error" }
      end
    end

    def self.scrape_monographs(doctor_info, user_info, scraping_history)
      search_url = URI.encode("#{@@proxy_url[:top]}#{@@proxy_url[:path]}http://researchmap.jp/search/?EE4F3041=127359533DFA91A8472D4239FA6E10B2&user_name=#{doctor_info[:name]}&op=search")
      search_html = get_parsed_html(search_url)
    
      search_html.css(".snsview_card_row").each do |person_html|
        show_url = @@proxy_url[:top] + person_html.css(".snsview_card_name > a").first[:href]
        show_html = get_parsed_html(show_url)

        if user_id_html = show_html.css("#_url_7").first
          if user_id_matches = user_id_html[:value].match(/&user_id=([0-9a-zA-Z]+)/)
            # heading_id=11で論文ブロック、heading_id=3でMisc.ブロックを取得
            { papers: 11, misc: 3, conferences: 4 }.each do |block_name, heading_id|
              loop_crawled_pages(doctor_info, user_info, { url: show_url.sub(/^#{@@proxy_url[:top]}#{@@proxy_url[:path]}/, ""), user_id: user_id_matches[1], block_name: block_name, heading_id: heading_id })
            end
          end
        end
      end
      scraping_history.update(completed_at: Time.now)
      DoctorsMailer.delay.pre_monographs_ready(user_info[:name], user_info[:email])
    end

    def self.loop_crawled_pages(doctor_info, user_info, page_info, page_first=0)
      monographs = []
      page_count = page_first
      catch(:loop_page_count) do
        loop do
          monographs_url = URI.encode("#{@@proxy_url[:top]}#{@@proxy_url[:path]}http://researchmap.jp/index.php?action=cv_view_main_page&user_id=#{page_info[:user_id]}&heading_id=#{page_info[:heading_id]}&pageNumber=#{page_count}&page_id=421096&block_id=668956&module_id=19&_token=06e4e2196243eaaf52189b0167189d57&_header=0&_=")
          monographs_html = get_parsed_html(monographs_url)

          items_html = monographs_html.css(".cv_user_item_#{page_info[:block_name].to_s}")
          # 最終ページに到達
          break if items_html.empty?

          items_html.each do |item_html|
            monograph_html = item_html.css(".cv_item_data > div")
            title = monograph_html.first.inner_text
            # 同名のタイトルがあれば、以降はすでに取得済みとみなす
            throw :loop_page_count if PreMonograph.find_by(doctor_id: doctor_info[:id], title: title)
            monographs.push(extract_monograph_info(doctor_info, page_info, item_html))
          end

          # 最終ページに到達
          break if monographs_html.css(".cv_page_switch > a").last.inner_text.to_i == page_count + 1
          page_count += 1

          # 100ページ以上のクロールは、別のジョブをつくる
          if page_count - page_first > 100
            PreMonographsController.delay(queue: "PreMonograph_#{doctor_info[:id].to_s}_#{@@SOURCE_ID_LIST[:researchmap]}").loop_crawled_pages(doctor_info, user_info, { show_url: page_info[:url], user_id: page_info[:user_id], block_name: page_info[:block_name], heading_id: page_info[:heading_id] }, page_count)
            break
          end
        end
      end
      PreMonograph.create!(monographs)
    end

    def self.extract_monograph_info(doctor_info, page_info, item_html)
      description = item_html.css(".cv_item_desc").empty? ? nil : item_html.css(".cv_item_desc").first.inner_text
      monograph_html = item_html.css(".cv_item_data > div")
      title_html = monograph_html.first
      title = title_html.inner_text
      url = title_html.css("> a").empty? ? page_info[:url] : title_html.css("> a").first[:href]
      # タグ情報をつける
      tags = [title.ascii_only? ? "外国語文" : "和文"]
      tags.push("査読論文") if !monograph_html.css(".cv_rmap_invited").inner_text.index("[査読有り]").nil?
      case page_info[:block_name].to_s
      when "papers" then
        tags.push("論文")
      when "misc" then
        tags.push("Misc")
      when "conferences" then
        tags.push("講演・口頭発表等")
      end
      # 共著者情報の有無で分岐
      if monograph_html.length > 2
        writers_list = monograph_html[1].inner_text
        monograph_attr_html = monograph_html[2]
      else
        writers_list = nil
        monograph_attr_html = monograph_html[1]
      end
      # 公開年月が含まれていたら、公開年月以降の情報は無視
      if monograph_attr_matches = (monograph_attr_html.inner_text.match(/(.*[0-9]+年[0-9]+月)/) || monograph_attr_html.inner_text.match(/(.*[0-9]+年)/))
        monograph_attr =  monograph_attr_matches[1].split(/(?:\xc2\xa0){3}/)
        year = monograph_attr[-1].match(/([0-9]+)年/)[1]
        if month_matches = monograph_attr[-1].match(/([0-9]+)月/)
          published = year + format("%02d", month_matches[1]) + "01"
          published_type = "YM"
        else
          published = year + "0101"
          published_type = "Y"
        end
      else
        monograph_attr =  monograph_attr_html.inner_text.split(/(?:\xc2\xa0){3}/)
        year = nil
        published = nil
        published_type = nil
      end

      monograph_attributes = { doctor_id: doctor_info[:id], title: title, url: url, tag_list: tags, year: year, published: published, published_type: published_type, writers: writers_list, description: description }
      case monograph_attr.length
      when 2 then
        monographs = { page: monograph_attr[0] }.merge(monograph_attributes)
      when 3 then
        monographs = { media: monograph_attr[0], page: monograph_attr[1] }.merge(monograph_attributes)
      else
        monographs = monograph_attributes
      end
      monographs
    end

    private
    def authenticate_staff
      unless RequestStore.store[:is_staff]
        render json: { status: "permission_error" }
      end
    end

    def monograph_params
      params.require(:monograph).permit(:title, :year, :description, :url, :writers, :media, :page, :published, :published_type, :tag_list)
    end

    def pre_monographs_html_params
      params.require(:pre_monographs_html).permit(:url, htmls: [], page_info: [:page, :block_name])
    end

    def self.get_parsed_html(url)
      html = open(url) do |f|
        f.read
      end
      sleep(rand(5..20))
      Nokogiri::HTML.parse(html)
    end
  end
end
