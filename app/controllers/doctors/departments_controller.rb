module Doctors
  class DepartmentsController < ApplicationApiController
    skip_before_action :authenticate_user_from_token!
    skip_before_action :authenticate_user_from_identity

    #診療科一覧を取得する。HospitalsControllerのshowと同じ
  	def index
      if params[:hospital].present?
        departments = Department.where(hospital_id: params[:hospital]).where("name LIKE ?", '%' + params[:name].to_s + '%').limit(20)
      else
        departments = Department.where("name LIKE ?", '%' + params[:name].to_s + '%').limit(20)
      end
  	  render json: departments, each_serializer: V1::NameAndIDSerializer, root: nil
  	end

  	#診療科に従属する役職一覧を取得する
  	def show
      if params[:name].present?
        positions = DepartmentPosition.where(department_id: params[:id]).where("name LIKE ?", '%' + params[:name] + '%').limit(20)
      else
        positions = DepartmentPosition.where(department_id: params[:id]).limit(20)
      end
  	  render json: positions, each_serializer: V1::NameAndIDSerializer, root: "positions"      
  	end
  end
end
