module Doctors
  class SkillsController < ApplicationApiController
    #スキルの登録はログインしてないと使えないしみれない が、本人確認ができていなくても推薦者の一覧は見る事ができる(ポイントはみれない)
    # skip_before_action :authenticate_user_from_token!
    skip_before_action :authenticate_user_from_identity

    def index
      user = login_user
      limit  = 20
      page   = params[:page].to_i
      offset = page * limit
      #skills = Skill.where.not(doctor_id: user.id).offset(offset).limit(limit)
      skills = Skill.where.not(doctor_id: user.id)
      skills.each do |skill| 
        skill.doctor.check_link_state(user)
        skill.recommended = skill.recommends.where(doctor_id: user.id).length > 0
      end
      render json: skills, each_serializer: V1::SkillWithDoctorSerializer, meta: { page: page, limit: limit, type: "society", word: params[:word]}
    end

    def show
      skill = Skill.find(params[:id])
      if skill
        render json: skill, serializer: V1::SkillWithRecommendersSerializer, root: nil
      else
        render json: { error: t('user_show_error') }, status: :unprocessable_entity
      end
    end
  end
end
