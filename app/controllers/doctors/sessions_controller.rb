# -*- coding: utf-8 -*-
module Doctors
  class SessionsController < ApplicationApiController
    skip_before_action :authenticate_user_from_token!
    skip_before_action :authenticate_user_from_identity

    # POST /v1/login
    def create
      @user = Doctor.unscoped.find_for_database_authentication email:
        params[:username]
      return invalid_login_attempt unless @user
      if @user.valid_password? params[:password]
        sign_in :user, @user
        render json: @user, serializer: V1::SessionSerializer, root: nil
      else
        invalid_login_attempt
      end
    end

    def reset_request
      @user = Doctor.unscoped.find_by email: params[:email]
      @user.no_timeline = true
      return invalid_attempt "メールアドレスが存在しません" unless @user
      if @user.send_reset_password_instructions
        render json: {status: :success}
      else
        invalid_attempt "エラーが生じました"
      end
    end

    def reset
      return reset_with_token if params[:token].present?
      reset_without_token
    end

    private
    def invalid_attempt message
      warden.custom_failure!
      render json: {error: message}, status: :unprocessable_entity
    end

    def reset_with_token
      logger.info "パスワードを忘れた場合"
      @user = Doctor.unscoped.find_by reset_password_token: params[:token]
      return invalid_login_attempt if @user.nil?
      @user.no_timeline = true
      @user.reset_password! params[:password], params[:password]

      if @user.valid_password? params[:password]
        sign_in :user, @user
        render json: @user, serializer: V1::SessionSerializer, root: nil
      else
        invalid_login_attempt
      end
    end

    def reset_without_token
      logger.info "loginしている場合"
      authenticate_user_from_token!
      if login_user.present?
        @user = Doctor.unscoped.find login_user.id
        @user.no_timeline = true
        @user.reset_password! params[:password], params[:password]
        if @user.valid_password? params[:password]
          render json: @user, serializer: V1::SessionSerializer, root: nil
        else
          invalid_login_attempt
        end
      else
        invalid_login_attempt
      end
    end
  end
end
