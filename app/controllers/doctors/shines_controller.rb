module Doctors
  class ShinesController < ApplicationApiController
    require "no_staff_create"
    include NoStaffCreate
    #skip_before_action :authenticate_user_from_token!
    #skip_before_action :authenticate_user_from_identity

    def show
      logger.info(params[:id])
      @Post = Post.find(params[:id])
      @doctors = @Post.shines.map{|s| s.doctor }
      @doctors.each do |d|
        d.check_link_state(login_user)
      end

      render json: @doctors, each_serializer: V1::ThumbnailSerializer, root: nil
    end

  	def create
      shine = Shine.new(doctor_id: login_user.id, parent_id: params[:pid], parent_type: params[:ptype])
      if shine.save
        render json: {success: true, total_point: shine.total_point }
      else
        render json: { error: shine.errors }
      end
  	end

  	def del
      shine = Shine.find_by(parent_id: params[:post_id], doctor_id: params[:doctor_id])
      if shine.present?
        if shine.destroy
          render json: {success: true}
        else
          render json: { error: shine.errors }
        end
      else
        render json: { error: "not found" }
      end
  	end
  end
end
