module Doctors
  class UploadedFilesController < ApplicationApiController

    def create
      @uploaded_file = UploadedFile.new()
      @uploaded_file.file_info = params[:file]
      if @uploaded_file.save
        render json: { file_id: @uploaded_file.id.to_s, url: @uploaded_file.file_info.thumb.url }
      else
        render json: { error: @uploaded_file.errors }
      end
    end
  end
end
