class Doctors::EmergencyRequestsController < ApplicationApiController
  skip_before_action :authenticate_user_from_identity
  before_action :check_emergency_request, except: :create

  def create
    if emergency_request = EmergencyRequest.create_emergency_request(
      emergency_request_create_params, login_user.id)
      render json: emergency_request
    else
      render json: {error: t(".create_fail")}, status: :unprocessable_entity
    end
  end

  def show
    render json: EmergencyRequest.emergency_request_and_feedbacks(
      params[:id], params[:status])
  end

  def update
    if @emergency_request.update_emergency_request(
      emergency_request_update_params, login_user.id)
      render json: @emergency_request,
        serializer: EmergencyRequestInfoSerializer, root: nil
    else
      render json: {error: t("update_fail")}, status: :unprocessable_entity
    end
  end

  private
  def emergency_request_update_params
    params.require(:emergency_request).permit :status, :closing_reason,
      :emergency_request_hospital_id, :closing_comment,
      emergency_request_hospitals_attributes: [:hospital_id, doctor_ids: []]
  end

  def emergency_request_create_params
    params.require(:emergency_request).permit(:time,
      emergency_request_hospitals_attributes: [:hospital_id, doctor_ids: []],
      whytlink_messages_attributes: [:message_type, :content, :attachment])
  end

  def check_emergency_request
    @emergency_request = login_user.emergency_requests.find_by_id params[:id]
    return render json: {error: t("not_available")},
      status: :unprocessable_entity unless @emergency_request
  end
end
