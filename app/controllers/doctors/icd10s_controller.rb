module Doctors
  class Icd10sController < ApplicationApiController
    skip_before_action :authenticate_user_from_token!
    skip_before_action :authenticate_user_from_identity
    
  	def index
      if params[:parent].present?
        icd10s = Icd10.where(icd10_id: params[:parent]).order(:index).order(:code)
        render json: icd10s, each_serializer: V1::Icd10Serializer, root: nil
      elsif params[:depth].present?
        icd10s = Icd10.where(depth: params[:depth]).order(:index).order(:code)
        render json: icd10s, each_serializer: V1::Icd10Serializer, root: nil
      elsif params[:byomei].present?
        icd10s = Icd10.where(depth: 4).where(icd10_id: params[:byomei])
        icd10d5 = Array.new
        icd10s.each do | icd10 |
          icd10d5.concat(Icd10.where(depth: 5).where(code: icd10.code))
        end
        icd10s.concat(icd10d5).sort{|a, b| a.code <=> b.code}
        render json: icd10s, each_serializer: V1::Icd10Serializer, root: nil
      elsif params[:code].present?
        icd10s = Icd10.where(code: params[:code]).order(:code)
        render json: icd10s, each_serializer: V1::Icd10WithGroupSerializer, root: nil
      elsif params[:word].present?
        icd10s = Icd10.where("depth >= ?", 3).where("name LIKE ?", ("%" + params[:word] + "%")).limit(10)
        render json: icd10s, each_serializer: V1::Icd10WithGroupSerializer, root: nil
      end
    end
  end
end

