class ScenariosController < ApplicationApiController
  skip_before_action :authenticate_user_from_token!
  skip_before_action :authenticate_user_from_identity
  skip_before_action :set_current_user_type
  before_action :authenticate_mr

  def index
    scenarios = Scenario.get_scenarios(@current_mr.id, params[:limit],
      params[:last_id])
    scenarios = scenarios.search(scenario_search_params.to_h).result if
      params[:search]
    render json: scenarios
  end

  def show
    scenario = @current_mr.scenarios.find_by id: params[:id]
    return render json: scenario if scenario.present?
    render json: {error: t("no_such_scenario")}, status: :unprocessable_entity
  end

  def create
    scenario_infos = scenario_params.except(:scenario_charts_attributes)
      .merge mr: @current_mr
    scenario = Scenario.new scenario_infos
    return render json: {
      success: t("successfully_created"),
      scenario_info: ScenarioSerializer.new(scenario, root: false)
    } if scenario.save_scenario? scenario_params[:scenario_charts_attributes]
    render json: {error: t("not_created")}, status: :unprocessable_entity
  end

  def update
    scenario = @current_mr.scenarios.find_by id: params[:id]
    return render json: {
      success: t("successfully_updated"),
      scenario_info: ScenarioSerializer.new(scenario, root: false)
    } if scenario.present? && scenario.update_scenario?(scenario_params)
    render json: {error: t("update_fail")}, status: :unprocessable_entity
  end

  private
  def scenario_params
    params.require(:scenario_info).permit :name, :description, :favorite,
      scenario_charts_attributes: [:chart_id, :chart_order]
  end

  def scenario_search_params
    params.require(:search).permit :name_cont
  end
end
