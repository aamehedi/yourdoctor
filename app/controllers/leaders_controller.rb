class LeadersController < ApplicationController
  layout "work_layout"
  before_action :redirect_root_if_not_leader
  def home

  end

  def update

    result = false
    if params[:password]
      result = current_user.update_with_password(leader_params)
    else
      result = current_user.update_attributes(leader_params)
    end

    if result
      flash[:notice] = '保存しました'
    else
      flash[:alert] = '保存に失敗しました'
    end
    redirect_to home_leader_path
  end

  def batch
    current_user.attributes = staffs_params
    current_user.save(:validate => false)
    redirect_to home_leader_path
  end

  private
    def leader_params
      if params[:leader][:password].empty?
        params[:leader].delete(:password)
      end
      params.require(:leader).permit(:email, :name, :password, :password_confirmation)
    end

    def staffs_params
      params.require(:leader).permit(staffs_attributes: [:email, :name, :password, :visible_wages, :suspended, :id])
    end

end
