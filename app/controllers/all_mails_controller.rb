# -*- coding: utf-8 -*-

class AllMailsController < ApplicationController
  layout "work_layout"
  before_action :redirect_root_if_not_login
  before_action :set_duplicate_docter_list, only: [:show, :update, :merge]

  respond_to :html

  def index
    @all_mails = AllMail.all.order("created_at DESC")
  end

  def edit

  #  @drs = Doctor.where(first_name: @doctor.first_name, last_name: @doctor.last_name)
#    if @doctor.university.nil?
#      @doctor.build_university
#    end
#    unless @doctor.society_lists.present?
#      @doctor.society_lists.build
#    end

#    @duplicate_doctor_list = DuplicateDoctorList.where('%d = ANY(doctor_ids)' % @doctor.id).first
  end

  def new
    @all_mail = AllMail.new
  end

  def create
    @all_mail = AllMail.new(all_mail_params)
    @all_mail.save
    redirect_to action: "index"
  end

  def update
    @doctor.update(doctor_params)
    # respond_with(@doctor)
    respond_to do |format|
      format.html { redirect_to edit_doctor_path(@doctor) }
    end
  end

  def destroy
    @doctor.destroy
    respond_with(@doctor)
  end

  private
    def set_doctor
      @doctor = Doctor.find(params[:id])
    end


  private

    def all_mail_params
      params.require(:all_mail).permit(:title, :content, :done)
    end
end
