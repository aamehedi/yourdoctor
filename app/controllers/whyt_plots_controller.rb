# -*- coding: utf-8 -*-
class WhytPlotsController < ApplicationController

  def index
    api_results = WhytPlotApi.find_by(name: params[:api_name])
    api_sql = String.new
    unless api_results.present?
      render :json => { message: 'api not found'}, :status => 404 and return
    end

    # select_optionsを組み立て
    selected_option = params[:selected_option] ? params[:selected_option] : nil
    meta_data = ActiveSupport::JSON.decode(api_results['meta_data'])
    unless meta_data['select_options']['options'] == 'wp_years'
      where_name = meta_data['select_options']['where_name']
      selected = meta_data['select_options']['options'][selected_option][1]
      api_sql = api_results['api_sql'].gsub(/\[#{where_name}\]/, selected)
    else
      api_sql = api_results['api_sql']
    end
    # 条件文抽出
    matches = api_sql.scan(/\[.+?\]/)
    args = create_condition_sentence(matches, params)

    # 動的に条件文作成
    key_value_pairs = [matches, args].transpose
    query = api_sql.gsub(/\[.+?\]/, Hash[*key_value_pairs.flatten])
    error_desc = String.new
    begin
      query_results = ActiveRecord::Base.connection.execute(query.gsub(/"/, "'"))
    rescue => e
      p e
      error_desc = e
    end

    unless query_results.present?
      render :json => { message: "Error detail: #{error_desc}" }, :status => 404 and return
    else
      if query_results.count == 0
        render :json => { message: "Record not found. total number: #{query_results.count}" }, :status => 202 and return
      else query_results.present?
        render :json => { contents: query_results }, :status => 200
      end
    end
  end

  private

    def create_condition_sentence(conditions, http_params)
      conditions.map { |match|
        # 条件文にマッチした文字列から配列作成
        parameters = match.delete("\s").delete("[").delete("]").split(",")
        parameters.map { |parameter|
          parameter.sub!(/=/, '')
          # パラメーターの加工、生成ロジック
          case http_params[parameter]
          when /^\d+,\d+$/; params[parameter] = http_params[parameter]
            # 数値がStringの場合に対応
          when /^\'\d.+\'/; params[parameter] = "#{http_params[parameter]}"
          when /^\'\d.+\'{2,}/; params[parameter] = "\"#{http_params[parameter].gsub!(/,/, '","')}\""
            # 文字列のパラメータ複数の場合に対応
          when /^\D.+,\D.+/; params[parameter] = "\"#{http_params[parameter].gsub!(/,/, '","')}\""
          when /^\D.+/; params[parameter] = "\"#{http_params[parameter]}\""
          when /\D/; params[parameter] = "\"#{http_params[parameter].gsub!(/,/, '","')}\""
          when /^(?!.*\")/ && /\D/
            params[parameter] = "\"#{http_params[parameter]}\""
          end
          param_num =
            params[parameter] =~ /,/ ? http_params[parameter].split(",").count : 1

          if param_num > 1
            params[parameter] ? "#{parameter} IN (#{http_params[parameter]})" : nil
          else
            params[parameter] ? "#{parameter} = #{http_params[parameter]}" : nil
          end
        }.compact.join(" AND ")
      }
    end

end
