class ChartTypesController < InheritedResources::Base
  layout "work_layout"
  private

    def chart_type_params
      params.require(:chart_type).permit(:name, :description)
    end
end
