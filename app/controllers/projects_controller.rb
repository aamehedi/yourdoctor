class ProjectsController < ApplicationApiController
  skip_before_action :authenticate_user_from_token!
  skip_before_action :authenticate_user_from_identity
  skip_before_action :set_current_user_type
  before_action :authenticate_mr
  before_action :authenticate_company_manager

  def index
    projects = if params[:search]
      @current_mr.company.projects.ransack(project_search_params.to_h).result
    else
      @current_mr.company.projects
    end
    render json: projects
  end

  def show
    render json: Project.find(params[:id])
  end

  def create
    project_infos = project_params.except(:mr_ids)
      .merge company: @current_mr.company
    project = Project.new project_infos
    if save_project? project
      render json: {
        success: t("successfully_created"),
        project_info: ProjectSerializer.new(project, root: false)
      }
    else
      render json: {error: t("not_created")}, status: :unprocessable_entity
    end
  end

  def update
    project = Project.find params[:id]
    project.param_mr_ids = project_params[:mr_ids]
    if project.update_attributes(project_params.except :mr_ids) &&
       save_project?(project)
      render json: {
        success: t("successfully_updated"),
        project_info: ProjectSerializer.new(project, root: false)
      }
    else
      render json: {error: t("update_fail")}, status: :unprocessable_entity
    end
  end

  private
  def project_params
    params.require(:project_info).permit :name, :company_id, :mr_id,
      :description, :this_month_request, :next_month_request, mr_ids: []
  end

  def project_search_params
    params.require(:search).permit :name_cont
  end

  def save_project? project
    project.mrs = Mr.find(project_params[:mr_ids] || [])
    project.save
  end
end
