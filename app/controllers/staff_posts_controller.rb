class StaffPostsController < InheritedResources::Base
  layout "work_layout"
  before_action :redirect_root_if_not_login  
  before_action :set_current_user_to_staff_posts

  private

    def staff_post_params
      params.require(:staff_post).permit(:body, :start_time, :end_time)
    end

    def set_current_user_to_staff_posts
      StaffPost.current_user = current_user
    end
end

