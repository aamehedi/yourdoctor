module NoStaffCreate  
  def self.included(base)
  base.class_eval {
    before_action :no_staff, only: [:create]
  }
  end

  def no_staff
    render json: { error: "スタッフには禁止されているアクションです" }, status: :unprocessable_entity if login_user.is_staff?
    return
  end
end
