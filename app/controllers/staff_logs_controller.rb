class StaffLogsController < InheritedResources::Base
  before_action :redirect_root_if_not_login
  layout "work_layout"
  def index
    if params[:user_id].present?

      this_month = Date.today.beginning_of_month
      start_month = Date.parse("2015-07-01")      
      @months = []
      while 1 do
        @months.push(this_month)
        if this_month == start_month
          break
        else
          this_month = this_month - 1.month
        end
      end

      this_month = Date.today.beginning_of_month
      @val = this_month.to_s
      if params[:month].present?
        this_month = Date.parse(params[:month]).beginning_of_month
        @val = params[:month]
      end


      sdate = this_month
      edate = this_month.end_of_month

      @user = User.find(params[:user_id])
      @staff_logs = StaffLog.where(user_id: params[:user_id]).where("created_at >= ? and created_at <= ?", sdate, edate)


    else
      redirect_to "https://www.reasonwhy.jp"
    end

  end

  private

    def staff_log_params
      params.require(:staff_log).permit(:user_id, :staff_log_id, :staff_log_type)
    end
end
