class MrsController < ApplicationApiController
  skip_before_action :authenticate_user_from_token!
  skip_before_action :authenticate_user_from_identity
  skip_before_action :set_current_user_type
  before_action :authenticate_mr, except: [:update]
  before_action :authenticate_company_manager, only: [:show, :create]

  def index
    if params[:search].nil?
      render json: Mr.mrs_except_manager_in_company(@current_mr),
      each_serializer: MrInfoSerializer
    else
      render json: Mr.search(mr_search_params.to_h).result(distinct: true)
        .mrs_except_manager_in_company(@current_mr),
        each_serializer: MrInfoSerializer
    end
  end

  def show
    render json: Mr.find(params[:id]), serializer: MrInfoSerializer
  end

  def create
    mr_information = mr_params.except(:mr_projects_attributes).merge company_id:
      @current_mr.company_id
    mr = Mr.new mr_information
    mr.projects = Project.where(id: mr_params[:mr_projects_attributes]) if
      mr_params[:mr_projects_attributes]
    if mr.save
      render json: {
        success: t("successfully_created"),
        mr_info: MrInfoSerializer.new(mr, root: false)
      }
    else
      render json: {error: t(".mr_create_error")}, status: :unprocessable_entity
    end
  end

  def edit
    @mr = Mr.find_by reset_password_token: mr_params[:reset_password_token]
  end

  def update
    # TODO: Dirty code. Need refactoring later
    current_mr = MrSession.find_by_access_token(request
      .headers["Authorization"]).try(:mr)
    if current_mr.nil?
      password_reset_handler && return if has_email_or_password_reset_token?
      return unauthorized_mr
    elsif current_mr.company_manager? && mr_params[:id].present?
      mr = Mr.find mr_params[:id]
      update_mr_info mr
    else
      update_self_info
    end
  end

  private
  def mr_params
    params.require(:mr).permit :id, :email, :password, :password_confirmation,
      :reset_password_token, :first_name, :last_name, :sex, :mr_type,
      :branch_id, :avatar_string, :birthdate, :new_password, :file,
      mr_projects_attributes: []
  end

  def mr_search_params
    params.require(:search).permit :branch_id_eq, :projects_id_eq, :mr_type_eq,
      :name_cont
  end

  def password_reset_mail
    mr = Mr.find_by email: mr_params[:email]
    return invalid_attempt unless mr

    if mr.password_reset
      render json: {ok: t(".mail_success")}
    else
      render json: {error: t(".mail_fail")}, status: :unprocessable_entity
    end
  end

  def password_reset_confirmation
    mr = Mr.find_by reset_password_token: mr_params[:reset_password_token]
    if mr.reset_password! mr_params[:password],
      mr_params[:password_confirmation]
      flash[:notice] = t ".password_changed"
      redirect_to mr
    else
      flash[:alert] = t ".unsuccessful"
      @mr = mr
      render :edit
    end
  end

  def update_avatar
    mr = authenticate_mr
    return unauthorized_mr unless mr
    io = StringIO.new(Base64.decode64 mr_params[:file])
    def io.original_filename; "avatar.png"; end
    if mr.update_attributes avatar_string: io
      render json: {
        ok: t(".avatar_success"),
        mr_info: MrInfoSerializer.new(mr, root: false)
      }
    else
      render json: {error: t(".avatar_error")}, status: :unprocessable_entity
    end
  end

  def invalid_attempt
    warden.custom_failure!
    render json: {error: t(".mail_fail")}, status: :unprocessable_entity
  end

  def update_password
    mr = authenticate_mr
    if mr.valid_password?(mr_params[:password]) && mr.reset_password!(
      mr_params[:new_password], mr_params[:password_confirmation])
      render json: {
        success: t("password_changed_successfully"),
        mr_info: MrInfoSerializer.new(mr, root: false)
      }
    else
      render json: {error: t("password_not_saved")},
        status: :unprocessable_entity
    end
  end

  def update_mr_info mr
    projects = Project.where(id: mr_params[:mr_projects_attributes])
    if mr.update_attributes(mr_params.except :reset_password_token,
      :new_password, :mr_projects_attributes)
      mr.projects = projects
      render json: {
        success: t(".info_updated_successfully"),
        mr_info: MrInfoSerializer.new(mr, root: false)
      }
    else
      render json: {error: t("update_fail")}, status: :unprocessable_entity
    end
  end

  def update_self_info
    return update_password if mr_params[:new_password]
    return update_avatar unless mr_params[:file].nil?
  end

  def password_reset_handler
    password_reset_confirmation if mr_params[:reset_password_token]
    password_reset_mail if mr_params[:email]
  end

  def has_email_or_password_reset_token?
    mr_params[:email] || mr_params[:reset_password_token]
  end
end
