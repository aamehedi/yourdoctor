class TypeaheadsController < ApplicationController
  def university
    render json: University.where("name like ?", "%#{params[:q]}%").as_json
  end

  def society
    render json: Society.where("name like ?", "%#{params[:q]}%").as_json
  end

  def society_position
    render json: SocietyPosition.where(society_id: params[:society_id]). where("name like ?", "%#{params[:q]}%").as_json
  end
end
