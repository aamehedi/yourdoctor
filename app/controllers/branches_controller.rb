class BranchesController < ApplicationApiController
  skip_before_action :authenticate_user_from_token!
  skip_before_action :authenticate_user_from_identity
  skip_before_action :set_current_user_type
  before_action :authenticate_mr
  before_action :authenticate_company_manager

  def index
    branches = if params[:search]
      @current_mr.company.branches.ransack(branch_search_params.to_h).result
    else
      @current_mr.company.branches
    end
    render json: branches
  end

  def show
    render json: Branch.find(params[:id])
  end

  def create
    branch = Branch.new branch_params.except :mr_ids
    if branch_save? branch
      render json: {
        success: t("successfully_created"),
        branch_info: BranchSerializer.new(branch, root: false)
      }
    else
      render json: {error: t("not_created")}, status: :unprocessable_entity
    end
  end

  def update
    branch = Branch.find params[:id]
    branch.param_mr_ids = branch_params[:mr_ids]
    if branch_updated? branch
      render json: {
        success: t("successfully_updated"),
        branch_info: BranchSerializer.new(branch, root: false)
      }
    else
      render json: {error: t("update_fail")}, status: :unprocessable_entity
    end
  end

  private
  def branch_params
    params.require(:branch_info).permit :name, :company_id, :mr_id,
      :this_month_request, :next_month_request, mr_ids: []
  end

  def branch_search_params
    params.require(:search).permit :name_cont
  end

  def branch_params_valid? mrs
    branch_params[:mr_ids].blank? || mrs.size == branch_params[:mr_ids].count
  end

  def branch_save? branch
    branch.company = @current_mr.company
    branch.mrs = Mr.branches_of_mr branch_params[:mr_ids], nil
    branch_params_valid?(branch.mrs) && branch.save
  end

  def branch_updated? branch
    mrs = Mr.branches_of_mr branch_params[:mr_ids], [nil, branch.id]
    if branch_params_valid? mrs
      branch.update_attributes(branch_params.except :mr_ids)
      branch.mrs = mrs unless mrs.blank?
      return true
    end
    false
  end
end
