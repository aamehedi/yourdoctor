# -*- coding: utf-8 -*-
class Users::SessionsController < Devise::SessionsController
  skip_filter :ban_if_suspended_user, :only => :destroy
  def after_sign_in_path_for(resource)
    if resource.admin
      whyt_plot_table_views_path
    else
      case resource.type
      when 'Leader' then home_leader_path
      when 'Staff'  then duplicate_doctor_lists_path  #database_work_path
      else
      end
    end
  end
end

