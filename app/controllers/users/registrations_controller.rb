# -*- coding: utf-8 -*-
class Users::RegistrationsController < Devise::RegistrationsController
  skip_before_action :require_no_authentication


  def new
    logger.info "!!!!!!!!!!!!!!!!!!!!!!"
    if User.count == 0 || (current_user && current_user.admin?)
      super
    else
      redirect_to :root
    end
  end

  def edit
    logger.info "!!!!!!!!!!!!!"
    if current_user.admin? and params[:id]
      @user = User.find(params[:id])
    else
      @user = User.find(current_user.id)
    end

  end

  def create


    if sign_up_params[:id].present?
      @user = User.find_by(id: sign_up_params[:id])
    end


    if @user.present?
       build_resource(sign_up_params)
       clean_up_passwords resource
       set_flash_message :notice, :already_id
       respond_with resource, :location => "/users/sign_up/"
    else
      if sign_up_params[:tokuisaki_id].present?
       t = sign_up_params[:tokuisaki_id].split(",") 
       sign_up_params[:tokuisaki_code] = t.first
       sign_up_params[:shiten_code] = t.last
      end


      build_resource(sign_up_params)

      if resource.save
        if resource.active_for_authentication?
          set_flash_message :success, :signed_up if is_navigational_format? 
          if current_user && current_user.admin?
            redirect_to "/user_list"
          else
            sign_up(resource_name, resource)         
            respond_with resource, :location => after_sign_up_path_for(resource)
          end
        else
          set_flash_message :notice, :"signed_up_but_#{resource.inactive_message}" if is_navigational_format?
          expire_session_data_after_sign_in!
          respond_with resource, :location => after_inactive_sign_up_path_for(resource)
        end
      else
        clean_up_passwords resource
        respond_with resource
    end

    end
 

    
  end

  def update

    @user = User.find(params[:user][:id])
    params[:user].delete(:password) if params[:user][:password].blank?
    params[:user].delete(:password_confirmation) if params[:user][:password].blank? and params[:user][:password_confirmation].blank?
    params[:user].delete(:current_password) if params[:user][:password].blank? and params[:user][:current_password].blank?
    logger.info params
    if @user.update_attributes(device_params)
      logger.info "AKAKAKAKAKAKA"
      flash[:success] = "情報を更新しました"
      redirect_to :action => 'edit'
    else
      render :action => 'edit'
    end
  end

  def device_params
    if params[:user]
      params.require(:user).permit(:device, :name, :email, :password, :password_confirmation, :encrypted_password, :salt, :role_ids, :is_admin, :chg_pwd, :last_name, :first_name, :role, :tokuisaki_id, :del_account, :tokuisaki_code, :shiten_code)
    end
  end
end 


