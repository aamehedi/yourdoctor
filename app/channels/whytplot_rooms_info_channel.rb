class WhytplotRoomsInfoChannel < ApplicationCable::Channel
  def subscribed
    stream_from "whytplot_rooms_info"
  end

  def unsubscribed
    stop_all_stream
  end

  def broadcast_room_info data
    whytplot_chatroom = WhytplotChatroom.find_by_id data["message"]
    whytplot_chatroom.broadcast_chatroom_info unless whytplot_chatroom.nil?
  end
end
