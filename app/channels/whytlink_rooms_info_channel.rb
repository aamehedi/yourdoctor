class WhytlinkRoomsInfoChannel < ApplicationCable::Channel
  def subscribed
    stream_from "whytlink_rooms_info"
  end

  def unsubscribed
    stop_all_stream
  end

  def broadcast_room_info data
    whytlink_chatroom = WhytlinkChatroom.find_by_id data["message"]
    whytlink_chatroom.broadcast_chatroom_info unless whytlink_chatroom.nil?
  end
end
