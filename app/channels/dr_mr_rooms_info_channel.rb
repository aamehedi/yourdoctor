class DrMrRoomsInfoChannel < ApplicationCable::Channel
  def subscribed
    stream_from "dr_mr_rooms_info"
  end

  def unsubscribed
    stop_all_stream
  end

  def broadcast_room_info data
    dr_mr_chatroom = DrMrChatroom.find_by_id data["message"]
    dr_mr_chatroom.broadcast_chatroom_info unless dr_mr_chatroom.nil?
  end
end
