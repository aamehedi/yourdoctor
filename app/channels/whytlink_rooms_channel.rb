class WhytlinkRoomsChannel < ApplicationCable::Channel
  def listen data
    if doctor = authenticated_doctor(data["message"]["access_token"])
      data["message"]["room_ids"].each do |chatroom_id|
        next unless authorized_to_listen?(chatroom_id, doctor.id)
        matched_stream = streams.select do |broadcasting|
          broadcasting[0] == "whytlink_room_#{chatroom_id}"
        end
        stream_from "whytlink_room_#{chatroom_id}" if matched_stream.empty?
      end
    end
  end

  def speak data
    WhytlinkMessage.create! data["message"]
  end

  def update_whytlink_doctor_chatroom data
    WhytlinkDoctorChatroom.update_whytlink_doctor_chatroom data["message"]
  end

  private
  def authenticated_doctor access_token
    return unless
      access_token && access_token.include?(Settings.access_token_splitter)
    doctor = Doctor.find_by_id(access_token.split(
      Settings.access_token_splitter).first)
    return unless
      doctor && Devise.secure_compare(doctor.access_token, access_token)
    doctor
  end

  def authorized_to_listen? chatroom_id, doctor_id
    WhytlinkDoctorChatroom.exists?(whytlink_chatroom_id: chatroom_id,
      doctor_id: doctor_id)
  end
end
