class DrMrRoomsChannel < ApplicationCable::Channel
  def listen data
    if user = authenticate_dr_or_mr(data["message"]["access_token"],
      data["message"]["from_dr"])
      data["message"]["room_ids"].each do |chatroom_id|
        next unless authorized_to_listen?(chatroom_id, user.id)
        matched_stream = streams.select do |broadcasting|
          broadcasting[0] == "dr_mr_room_#{chatroom_id}"
        end
        stream_from "dr_mr_room_#{chatroom_id}" if matched_stream.empty?
      end
    end
  end

  def speak data
    DrMrMessage.create! data["message"]
  end

  def update_dr_mr_chatroom data
    DrMrChatroom.update_dr_mr_chatroom data["message"]
  end

  private
  def authenticate_dr_or_mr access_token, from_dr
    return unless
        access_token && access_token.include?(Settings.access_token_splitter)
    @from_dr = ActiveRecord::Type::Boolean.new.cast from_dr
    if @from_dr
      doctor = Doctor.find_by_id(access_token.split(
        Settings.access_token_splitter).first)
      return unless
          doctor && Devise.secure_compare(doctor.access_token, access_token)
      doctor
    else
      MrSession.find_by_access_token(access_token).try :mr
    end
  end

  def authorized_to_listen? chatroom_id, user_id
    return DrMrChatroom.exists?(id: chatroom_id, doctor_id: user_id,
      status: :accepted) if @from_dr
    DrMrChatroom.exists? id: chatroom_id, mr_id: user_id, status: :accepted
  end
end
