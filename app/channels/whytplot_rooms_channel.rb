class WhytplotRoomsChannel < ApplicationCable::Channel
  def listen data
    if mr = authenticate_mr(data["message"]["access_token"])
      data["message"]["room_ids"].each do |chatroom_id|
        next unless authorized_to_listen?(chatroom_id, mr.id)
        matched_stream = streams.select do |broadcasting|
          broadcasting[0] == "whytplot_room_#{chatroom_id}"
        end
        stream_from "whytplot_room_#{chatroom_id}" if matched_stream.empty?
      end
    end
  end

  def speak data
    WhytplotMessage.create! data["message"]
  end

  def update_whytplot_mr_chatroom data
    WhytplotMrChatroom.update_whytplot_mr_chatroom data["message"]
  end

  private
  def authenticate_mr access_token
    return unless
      access_token && access_token.include?(Settings.access_token_splitter)
    MrSession.find_by_access_token(access_token).try :mr
  end

  def authorized_to_listen? chatroom_id, mr_id
    WhytplotMrChatroom.exists? whytplot_chatroom_id: chatroom_id, mr_id: mr_id
  end
end
