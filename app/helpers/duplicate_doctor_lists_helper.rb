module DuplicateDoctorListsHelper
  
  def duplicate_doctor_ids_without(doctor_ids, id)
    row_doctor_ids = Marshal.load(Marshal.dump(doctor_ids))
    col_doctor_ids = Marshal.load(Marshal.dump(doctor_ids))
    row_doctor_ids.delete(id)
    col_doctor_ids.delete(id)
    # ２次元配列を行列と見立てて、行と列の入れ替えを行う。行[1,2,3] 列[1,2,3] => ２次元配列[[1,1],[2,2],[3,3]]
    row_doctor_ids.zip(col_doctor_ids)
  end

  def duplicate_doctor_convert_check(check)
    if check
      "済"
  	else
      ""
  	end 
  end
end
