# encoding: utf-8

class FileUploader < ImageUploader
  #include CarrierWave::RMagick
  include CarrierWave::MiniMagick

  process :resize_to_fit => [800, nil]
  def store_dir
    "uploads/#{model.class.to_s.underscore}"
  end

  version :thumb do
     process :resize_to_fill => [100, 100]
  end
end
