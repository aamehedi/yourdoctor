source "https://rubygems.org"

gem "rails", "~> 5.0"
gem "active_model_serializers", "~> 0.8.3" # NOTE: not the 0.9
gem "rack-cors", require: "rack/cors"
gem "pg"
gem "devise"
gem "simple_form", github: "bs90/simple_form", branch: "rails-5-0"
gem "slim"
gem "slim-rails"
gem "kaminari"
gem "validates_email_format_of"
gem "charwidth", "~> 0.1.3"
gem "daemons"
gem "act-fluent-logger-rails", github: "Bugagazavr/act-fluent-logger-rails"
gem "lograge"
gem "aws-sdk-resources", "~> 2.1"
gem "oj"
gem "activesupport"
gem "inherited_resources", github: "activeadmin/inherited_resources"
gem "mandrill-api"
gem "carrierwave-ftp", require: "carrierwave/storage/ftp/all"
gem "delayed_job_active_record"
gem "coffee-rails"
gem "sass-rails"
gem 'bootstrap-sass', '~> 3.3.6'
gem "config"
gem "ransack"
gem "railties", "~> 5.0"
gem "uglifier", ">= 1.0.3"
gem "puma"
gem "carrierwave"
gem "fog"
gem "unf"
gem "enum_help"
gem "jquery-rails"
gem "jquery-ui-rails"
gem "therubyracer", platforms: :ruby
gem "momentjs-rails"
gem "mini_magick"
gem "acts-as-taggable-on", "~> 3.5",
  github: "abdullahallmehedi/acts-as-taggable-on",
  branch: "activerecord-5.0-support"
gem "seed-fu"
gem "paper_trail", "~> 4.0.0.rc"
gem "mechanize", "~> 2.7.3"
gem "request_store"
gem "roo", "~> 2.3.2"
gem "roo-xls"
gem "sqlite3"
gem "rubyzip"
gem "geocoder"
gem "slack-incoming-webhooks"

group :development do
  gem "guard-livereload"
  gem "better_errors"
  gem "binding_of_caller"
  gem "brakeman"
  gem "rails_best_practices"
end

group :test do
  gem "faker", "~> 1.6", ">= 1.6.6"
  gem "capybara", "~> 2.7", ">= 2.7.1"
  gem "shoulda-matchers", "~> 3.1", ">= 3.1.1"
  gem "database_cleaner", "~> 1.5", ">= 1.5.3"
  gem "launchy", "~> 2.4", ">= 2.4.3"
  gem "poltergeist", "~> 1.10"
end

group :development, :test do
  gem "rspec-rails", "~> 3.5", ">= 3.5.1"
  gem "factory_girl_rails", "~> 4.7"
  gem "rspec-collection_matchers", "~> 1.1", ">= 1.1.2"
  gem "guard-rspec", "~> 4.7", ">= 4.7.3", require: false
  gem "zeus-parallel_tests", "~> 0.3.1"
  gem "railroady"
  gem "pry-rails"
  gem "pry-byebug"
  gem "hirb"
  gem "hirb-unicode"
  gem "timecop"
  gem "rubocop", require: false
end

