class String
  AZ_LENGTH  = 'Z'.ord - 'A'.ord + 1
  OFFSET_NUM = 'A'.ord - 1

  def to_cn
    convert_str( 0, self.split(''))
  end

  def to_cn0
    convert_str( 0, self.split('')) - 1
  end

  private
  def convert_str(ret, str_array)
    if str_array.empty?
      ret
    else
      c = str_array.shift
      convert_str( AZ_LENGTH ** str_array.length * (c.ord - OFFSET_NUM) + ret, str_array )
    end
  end
end
