Rails.application.routes.draw do
  post_to = ->path{post path, to: path.gsub("/", "#")}
  devise_for :mrs, only: []
  devise_for :doctor, only: []
  namespace :doctors, defaults: {format: :json} do
    resources :comments,        only: [:show, :create], controller: :comments
    resource  :confirm_email, only: [:create],          controller: :registrations
    resource  :check_uuid,    only: [:show],            controller: :registrations
    resource  :register,      only: [:update],          controller: :registrations
    resource  :login,         only: [:create],          controller: :sessions
    resource  :reset_request, only: [:reset_request],   controller: :sessions
    post "/reset_request"  => "sessions#reset_request"
    post "/reset"          => "sessions#reset"
    resources :posts,         only: [:new, :create, :destroy], controller: :posts
    resources :users,         only: [:index],           controller: :users
    resource  :users,         only: [:update],          controller: :users
    post "/resign_doctor" => "doctors#resign"
    post "/wrong_doctor" => "doctors#wrong"
    post "/license_check" => "doctors#license_check"
    resources :doctors,       only: [:index, :show, :update], controller: :doctors
    resources :images,        only: [:create],          controller: :images
    resource  :images,        only: [:destroy],         controller: :images
    resources :areas,         only: [:index, :show],    controller: :areas
    resources :prefectures,   only: [:index],           controller: :prefectures
    resources :icd10s,        only: [:index],           controller: :icd10s
    resources :hospitals,     only: [:index, :show],    controller: :hospitals
    resources :departments,   only: [:index, :show],    controller: :departments
    resources :societies,     only: [:index, :show],    controller: :societies
    resources :universities,  only: [:index],           controller: :universities
    resources :recommends,    only: [:create, :destroy], controller: :recommends
    resources :shines,        only: [:show, :create], controller: :shines
    resources :links,         only: [:index, :create, :show], controller: :links
    resource  :links,         only: [:update],          controller: :links
    resource  :links,         only: [:destroy],         controller: :links
    resources :invitees,      only: [:index],           controller: :temporary_doctors
    resources :skills,        only: [:index, :show],    controller: :skills
    resources :timelines,     only: [:index, :destroy], controller: :timelines
    resources :uploaded_files, only: [:create, :destroy], controller: :uploaded_files
    resources :monographs, only: [:create, :update], controller: :monographs
    resources :pre_monographs, only: [:show], controller: :pre_monographs
    resources :whytlink_messages, only: [:index, :create]
    resources :whytlink_doctor_chatrooms, only: :destroy
    resources :emergency_doctor_hospitals, only: [:index, :create]
    resources :doctor_feedbacks, only: [:index, :update]
    resources :emergency_requests, only: [:create, :show, :update]
    resources :whytlink_chatrooms, only: [:index, :show, :create, :update]
    resources :favorite_groups, only: [:index, :show, :create, :update]
    resources :specialties, only: :index
    resources :doctor_specialties, only: :create
    resources :whytlink_group_chat_requests, only: [:create, :update]
    resources :emergency_doctors, only: :update
    resources :emergency_request_hospitals, only: :update
    get  "/invitees/test:id"   => "invitees#test"
    get  "/doctors/test/:id"   => "doctors#test"
    get  "/skills/test/"       => "skills#test"
    get  "/pre_monographs/status/:doctor_id" => "pre_monographs#status"
    get  "/pre_monographs/scrape/:doctor_id" => "pre_monographs#scrape"
    post "/pre_monographs/scrape_html/:doctor_id" => "pre_monographs#scrape_html"
    put  "/pre_monographs/confirm/:id" => "pre_monographs#confirm"
    post "/doctors/search" => "doctors#custom_search"
    post "/shines_del" => "shines#del"
    post "/comments_del" => "comments#del"
  end

  namespace :mrs, defaults: {format: :json} do
    resources :whytplot_messages, only: [:index]
    resources :whytplot_chatrooms, only: [:index, :create, :update, :show]
    resources :whytplot_mr_chatrooms, only: [:update]
  end

  %w(
    mr_sessions/create mr_sessions/destroy
    mrs/update
  ).each &post_to

  resources :mrs, only: [:index, :show, :create, :edit]
  resources :projects, only: [:index, :create, :update, :show]
  resources :branches, only: [:index, :create, :update, :show]
  resources :companies, only: [:show]
  resources :charts, only: [:index, :create, :update, :show]
  resources :scenarios, only: [:index, :create, :update, :show]
  resources :dr_mr_chatrooms, only: [:index, :create, :update, :show]
  resources :dr_mr_messages, only: [:index, :create]

  scope :admin do
    resources :all_mails
    resources :mail_logs
    resources :chart_types
    resources :whytplot_export_errors, :only => [:index, :destroy]
    get   '/datafiles/'          => 'datafiles#index'
    get   '/datafiles/:year'     => 'datafiles#list'
    get   '/datafiles/:year/:id' => 'datafiles#show'
    post  '/datafiles/import'    => 'datafiles#import'
  end

  scope :whyt_plot do
    get '/api/:api_name', :to=> "whyt_plots#index"
  end


  resources :whyt_plot_apis
  resources :whyt_plot_table_views do
    collection do
      get  'download'
      get  'timestamp_latest' => 'whyt_plot_table_views#latest'
      post 'export'
      post 'drop_view'
    end
  end

  resources :society_positions
  resources :staff_logs

  resources :departments
  devise_for :users, controllers: {
    sessions: "users/sessions"
  }

  get    "/leader"       => "leaders#home",   as: "home_leader"
  patch  "/leader"       => "leaders#update", as: "update_leader"
  patch  "/leader/batch" => "leaders#batch",  as: "batch_leader"

  resources :staffs
  resources :sheets
  resources :doctors

  scope :work do
    resources :duplicate_doctor_lists do
      member do
        patch "merge"
      end
    end
    resources :universities do
      collection do
        patch "merge"
      end
    end
    resources :societies do
      collection do
        patch "merge"
      end
    end
    resources :hospitals do
      collection do
        patch "merge"
      end
    end
    resources :duplicate_hospitals do
      collection do
        patch "merge"
      end
    end
    resources :staff_posts
  end

  root "tops#index"
  get "/strong_logout" => "tops#logout"
  get "/kanri" => "tops#index"
  get "/database_work" => "works#top"
  get "/department_official_category" => "works#department_list_json"
  get "/department_position_names" => "works#department_position_names"
  get "/work_prefecture_select/:id" => "works#prefecture_select"
  get "/work_department_select/:id" => "works#department_select"
  get "/work_department/:id" => "works#department"
  patch "/work_department/:id/list" => "works#update_department_lists", as: "update_work_department_lists"

  get "/plural_doctors_all" => "doctors#plural_doctors_all"
  get "/plural_doctors/:id" => "doctors#plural_doctors"
  get "/typeaheads/university" => "typeaheads#university"
  get "/typeaheads/society" => "typeaheads#society"
  get "/typeaheads/society_position/:society_id" => "typeaheads#society_position"

  resources :specialties, only: :index
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root "welcome#index"

  # Example of regular route:
  #   get "products/:id" => "catalog#view"

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get "products/:id/purchase" => "catalog#purchase", as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get "short"
  #       post "toggle"
  #     end
  #
  #     collection do
  #       get "sold"
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get "recent", on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post "toggle"
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
