#!/bin/bash

cd /myapp

bundle exec ./bin/delayed_job start

bundle exec puma -C ./config/puma.rb
# bundle exec rake jobs:work
