require 'roo'
require 'roo-xls'

class Import::RwExcel
  attr_reader :book, :sheet_info, :file_type, :extensions, :year

  def initialize(filepath, year)
    @book   = open_book(filepath)

    info_sheet  = book.sheet('rw_info')

    if info_sheet && info_sheet.last_row > 1
      @file_type = info_sheet.cell(2, 1)

      @year = year
      if @year == 0
        @year = nil
      end

      @sheet_info = Array.new
      2.upto info_sheet.last_row do | index |
        row = info_sheet.row(index)
        @sheet_info.push(RwExcelSheetInfo.new(row, @book))
      end

      #一度別シートを参照すると取り出したシートも参照したシートになってしまうらしい
      info_sheet  = book.sheet('rw_info')

      if info_sheet.last_column > 4
      	@extensions = Array.new
        5.upto info_sheet.last_column do | index |
          col = info_sheet.column(index).drop(1)
          @extensions.push(col)
        end
      end
    end
  end

  def sheet(name)
      @book.sheet(name)
  end

  def open_book(filepath)
    case File.extname(filepath)
    when '.xlsx', '.ods', '.csv', '.CSV' then
      Roo::Excelx.new(filepath)
    when '.xls' then
      Roo::Excel.new(filepath)
    else
      nil
    end
  end

  #for debug
  def year=(value)
    year = value
  end

  class RwExcelSheetInfo
    attr_reader :name, :start_row, :start_column
    def initialize(row, book)
      @name         = row[1]        if row.count > 1
      @start_row    = row[2].to_i   if row.count > 2 && row[2].present?
      @start_column = row[3].to_cn  if row.count > 3 && row[3].present?
    end
    #ここにシートオブジェクトを持たせることはできない
    #book.sheet(name)はdefault_sheetを切り替え、戻る値はdefault_sheetを参照しているだけのようだ
    #このためbook.sheet(name)をあたらに呼び出すと前のシートの参照先も変わってしまう。
    #また、以下のように毎回名前をセットして取り出すことで上記を回避できると思われたが
    #@book.sheet(name)メソッドはとても重い。
    #def sheet
    #  @book.sheet(@name)
    #end
  end
end
