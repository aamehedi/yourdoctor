class Import::ImportResult
  attr_accessor :total_line_num, :error_line_num, :warning_line_num, :messages
  def initialize
    @total_line_num   = 0
    @error_line_num   = 0
    @warning_line_num = 0
    @messages         = ""
  end
end
