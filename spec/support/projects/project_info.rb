require "rails_helper"
require "spec_helper"

RSpec.shared_examples "project info rendered successfully" do
  context "returns succeed" do
    subject{response}
    it{should be_success}
  end

  context "returns project_info hash" do
    subject{@json["project_info"]}
    it{should be_a Hash}
  end

  context "returns project_info has integer id" do
    subject{@json["project_info"]["id"]}
    it{should be_a Integer}
  end

  context "returns project_info has string name" do
    subject{@json["project_info"]["name"]}
    it{should be_a String}
  end

  context "returns project_info has string project_owner_id" do
    subject{@json["project_info"]["project_owner_id"]}
    it{should be_a String}
  end

  context "returns project_info has string project_owner_name" do
    subject{@json["project_info"]["project_owner_name"]}
    it{should be_a String}
  end

  context "returns project_info has string this_month_request" do
    subject{@json["project_info"]["this_month_request"]}
    it{should be_a String}
  end

  context "returns project_info has string next_month_request" do
    subject{@json["project_info"]["next_month_request"]}
    it{should be_a String}
  end

  context "returns project_info has a array of mrs" do
    subject{@json["project_info"]["mrs"]}
    it{should be_a Array}
  end

  context "returns project_info has all assigned mrs" do
    subject{@json["project_info"]["mrs"].count}
    it{should eq Project.last.mrs.size}
  end
end
