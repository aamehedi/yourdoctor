require "rails_helper"
require "spec_helper"

RSpec.shared_examples "request to be failed" do
  context "returns failure" do
    subject{response}
    it{should_not be_success}
  end
  context "returns errors" do
    subject{@json["error"]}
    it{should be_a String}
  end
end
