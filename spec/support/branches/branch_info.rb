require "rails_helper"

RSpec.shared_examples "branch info rendered successfully" do
  context "returns succeed" do
    subject{response}
    it{should be_success}
  end

  context "returns branch_info hash" do
    subject{@json["branch_info"]}
    it{should be_a Hash}
  end

  context "returns branch_info has integer 'id'" do
    subject{@json["branch_info"]["id"]}
    it{should be_a Integer}
  end

  context "returns branch_info has string 'name'" do
    subject{@json["branch_info"]["name"]}
    it{should be_a String}
  end

  context "returns branch_info has string 'branch_manager_id'" do
    subject{@json["branch_info"]["branch_manager_id"]}
    it{should be_a String}
  end

  context "returns branch_info has string branch_manager_name" do
    subject{@json["branch_info"]["branch_manager_name"]}
    it{should be_a String}
  end

  context "returns branch_info has string this_month_request" do
    subject{@json["branch_info"]["this_month_request"]}
    it{should be_a String}
  end

  context "returns branch_info has string next_month_request" do
    subject{@json["branch_info"]["next_month_request"]}
    it{should be_a String}
  end

  context "returns branch_info has a array of mrs" do
    subject{@json["branch_info"]["mrs"]}
    it{should be_a Array}
  end
end
