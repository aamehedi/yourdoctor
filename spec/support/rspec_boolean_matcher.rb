require "rspec/expectations"

RSpec::Matchers.define :be_boolean do
  match do |actual|
    [true, false].include? actual
  end

  failure_message do |actual|
    "expected that #{actual.inspect} would be a boolean(true or false)"
  end

  failure_message do |actual|
    "expected that #{actual.inspect} would not be a boolean(true or false)"
  end

  description do
    "be a boolean(true or false)"
  end
end
