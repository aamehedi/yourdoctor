require "rails_helper"
require "spec_helper"

RSpec.shared_examples "scenario info rendered successfully" do
  context "returns succeed" do
    subject{response}
    it{should be_success}
  end

  context "returns scenario_info hash" do
    subject{@json["scenario_info"]}
    it{should be_a Hash}
  end

  context "returns scenario_info has integer id" do
    subject{@json["scenario_info"]["id"]}
    it{should be_a Integer}
  end

  context "returns scenario_info has string name" do
    subject{@json["scenario_info"]["name"]}
    it{should be_a String}
  end

  context "returns scenario_info has string description" do
    subject{@json["scenario_info"]["description"]}
    it{should be_a String}
  end

  context "returns scenario_info has bool favourite" do
    subject{@json["scenario_info"]["favorite"]}
    it{should be_boolean}
  end

  context "returns scenario_info has hash mr" do
    subject{@json["scenario_info"]["mr"]}
    it{should be_a Hash}
  end

  context "returns scenario_info has integer mr id" do
    subject{@json["scenario_info"]["mr"]["id"]}
    it{should be_a Integer}
  end

  context "returns scenario_info has string mr name" do
    subject{@json["scenario_info"]["mr"]["name"]}
    it{should be_a String}
  end

  context "returns scenario_info has string mr type" do
    subject{@json["scenario_info"]["mr"]["mr_type"]}
    it{should be_a String}
  end

  context "returns scenario_info has string avatar_string" do
    subject{@json["scenario_info"]["mr"]["avatar_string"]}
    it{should be_a String}
  end

  context "returns scenario_info has array of mr projects" do
    subject{@json["scenario_info"]["mr"]["project_list"]}
    it{should be_a Array}
  end

  context "returns scenario_info has a array of charts" do
    subject{@json["scenario_info"]["chart_list"]}
    it{should be_a Array}
  end

  context "returns scenario_info has all selected charts" do
    subject{@json["scenario_info"]["chart_list"].count}
    it{should eq Scenario.last.charts.size}
  end
end
