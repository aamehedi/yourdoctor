require "rails_helper"
require "spec_helper"

RSpec.shared_examples "chart list in scenario rendered successfully" do
  context "returns chart list of scenario_info hash" do
    subject{@json["scenario_info"]["chart_list"][0]}
    it{should be_a Hash}
  end

  context "returns chart list of scenario_info has integer id" do
    subject{@json["scenario_info"]["chart_list"][0]["id"]}
    it{should be_a Integer}
  end

  context "returns chart list of scenario_info has string name" do
    subject{@json["scenario_info"]["chart_list"][0]["name"]}
    it{should be_a String}
  end

  context "returns chart list of scenario_info has string description" do
    subject{@json["scenario_info"]["chart_list"][0]["description"]}
    it{should be_a String}
  end

  context "returns chart list of scenario_info has bool favourite" do
    subject{@json["scenario_info"]["chart_list"][0]["favorite"]}
    it{should be_boolean}
  end

  context "returns chart list of scenario_info has string
    raw_query_param_values" do
    subject{@json["scenario_info"]["chart_list"][0]["raw_query_param_values"]}
    it{should be_a String}
  end

  context "returns chart list of scenario_info has string
    query_param_values" do
    subject{@json["scenario_info"]["chart_list"][0]["query_param_values"]}
    it{should be_a String}
  end

  context "returns chart list of scenario_info has string
    option" do
    subject{@json["scenario_info"]["chart_list"][0]["option"]}
    it{should be_a String}
  end

  context "returns chart list of scenario_info has integer chart type" do
    subject{@json["scenario_info"]["chart_list"][0]["chart_type"]}
    it{should be_a String}
  end

  context "returns chart list of scenario_info has hash mr" do
    subject{@json["scenario_info"]["chart_list"][0]["mr"]}
    it{should be_a Hash}
  end

  context "return chart list is in desired order" do
    subject{@json["scenario_info"]["chart_list"].pluck "id"}
    it{should eq [3, 1]}
  end
end
