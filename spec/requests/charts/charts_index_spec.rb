# -*- coding: utf-8 -*-
require "rails_helper"
require "spec_helper"

RSpec.describe "Show all charts", type: :request do
  describe "GET /charts" do
    shared_examples_for "request to be succeed" do
      describe "succeed" do
        subject{response}
        it{should be_success}
      end
      describe "returns charts array" do
        subject{@json["charts"]}
        it{should be_a Array}
      end
      describe "charts array has 4 chart objects" do
        subject{@json["charts"].count}
        it{is_expected.to be 4}
      end
      describe "each chart is a hash" do
        4.times do |i|
          subject{@json["charts"][i]}
          it{should be_a Hash}
        end
      end
      describe "each chart has an 'id'" do
        4.times do |i|
          subject{@json["charts"][i]["id"]}
          it{should be_a Integer}
        end
      end
      describe "each chart has a 'name'" do
        4.times do |i|
          subject{@json["charts"][i]["name"]}
          it{should be_a String}
        end
      end
      describe "each chart has a 'favorite' field" do
        4.times do |i|
          subject{@json["charts"][i]["favorite"]}
          it{should be_in [true, false]}
        end
      end
      describe "each chart has a 'chart_type'" do
        4.times do |i|
          subject{@json["charts"][i]["chart_type"]}
          it{should be_a String}
        end
      end
      describe "each chart has 'raw_query_param_values'" do
        4.times do |i|
          subject{@json["charts"][i]["raw_query_param_values"]}
          it{should be_a String}
        end
      end
      describe "each chart has 'query_param_values'" do
        4.times do |i|
          subject{@json["charts"][i]["query_param_values"]}
          it{should be_a String}
        end
      end
      describe "each chart has 'option'" do
        4.times do |i|
          subject{@json["charts"][i]["option"]}
          it{should be_a String}
        end
      end
      describe "each chart has an 'mr' hash" do
        4.times do |i|
          subject{@json["charts"][i]["mr"]}
          it{should be_a Hash}
        end
      end
    end
    shared_examples_for "request to be failed" do
      describe "succeed" do
        subject{response}
        it{should_not be_success}
      end
      describe "returns errors" do
        subject{@json["error"]}
        it{should be_a String}
      end
    end

    let(:endpoint){"/charts"}

    describe "Success request with valid access_token" do
      before do
        Mr.login 1, nil
        get endpoint, nil, "Authorization" => MrSession.first.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be succeed"
    end

    describe "Failed request with invalid access_token" do
      before do
        get endpoint, nil, "Authorization" => "Wrong access_token"
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end
  end
end
