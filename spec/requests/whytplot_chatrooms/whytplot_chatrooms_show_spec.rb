# -*- coding: utf-8 -*-
require "rails_helper"
require "spec_helper"

RSpec.describe "Show a Whytplot Chatroom", :type => :request do

  describe "GET whytplot_chatrooms/:id" do
    shared_examples_for "request to be succeed" do
      describe "succeed" do
        subject{response}
        it{should be_success}
      end
      describe "returns whytplot_chatroom info" do
        subject{@json["whytplot_chatroom"]}
        it{should be_a Hash}
      end
      describe "whytplot_chatroom has id" do
        subject{@json["whytplot_chatroom"]["id"]}
        it{should be_a Integer}
      end
      describe "whytplot_chatroom has right id" do
        subject{@json["whytplot_chatroom"]["id"]}
        it{is_expected.to be 1}
      end
      describe "whytplot_chatroom has info about unread messages of current mrs" do
        subject{@json["whytplot_chatroom"]["unread_messages"]}
        it{should be_a Integer}
      end
      describe "whytplot_chatroom contains mrs" do
        subject {@json["whytplot_chatroom"]["whytplot_mr_chatrooms"]}
        it {should be_a Array}
      end
      describe "whytplot_chatroom contains 2 mrs" do
        subject {@json["whytplot_chatroom"]["whytplot_mr_chatrooms"].count}
        it {is_expected.to be 2}
      end
      describe "mrs have ids" do
        2.times do |i|
          subject {@json["whytplot_chatroom"]["whytplot_mr_chatrooms"][i]["id"]}
          it {should be_a Integer}
        end
      end
      describe "mrs have names" do
        2.times do |i|
          subject {@json["whytplot_chatroom"]["whytplot_mr_chatrooms"][i]["mr_name"]}
          it {should be_a String}
        end
      end
      describe "mrs have avatars" do
        2.times do |i|
          subject {@json["whytplot_chatroom"]["whytplot_mr_chatrooms"][i]["mr_avatar"]}
          it {should be_a String}
        end
      end
    end
    shared_examples_for "request to be failed" do
      describe "returns errors" do
        subject{@json["error"]}
        it{should be_a String}
      end
    end

    describe "Success request with valid access_token" do
      before do
        Mr.login 2, nil
        get "/mrs/whytplot_chatrooms/1", nil, {"Authorization" => MrSession.last.access_token}
        @json = JSON.parse(response.body)
      end
      it_behaves_like "request to be succeed"
    end

    describe "Failed request with wrong id" do
      before do
        Mr.login 2, nil
        get "/mrs/whytplot_chatrooms/100", nil, {"Authorization" => MrSession.last.access_token}
        @json = JSON.parse(response.body)
      end
      it_behaves_like "request to be failed"
    end
  end
end
