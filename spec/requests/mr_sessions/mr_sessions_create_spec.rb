# -*- coding: utf-8 -*-
require "rails_helper"
require "spec_helper"

RSpec.describe "Mrs login", :type => :request do

  describe "POST /mr_sessions/create" do
    shared_examples_for "request to be succeed" do
      describe "succeed" do
        subject{response}
        it{should be_success}
      end
      describe "returns mr_session" do
        subject{@json["mr_session"]}
        it{should be_a Hash}
      end
      describe "mr_session has 2 fields" do
        subject {@json["mr_session"].keys.count}
        it {is_expected.to be 2}
      end
      describe "returns access_token" do
        subject{@json["mr_session"]["access_token"]}
        it{should be_a String}
      end
      describe "returns mr hash" do
        subject{@json["mr_session"]["mr"]}
        it{should be_a Hash}
      end
      describe "mr hash has 11 fields" do
        subject {@json["mr_session"]["mr"].keys.count}
        it{is_expected.to be 11}
      end
      describe "returns id" do
        subject{@json["mr_session"]["mr"]["id"]}
        it{should be_a Integer}
      end
      describe "returns right email" do
        subject{@json["mr_session"]["mr"]["email"]}
        it{is_expected.to eq "mr1@reasonwhy.com"}
      end
      describe "returns right name" do
        subject{@json["mr_session"]["mr"]["name"]}
        it{is_expected.to eq "Staff 1"}
      end
      describe "returns mr_type" do
        subject{@json["mr_session"]["mr"]["mr_type"]}
        it{should be_a String}
      end
      describe "returns right mr_type" do
        subject{@json["mr_session"]["mr"]["mr_type"]}
        it{is_expected.to eq "company_manager"}
      end
      describe "returns avatar_string" do
        subject{@json["mr_session"]["mr"]["avatar_string"]}
        it{should be_a String}
      end
      describe "returns created_at" do
        subject{@json["mr_session"]["mr"]["created_at"]}
        it{should be_a String}
      end
      describe "returns updated_at" do
        subject{@json["mr_session"]["mr"]["updated_at"]}
        it{should be_a String}
      end
      describe "returns projects list" do
        subject{@json["mr_session"]["mr"]["project_list"]}
        it{should be_a Array}
      end
      describe "returns company hash" do
        subject{@json["mr_session"]["mr"]["company"]}
        it{should be_a Hash}
      end
      describe "company hash should have 3 fields" do
        subject{@json["mr_session"]["mr"]["company"].keys.count}
        it{is_expected.to be 3}
      end
      describe "company hash should have an id" do
        subject{@json["mr_session"]["mr"]["company"]["id"]}
        it{should be_a String}
      end
      describe "company hash should have a 'using_chat' field" do
        subject{@json["mr_session"]["mr"]["company"]["using_chat"]}
        it{should be_in [true, false]}
      end
      describe "company hash should have a name" do
        subject{@json["mr_session"]["mr"]["company"]["name"]}
        it{should be_a String}
      end
    end
    shared_examples_for "request to be failed" do
      describe "succeed" do
        subject{response}
        it{should_not be_success}
      end
      describe "returns errors" do
        subject{@json["error"]}
        it{should be_a String}
      end
    end
    let(:endpoint){"/mr_sessions/create"}
    let :info_with_device_token do
      {
        "mr_info":
        {
          "email":"mr1@reasonwhy.com",
          "password":"123456789",
          "device_token":"POSTMAN"
        }
      }
    end
    let :info_without_device_token do
      {
        "mr_info":
        {
          "email":"mr1@reasonwhy.com",
          "password":"123456789"
        }
      }
    end
    let :wrong_info_1 do
      {
        "mr_info":
        {
          "email":"mr1@reasonwhy.com",
          "password":"wrong_password"
        }
      }
    end
    let :wrong_info_2 do
      {
        "mr_info":
        {
          "email":"wrong@email.com",
          "password":"123456789"
        }
      }
    end

    describe "Success request with device_token" do
      before do
        post endpoint, params: info_with_device_token
        @json = JSON.parse(response.body)
      end
      it_behaves_like "request to be succeed"
    end
    describe "Success request without device_token" do
      before do
        post endpoint, params: info_without_device_token
        @json = JSON.parse(response.body)
      end
      it_behaves_like "request to be succeed"
    end
    describe "Eroor request with wrong information" do
      before do
        post endpoint, params: wrong_info_1
        @json = JSON.parse(response.body)
      end
      it_behaves_like "request to be failed"
    end
    describe "Eroor request with wrong information" do
      before do
        post endpoint, params: wrong_info_1
        @json = JSON.parse(response.body)
      end
      it_behaves_like "request to be failed"
    end
  end
end
