# -*- coding: utf-8 -*-
require "rails_helper"
require "spec_helper"

RSpec.describe "Mrs login", :type => :request do

  describe "GET /mrs" do
    shared_examples_for "request to be succeed" do
      describe "succeed" do
        subject{response}
        it{should be_success}
      end
      describe "returns mrs array" do
        subject{@json["mrs"]}
        it{should be_a Array}
      end
      describe "mrs array has 4 elements" do
        subject {@json["mrs"].count}
        it {is_expected.to be 4}
      end
      describe "each mr is 1 hash" do
        4.times do |i|
          subject {@json["mrs"][i]}
          it {should be_a Hash}
        end
      end
      describe "each mr has 1 id" do
        4.times do |i|
          subject {@json["mrs"][i]["id"]}
          it {should be_a Integer}
        end
      end
      describe "each mr has 1 name" do
        4.times do |i|
          subject {@json["mrs"][i]["name"]}
          it {should be_a String}
        end
      end
    end
    shared_examples_for "request to be failed" do
      describe "succeed" do
        subject{response}
        it{should_not be_success}
      end
      describe "returns errors" do
        subject{@json["error"]}
        it{should be_a String}
      end
    end
    let(:endpoint){"/mrs"}

    describe "Success request with valid access_token" do
      before do
        Mr.login 1, nil
        get endpoint, nil, {"Authorization" => MrSession.first.access_token}
        @json = JSON.parse(response.body)
      end
      it_behaves_like "request to be succeed"
    end

    describe "Failed request with invalid access_token" do
      before do
        get endpoint, nil, {"Authorization" => "Wrong access_token"}
        @json = JSON.parse(response.body)
      end
      it_behaves_like "request to be failed"
    end
  end
end
