# -*- coding: utf-8 -*-
require "rails_helper"
require "spec_helper"

RSpec.describe "Show a Company info", :type => :request do

  describe "GET /companies/:id" do
    shared_examples_for "request to be succeed" do
      describe "succeed" do
        subject{response}
        it{should be_success}
      end
      describe "returns about_company info" do
        subject{@json["about_company"]}
        it{should be_a Hash}
      end
      describe "returns company name" do
        subject{@json["about_company"]["name"]}
        it{should be_a String}
      end
      describe "returns using_chat" do
        subject{@json["about_company"]["using_chat"]}
        it{is_expected.to be true}
      end
      describe "returns number_of_request info" do
        subject{@json["number_of_request"]}
        it{should be_a Hash}
      end
      describe "returns number_of_user info" do
        subject{@json["number_of_user"]}
        it{should be_a Hash}
      end

    end
    shared_examples_for "request to be failed" do
      describe "returns errors" do
        subject{@json["error"]}
        it{should be_a String}
      end
    end

    shared_examples_for "invalid mr access_token" do
      describe "returns errors" do
        subject{@json["error"]}
        it{should be_a String}
      end
      describe "returns right error_code" do
        subject{@json["error_code"]}
        it{is_expected.to be 600}
      end
    end

    describe "Success request with valid access_token" do
      before do
        Mr.login 1, nil
        get "/companies/1", nil, {"Authorization" => MrSession.last.access_token}
        @json = JSON.parse(response.body)
      end
      it_behaves_like "request to be succeed"
    end

    describe "Success request with wrong id" do
      before do
        Mr.login 1, nil
        get "/companies/2", nil, {"Authorization" => MrSession.last.access_token}
        @json = JSON.parse(response.body)
      end
      it_behaves_like "request to be failed"
    end

    describe "Failed request with wrong access_token" do
      before do
        Mr.login 2, nil
        get "/companies/1", nil, {"Authorization" => "Wrong access_token"}
        @json = JSON.parse(response.body)
      end
      it_behaves_like "request to be failed"
      it_behaves_like "invalid mr access_token"
    end
  end
end
