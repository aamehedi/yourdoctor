# -*- coding: utf-8 -*-
require "rails_helper"
require "spec_helper"

RSpec.describe "Create project", type: :request do
  describe "POST /projects" do
    let(:endpoint){"/projects"}

    describe "Success request without optional param" do
      before do
        Mr.login 1, nil
        Project.destroy_all
        params = {
          project_info: {
            name: "reasonwhy",
            mr_id: 3
          }
        }
        post endpoint, params, Authorization: MrSession.first.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "project info rendered successfully"
    end

    describe "Success request with optional param" do
      before do
        Mr.login 1, nil
        Project.destroy_all
        params = {
          project_info: {
            name: "reasonwhy",
            mr_id: 3,
            this_month_request: 20,
            mr_ids: [1, 2]
          }
        }
        post endpoint, params, Authorization: MrSession.first.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "project info rendered successfully"
    end

    describe "Failed request with unauthorized access_token" do
      before do
        Mr.login 2, nil
        params = {
          project_info: {
            name: "reasonwhy",
            mr_id: 3
          }
        }
        post "/projects", params, Authorization: MrSession.last.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end

    describe "Failed request with no name" do
      before do
        params = {
          project_info: {
            this_month_request: 20,
            mr_id: 3
          }
        }
        post "/projects", params, Authorization: MrSession.first.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end

    describe "Failed request with no mr" do
      before do
        params = {
          project_info: {
            name: "asdf",
            this_month_request: 20
          }
        }
        post "/projects", params, Authorization: MrSession.first.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end

    describe "Failed request with invalid access_token" do
      before do
        params = {
          project_info: {
            name: "reasonwhy",
            mr_id: 3
          }
        }
        post "/projects", params, Authorization: "Wrong access token"
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end
  end
end
