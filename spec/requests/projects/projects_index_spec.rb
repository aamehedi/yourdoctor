# -*- coding: utf-8 -*-
require "rails_helper"
require "spec_helper"

RSpec.describe "Show all projects", type: :request do
  describe "GET /projects" do
    shared_examples_for "request to be succeed" do
      describe "succeed" do
        subject{response}
        it{should be_success}
      end
      describe "returns projects array" do
        subject{@json["projects"]}
        it{should be_a Array}
      end
      describe "projects array has 3 elements" do
        subject{@json["projects"].count}
        it{is_expected.to be 3}
      end
      describe "each project is 1 hash" do
        3.times do |i|
          subject{@json["projects"][i]}
          it{should be_a Hash}
        end
      end
      describe "each project has 1 id" do
        3.times do |i|
          subject{@json["projects"][i]["id"]}
          it{should be_a Integer}
        end
      end
      describe "each project has 1 name" do
        3.times do |i|
          subject{@json["projects"][i]["name"]}
          it{should be_a String}
        end
      end

      describe "each project has 1 project_owner_id" do
        3.times do |i|
          subject{@json["projects"][i]["project_owner_id"]}
          it{should be_a String}
        end
      end

      describe "each project has 1 project_owner_name" do
        3.times do |i|
          subject{@json["projects"][i]["project_owner_name"]}
          it{should be_a String}
        end
      end

      describe "each project has 1 this_month_request" do
        3.times do |i|
          subject{@json["projects"][i]["this_month_request"]}
          it{should be_a String}
        end
      end

      describe "each project has 1 mrs" do
        3.times do |i|
          subject{@json["projects"][i]["mrs"]}
          it{should be_a Array}
        end
      end

      describe "each mr in project is a hash" do
        3.times do |i|
          subject{@json["projects"][i]["mrs"][0]}
          it{should be_a Hash}
        end
      end

      describe "each mr in project has 1 id" do
        3.times do |i|
          subject{@json["projects"][i]["mrs"][0]["id"]}
          it{should be_a Integer}
        end
      end

      describe "each mr in project has 1 name" do
        3.times do |i|
          subject{@json["projects"][i]["mrs"][0]["name"]}
          it{should be_a String}
        end
      end

      describe "each mr in project has 1 mr_type" do
        3.times do |i|
          subject{@json["projects"][i]["mrs"][0]["mr_type"]}
          it{should be_a String}
        end
      end

      describe "each mr in project has 1 avatar_string" do
        3.times do |i|
          subject{@json["projects"][i]["mrs"][0]["avatar_string"]}
          it{should be_a String}
        end
      end

      describe "each mr in project has 1 project_list" do
        3.times do |i|
          subject{@json["projects"][i]["mrs"][0]["project_list"]}
          it{should be_a Array}
        end
      end
    end

    shared_examples_for "request to be failed" do
      describe "succeed" do
        subject{response}
        it{should_not be_success}
      end
      describe "returns errors" do
        subject{@json["error"]}
        it{should be_a String}
      end
    end
    let(:endpoint){"/projects"}

    describe "Success request with valid access_token" do
      before do
        Mr.login 1, nil
        get endpoint, nil, "Authorization" => MrSession.first.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be succeed"
    end

    describe "Failed request with unauthorized access_token" do
      before do
        Mr.login 2, nil
        get endpoint, nil, "Authorization" => MrSession.last.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end

    describe "Failed request with invalid access_token" do
      before do
        get endpoint, nil, "Authorization" => "Wrong access_token"
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end
  end
end
