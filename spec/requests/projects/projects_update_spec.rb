# -*- coding: utf-8 -*-
require "rails_helper"
require "spec_helper"

RSpec.describe "Update project", type: :request do
  describe "PATCH /projects/:id" do
    let(:endpoint){"/projects/3"}
    let(:project){Project.find 3}

    describe "Success request without some param" do
      before do
        Mr.login 1, nil
        params = {
          project_info: {
            name: "reasonwhy",
            mr_id: 3
          }
        }
        patch endpoint, params, Authorization: MrSession.first.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "project info rendered successfully"

      context "project updated successfully" do
        it{expect(project.name).to eq "reasonwhy"}
        it{expect(project.mr_id).to eq 3}
      end
    end

    describe "Success request with all param" do
      before do
        Mr.login 1, nil
        params = {
          project_info: {
            name: "asdf",
            mr_id: 5,
            this_month_request: 20,
            mr_ids: [1, 2]
          }
        }
        patch endpoint, params, Authorization: MrSession.first.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "project info rendered successfully"

      context "should project update successfully" do
        it{expect(project.name).to eq "asdf"}
        it{expect(project.mr_id).to eq 5}
        it{expect(project.this_month_request).to eq 20}
        it{expect(project.mrs.ids.sort).to eq [1, 2]}
        it{expect(project.whytplot_chatroom.mrs.ids.sort).to eq [1, 2]}
      end
    end

    describe "Success request with all param and different mr ids" do
      before do
        Mr.login 1, nil
        params = {
          project_info: {
            mr_ids: [1, 3]
          }
        }
        patch endpoint, params, Authorization: MrSession.first.access_token
      end
      it{expect(project.mrs.ids.include? 2).to be_falsy}
      it{expect(project.mrs.ids.include? 3).to be_truthy}
    end

    describe "Failed request with invalid data" do
      before do
        Mr.login 1, nil
        params = {
          project_info: {
            name: "1"
          }
        }
        patch endpoint, params, Authorization: MrSession.first.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end

    describe "Failed request with unauthorized access_token" do
      before do
        Mr.login 2, nil
        params = {
          project_info: {
            this_month_request: 20
          }
        }
        patch endpoint, params, Authorization: MrSession.last.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end

    describe "Failed request with invalid access_token" do
      before do
        params = {
          project_info: {
            mr_id: 4
          }
        }
        patch endpoint, params, Authorization: "Wrong access token"
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end
  end
end
