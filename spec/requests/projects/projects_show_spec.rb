# -*- coding: utf-8 -*-
require "rails_helper"
require "spec_helper"

RSpec.describe "Show a Project Info", type: :request do
  let(:endpoint){"projects/3"}
  let(:project){Project.find 3}

  describe "GET /projects/:id" do
    shared_examples_for "request to be succeed" do
      describe "succeed" do
        subject{response}
        it{should be_success}
      end
      describe "returns a project" do
        subject{@json["project"]}
        it{should be_a Hash}
      end
      describe "Project has a id" do
        subject{@json["project"]["id"]}
        it{should be_a Integer}
      end
      describe "each project has 1 name" do
        subject{@json["project"]["name"]}
        it{should be_a String}
      end
      describe "Project has a project_owner_id" do
        subject{@json["project"]["project_owner_id"]}
        it{should be_a String}
      end
      describe "Project has a project_owner_name" do
        subject{@json["project"]["project_owner_name"]}
        it{should be_a String}
      end
      describe "each project has 1 this_month_request" do
        subject{@json["project"]["this_month_request"]}
        it{should be_a String}
      end
      describe "Project has a mrs" do
        subject{@json["project"]["mrs"]}
        it{should be_a Array}
      end
      describe "mr in project is a hash" do
        subject{@json["project"]["mrs"][0]}
        it{should be_a Hash}
      end
      describe "mr in project has a id" do
        subject{@json["project"]["mrs"][0]["id"]}
        it{should be_a Integer}
      end
      describe "mr in project has a name" do
        subject{@json["project"]["mrs"][0]["name"]}
        it{should be_a String}
      end
      describe "mr in project has a mr_type" do
        subject{@json["project"]["mrs"][0]["mr_type"]}
        it{should be_a String}
      end
      describe "mr in project has a avatar_string" do
        subject{@json["project"]["mrs"][0]["avatar_string"]}
        it{should be_a String}
      end
      describe "mr in project has a project_list" do
        subject{@json["project"]["mrs"][0]["project_list"]}
        it{should be_a Array}
      end
    end

    shared_examples_for "request to be failed" do
      describe "succeed" do
        subject{response}
        it{should_not be_success}
      end
      describe "returns errors" do
        subject{@json["error"]}
        it{should be_a String}
      end
    end

    let(:endpoint){"/projects/3"}

    describe "Success request with valid access_token" do
      before do
        Mr.login 1, nil
        get endpoint, nil, "Authorization" => MrSession.first.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be succeed"
    end

    describe "Failed request with unauthorized access_token" do
      before do
        Mr.login 2, nil
        get endpoint, nil, "Authorization" => MrSession.last.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end

    describe "Failed request with invalid access_token" do
      before do
        get endpoint, nil, "Authorization" => "Wrong access_token"
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end
  end
end
