require "rails_helper"

RSpec.describe "Create scenario", type: :request do
  describe "POST /scenarios" do
    let(:endpoint){"/scenarios"}
    before :all do
      Mr.login 1, nil
      @auth_token = MrSession.first.access_token
    end

    describe "Success request without optional param" do
      before do
        params = {
          scenario_info: {
            name: "slide 1"
          }
        }
        post endpoint, params, Authorization: @auth_token
        @json = JSON.parse response.body
      end
      it_behaves_like "scenario info rendered successfully"
    end

    describe "Success request with all param" do
      before do
        params = {
          scenario_info: {
            name: "slide 2",
            description: "fantabulastic",
            favorite: true,
            scenario_charts_attributes: [
              {chart_id: 1, chart_order: 2},
              {chart_id: 3, chart_order: 1}
            ]
          }
        }
        post endpoint, params, Authorization: @auth_token
        @json = JSON.parse response.body
      end
      it_behaves_like "scenario info rendered successfully"

      it_behaves_like "chart list in scenario rendered successfully"
    end

    describe "Failed request with no name" do
      before do
        params = {
          scenario_info: {
            description: "good",
            favourite: true
          }
        }
        post endpoint, params, Authorization: @auth_token
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end

    describe "Failed request with invalid access_token" do
      before do
        params = {
          scenario_info: {
            name: "Slide 5",
            mr_id: 1
          }
        }
        post endpoint, params, Authorization: "Wrong Access Token"
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end
  end
end
