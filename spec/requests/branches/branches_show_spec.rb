require "rails_helper"
require "spec_helper"

RSpec.describe "Show a branch", type: :request do
  before :all do
    FactoryGirl.create :branch
  end
  let(:endpoint){"/branches/1"}

  describe "GET /branches/:id" do
    shared_examples_for "request to be succeed" do
      context "succeed" do
        subject{response}
        it{should be_success}
      end
      describe "returns a branch" do
        subject{@json["branch"]}
        it{should be_a Hash}
      end
      describe "branch has an 'id'" do
        subject{@json["branch"]["id"]}
        it{should be_a Integer}
      end
      describe "branch has a 'name'" do
        subject{@json["branch"]["name"]}
        it{should be_a String}
      end
      describe "branch has a 'this_month_request' field" do
        subject{@json["branch"]["this_month_request"]}
        it{should be_a String}
      end
      describe "branch has a 'next_month_request' field" do
        subject{@json["branch"]["next_month_request"]}
        it{should be_a String}
      end
      describe "branch has an 'mrs' array" do
        subject{@json["branch"]["mrs"]}
        it{should be_a Array}
      end
    end

    shared_examples_for "request to be failed" do
      describe "succeed" do
        subject{response}
        it{should_not be_success}
      end
      describe "returns errors" do
        subject{@json["error"]}
        it{should be_a String}
      end
    end

    describe "Success request with valid access_token" do
      before do
        Mr.login 1, nil
        get endpoint, nil, "Authorization" => MrSession.first.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be succeed"
    end

    describe "Failed request with invalid access_token" do
      before do
        get endpoint, nil, "Authorization" => "Wrong access_token"
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end
  end
end
