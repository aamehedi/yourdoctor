# -*- coding: utf-8 -*-
require "rails_helper"

RSpec.describe "Create branch", type: :request do
  describe "POST /branches" do
    let(:endpoint){"/branches"}
    let(:params){{branch_info: {name: "Dhaka"}}}

    describe "Successful request with minimam number of params" do
      before do
        Mr.login 1, nil
        Branch.destroy_all
        post endpoint, params, Authorization: MrSession.first.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "branch info rendered successfully"
    end

    describe "Successful request with all param" do
      before do
        Mr.login 1, nil
        Branch.destroy_all
        params = {
          branch_info: {
            name: "Dhaka",
            mr_id: 1,
            this_month_request: 20,
            next_month_request: 10,
            mr_ids: [1, 2]
          }
        }
        post endpoint, params, Authorization: MrSession.first.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "branch info rendered successfully"
    end

    describe "Failed request with no name" do
      before do
        post "/branches", params, Authorization: MrSession.first.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end

    describe "Failed request with unauthorized access_token" do
      before do
        Mr.login 2, nil
        post "/branches", params, Authorization: MrSession.last.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end

    describe "Failed request with invalid access_token" do
      before do
        post "/branches", params, Authorization: "Wrong access token"
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end
  end
end
