# -*- coding: utf-8 -*-
require "rails_helper"

RSpec.describe "Update branch", type: :request do
  before :all do
    FactoryGirl.create :branch
  end

  describe "PATCH /branches/:id" do
    let(:endpoint){"/branches/1"}
    let(:branch){Branch.find 1}
    let(:params) do
      {
        branch_info: {
          name: "Bolod Nogor",
          mr_id: 1,
          mr_ids: [1, 3],
          this_month_request: 2,
          next_month_request: 1
        }
      }
    end

    describe "Successfully update branch info" do
      before do
        Mr.login 1, nil
        patch endpoint, params, Authorization: MrSession.first.access_token
        @json = JSON.parse response.body
      end

      it{expect(branch.name).to eq params[:branch_info][:name]}
      it{expect(branch.mr_id).to eq params[:branch_info][:mr_id]}
      it{expect(branch.mrs.ids.sort).to eq params[:branch_info][:mr_ids]}
      it_behaves_like "branch info rendered successfully"

      it "returns correct value of 'this_month_request'" do
        expect(branch.this_month_request).to eq params[
          :branch_info][:this_month_request]
      end

      it "returns correct value of 'next_month_request'" do
        expect(branch.next_month_request).to eq params[
          :branch_info][:next_month_request]
      end

      it "returns correct mr_ids" do
        expect(branch.whytplot_chatroom.mrs.ids.sort).to eq params[
          :branch_info][:mr_ids]
      end
    end

    describe "Failed request with invalid access_token" do
      before do
        patch endpoint, params, Authorization: "Wrong access token"
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end

    describe "Failed request with unauthorized access_token" do
      before do
        Mr.login 2, nil
        patch endpoint, params, Authorization: MrSession.last.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end
  end
end
