# -*- coding: utf-8 -*-
require "rails_helper"
require "spec_helper"

RSpec.describe "Show all branches", type: :request do
  before :all do
    2.times{FactoryGirl.create :branch}
  end
  describe "GET /branches" do
    shared_examples_for "request to be succeed" do
      context "succeed" do
        subject{response}
        it{should be_success}
      end
      describe "returns branches array" do
        subject{@json["branches"]}
        it{should be_a Array}
      end
      describe "branches array has 2 branch objects" do
        subject{@json["branches"].count}
        it{is_expected.to be 2}
      end
      describe "each branch is a hash" do
        2.times do |i|
          subject{@json["branches"][i]}
          it{should be_a Hash}
        end
      end
      describe "each branch has an 'id'" do
        2.times do |i|
          subject{@json["branches"][i]["id"]}
          it{should be_a Integer}
        end
      end
      describe "each branch has a 'name'" do
        2.times do |i|
          subject{@json["branches"][i]["name"]}
          it{should be_a String}
        end
      end
      describe "each branch has a 'this_month_request' field" do
        2.times do |i|
          subject{@json["branches"][i]["this_month_request"]}
          it{should be_a String}
        end
      end
      describe "each branch has a 'next_month_request' field" do
        2.times do |i|
          subject{@json["branches"][i]["next_month_request"]}
          it{should be_a String}
        end
      end
      describe "each branch has an 'mrs' array" do
        2.times do |i|
          subject{@json["branches"][i]["mrs"]}
          it{should be_a Array}
        end
      end
    end
    shared_examples_for "request to be failed" do
      describe "succeed" do
        subject{response}
        it{should_not be_success}
      end
      describe "returns errors" do
        subject{@json["error"]}
        it{should be_a String}
      end
    end

    let(:endpoint){"/branches"}

    describe "Success request with valid access_token" do
      before do
        Mr.login 1, nil
        get endpoint, nil, "Authorization" => MrSession.first.access_token
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be succeed"
    end

    describe "Failed request with invalid access_token" do
      before do
        get endpoint, nil, "Authorization" => "Wrong access_token"
        @json = JSON.parse response.body
      end
      it_behaves_like "request to be failed"
    end
  end
end
