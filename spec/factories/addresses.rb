# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :address do
    city "MyString"
    city_kana "MyString"
    town "MyString"
    town_kana "MyString"
    postal_code "MyString"
    local_government_code 1
    city1 "MyString"
    city2 "MyString"
    full "MyString"
    full_kana "MyString"
    prefecture nil
  end
end
