# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :society_position_list do
    doctor nil
    society_position nil
  end
end
