# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :number_by_code do
    number 1
    type ""
    costly_comorbidity "MyString"
    index 1
    ag_by_mdc nil
  end
end
