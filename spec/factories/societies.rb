# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :society do
    name "MyText"
    english_name "MyText"
    address "MyText"
    email "MyText"
  end
end
