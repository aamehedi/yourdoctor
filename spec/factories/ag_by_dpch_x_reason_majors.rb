# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :ag_by_dpch_x_reason_major do
    same_reason "9.99"
    different_reason "9.99"
    same_reason_nonchemo "9.99"
    different_reason_nonchemo "9.99"
    reason_for_rehospitalization nil
    dpc_hospital nil
  end
end
