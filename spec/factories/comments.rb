# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :comment do
    doctor nil
    content "MyText"
    post nil
  end
end
