# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :regimen_regimen_ranking do
    no 1
    cases_num 1
    cases_ratio "9.99"
    cases_cumulative_ratio "9.99"
    hospitals_num 1
    hospitals_ratio "9.99"
    average_hospital_days "9.99"
    regimen "MyString"
    ag_by_mdc_l1 nil
  end
end
