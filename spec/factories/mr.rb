# -*- coding: utf-8 -*-
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :mr do
    sequence(:first_name){|n| n}
    last_name "Staff"
    sequence(:email){|n| "mr#{n}@reasonwhy.com"}
    password "123456789"
    password_confirmation "123456789"
    company_id 1
    sequence(:mr_type){|n| n==1 ? 0 : 3}
  end
end
