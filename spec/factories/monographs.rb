# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :monograph do
    title "MyString"
    year 1
    description "MyText"
    url "MyText"
  end
end
