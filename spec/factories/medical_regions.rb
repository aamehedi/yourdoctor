# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :medical_region do
    name "MyString"
    level 1
    code "MyString"
    parent_id 1
    children_id 1
  end
end
