# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :ag_by_mdc do
    code "MyString"
    inclusion false
    dpc_num_for_the_mdc 1
    cases_num_for_the_mdc 1
    number 1
    dpc_ratio_for_all_cases ""
    male_num 1
    female_num 1
    from0to2_num 1
    from3to5_num 1
    from6to15_num 1
    from16to20_num 1
    from21to40_num 1
    from41to60_num 1
    from61to79_num 1
    over80_num 1
    introduction_num 1
    outpatient_num 1
    transported_num 1
    unexpected_hospitalization_num 1
    emergency_hospitalization_num 1
    status_healed 1
    status_improvement 1
    status_palliation 1
    status_unchanging 1
    status_worse 1
    status_death_of_illness_costed_most 1
    status_death_of_other_illness_costed_most 1
    status_other 1
    hospital_days_average 1
    hospital_days_minimum 1
    hospital_days_maximum 1
    hospital_days_coefficient_of_variation 1
    hospital_days_value_of_25percentile 1
    hospital_days_value_of_50percentile 1
    hospital_days_value_of_75percentile 1
    hospital_days_value_of_90percentile 1
    artificial_respiration 1
    artificial_kidney 1
    central_venous_injection 1
    blood_transfusion 1
    mdc_layer3 nil
  end
end
