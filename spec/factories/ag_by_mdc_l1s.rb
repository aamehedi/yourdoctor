# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :ag_by_mdc_l1 do
    drugs_num 1
    regimens_num 1
    cases_num 1
    hospitals_num 1
    mdc_layer1 nil
  end
end
