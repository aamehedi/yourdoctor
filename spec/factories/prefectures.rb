# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :prefecture do
    name "MyString"
    area nil
  end
end
