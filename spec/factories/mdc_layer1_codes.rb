# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :mdc_layer1_code do
    year 1
    code "MyString"
    name "MyString"
  end
end
