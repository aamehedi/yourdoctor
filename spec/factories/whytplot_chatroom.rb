# -*- coding: utf-8 -*-
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :whytplot_chatroom do
    name "Test WhytplotChatroom"
    chatroom_type 0
  end
end
