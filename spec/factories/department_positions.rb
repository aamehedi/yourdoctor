# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :department_position do
    name "MyString"
    department nil
  end
end
