# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :medical_municipality do
    name "MyString"
    code 1
    medical_region_code "MyString"
  end
end
