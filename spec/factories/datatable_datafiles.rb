# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :datatable_datafile do
    datable_id 1
    datable_type "MyString"
    datafile nil
  end
end
