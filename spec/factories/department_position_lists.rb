# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :department_position_list do
    department_list nil
    department_position nil
  end
end
