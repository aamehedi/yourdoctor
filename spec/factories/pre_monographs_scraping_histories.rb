# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :pre_monographs_scraping_history do
    doctor_id 1
    created_by_id 1
    completed_at "2016-03-17 18:29:54"
    source 1
  end
end
