FactoryGirl.define do
  factory :doctor do
    transient do
      doctor_first_name{Faker::Name.first_name}
      doctor_password{Faker::Internet.password 8}
    end

    sequence(:id){|n| n}
    last_name{Faker::Name.last_name}
    access_token{|n| "#{n}:#{Faker::Internet.password 20}"}

    after :build do |doctor, evaluator|
      doctor.first_name = evaluator.doctor_first_name
      doctor.email = "#{evaluator.doctor_first_name}@reasonwhy.jp"
      doctor.password = evaluator.doctor_password
      doctor.password_confirmation = evaluator.doctor_password
    end
  end
end
