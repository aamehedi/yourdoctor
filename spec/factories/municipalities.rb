# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :municipalities do
    code 1
    prefecture_name "MyString"
    city_name "MyString"
    prefecture_name_kana "MyString"
    city_name_kana "MyString"
    is_ordinance_designated_city false
  end
end
