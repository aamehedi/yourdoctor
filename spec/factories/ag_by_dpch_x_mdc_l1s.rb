# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :ag_by_dpch_x_mdc_l1 do
    number 1
    days "9.99"
    label "MyString"
    is_operated false
    dpc_hospital nil
    mdc_layer1 nil
  end
end
