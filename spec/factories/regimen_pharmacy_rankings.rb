# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :regimen_pharmacy_ranking do
    no 1
    regimens_num 1
    cases_num 1
    hospitals_num 1
    regimens_ratio "9.99"
    cases_ratio "9.99"
    hospitals_ratio "9.99"
    ag_by_mdc_l1 nil
  end
end
