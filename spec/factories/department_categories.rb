# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :department_category do
    name "MyString"
    code "MyString"
    comment "MyText"
  end
end
