# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :mail_log do
    doctor nil
    all_mail nil
    done false
  end
end
