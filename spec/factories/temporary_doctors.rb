# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :temporary_doctor do
    email "MyString"
    password "MyString"
    registration_number "MyString"
    first_name "MyString"
    last_name "MyString"
    uuid "MyString"
  end
end
