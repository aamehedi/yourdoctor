# -*- coding: utf-8 -*-
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :whytplot_mr_chatroom do
    whytplot_chatroom_id 1
    sequence(:mr_id){|n| n+1}
    favorite true
  end
end
