# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :post do
    doctor nil
    memo "MyText"
    read_level 1
    placeholder nil
  end
end
