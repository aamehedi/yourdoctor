# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :icd10 do
    name "MyString"
    code "MyString"
    replace_code "MyString"
    depth 1
    icd10 nil
  end
end
