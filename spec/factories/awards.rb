# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :award do
    title "MyString"
    year 1
    description "MyText"
  end
end
