# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :mdc_layer1 do
    code "MyString"
    name "MyString"
    mdc_layer1_code nil
    mdc_layer2 ""
    ag_by_dpch_x_mdc_l2 ""
  end
end
