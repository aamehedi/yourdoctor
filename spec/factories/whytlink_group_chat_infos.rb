FactoryGirl.define do
  factory :whytlink_group_chat_info do
    name{Faker::Name.name}
    affliation_type{Faker::Number.between(0, 2)}
    affliation_id{Faker::Number.number(5)}
    opened{Faker::Boolean.boolean}
  end
end
