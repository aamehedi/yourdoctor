# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :all_mail do
    title "MyText"
    content "MyText"
    done false
  end
end
