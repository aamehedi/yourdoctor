FactoryGirl.define do
  factory :whytlink_chatroom do
    transient{whytlink_messages_count 10}
    factory :whytlink_chatroom_dual do
      chatroom_type :dual
      after :create do |whytlink_chatroom|
        doctors = whytlink_chatroom.doctors
        10.times do
          FactoryGirl.create :whytlink_message,
            whytlink_chatroom: whytlink_chatroom, doctor: doctors[rand 0..1]
        end
      end
    end

    factory :whytlink_chatroom_multi do
      chatroom_type :multi
      whytlink_group_chat_info{FactoryGirl.create :whytlink_group_chat_info}
      transient{whytlink_doctor_chatrooms_count 3}
      after :create do |whytlink_chatroom, evaluator|
        create_list(:whytlink_doctor_chatroom,
          evaluator.whytlink_doctor_chatrooms_count,
          whytlink_chatroom: whytlink_chatroom)
        doctors = whytlink_chatroom.doctors
        10.times do
          FactoryGirl.create :whytlink_message,
            whytlink_chatroom: whytlink_chatroom, doctor: doctors[rand 0..2]
        end
      end
    end
  end
end
