# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :mdc_layer3 do
    code_1 "MyString"
    code_2 "MyString"
    code_3 "MyString"
    code_4 "MyString"
    mdc_layer2 nil
  end
end
