# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :mdc_layer2 do
    code "MyString"
    name "MyString"
    no_procedure false
    mdc_layer1 nil
    mdc_layer3 ""
  end
end
