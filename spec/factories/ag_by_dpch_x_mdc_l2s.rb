# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :ag_by_dpch_x_mdc_l2 do
    number 1
    days "9.99"
    dpc_hospital nil
    mdc_layer2 nil
  end
end
