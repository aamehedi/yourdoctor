# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :duplicate_doctor_list do
    doctor_ids 1
    check false
    log 1
  end
end
