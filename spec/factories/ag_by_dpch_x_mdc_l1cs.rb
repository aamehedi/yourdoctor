# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :ag_by_dpch_x_mdc_l1c do
    patient_number_surgery 1
    patient_number_no_surgery 1
    hospitalization_planned_num 1
    hospitalization_chemo_num 1
    hospitalization_unplanned_num 1
    hospitalization_emergency_num 1
    ambulance_transport 1
    ambulance_transport_total 1
    death_within_24hours_num 1
    death_within_24hours_num_total 1
    avg_hospital_days_number 1
    avg_hospital_days_by_hopital "9.99"
    avg_hospital_days_whole_country "9.99"
    avg_hospital_days_index_patient_format "9.99"
    avg_hospital_days_whole_country_patient_format "9.99"
    avg_hospital_days_index "9.99"
    rehospitalization_surgery_rate "9.99"
    rehospitalization_chemical_treatment_rate "9.99"
    dpc_hospital nil
    mdc_layer1_code nil
  end
end
