# -*- coding: utf-8 -*-
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :chart do
    sequence(:name){|n| "Chart #{n}"}
    description "First Chart"
    mr_id 1
    favorite false
    chart_type "demo_chart"
    raw_query_param_values "raw_query_param_values"
    query_param_values "query_param_values"
    option "option"
  end
end
