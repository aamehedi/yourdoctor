# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :uploaded_file do
    name "MyText"
    url "MyText"
    parent_id 1
    parent_type "MyString"
  end
end
