# -*- coding: utf-8 -*-
# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :branch do
    sequence(:name){|n| "Branch #{n}"}
    company_id 1
    this_month_request 0
  end
end
