# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :society_position do
    name "MyText"
    comment "MyText"
    society nil
  end
end
