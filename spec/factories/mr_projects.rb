FactoryGirl.define do
  factory :mr_project do
    sequence(:project_id){|n| n}
    sequence(:mr_id){|n| n + 1}
  end
end
