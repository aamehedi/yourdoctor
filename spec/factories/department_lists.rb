# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :department_list do
    department nil
    doctor nil
  end
end
