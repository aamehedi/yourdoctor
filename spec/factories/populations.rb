# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :population do
    year 1
    male_0to4 1
    female_0to4 1
    male_5to9 1
    female_5to9 1
    male_10to14 1
    female_10to14 1
    male_15to19 1
    female_15to19 1
    male_20to24 1
    female_20to24 1
    male_25to29 1
    female_25to29 1
    male_30to34 1
    female_30to34 1
    male_35to39 1
    female_35to39 1
    male_40to44 1
    female_40to44 1
    male_45to49 1
    female_45to49 1
    male_50to54 1
    female_50to54 1
    male_55to59 1
    female_55to59 1
    male_60to64 1
    female_60to64 1
    male_65to69 1
    female_65to69 1
    male_70to74 1
    female_70to74 1
    male_75to79 1
    female_75to79 1
    male_80to84 1
    female_80to84 1
    male_85to89 1
    female_85to89 1
    over90 1
  end
end
