# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :ag_by_dpch_x_reason_minor do
    ratio "9.99"
    reason_for_rehospitalization nil
  end
end
