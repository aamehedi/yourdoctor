# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :link do
    follower_id 1
    followed_id 1
    state 1
    message "MyText"
  end
end
