FactoryGirl.define do
  factory :whytlink_doctor_chatroom do
    doctor
    whytlink_chatroom

    factory :whytlink_doctor_chatroom_with_no_unread_messages do
      last_read_message_id 100
    end
  end
end
