FactoryGirl.define do
  factory :whytlink_message do
    content{Faker::Lorem.sentence}
    message_type :text
    doctor
    whytlink_chatroom
  end
end
