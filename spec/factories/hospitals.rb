# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :hospital do
    medical_group nil
    area nil
    prefecture nil
    std_region_code 1
    name "MyText"
    postal_code 1
    address "MyText"
    tmp_head "MyString"
    url "MyText"
    bed_num 1
    acute_phase_bed_num 1
    admin_user_id 1
  end
end
