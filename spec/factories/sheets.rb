# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :sheet do
    area "MyString"
    medical_group "MyString"
    work_status "MyString"
    hospital_id "MyString"
    hospital_name "MyString"
    doctor_name "MyString"
    position "MyString"
    department "MyString"
    university "MyString"
    graduation_year "MyString"
    society "MyText"
    profession "MyString"
    skill "MyText"
    result "MyText"
    link "MyText"
    history "MyText"
    top_or_next "MyString"
    user "MyString"
    created_at "MyString"
    updated_at "MyString"
    update_user "MyString"
    comment "MyText"
  end
end
