# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :whyt_plot_api do
    url "MyString"
    chart_jp "MyString"
    chart_en "MyString"
    api "MyString"
    api_attributes "MyString"
    create_view_sql "MyString"
  end
end
