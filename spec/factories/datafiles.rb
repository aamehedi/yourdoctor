# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :datafile do
    filepath "MyString"
    status 1
    total_line_num 1
    error_line_num 1
  end
end
