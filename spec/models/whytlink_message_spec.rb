require "rails_helper"

RSpec.describe WhytlinkMessage do
  before :all do
    whytlink_chatroom_multi = FactoryGirl.create :whytlink_chatroom_multi
    index = 1
    whytlink_chatroom_multi.whytlink_messages.all.each do |whytlink_message|
      instance_variable_set "@whytlink_message_#{index}", whytlink_message
      index += 1
    end
  end

  describe "whytlink_message to be valid" do
    it do
      [@whytlink_message_1, @whytlink_message_2, @whytlink_message_3,
        @whytlink_message_4].each do |message|
        expect(message).to be_valid
      end
    end
  end

  describe ".unread" do
    subject do
      WhytlinkMessage.unread @whytlink_message_1.whytlink_chatroom_id,
        @whytlink_message_2.id
    end

    it{is_expected.to have_exactly(8).whytlink_messages}
    it{is_expected.to include(@whytlink_message_3, @whytlink_message_4)}
  end

  describe ".get_messages" do
    context "when last_message_excluded is true" do
      subject{WhytlinkMessage.get_messages(@whytlink_message_4, 3, "true").to_a}

      it{is_expected.to have_exactly(3).whytlink_messages}
      it do
        is_expected.to include(@whytlink_message_1, @whytlink_message_2,
          @whytlink_message_3)
      end
      it "should have messeges in the descending order of id" do
        is_expected.to eq [@whytlink_message_3, @whytlink_message_2,
          @whytlink_message_1]
      end
    end

    context "when last_message_excluded is false" do
      subject{WhytlinkMessage.get_messages @whytlink_message_4, 3, "false"}

      it{is_expected.to have_exactly(3).whytlink_messages}
      it do
        is_expected.to include(@whytlink_message_2, @whytlink_message_3,
          @whytlink_message_4)
      end
      it "should have messeges in the descending order of id" do
        is_expected.to eq [@whytlink_message_4, @whytlink_message_3,
          @whytlink_message_2]
      end
    end
  end
end
