require "rails_helper"

RSpec.describe Project, type: :model do
  let(:project){FactoryGirl.create :project, mr_ids: [1, 2]}
  let(:project1){FactoryGirl.build :project, mr_ids: [1, 3]}

  it "has a valid factory" do
    expect(FactoryGirl.build(:project)).to be_valid
  end

  it{should validate_presence_of :name}
  it{should validate_presence_of :mr_id}
  it{should validate_presence_of :company_id}

  describe "#find_or_create_default_chatroom" do
    context "data saved successfully" do
      subject{lambda{project.run_callbacks(:commit)}}
      it{should change{WhytplotChatroom.count}.by 1}
      it{should change{WhytplotMrChatroom.count}.by 2}
    end

    it "should call save only once" do
      expect(project1).to receive(:save).exactly :once
      project1.find_or_create_default_chatroom
    end
  end
end
