require "rails_helper"

RSpec.describe Branch, type: :model do
  it "has a valid factory" do
    expect(FactoryGirl.create :branch).to be_valid
  end
  it "is invalid without a name" do
    branch = Branch.new name: nil
    expect(branch).to_not be_valid
  end
  it "is invalid without a company" do
    branch = Branch.new(name: "Framgia", company: nil)
    expect(branch).to_not be_valid
  end

  context "#find_or_create_default_chatroom" do
    let(:branch){FactoryGirl.build(:branch, mr_ids: [1,2])}
    it "should create a chatroom with mrs of branch" do
      expect{branch.find_or_create_default_chatroom}.to change{
        WhytplotChatroom.last.mr_ids}.from([]).to([1,2])
    end
    it "should call :save only once" do
      branch.mr_ids = [1, 3]
      expect(branch).to receive(:save).exactly :once
      branch.find_or_create_default_chatroom
    end
  end

  context "#update_default_chatroom" do
    let(:branch){FactoryGirl.create(:branch, mr_ids: [1,2])}
    it "should create a chatroom with mrs of branch" do
      branch.mr_ids = [1, 3]
      expect{branch.update_default_chatroom}.to change{
        WhytplotChatroom.last.mr_ids}.from([1,2]).to([1,3])
    end
  end
end
