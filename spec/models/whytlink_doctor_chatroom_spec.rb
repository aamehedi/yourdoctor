require "rails_helper"

RSpec.describe WhytlinkDoctorChatroom do
  before :all do
    whytlink_chatroom_multi = FactoryGirl.create :whytlink_chatroom_multi
    @whytlink_doctor_chatroom = whytlink_chatroom_multi
      .whytlink_doctor_chatrooms.all.first
  end

  describe "whytlink_doctor_chatroom to be valid" do
    subject{@whytlink_doctor_chatroom}
    it{is_expected.to be_valid}
  end

  describe ".update_whytlink_doctor_chatroom" do
    it "should update last_read_message_id" do
      old_last_read_message_id = @whytlink_doctor_chatroom.last_read_message_id
      expect do
        WhytlinkDoctorChatroom.update_whytlink_doctor_chatroom("id" =>
          @whytlink_doctor_chatroom.id.to_s, "last_read_message_id" => "1")
      end.to change{@whytlink_doctor_chatroom.reload.last_read_message_id}
        .from(old_last_read_message_id).to 1
    end

    describe "should call Actioncable server broadcast on successful update" do
      before do
        allow_any_instance_of(WhytlinkDoctorChatroom).to(
          receive(:update_attributes).and_return true)
      end

      it do
        expect(ActionCable.server).to receive(:broadcast).with(
          "whytlink_room_#{@whytlink_doctor_chatroom.whytlink_chatroom_id}",
          @whytlink_doctor_chatroom)
        WhytlinkDoctorChatroom.update_whytlink_doctor_chatroom("id" =>
          @whytlink_doctor_chatroom.id.to_s, "last_read_message_id" => "1")
      end
    end
  end
end
