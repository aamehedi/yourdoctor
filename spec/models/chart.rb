require "rails_helper"

RSpec.describe Chart, type: :model do
  it "has a valid factory" do
    expect(FactoryGirl.create(:chart)).to be_valid
  end
  it "is invalid without a name" do
    chart = Chart.new name: nil
    expect(chart).to_not be_valid
  end
  it "is invalid without an mr" do
    chart = Chart.new mr: nil
    expect(chart).to_not be_valid
  end
  it "is invalid without a chart_type" do
    chart = Chart.new chart_type: nil
    expect(chart).to_not be_valid
  end
end
