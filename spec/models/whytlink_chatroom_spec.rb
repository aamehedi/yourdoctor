require "rails_helper"

RSpec.describe WhytlinkChatroom do
  before :all do
    @multi_chatroom_1 = FactoryGirl.create :whytlink_chatroom_multi
    6.times do |i|
      instance_variable_set "@doctor_#{i + 1}", FactoryGirl.create(:doctor)
    end
    @dual_chatroom_1 = FactoryGirl.create :whytlink_chatroom_dual,
      doctor_ids: [@doctor_5.id, @doctor_6.id]
    @dual_chatroom_2 = FactoryGirl.create :whytlink_chatroom_dual,
      doctor_ids: [@doctor_1.id, @doctor_2.id]
    @multi_chatroom_2 = FactoryGirl.create :whytlink_chatroom_multi,
      doctor_ids: [@doctor_1.id, @doctor_2.id, @doctor_3.id]
  end

  describe "chatroom to be valid" do
    context :dual do
      subject{@dual_chatroom_1}
      it{is_expected.to be_valid}
    end

    context :multi do
      subject{@multi_chatroom_1}
      it{is_expected.to be_valid}
    end
  end

  describe "to have exact number of whytlink_doctor_chatrooms" do
    context :dual do
      subject{@dual_chatroom_1.whytlink_doctor_chatrooms.count}
      it{is_expected.to eq 2}
    end

    context :multi do
      subject{@multi_chatroom_1.whytlink_doctor_chatrooms.count}
      it{is_expected.to eq 3}
    end
  end

  describe "to have exact number of whytlink_messages" do
    context :dual do
      subject{@dual_chatroom_1.whytlink_messages.count}
      it{is_expected.to eq 10}
    end

    context :multi do
      subject{@multi_chatroom_1.whytlink_messages.count}
      it{is_expected.to eq 10}
    end
  end

  describe "to have exact number of doctors" do
    context :dual do
      subject{@dual_chatroom_1}
      it{is_expected.to have_exactly(2).doctors}
    end

    context :multi do
      subject{@multi_chatroom_1}
      it{is_expected.to have_exactly(3).doctors}
    end
  end

  describe "#last_message_id" do
    context :dual do
      subject{@dual_chatroom_1.last_message_id}
      it{is_expected.to eq @dual_chatroom_1.whytlink_messages.all.last.id}
    end

    context :multi do
      subject{@multi_chatroom_1.last_message_id}
      it{is_expected.to eq @multi_chatroom_1.whytlink_messages.all.last.id}
    end
  end

  describe ".find_dual_chatroom" do
    describe "should find chatroom if doctor_ids match" do
      context :dual do
        subject do
          WhytlinkChatroom.find_dual_chatroom(@dual_chatroom_1.doctor_ids).id
        end
        it{is_expected.to eq @dual_chatroom_1.id}
      end

      context "should return nil for multi chatroom" do
        subject do
          WhytlinkChatroom.find_dual_chatroom @multi_chatroom_1.doctor_ids
        end
        it{is_expected.to be_nil}
      end
    end

    describe "should return nil if doctor_ids does not match" do
      subject{WhytlinkChatroom.find_dual_chatroom [rand(11..20), rand(21..30)]}
      it{is_expected.to be_nil}
    end
  end

  describe ".find_all_chatrooms" do
    describe "should find all chatrooms with specified doctor_id" do
      before{@chatrooms = WhytlinkChatroom.find_all_chatrooms @doctor_1.id, nil}

      it{expect(@chatrooms.size).to eq 2}
      it do
        @chatrooms.each{|chatroom| expect(chatroom).to be_a WhytlinkChatroom}
      end
      it do
        @chatrooms.each do |chatroom|
          expect(chatroom.doctor_ids).to include @doctor_1.id
        end
      end

      context "with chatroom_type dual" do
        before do
          @chatrooms = WhytlinkChatroom.find_all_chatrooms @doctor_1.id, :dual
        end

        it{expect(@chatrooms.size).to eq 1}
        it{@chatrooms.each{|chatroom| expect(chatroom.dual?).to eq true}}
      end

      context "with chatroom_type multi" do
        before do
          @chatrooms = WhytlinkChatroom.find_all_chatrooms @doctor_1.id, :multi
        end

        it{expect(@chatrooms.size).to eq 1}
        it{@chatrooms.each{|chatroom| expect(chatroom.multi?).to eq true}}
      end
    end

    describe "should return empty array if no chatroom found" do
      subject{WhytlinkChatroom.find_all_chatrooms @doctor_4.id, nil}

      it{is_expected.to have_exactly(0).items}
    end
  end

  describe ".create_chatroom" do
    it do
      expect do
        WhytlinkChatroom.create_chatroom(
          {chatroom_type: :dual, doctor_ids: [@doctor_1.id, @doctor_3.id]},
          @doctor_1.id)
      end.to change(WhytlinkChatroom, :count).by 1
    end
  end

  describe ".find_or_create_chatroom" do
    # TODO: Need to refactor here to meet new structure of muti-chatroom
    # context "with chatroom_type multi" do
    #   it "will create new chatroom always" do
    #     expect do
    #       WhytlinkChatroom.find_or_create_chatroom chatroom_type: :multi,
    #         doctor_ids: [@doctor_1.id, @doctor_2.id, @doctor_4.id]
    #     end.to change(WhytlinkChatroom, :count).by 1
    #   end
    # end

    context "with chatroom_type dual" do
      # TODO: Need to refactor.
      # describe "should try finding chatroom matched by doctor_ids" do
      #   subject do
      #     WhytlinkChatroom.find_or_create_chatroom({chatroom_type: :dual,
      #       doctor_ids: @dual_chatroom_1.doctor_ids},
      #       @dual_chatroom_1.doctor_ids.first).try :id
      #   end
      #   it{is_expected.to eq @dual_chatroom_1.id}
      # end

      it "should create new chatroom if no chatroom found matched by
        doctor_ids" do
        expect do
          WhytlinkChatroom.find_or_create_chatroom(
            {chatroom_type: :dual, doctor_ids: [@doctor_1.id, @doctor_4.id]},
            @doctor_1.id)
        end.to change(WhytlinkChatroom, :count).by 1
      end
    end
  end
end
